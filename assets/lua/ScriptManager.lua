local TAG = "ScriptManager.lua"

assert(XANGARATH_ENVIRONMENT ~= nil)

local eventRegistry = {}
local dynamicFunctionID = 0
local dynamicFunctionRegistry = {}

-- Debugging output --

function logInfo(tag, message)
	local logger = as3.get(XANGARATH_ENVIRONMENT, "logger")
	assert(logger ~= nil)
	logger.i( tag, message )
end

function logDebug(tag, message)
	local logger = as3.get(XANGARATH_ENVIRONMENT, "logger")
	assert(logger ~= nil)
	logger.d( tag, message )
end

function logWarn(tag, message)
	local logger = as3.get(XANGARATH_ENVIRONMENT, "logger")
	assert( logger ~= nil )
	logger.w( tag, message )
end

function logError(tag, message)
	local logger = as3.get(XANGARATH_ENVIRONMENT, "logger")
	assert( logger ~= nil )
	logger.e( tag, message )
end

-- Convenience Functions --

function EQ( left, right )
	return as3.tolua( left ) == as3.tolua( right );
end

function NEQ( left, right )
	return as3.tolua( left ) ~= as3.tolua( right );
end

function LT( left, right )
	return as3.tolua( left ) < as3.tolua( right );
end

function LEQ( left, right )
	return as3.tolua( left ) <= as3.tolua( right );
end

function GT( left, right )
	return as3.tolua( left ) > as3.tolua( right );
end

function GEQ( left, right )
	return as3.tolua( left ) >= as3.tolua( right );
end

function band( a, b )
	return as3.tolua( XANGARATH_ENVIRONMENT.band( a, b ) )
end

function bor( a, b )
	return as3.tolua( XANGARATH_ENVIRONMENT.bor( a, b ) )
end

-- Game Object Registry Glue -- 

function registryLookup( id )
	local registry = as3.get(XANGARATH_ENVIRONMENT, "registry");
	assert( registry ~= nil )
	return as3.call( registry, "getObject", id )
end

function registryLookupByName( name )
	local registry = as3.get(XANGARATH_ENVIRONMENT, "registry");
	assert( registry ~= nil )
	return registry.getObjectByName( name )
end

-- Hooks & Callbacks --

function luaEventBus( eventType, id )
	local luaId = as3.tolua(id)
	if (eventRegistry[eventType] ~= nil) and (eventRegistry[eventType][luaId] ~= nil) then
		for i, v in ipairs( eventRegistry[eventType][luaId] ) do
			v.callback( XANGARATH_ENVIRONMENT.event, id, v.arg1 )
		end
	end
end

function registerHook( event, id, fn, param )
	if eventRegistry[event] == nil then
		eventRegistry[event] = {}
	end
	
	local luaId = as3.tolua(id)
	
	if eventRegistry[event][luaId] == nil then
		eventRegistry[event][luaId] = {}
		XANGARATH_ENVIRONMENT.registerEventListener( id, event )
	end
	
	table.insert( eventRegistry[event][luaId], {callback = fn, arg1 = param} )
end

function unregisterHook( event, id, fn )
	local luaId = as3.tolua(id)
	if (eventRegistry[event] == nil) or (eventRegistry[event][luaId] == nil) then
		return
	end
	
	local tableSize = #eventRegistry[event][luaId]
	
	if tableSize >= 1 then
		for i,v in ipairs( eventRegistry[event][luaId] ) do
			if v.callback == fn then
				table.remove( eventRegistry[event][luaId], i )
				break
			end
		end
	end
	
	if tableSize == 0 or tableSize == 1 then
		eventRegistry[event][luaId] = nil
		as3.call(XANGARATH_ENVIRONMENT, "unregisterEventListener", id, event)
	end
end

function createDynamicFunction( fn )
	local id = dynamicFunctionID
	dynamicFunctionID = dynamicFunctionID + 1
	local newFun;
	local ret = {}
	dynamicFunctionRegistry[ id ] = fn
	logDebug(TAG, "Created registry entry "..id)
	newFun = XANGARATH_ENVIRONMENT.dynFnMaker( id )
	ret.id = id
	ret.fn = newFun
	return ret
end

function destroyDynamicFunction( fnId )
	if (fnId == nil ) then
		return
	end
	
	if (dynamicFunctionRegistry[ fnId ] == nil) then 
		return
	end
	
	dynamicFunctionRegistry[ fnId ] = nil
end

function dynamicFunctionRegistryInvoke( id, args )
	local fnId = id
	if (dynamicFunctionRegistry[ fnId ] == nil) then
		logError( TAG, "Unable to locate requested dynamic function!");
		return
	end
	return dynamicFunctionRegistry[ fnId ]( args )
end

-- unregister hooks and whatnot --
local function cleanup()
	-- IMPLEMENT ME!
end


as3.onclose(cleanup)

logDebug( TAG, "Lua bootstrap done." )
