package components;

import java.io.File;
import java.util.ArrayList;
import org.xangarath.builder.GameObjectToken;

// It's shared state, tool.
public class SharedToolState 
{
	public static GameObjectToken activeToken = null;
	public static ArrayList<GameObjectToken> tokenList = new ArrayList<GameObjectToken>();
	public static String worldName = "null";
	public static String mapImageName = "null";
	public static File targetFile = null;
	public static boolean outstandingDeltas = false;
	public SharedToolState()
	{
		
	}
}
