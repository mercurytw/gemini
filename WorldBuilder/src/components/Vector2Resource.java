package components;

public class Vector2Resource 
{
	public float x;
	public float y;
	public Vector2Resource( float x, float y )
	{
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString()
	{
		return "[ " + x + ", " + y + " ]";
	}
}
