package components;

import java.util.ArrayList;

public class StringArrayResource 
{
	public ArrayList<String> stringArray;
	public StringArrayResource()
	{
		stringArray = new ArrayList<String>();
	}
	
	public StringArrayResource( String[] arr )
	{
		stringArray = new ArrayList<String>();
		if ( arr != null )
		{
			for ( int i = 0; i < arr.length; i++ )
			{
				stringArray.add( arr[i] );
			}
		}
	}
	
	@Override
	public String toString()
	{
		String res = "[ ";
		
		for ( int i = 0; i < stringArray.size(); i++ )
		{
			if ( i != 0 )
			{
				res += ", ";
			}
			
			res += "\"" + stringArray.get( i )+ "\"";
		}
		
		res += " ]";
		return res;
	}
}
