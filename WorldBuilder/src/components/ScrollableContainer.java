package components;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class ScrollableContainer extends JLayeredPane
								 implements Scrollable,
								 MouseMotionListener
{
	 /**
	 * WTF
	 */
	private static final long serialVersionUID = 1L;
	private int maxUnitIncrement = 1;
	private JLabel worldMap;
	public static final Integer WORLD_OBJECT_LAYER = 1;
	public static final Integer WORLD_TEMP_LAYER = 2;
	public ScrollableContainer(ImageIcon i, int m, MouseMotionListener listen) 
	{
		super();

		worldMap = new JLabel( i, m );
		worldMap.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		worldMap.setOpaque(true);
		worldMap.setBackground(Color.white);
		maxUnitIncrement = m;
		add( worldMap, new Integer(0) );
		worldMap.setBounds( 0, 0,i.getIconWidth(), i.getIconHeight());
		setPreferredSize( new Dimension( i.getIconWidth(), i.getIconHeight()));

		//Let the user scroll by dragging to outside the window.
		setAutoscrolls(true); //enable synthetic drag events
		addMouseMotionListener(listen); //handle mouse drags
		addMouseListener( (MouseListener) listen);

	}

	public void addWorldObject( JComponent c )
	{
		
	}

	//Methods required by the MouseMotionListener interface:
	public void mouseMoved(MouseEvent e) { }
	public void mouseDragged(MouseEvent e) {
		//The user is dragging us, so scroll!
		Rectangle r = new Rectangle(e.getX(), e.getY(), 1, 1);
		scrollRectToVisible(r);
	}

	public Dimension getPreferredSize() {
		return super.getPreferredSize();
	}

	public Dimension getPreferredScrollableViewportSize() {
		return getPreferredSize();
	}

	public int getScrollableUnitIncrement(Rectangle visibleRect,
			int orientation,
			int direction) {
		//Get the current position.
		int currentPosition = 0;
		if (orientation == SwingConstants.HORIZONTAL) {
			currentPosition = visibleRect.x;
		} else {
			currentPosition = visibleRect.y;
		}

		//Return the number of pixels between currentPosition
		//and the nearest tick mark in the indicated direction.
		if (direction < 0) {
			int newPosition = currentPosition -
					(currentPosition / maxUnitIncrement)
					* maxUnitIncrement;
			return (newPosition == 0) ? maxUnitIncrement : newPosition;
		} else {
			return ((currentPosition / maxUnitIncrement) + 1)
					* maxUnitIncrement
					- currentPosition;
		}
	}

	public int getScrollableBlockIncrement(Rectangle visibleRect,
			int orientation,
			int direction) {
		if (orientation == SwingConstants.HORIZONTAL) {
			return visibleRect.width - maxUnitIncrement;
		} else {
			return visibleRect.height - maxUnitIncrement;
		}
	}

	public boolean getScrollableTracksViewportWidth() {
		return false;
	}

	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	public void setMaxUnitIncrement(int pixels) {
		maxUnitIncrement = pixels;
	}

}
