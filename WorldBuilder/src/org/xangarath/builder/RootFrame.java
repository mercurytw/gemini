package org.xangarath.builder;


import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import components.SharedToolState;

class RootFrame extends JFrame implements ActionListener
{
	/**
	 * srsly wat 
	 */
	private static final long serialVersionUID = 1L;
	private static final String TAG = "RootFrame";
	private ControlPanel ctlPanel;
	private WorldDisplayPanel worldPanel;
	private JMenuBar menuBar;
	private JMenu fileMenu;
	private JMenuItem newWorld, loadWorld, saveWorld;
	private JFileChooser choosa;
	private WorldSerializer serializer;
	private WorldParser parser;
	private FileFilter imageFilter, worldFilter;
	private File cwd = null;
	public RootFrame() 
	{
		setTitle( "Gemini World Builder" );
		setSize( 800, 600 );
		setLocation( 200, 100 );
		worldPanel = new WorldDisplayPanel();
		this.add( worldPanel );
		
		ctlPanel = new ControlPanel();
		this.add( ctlPanel, BorderLayout.EAST );
		
		//worldPanel.setMapImage( ".\\niftyville_terminal.png");
		
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		newWorld = new JMenuItem("New...");
		newWorld.addActionListener(this);
		loadWorld = new JMenuItem("Load");
		loadWorld.addActionListener(this);
		saveWorld = new JMenuItem("Save");
		saveWorld.setEnabled(false);
		saveWorld.addActionListener(this);
		fileMenu.add(newWorld);
		fileMenu.add(loadWorld);
		fileMenu.add(saveWorld);
		menuBar.add(fileMenu);
		
		imageFilter = new FileNameExtensionFilter( "World Map Image", "png", "jpg", "jpeg" );
		worldFilter = new FileNameExtensionFilter( "World File", "wld" );
		choosa = new JFileChooser();
		serializer = new WorldSerializer();
		parser = new WorldParser( worldPanel );
		
		this.setJMenuBar(menuBar);
		
		// Window Listeners
		addWindowListener( new WindowAdapter() {
			public void windowClosing( WindowEvent e ) 
			{
				dispose();
				System.exit( 0 );
			}});
		
		this.repaint();
	}
	 
	public void paint( Graphics g )
	{
		super.paint( g );
	}

	public static void main(String[] args) 
	{
		JFrame f = new RootFrame();
		f.setVisible( true );
	}

	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		if (arg0.getSource() == newWorld)
		{
			System.out.println("New world");
			choosa.setFileFilter( imageFilter );
			choosa.removeChoosableFileFilter( worldFilter );
			int retval = choosa.showOpenDialog( this.getParent() );
			if ( retval == JFileChooser.APPROVE_OPTION )
			{
				worldPanel.setMapImage( choosa.getSelectedFile().getAbsolutePath() );
				saveWorld.setEnabled(true);
				newWorld.setEnabled(false);
				loadWorld.setEnabled(false);
				
				setTitle( "Gemini World Builder - New World" );
				
				SharedToolState.worldName = "New World";
				SharedToolState.outstandingDeltas = true;
				SharedToolState.mapImageName = choosa.getSelectedFile().getName();
				
				cwd = choosa.getSelectedFile().getParentFile();
				assert( cwd.isDirectory() ); // lazy bitch
				choosa.setCurrentDirectory( cwd );
			}
		}
		else if (arg0.getSource() == loadWorld)
		{
			if ( SharedToolState.targetFile != null )
				return;
			
			System.out.println( "load world" );
			choosa.setFileFilter( worldFilter );
			choosa.removeChoosableFileFilter( imageFilter );
			int retval = choosa.showOpenDialog( this.getParent() );
			if ( retval == JFileChooser.APPROVE_OPTION )
			{
				System.out.println( "chose to open " + choosa.getSelectedFile().getName() );
				if ( choosa.getSelectedFile().getName().substring( choosa.getSelectedFile().getName().indexOf('.') ).compareTo( ".wld" ) != 0 )
				{
					return;
				}
				parser.parseScript( choosa.getSelectedFile() );
				loadWorld.setEnabled(false); // for now
				saveWorld.setEnabled(true);
			}
		}
		else if (arg0.getSource() == saveWorld)
		{
			System.out.println("save world");
			if ( SharedToolState.outstandingDeltas )
			{
				if ( SharedToolState.targetFile == null )
				{
					choosa.setFileFilter( worldFilter );
					choosa.removeChoosableFileFilter( imageFilter );
					int retval = choosa.showSaveDialog( this.getParent() );
					
					if ( retval == JFileChooser.APPROVE_OPTION )
					{
						System.out.println( "chose to save as " + choosa.getSelectedFile().getName() );
						String filename = choosa.getSelectedFile().getName();
						int len = filename.length();
						File f;
						if ( ( len > 4 ) &&  
							 ( filename.charAt( len - 4 ) == '.' ) && 
							 ( filename.charAt( len - 3 ) == 'w' ) && 
							 ( filename.charAt( len - 2 ) == 'l' ) && 
							 ( filename.charAt( len - 1 ) == 'd' ) )
						{
							filename = filename.substring(0, filename.indexOf('.'));
							f = choosa.getSelectedFile();
						}
						else
						{
							f = new File( choosa.getSelectedFile().getAbsolutePath() + ".wld" );
							filename = f.getName();
						}
						SharedToolState.worldName = filename;
						setTitle( "Gemini World Builder - " + filename );
						System.out.println( "shortened name was " + filename );
						SharedToolState.targetFile = f;
					}
					else
					{
						return;
					}
				}
				String serializedWorld = serializer.serialize();
				try {
					FileWriter fw = new FileWriter( SharedToolState.targetFile );
					fw.write( serializedWorld );
					fw.close();
				} catch (IOException e) {
					//TODO: pump error to error window
					System.err.println( TAG + ": Failed to write outfile: " +  e.getMessage() );
					throw new Error( TAG + ": Failed to write outfile: " +  e.getMessage() );
				}
				
				SharedToolState.outstandingDeltas = false;
			}
		}
	}
}