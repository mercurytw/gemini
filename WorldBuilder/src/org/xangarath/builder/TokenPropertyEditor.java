package org.xangarath.builder;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import components.SharedToolState;
import components.StringArrayResource;

public class TokenPropertyEditor extends JFrame implements ActionListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private GameObjectToken focus;
	private HashMap<String, Component> propertyMap;
	public TokenPropertyEditor() throws HeadlessException 
	{
		super(" properties");
	}
	
	public void setFocus( GameObjectToken tok )
	{
		setTitle( tok.getTypeString() + " properties");
		focus = tok;
		setSize( 300, 600 );
		setLocation( 200, 100 );
		propertyMap = new HashMap<String, Component>();
		
		getContentPane().setLayout( new BoxLayout( getContentPane(), BoxLayout.PAGE_AXIS ) );
		constructProperties();
		buildButtons();
		repaint();
	}
	
	private void clear()
	{
		propertyMap.clear();
		focus = null;
		getContentPane().removeAll();
	}
	
	private void buildButtons()
	{	
		Container objContainer;
		objContainer = new Container();
		objContainer.setLayout( new BoxLayout( objContainer, BoxLayout.LINE_AXIS ) );
		objContainer.setComponentOrientation( ComponentOrientation.LEFT_TO_RIGHT );
		getContentPane().add( objContainer );
		
		JButton btn = new JButton( "OK" );
		btn.setVerticalTextPosition( AbstractButton.CENTER );
		btn.setHorizontalTextPosition( AbstractButton.LEFT );
		
		Dimension d2 = new Dimension();
		d2.width = 120;
		d2.height = 20;
		btn.setMinimumSize(d2);
		btn.setPreferredSize( d2 );
		btn.setMaximumSize( d2);
		btn.setActionCommand( "ok" );
		btn.addActionListener( this );
		
		objContainer.add( btn );

		btn = new JButton( "Cancel" );
		btn.setVerticalTextPosition( AbstractButton.CENTER );
		btn.setHorizontalTextPosition( AbstractButton.RIGHT );
		
		d2 = new Dimension();
		d2.width = 120;
		d2.height = 20;
		btn.setMinimumSize(d2);
		btn.setPreferredSize( d2 );
		btn.setMaximumSize( d2);
		btn.setActionCommand( "cancel" );
		btn.addActionListener( this );
		
		objContainer.add( btn );
		
	}
	
	private void constructPropertiesHelper( String name, Object property )
	{
		Container objContainer;
		objContainer = new Container();
		objContainer.setLayout( new BoxLayout( objContainer, BoxLayout.LINE_AXIS ) );
		objContainer.setComponentOrientation( ComponentOrientation.LEFT_TO_RIGHT );
		getContentPane().add( objContainer );
		
		JLabel lbl = new JLabel( name );
		lbl.setHorizontalTextPosition( JLabel.LEFT );
		lbl.setHorizontalAlignment( SwingConstants.LEFT );
		Dimension d = new Dimension();
		d.width = 100;
		d.height = 20;
		lbl.setMinimumSize(d);
		lbl.setPreferredSize(d);
		lbl.setBounds( objContainer.getBounds().x, objContainer.getBounds().y, 300, 20 );
		objContainer.add( lbl );
		
		if ( property.getClass() == StringArrayResource.class )
		{
			TextField f = new TextField( "", 20 );
			Dimension d2 = new Dimension();
			d2.width = 200;
			d2.height = 20;
			f.setMinimumSize(d2);
			f.setPreferredSize(d2);
			f.setBounds( objContainer.getBounds().x, objContainer.getBounds().y, 300, 20 );
			objContainer.add( f );
			propertyMap.put( name, f );
			
			StringArrayResource arr = (StringArrayResource) property;
			String out = "";
			for (int i = 0; i < arr.stringArray.size(); i++)
			{
				if (i != 0)
				{
					out += ", ";
				}
				out += arr.stringArray.get(i);
			}
			f.setText(out);
		}
	}
	
	private void constructProperties()
	{
		Object[] keys = focus.entityData.keySet().toArray();
		for ( int i = 0; i < focus.entityData.size(); i++ )
		{
			if ( (((String) keys[ i ]).compareTo( "type" ) == 0 ) ||
				 (((String) keys[ i ]).compareTo( "position" ) == 0 ) )
			{
				continue;
			}
			
			Object property = focus.entityData.get( keys[ i ] );
			constructPropertiesHelper( (String) keys[ i ], property );
		}
	}
	
	private void saveChanges()
	{
		Object[] keys = propertyMap.keySet().toArray();
		for ( int i = 0; i < propertyMap.size(); i++ )
		{
			Object d = focus.entityData.get( keys[i] );
			Component o = propertyMap.get(keys[i]);
			if (d.getClass() == StringArrayResource.class)
			{
				StringArrayResource dest = (StringArrayResource) d;
				TextField srcCmp = (TextField) o;
				String[] strs = srcCmp.getText().split(",");
				dest.stringArray.clear();
				for (int j = 0; j < strs.length; j++)
				{
					dest.stringArray.add(strs[j].trim());
				}
			}
		}
		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{	
		if( arg0.getActionCommand().compareTo("ok") == 0 )
		{
			System.out.println("Accepted!");
			saveChanges();
			SharedToolState.outstandingDeltas = true;
			clear();
			this.setVisible(false);
		}
		else if( arg0.getActionCommand().compareTo("cancel") == 0 )
		{
			System.out.println("changes discarded");
			clear();
			this.setVisible(false);
		}
	}
}
