package org.xangarath.builder;

import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import components.SharedToolState;

public class ControlPanel extends JPanel implements ActionListener
{
	/**
	 * :|
	 */
	private static final long serialVersionUID = 1L;
	private static final String[] objTypes = { "Monster", "Item", "Decor", "Meta" };
	private static final String[] monsterTypes = { "scootaspider", "shooter", "haunter", "tentacle", "rocketloo" };
	private static final String[] metaTypes = { "player_start" };
	private static final String[] itemTypes = { "small_key" };
	private static final String[] decorTypes = { "door" };
	private JComboBox<String> objectBox;
	private JComboBox<String> typeBox;
	private GameObjectTokenFactory fact;
	public ControlPanel()
	{
		super();
		fact = new GameObjectTokenFactory();
		
		this.setLayout( new BoxLayout( this, BoxLayout.PAGE_AXIS ) );
		add( Box.createVerticalStrut((int)(getSize().height * 0.48 )));
		add( Box.createVerticalGlue() );
		
		buildObjectBox();
		buildTypeBox();
		buildButton();
		
		add( Box.createVerticalGlue() );
		add( Box.createVerticalStrut((int)(getSize().height * 0.48 )));
		
		Dimension d3 = new Dimension();
		d3.width = 200;
		d3.height = getSize().height;
		this.setPreferredSize(d3);
		
		repaint();
	}
	
	private void buildObjectBox()
	{
		Container objContainer;
		objContainer = new Container();
		objContainer.setLayout( new BoxLayout( objContainer, BoxLayout.LINE_AXIS ) );
		objContainer.setComponentOrientation( ComponentOrientation.LEFT_TO_RIGHT );
		objContainer.add( Box.createHorizontalStrut((int)(20)));
		objContainer.add( Box.createHorizontalGlue());
		this.add( objContainer );
		
		JLabel lbl = new JLabel( "Object" );
		lbl.setHorizontalTextPosition( JLabel.LEFT );
		lbl.setHorizontalAlignment( SwingConstants.LEFT );
		Dimension d = new Dimension();
		d.width = 50;
		d.height = 20;
		lbl.setMinimumSize(d);
		lbl.setPreferredSize(d);
		lbl.setBounds( objContainer.getBounds().x, objContainer.getBounds().y, 800, 20 );
		objContainer.add( lbl );
		
		
		objectBox = new JComboBox<String>( objTypes );
		objectBox.setSelectedIndex( 0 );
		Dimension d2 = new Dimension();
		d2.width = 120;
		d2.height = 20;
		objectBox.setMinimumSize(d2);
		objectBox.setPreferredSize( d2 );
		objectBox.setMaximumSize( d2);
		//cbox.setBounds( objContainer.getBounds().x, y, width, height)
		objContainer.add( objectBox );
		objContainer.add( Box.createHorizontalGlue());
		objContainer.add( Box.createHorizontalStrut((int)(20)));
		
		objectBox.addActionListener( this );
	}
	
	private void buildTypeBox()
	{
		Container objContainer;
		objContainer = new Container();
		objContainer.setLayout( new BoxLayout( objContainer, BoxLayout.LINE_AXIS ) );
		objContainer.setComponentOrientation( ComponentOrientation.LEFT_TO_RIGHT );
		objContainer.add( Box.createHorizontalStrut((int)(20)));
		objContainer.add( Box.createHorizontalGlue());
		this.add( objContainer );
		
		JLabel lbl = new JLabel( "Type" );
		lbl.setHorizontalTextPosition( JLabel.LEFT );
		lbl.setHorizontalAlignment( SwingConstants.LEFT );
		Dimension d = new Dimension();
		d.width = 50;
		d.height = 20;
		lbl.setMinimumSize(d);
		lbl.setPreferredSize(d);
		lbl.setBounds( objContainer.getBounds().x, objContainer.getBounds().y, 800, 20 );
		objContainer.add( lbl );
		
		typeBox = new JComboBox<String>( monsterTypes );
		typeBox.setSelectedIndex( 0 );
		Dimension d2 = new Dimension();
		d2.width = 120;
		d2.height = 20;
		typeBox.setMinimumSize(d2);
		typeBox.setPreferredSize( d2 );
		typeBox.setMaximumSize( d2);
		//cbox.setBounds( objContainer.getBounds().x, y, width, height)
		objContainer.add( typeBox );
		objContainer.add( Box.createHorizontalGlue());
		objContainer.add( Box.createHorizontalStrut((int)(20)));
	}
	
	private void buildButton()
	{	
		Container objContainer;
		objContainer = new Container();
		objContainer.setLayout( new BoxLayout( objContainer, BoxLayout.LINE_AXIS ) );
		objContainer.setComponentOrientation( ComponentOrientation.LEFT_TO_RIGHT );
		objContainer.add( Box.createHorizontalStrut((int)(20)));
		objContainer.add( Box.createHorizontalGlue());
		this.add( objContainer );
		
		JButton btn = new JButton( "Create" );
		btn.setVerticalTextPosition( AbstractButton.CENTER );
		btn.setHorizontalTextPosition( AbstractButton.CENTER );
		
		Dimension d2 = new Dimension();
		d2.width = 120;
		d2.height = 20;
		btn.setMinimumSize(d2);
		btn.setPreferredSize( d2 );
		btn.setMaximumSize( d2);
		btn.setActionCommand( "construct" );
		btn.addActionListener( this );
		
		objContainer.add( btn );
		objContainer.add( Box.createHorizontalGlue());
		objContainer.add( Box.createHorizontalStrut((int)(20)));
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		String majorType, minorType;
		
		if( arg0.getActionCommand().compareTo("construct") == 0 )
		{
			majorType = (String)objectBox.getSelectedItem();
			minorType = (String)typeBox.getSelectedItem();
			System.out.println( "Constructing selected: " + majorType + ", " + minorType );
			
			SharedToolState.activeToken = fact.construct( majorType, minorType );
		}
		else if ( arg0.getActionCommand().compareTo( "comboBoxChanged" ) == 0 ) 
		{
			typeBox.removeAllItems();
			String[] items = null;
			if      ( objectBox.getSelectedItem() == "Monster" ) { items = monsterTypes; }
			else if ( objectBox.getSelectedItem() == "Meta"    ) { items = metaTypes; }
			else if ( objectBox.getSelectedItem() == "Item"    ) { items = itemTypes; }
			else    											 { items = decorTypes; }
			
			for ( int i = 0; i < items.length; i++ )
			{
				typeBox.addItem( items[i] );
			}
		}
	}
}
