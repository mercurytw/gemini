package org.xangarath.builder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.CharBuffer;

import org.json.JSONArray;
import org.json.JSONObject;

import components.SharedToolState;
import components.Vector2Resource;

/**
 * Take a .wld file and parse it. 
 * May want to extend this to scenarios when the time comes.
 * @author mercury
 *
 */
public class WorldParser 
{
	private static final String TAG = "WorldParser";
	private WorldDisplayPanel dispPanel;
	private GameObjectTokenFactory fact;
	public WorldParser( WorldDisplayPanel p )
	{
		dispPanel = p;
		fact = new GameObjectTokenFactory();
	}
	
	private void parseMeta( JSONObject obj )
	{
		String mapPath;
		JSONObject meta = obj.getJSONObject( "Meta" );
		
		String pathToRoot = System.getenv( "GEMINI_ROOT" );
		mapPath = pathToRoot + "\\WorldBuilderMaps\\" + meta.getString( "map_file" );
		dispPanel.setMapImage( mapPath );
		SharedToolState.mapImageName = meta.getString( "map_file" );
		
		JSONObject startObj = meta.optJSONObject( "player_start" );
		
		if ( startObj != null )
		{
			JSONArray playerStart = startObj.getJSONArray( "position" );
			GameObjectToken tok = fact.construct("Meta", "player_start");
			dispPanel.insertToken( tok, playerStart.getInt(0), playerStart.getInt(1) );
			System.out.println( "Got player start: " + playerStart.getInt(0) + ", " + playerStart.getInt(1) );
		}
	}
	
	private void parseObjects( JSONObject obj )
	{
		System.out.println( TAG + ": Now parsing Decor...");
		JSONArray section = obj.getJSONArray( "Decor" );
		
		int length = section.length();
		for( int i = 0; i < length; i++ )
		{
			GameObjectToken tok = fact.constructFromJson( "Decor", section.getJSONObject( i ) );
			Vector2Resource pos = (Vector2Resource) tok.entityData.get( "position" );
			dispPanel.insertToken( tok, (int) pos.x, (int) pos.y );
		}
		
		System.out.println( TAG + ": Now parsing Items...");
		section = obj.getJSONArray( "Item" );
		
		length = section.length();
		for( int i = 0; i < length; i++ )
		{
			GameObjectToken tok = fact.constructFromJson( "Item", section.getJSONObject( i ) );
			Vector2Resource pos = (Vector2Resource) tok.entityData.get( "position" );
			dispPanel.insertToken( tok, (int) pos.x, (int) pos.y );
		}
		
		System.out.println( TAG + ": Now parsing Monsters...");
		section = obj.getJSONArray( "Monster" );
		
		length = section.length();
		for( int i = 0; i < length; i++ )
		{
			GameObjectToken tok = fact.constructFromJson( "Monster", section.getJSONObject( i ) );
			Vector2Resource pos = (Vector2Resource) tok.entityData.get( "position" );
			dispPanel.insertToken( tok, (int) pos.x, (int) pos.y );
		}
	}
	
	public void parseScript( File f )
	{	
		CharBuffer buff;
		int readAmount = 0;
		int amountToRead;
		SharedToolState.worldName = f.getName().substring( 0, f.getName().indexOf( '.' ) );
		System.out.println( "World name is " + SharedToolState.worldName );
		
		try {
			FileReader r = new FileReader(f);
			amountToRead = (int) f.length();
			buff = CharBuffer.allocate(amountToRead);
			while ( readAmount < amountToRead )
			{
				int tmp = r.read(buff);
				
				if ( tmp == -1 )
					break;
				
				readAmount += tmp;
			}
			r.close();
		} catch (FileNotFoundException e) {
			System.err.println( TAG + ": File not found\n" );
			throw new Error( TAG + ": File not found\n" );
		} catch (IOException e) {
			System.err.println( TAG + ": Unable to read file\n" );
			throw new Error( TAG + ": Unable to read file\n" );
		}
		
		JSONObject obj = new JSONObject( String.copyValueOf( buff.array() ) );
		buff.clear();
		
		parseMeta( obj );
		parseObjects( obj );
		
	}
}
