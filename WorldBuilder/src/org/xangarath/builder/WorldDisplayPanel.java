package org.xangarath.builder;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import components.ScrollableContainer;
import components.SharedToolState;
import components.Vector2Resource;

public class WorldDisplayPanel extends JPanel implements MouseMotionListener, MouseListener, ActionListener
{
	/**
	 * :|
	 */
	private static final long serialVersionUID = 1L;
	private static final String TAG = "WorldDisplayPanel";
	private JScrollPane background;
	private ScrollableContainer spic;
	private boolean isInitialized = false;
	private PopupMenu ctxMenu;
	private GameObjectToken focus = null;
	private TokenPropertyEditor editor;
	public WorldDisplayPanel()
	{
		super();
		this.setBackground( new Color( 0xFF, 0x00, 0xFF ) );
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		addMouseMotionListener(this);
		this.addMouseListener(this);
		ctxMenu = new PopupMenu("TokenEditMenu");
		ctxMenu.add("Edit");
		ctxMenu.add("Delete");
		add(ctxMenu);
		ctxMenu.addActionListener(this);
		editor = new TokenPropertyEditor();
	}
	
	public void insertToken( GameObjectToken t, int x, int y )
	{
		if ( !isInitialized )
		{
			return;
		}
		spic.add( t.drawable, ScrollableContainer.WORLD_OBJECT_LAYER );
		t.drawable.addMouseListener(this);
		t.drawable.setBounds( x, 
							  y, 
							  (int) t.drawableDims.getWidth(), 
							  (int) t.drawableDims.getHeight() );
		SharedToolState.activeToken = null;
		Vector2Resource res = (Vector2Resource) t.entityData.get("position");
		if ( res == null )
		{
			System.err.println( TAG + "::mouseReleased: Game object token did not contain position data" );
			throw new Error( TAG + "::mouseReleased: Game object token did not contain position data" );
		}
		res.x = x;
		res.y = y;
		SharedToolState.outstandingDeltas = true;
		SharedToolState.tokenList.add( t );
		spic.validate();
		spic.repaint();
	}
	
	private void setActiveToken( GameObjectToken t )
	{
		if ( !isInitialized )
		{
			return;
		}
		
		if ( SharedToolState.activeToken != null )
		{
			spic.remove( SharedToolState.activeToken.drawable );
			spic.validate();
			spic.paint(spic.getGraphics());
		}
		
		if ( t != null )
		{
			spic.add( t.drawable, ScrollableContainer.WORLD_TEMP_LAYER );
		}
	}
	
	public void setMapImage( String filename )
	{
		if ( isInitialized )
		{
			System.err.println( TAG + "::setMapImage: Resetting map image is not handled yet! Massive memory waste\n" );
		}
		BufferedImage myPicture;
		try {
			myPicture = ImageIO.read( new File( filename ) );
		} catch (IOException e) {
			System.err.println( "Failed to read infile: " + e.getMessage() );
			return;
		}
		
		spic = new ScrollableContainer(new ImageIcon(myPicture), 10, this);
		background = new JScrollPane( spic );

		add( background, BorderLayout.NORTH );
		
		System.gc();
		
		Runtime r = Runtime.getRuntime();
		
		String sout = "";
		
		sout = sout + ( r.freeMemory() / 1024 ) + " kB";
		sout = sout + " / ";
		sout = sout + ( r.maxMemory() / 1024 ) + " kB";
		
		System.err.println( "Memory status: " + sout );
		
		isInitialized = true;
		
		this.validate();
		this.paint(this.getGraphics());
	}
	
	@Override
    public void mouseEntered(MouseEvent e) 
    {
       if ( ( isInitialized ) && ( SharedToolState.activeToken != null ) )
       {
    	   setActiveToken( SharedToolState.activeToken );
       }
    }
    
    @Override
    public void mouseExited( MouseEvent e )
    {
    	if ( ( isInitialized ) && ( SharedToolState.activeToken != null ) )
    	{
    		setActiveToken( null );
    	}
    }

	@Override
	public void mouseMoved(MouseEvent e) 
	{ 
		if ( !isInitialized )
		{
			return;
		}
		
		
		if ( SharedToolState.activeToken != null )
		{
			SharedToolState.activeToken.drawable.setBounds( e.getX(), 
															e.getY(), 
															(int) SharedToolState.activeToken.drawableDims.getWidth(), 
															(int) SharedToolState.activeToken.drawableDims.getHeight() );
		}
		
	}
	
	@Override
    public void mouseDragged(MouseEvent e) 
	{
        
    }
	
	public Point getScrollPos()
	{
		return new Point( background.getHorizontalScrollBar().getValue(), background.getVerticalScrollBar().getValue() );
	}

	@Override
	public void mouseClicked(MouseEvent arg0) 
	{
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) 
	{
		if ( isInitialized )
		{
			if ( ( SharedToolState.activeToken != null ) && ( arg0.getButton() == MouseEvent.BUTTON1 ) )
		    {
				GameObjectToken focus = SharedToolState.activeToken;
				spic.remove( SharedToolState.activeToken.drawable );
				spic.add( focus.drawable, ScrollableContainer.WORLD_OBJECT_LAYER );
				focus.drawable.addMouseListener(this);
				SharedToolState.activeToken = null;
				Vector2Resource res = (Vector2Resource) focus.entityData.get("position");
				if ( res == null )
				{
					System.err.println( TAG + "::mouseReleased: Game object token did not contain position data" );
					throw new Error( TAG + "::mouseReleased: Game object token did not contain position data" );
				}
				res.x = arg0.getX();
				res.y = arg0.getY();
				SharedToolState.outstandingDeltas = true;
				SharedToolState.tokenList.add( focus );
				arg0.consume();
		    }
			else if ( ( SharedToolState.activeToken == null ) && ( arg0.getButton() == MouseEvent.BUTTON3 ) )
			{
				if ( arg0.getComponent().getClass() == JLabel.class )
				{
					JLabel target = (JLabel) arg0.getComponent();
					int len = SharedToolState.tokenList.size(); 
					for ( int i = 0; i < len; i++ )
					{
						if ( SharedToolState.tokenList.get( i ).drawable == target )
						{
							focus = SharedToolState.tokenList.get( i );
							break;
						}
					}
					ctxMenu.show(arg0.getComponent(), arg0.getX(), arg0.getY());
					arg0.consume();
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		if ( arg0.getSource().equals( ctxMenu ) )
		{
			if ( arg0.getActionCommand().compareTo("Edit") == 0 )
			{
				editor.setFocus( focus );
				editor.setVisible( true );
			}
			else // delete
			{
				SharedToolState.tokenList.remove( focus );
				spic.remove( focus.drawable );
				spic.validate();
				spic.paint( spic.getGraphics() );
				SharedToolState.outstandingDeltas = true;
				focus = null;
			}
		}
	}
}
