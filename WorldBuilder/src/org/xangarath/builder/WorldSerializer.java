package org.xangarath.builder;

import components.SharedToolState;

/**
 * Turn the world into a JSON object
 * @author mercury
 *
 */
public class WorldSerializer 
{
	private static final String TAG = "WorldSerializer";
	private int tabLevel = 0;
	public WorldSerializer()
	{
		
	}
	
	private String tab()
	{
		String json = "";
		for ( int i = 0; i < tabLevel; i++ )
		{
			json += "\t";
		}
		return json;
	}
	
	private String serializeMeta()
	{
		String json = "";
		json += tab();
		json += "\"Meta\":{\n";
		tabLevel++;
		
		json += tab();
		json += "\"map_file\":\"" + SharedToolState.mapImageName + "\"";
		
		int len = SharedToolState.tokenList.size();
		for ( int i = 0; i < len; i++ )
		{	
			GameObjectToken tok = SharedToolState.tokenList.get( i );
			if ( tok.cat != GameObjectToken.EntityCategory.META )
				continue;
			
			json += ",\n";
			
			json += tab();
			json += tok.toMetaString();
		}
		
		json += "\n";
		
		tabLevel--;
		json += tab();
		json += "}";
		return json;
	}
	
	private String serializeHelper( GameObjectToken.EntityCategory category )
	{
		String json = "";
		tabLevel++;
	
		json += tab();
		switch(category)
		{
		case META:
			json = serializeMeta();
			tabLevel--;
			return json;
		case DECOR:
			json += "\"Decor\":[\n";
			break;
		case ITEM:
			json += "\"Item\":[\n";
			break;
		case MONSTER:
			json += "\"Monster\":[\n";
			break;
		default:
			System.err.println( TAG + ": Encountered unknown entity category " );
			throw new Error( TAG + ": Encountered unknown entity category " );
		}
		tabLevel++;
		
		int len = SharedToolState.tokenList.size();
		boolean first = true;
		for ( int i = 0; i < len; i++ )
		{	
			GameObjectToken tok = SharedToolState.tokenList.get( i );
			if ( tok.cat != category )
				continue;
			
			if ( !first )
			{
				json += ",\n";
			}
			
			json += tab();
			json += tok.toString();
			first = false;
		}
		
		if ( len != 0 )
			json += "\n";
		
		tabLevel--;
		json += tab();
		json += "]";
		tabLevel--;
		return json;
	}
	
	public String serialize()
	{
		tabLevel = 0;
		String result = "{\n";
		
		result += serializeHelper( GameObjectToken.EntityCategory.META );
		result += ",\n";
		result += serializeHelper( GameObjectToken.EntityCategory.DECOR );
		result += ",\n";
		result += serializeHelper( GameObjectToken.EntityCategory.ITEM );
		result += ",\n";
		result += serializeHelper( GameObjectToken.EntityCategory.MONSTER );
		result += "\n";
		
		result += "}\n";
		return result;
	}
}
