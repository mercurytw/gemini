package org.xangarath.builder;

import java.awt.Dimension;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JLabel;

/**
 * Represents an object that can be placed on the map. 
 * This encapsulates both the drawable component of the 
 * data structure, and descriptive information that 
 * can be serialized into JSON
 **/
public class GameObjectToken 
{
	public enum EntityType
	{
		NONE,
		SCOOTASPIDER,
		WIZARD,
		ROCKETLOO,
		TENTACLE,
		HAUNTER,
		SMALL_KEY,
		DOOR,
		PLAYER_START
	}
	
	public enum EntityCategory
	{
		NONE,
		MONSTER,
		ITEM,
		DECOR,
		META
	}
	
	private static final String TAG = "GameObjectToken";
	public JLabel drawable;
	public Dimension drawableDims;
	public EntityType type = EntityType.NONE;
	public EntityCategory cat = EntityCategory.NONE;
	public HashMap<String, Object> entityData;
	public GameObjectToken( JLabel d, Dimension dims )
	{
		if ( d != null )
		{
			drawable = d;
			if ( dims == null )
			{
				System.err.println("Dims is null you shit");
				throw new Error( "GameObjectToken::ctor: Dims was null" );
			}
			drawableDims = dims;
		}
		else
		{
			drawable = null;
			drawableDims = null;
		}
		entityData = new HashMap<String, Object>();
	}
	
	public String getTypeString()
	{
		switch(type)
		{
		case NONE:
			return "none";
		case SCOOTASPIDER:
			return "scootaspider";
		case WIZARD:
			return "wizard";
		case ROCKETLOO:
			return "bull";
		case TENTACLE:
			return "tentacle";
		case HAUNTER:
			return "haunter";
		case DOOR:
			return "door";
		case SMALL_KEY:
			return "small_key";
		case PLAYER_START:
			return "player_start";
		default:
			throw new Error( TAG + ": Encountered unknown entity type! " + type);
		}
	}
	
	// For stringifying meta objects
	public String toMetaString()
	{
		String ret = "\"" + getTypeString() + "\":{";
		
		Iterator<String> it = entityData.keySet().iterator();
		boolean first = true;
		while( it.hasNext() )
		{
			if ( !first )
			{
				ret += ", ";
			}
			
			String key = it.next();
			Object o = entityData.get( key );
			if ( o.getClass() == String.class )
			{
				ret += "\"" + key + "\":\"" + o.toString() + "\"";
			}
			else
			{
				ret += "\"" + key + "\":" + o.toString();
			}
			first = false;
		}
		ret += "}";
		return ret;
	}
	
	@Override
	public String toString()
	{
		String ret = "{";
		ret += "\"type\":\"" + getTypeString() + "\"";

		Iterator<String> it = entityData.keySet().iterator();
		while( it.hasNext() )
		{
			ret += ", ";
			
			String key = it.next();
			Object o = entityData.get( key );
			if ( o.getClass() == String.class )
			{
				ret += "\"" + key + "\":\"" + o.toString() + "\"";
			}
			else
			{
				ret += "\"" + key + "\":" + o.toString();
			}
		}
		
		ret += "}";
		return ret;
	}
}
