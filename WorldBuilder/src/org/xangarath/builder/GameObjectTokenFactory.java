package org.xangarath.builder;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import org.json.JSONArray;
import org.json.JSONObject;

import components.StringArrayResource;
import components.Vector2Resource;

public class GameObjectTokenFactory 
{
	private static final String TAG = "GameObjectTokenFactory";
	public GameObjectTokenFactory()
	{
		
	}
	
	private GameObjectToken reviveDecorEntity( JSONObject o )
	{
		GameObjectToken ret = null;
		JSONArray tmpArr;
		
		String type = o.getString("type");
		if (type == null)
			return null;
		
		ret = constructDecor(type);
		if (ret == null)
			return null;
		
		switch (type)
		{
		case "door":
			StringArrayResource strRes = (StringArrayResource) ret.entityData.get("lockReqs");
			tmpArr = o.getJSONArray("lockReqs");
			for (int i = 0; i < tmpArr.length(); i++)
			{
				strRes.stringArray.add( tmpArr.getString( i ) );
			}
			break;
		default:
			throw new Error(TAG + ":reviveDecorEntity: Encountered unknown minor type: \"" + type + "\"");
		}
		Vector2Resource res = (Vector2Resource) ret.entityData.get("position");
		if ( res == null )
		{
			System.err.println( TAG + "::mouseReleased: Game object token did not contain position data" );
			throw new Error( TAG + "::mouseReleased: Game object token did not contain position data" );
		}
		tmpArr = o.getJSONArray("position");
		res.x = (float) tmpArr.getInt(0);
		res.y = (float) tmpArr.getInt(1);
		return ret;
	}
	
	private GameObjectToken reviveItemEntity( JSONObject o )
	{
		GameObjectToken ret = null;
		JSONArray tmpArr;
		
		String type = o.getString("type");
		if (type == null)
			return null;
		
		ret = constructItem(type);
		if (ret == null)
			return null;
		
		switch (type)
		{
		case "small_key":
			break;
		default:
			throw new Error(TAG + ":reviveDecorEntity: Encountered unknown minor type: \"" + type + "\"");
		}
		Vector2Resource res = (Vector2Resource) ret.entityData.get("position");
		if ( res == null )
		{
			System.err.println( TAG + "::mouseReleased: Game object token did not contain position data" );
			throw new Error( TAG + "::mouseReleased: Game object token did not contain position data" );
		}
		tmpArr = o.getJSONArray("position");
		res.x = (float) tmpArr.getInt(0);
		res.y = (float) tmpArr.getInt(1);
		return ret;
	}
	
	private GameObjectToken reviveMonsterEntity( JSONObject o )
	{
		GameObjectToken ret = null;
		JSONArray tmpArr;
		
		String type = o.getString("type");
		if (type == null)
			return null;
		
		ret = constructMonster(type);
		if (ret == null)
			return null;

		Vector2Resource res = (Vector2Resource) ret.entityData.get("position");
		if ( res == null )
		{
			System.err.println( TAG + "::mouseReleased: Game object token did not contain position data" );
			throw new Error( TAG + "::mouseReleased: Game object token did not contain position data" );
		}
		tmpArr = o.getJSONArray("position");
		res.x = (float) tmpArr.getInt(0);
		res.y = (float) tmpArr.getInt(1);
		return ret;
	}
	
	public GameObjectToken constructFromJson( String majorType, JSONObject obj )
	{
		GameObjectToken res = null;
		if (majorType.compareTo("Meta") == 0)
		{
			// The json object in the meta category doesn't contain the minor type,
			// so I can't support this
			return null;
		}
		else if (majorType.compareTo("Decor") == 0)
		{
			res = reviveDecorEntity( obj );
		}
		else if ( majorType.compareTo( "Item" ) == 0 )
		{
			res = reviveItemEntity( obj );
		}
		else if ( majorType.compareTo( "Monster" ) == 0 )
		{
			res = reviveMonsterEntity( obj );
		}
		else
		{
			System.err.println( "Failed to construct object from JSON: Unknown major type: " + majorType );
			System.exit( -1 );
		}
	
		return res;
	}
	
	public GameObjectToken construct(String major, String minor)
	{
		switch(major)
		{
		case "Monster":
			System.out.println("Major type was monster");
			return constructMonster( minor );
		case "Item":
			System.out.println( "Major type was item" );
			return constructItem( minor );
		case "Decor":
			System.out.println( "Major type was decor" );
			return constructDecor( minor );
		case "Meta":
			System.out.println( "Major type was meta" );
			return constructMeta( minor );
		default:
			System.err.println( TAG + " Encountered unknown major token type: " + major );
			return null;
		}
	}
	
	private ImageIcon loadImage(String filename)
	{
		String pathToRoot = System.getenv( "GEMINI_ROOT" );
		filename = pathToRoot + "\\assets\\" + filename;
		BufferedImage myPicture = null;
		try {
			myPicture = ImageIO.read( new File( filename ) );
		} catch (IOException e) {
			throw new Error( TAG + "Failed to read infile: " + e.getMessage() );
		}
		
		return new ImageIcon(myPicture);
	}
	
	private GameObjectToken constructItem( String minor )
	{
		GameObjectToken t = null;
		ImageIcon ico = null;
		switch(minor)
		{
		case "small_key":
			System.out.println("Minor type was small_key");
			ico = loadImage( "scootakey.png" );
			t = new GameObjectToken( new JLabel(ico), 
 					new Dimension( ico.getIconWidth(), ico.getIconHeight() ) );
			t.type = GameObjectToken.EntityType.SMALL_KEY;
			break;
		default:
			System.err.println( TAG + " Encountered unknown minor token type: " + minor );
			return null;
		}
		t.cat = GameObjectToken.EntityCategory.ITEM;
		t.entityData.put( "position", new Vector2Resource( Float.NaN, Float.NaN ) );
		return t;
	}
	
	private GameObjectToken constructDecor(String minor)
	{
		GameObjectToken t = null;
		ImageIcon ico = null;
		switch(minor)
		{
		case "door":
			System.out.println("Minor type was horizontal_door");
			ico = loadImage( "terminalDoorHoriz.jpg" );
			t = new GameObjectToken( new JLabel(ico), 
 					new Dimension( ico.getIconWidth(), ico.getIconHeight() ) );
			t.type = GameObjectToken.EntityType.DOOR;
			t.entityData.put( "lockReqs", new StringArrayResource() );
			break;
		default:
			System.err.println( TAG + " Encountered unknown minor token type: " + minor );
			return null;
		}
		t.cat = GameObjectToken.EntityCategory.DECOR;
		t.entityData.put( "position", new Vector2Resource( Float.NaN, Float.NaN ) );
		return t;
	}
	
	private GameObjectToken constructMeta(String minor)
	{
		GameObjectToken t = null;
		ImageIcon ico = null;
		switch(minor)
		{
		case "player_start":
			System.out.println("Minor type was player_start");
			ico = loadImage( "player_start.png" );
			t = new GameObjectToken( new JLabel(ico), 
 					new Dimension( ico.getIconWidth(), ico.getIconHeight() ) );
			t.type = GameObjectToken.EntityType.PLAYER_START;
			break;
		default:
			System.err.println( TAG + " Encountered unknown minor token type: " + minor );
			return null;
		}
		t.cat = GameObjectToken.EntityCategory.META;
		t.entityData.put( "position", new Vector2Resource( Float.NaN, Float.NaN ) );
		return t;
	}
	
	private GameObjectToken constructMonster(String minor)
	{
		GameObjectToken t = null;
		ImageIcon ico = null;
		switch(minor)
		{
		case "scootaspider":
			System.out.println("Minor type was scootaspider");
			ico = loadImage( "scootaspider.jpg" );
			t = new GameObjectToken( new JLabel(ico), 
 					new Dimension( ico.getIconWidth(), ico.getIconHeight() ) );
			t.type = GameObjectToken.EntityType.SCOOTASPIDER;
			break;
		case "shooter":
		case "wizard":
			System.out.println("Minor type was shooter");
			ico = loadImage( "wizard.png" );
			t = new GameObjectToken( new JLabel(ico), 
 					new Dimension( ico.getIconWidth(), ico.getIconHeight() ) );
			t.type = GameObjectToken.EntityType.WIZARD;
			break;
		case "bull":
		case "rocketloo":
			System.out.println( "Minor type was rocketloo" );
			ico = loadImage( "bull.png" );
			t = new GameObjectToken( new JLabel(ico), 
 					new Dimension( ico.getIconWidth(), ico.getIconHeight() ) );
			t.type = GameObjectToken.EntityType.ROCKETLOO;
			break;
		case "tentacle":
			System.out.println( "Minor type was tentacle" );
			ico = loadImage( "tentacleGuy.png" );
			t = new GameObjectToken( new JLabel(ico), 
 					new Dimension( ico.getIconWidth(), ico.getIconHeight() ) );
			t.type = GameObjectToken.EntityType.TENTACLE;
			break;
		case "haunter":
			System.out.println( "Minor type was haunter" );
			ico = loadImage( "ghost.png" );
			t = new GameObjectToken( new JLabel(ico), 
 					new Dimension( ico.getIconWidth(), ico.getIconHeight() ) );
			t.type = GameObjectToken.EntityType.HAUNTER;
			break;
		default:
			System.err.println( TAG + " Encountered unknown minor token type: " + minor );
			return null;
		}
		t.cat = GameObjectToken.EntityCategory.MONSTER;
		t.entityData.put( "position", new Vector2Resource( Float.NaN, Float.NaN ) );
		return t;
	}
}
