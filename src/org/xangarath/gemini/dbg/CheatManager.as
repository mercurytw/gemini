package org.xangarath.gemini.dbg 
{
	import flash.display.Shape;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.console.InvocationTarget;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.DebugConsole;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.ActionFactory;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.controllers.MonsterController;
	import org.xangarath.gemini.controllers.PlayerController;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.events.LevelChangeEvent;
	import org.xangarath.gemini.factories.ActorFactory;
	import org.xangarath.gemini.factories.ControllerFactory;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.performs.*;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	
	/**
	 * Controls cheats/debug commands in the game
	 * @author Jeffrey Cochran
	 */
	public class CheatManager implements ITickable
	{
		private var console:DebugConsole;
		private var data:GeminiData;
		private var ctlr:PlayerController;
		private var factory:ActionFactory;
		private var dbgDrawTree:Shape;
		private var drawQuadtree:Boolean = false;
		
		private static const invincibleInfo:String =  "> invincible < true | false > \n " +
											          "  sets invincibility. Calling with no arguements toggles invincibility";
	    private static const spiderInfo:String = "> spawn x y <health>\n" +
												 "  create a new scootaspider at the given location with the given hp \n" +
												 "  the health is optional and will default to 4 if not supplied";
		private static const weapoInfo:String = "> dump_actions\n" + 
												"  displays current stats of player actions";
												
		private static const setWeapData:String = "> set_weap_stat slot stat value\n" +
												  "  modifies the stats of weapon in slot 'slot'\n" +
												  "  valid stats are damage, stun, and knockback";
												  
		private static const setActData:String = "> set_action_stat slot stat value\n" +
												  "  modifies the stats of action in slot 'slot'\n" +
												  "  stats common to all actions are are delay, cooldown, and cost";
												  
		private static const healInfo:String = "> heal\n" +
		                                       "  heals player to max health\n";
											   
		private static const buildWeapInfo:String = "> new_weap slot type delay cost damage stun knockback cooldown\n" +
													"  Builds a new weapon with the given stats and attaches it to slot 'slot'";
													
		private static const buildActionInfo:String = "> new_action slot type delay cost cooldown param1 val1 param2 val2...";
		
		private static const detatchInfo:String = "> detatch slot\n" +
												  "  destroys the action in slot 'slot'";
												  
												  
		private static const drawWallInfo:String = "> draw_walls ( true | false ) \n " +
											       "  shows or hides invisible walls";
												   
		private static const drawQuadtreeInfo:String = "> draw_quadtree \n" +
												   " toggles quadtree on/off";
												   
	    private static const setLoglevelInfo:String = "> set_log_level number";
		
		private static const noclipInfo:String = ">noclip \n" +
											 " toggles wallwalk for player";
											 
		private static const rebindInfo:String = ">rebind <path>\n" +
												 " attempts to reload a script";
		
		private static const dumpPosInfo:String = ">dump_pos \n" +
												  " prints the player's position to the console";
												  
		private static const giveAllInfo:String = ">give_all \n" + 
												  " gives the player all of conner and annie's moves"
												  
		private static const loadLevelInfo:String = ">load_level <name>\n" +
												" changes to level <name> and places the player at the default starting point";
		
		public function CheatManager(console:DebugConsole, data:GeminiData, ctlr:PlayerController) 
		{
			this.console = console;
			this.data = data;
			this.ctlr = ctlr;
			this.factory = new ActionFactory(data);
			
			
			dbgDrawTree = new Shape();
			dbgDrawTree.name = "dbgTree";
			data.sceneMgr.fxLayer.addChild( dbgDrawTree );
			
			console.registerInvocationTarget(new InvocationTarget("invincible", invincible, invincibleInfo));
			console.registerInvocationTarget(new InvocationTarget("spawn", spawn, spiderInfo));
			console.registerInvocationTarget(new InvocationTarget("dump_actions", dumpWeaponInfo, weapoInfo));
			console.registerInvocationTarget(new InvocationTarget("set_weap_stat", setWeapStats, setWeapData));
			console.registerInvocationTarget(new InvocationTarget("heal", heal, healInfo));
			console.registerInvocationTarget(new InvocationTarget("new_weap", newWeapon, buildWeapInfo));
			console.registerInvocationTarget(new InvocationTarget("detatch", detatchWeapon, detatchInfo));
			console.registerInvocationTarget(new InvocationTarget("set_action_stat", setActionStats, setActData));
			console.registerInvocationTarget(new InvocationTarget("new_action", newAction, buildActionInfo));
			console.registerInvocationTarget( new InvocationTarget( "draw_walls", drawWalls, drawWallInfo ) );
			console.registerInvocationTarget( new InvocationTarget( "draw_quadtree", showQuadtree, drawQuadtreeInfo ) );
			console.registerInvocationTarget( new InvocationTarget( "set_log_level", setLoglevel, setLoglevelInfo ) );
			console.registerInvocationTarget( new InvocationTarget( "noclip", noclip, noclipInfo ) );
			console.registerInvocationTarget( new InvocationTarget( "rebind", rebind, rebindInfo ) );
			console.registerInvocationTarget( new InvocationTarget( "dump_pos", dumpPos, dumpPosInfo ) );
			console.registerInvocationTarget( new InvocationTarget( "spawn_by_me", spawnNear, "spawn_by_me <actor> <controller>" ) );
			console.registerInvocationTarget( new InvocationTarget( "give_all", giveAll, giveAllInfo ) );
			console.registerInvocationTarget( new InvocationTarget( "load_level", loadLevel, loadLevelInfo ) );
		}
		
		public function loadLevel( a:Array ):String
		{
			if ( a.length == 0 )
			{
				return loadLevelInfo;
			}
			data.activeLevel.dispatchEvent( new LevelChangeEvent( a[0] ) );
			console.toggle();
			return "loading " + a[0];
		}
		
		public function spawnNear( a:Array ):String
		{
			var afact:ActorFactory = new ActorFactory( data );
			var ctlrfact:ControllerFactory = new ControllerFactory( data );
			var spider:Actor;
			var ctlr:ITickable;
			if ( a.length == 0 )
			{
				return "spawn_by_me: usage: spawn_by_me <actor> <controller>";
			}
			if ( a.length >= 1 )
			{
				spider = afact.getGenericActor( a[0] as String );
				spider.badWallMask = TerrainManager.TERRAIN_TYPE_UNCROSSABLE | 
								GeminiTerrainInterpreter.TERRAIN_TYPE_WALL |
								GeminiTerrainInterpreter.TERRAIN_TYPE_WATER;
			}
			if ( a.length == 2 )
			{
				ctlr = ctlrfact.getController( spider as Creature, a[1] as String );
			}
			
			spider.spawnInTheSameRoomAs( data.playerAtBat, data );
			return "spawn_by_me: success";
		}
		
		public function giveAll( a:Array ):String
		{
			data.saveData.annieHasE = data.saveData.annieHasQ = data.saveData.annieHasR = data.saveData.annieHasW = 
			data.saveData.connerHasE = data.saveData.connerHasQ = data.saveData.connerHasR = data.saveData.connerHasW = true;
			
			for each (var act:Action in data.conner.actions)
			{
				act.enabled = true;
			}
			for each (var b:Action in data.annie.actions)
			{
				b.enabled = true;
			}
			return "give_all: success";
		}
		
		public function dumpPos( a:Array ):String
		{
			if (data.playerAtBat == data.conner)
				return "conner at " + data.conner.x + "," + data.conner.y;
				
			return "annie at " + data.annie.x + "," + data.annie.y;
		}
		
		public function rebind( a:Array ):String
		{
			if ( a.length == 1 )
			{
				var res:Array =  data.scriptMgr.redoReloadableFile( a[0] );
				if ((res == null) || (res[0] == false))
				{
					return "rebind: failed to rebind script " + a[0];
				}
				return "rebind: success";
			}
			return "rebind: incorrect number of parameters";
		}
		
		private var isNoclipActive:Boolean = false;
		private var annieWallMask:uint = 0x0;
		private var connorWallMask:uint = 0x0;
		public function noclip( a:Array ):String
		{
			if ( a.length == 0 )
			{
				if (!isNoclipActive)
				{
					connorWallMask = data.conner.badWallMask;
					data.conner.badWallMask = 0;
					data.conner.isSolid = false;
					data.conner.ignoreEvents = true;
					if (data.annie != null)
					{
						annieWallMask = data.annie.badWallMask;
						data.annie.badWallMask = 0;
						data.annie.ignoreEvents = true;
						data.annie.isSolid = false;
					}
					isNoclipActive = true;
					return "noclip: activated";
				}
				else
				{
					data.conner.badWallMask = connorWallMask;
					data.conner.isSolid = true;
					data.conner.ignoreEvents = false;
					if (data.annie != null)
					{
						data.annie.badWallMask = annieWallMask;
						data.annie.isSolid = true;
						data.annie.ignoreEvents = false;
					}
					isNoclipActive = false;
					return "noclip: deactivated";
				}
				
			}
			return "set_log_level: incorrect number of parameters";
		}
		
		public function setLoglevel( a:Array ):String
		{
			if ( a.length == 1 )
			{
				var level:int = parseInt(a[0]);
				if ( !isNaN( level ) )
				{
					var str:String = "set_log_level: Logging set to level ";
					switch ( level )
					{
						case 0:
							str += "error";
							break;
						case 1:
							str += "warn";
							break;
						case 2:
							str += "debug";
							break;
						case 3:
							str += "info";
							break;
						case 4:
							str += "verbose";
							break;
						default:
							str += "invalid";
							break;
					}
					DebugLog.getSingleton()..setLoglevel( level );
					return str;
				}
				return "set_log_level: invalid parameter " + a[0];
			}
			return "set_log_level: incorrect number of parameters";
		}
		
		public function showQuadtree( a:Array ):String
		{
			var ret:String = "Quadtree drawing: ";
			if ( ( drawQuadtree = !drawQuadtree ) )
			{
				ret += "on";
			}
			else 
			{
				dbgDrawTree.graphics.clear();
				ret += "off";
			}
			return ret;
		}
		
		public function newAction( a:Array ):String
		{
			if ( ( a.length >= 5 ) )
			{
				if ( !(a.length & 1) )
				{
					return("Invalid number of parameters " + a.join(" ") );
				}
				var slot:Number = parseInt(a[0]);
				var type:String = a[1];
				var delay:Number = parseFloat(a[2]);
				var cost:Number = parseFloat(a[3]);
				var cool:Number = parseFloat(a[4]);
				
				if ( isNaN(slot) || isNaN(delay) || isNaN(cost) || isNaN(cool) )
				{
					return "Invalid command: new_weap " + a.join(" ");
				}
				if ( ( slot < 0 ) || ( slot > 4 ) )
				{
					return "Invalid weapon slot. valid slots: 0-3";
				}
				var obj:Object = new Object();
				obj["delay"] = delay;
				obj["cost"] = cost;
				obj["type"] = type;
				obj["name"] = "DynAction";
				obj["cooldown"] = cool;
				
				a = a.slice(5);
				
				var str:String;
				var valstr:String;
				var val:Number;
				while ( a.length > 0 )
				{
					str = a.shift();
					valstr = a.shift();
					val = parseFloat(valstr);
					if ( isNaN( val ) )
					{
						return "new_action: specified invalid value " + valstr + " to parameter " + str;
					}
					obj[str] = val;
				}
				
				var action:Action = factory.makeActionFromJson( obj, data.playerAtBat, Actor.PLAYER_TEAM, data.playerAtBat.stats );
				data.playerAtBat.attachAction( action, slot );
				return("action attached");
			}
			return "Invalid command: new_action " + a.join(" ");
		}
		
		public function heal( a:Array ):String
		{
			data.playerAtBat.notifyHolder(new HealthEvent(HealthEvent.HEAL_TYPE, null, data.playerAtBat.stats.maxHP));
			return "Player healed to max HP (" + data.playerAtBat.stats.maxHP + ")";
		}
		
		public function detatchWeapon( a:Array ):String
		{
			if ( a.length > 0 )
			{
				var slot:int = parseInt(a[0]);
				if ( isNaN(slot) )
				{
					return "Invalid command: detatch " + a.join(" ");
				}
				if ( ( slot < 0 ) || ( slot > 4 ) )
				{
					return "Invalid action slot. valid slots: 0-3";
				}
				data.playerAtBat.detatchAction( slot );
				return "Action detatched";
			}
			return "Invalid command: detatch " + a.join(" ");
		}
		
		public function newWeapon( a:Array ):String
		{
			if ( a.length == 8 )
			{
				var slot:Number = parseInt(a[0]);
				var type:String = a[1];
				var delay:Number = parseFloat(a[2]);
				var cost:Number = parseFloat(a[3]);
				var dmg:Number = parseFloat(a[4]);
				var stun:Number = parseFloat(a[5]);
				var knock:Number = parseFloat(a[6]);
				var cool:Number = parseFloat(a[7]);
				
				if ( isNaN(slot) || isNaN(delay) || isNaN(cost) || isNaN(dmg) || isNaN(stun) || isNaN(knock) || isNaN(cool) )
				{
					return "Invalid command: new_weap " + a.join(" ");
				}
				if ( ( slot < 0 ) || ( slot > 4 ) )
				{
					return "Invalid weapon slot. valid slots: 0-3";
				}
				var obj:Object = new Object();
				obj["delay"] = delay;
				obj["cost"] = cost;
				obj["type"] = type;
				obj["name"] = "DynWeap";
				obj["damage"] = dmg;
				obj["stun"] = stun;
				obj["knockback"] = knock;
				obj["cooldown"] = cool;
				var b:String = null;
				if ( type.toLowerCase() == StabAttackState.STAB_ATTACK_TYPE ) { b = "sword" }
				else if ( type.toLowerCase() == SweepAttackState.SWEEP_ATTACK_TYPE ) { b = "sweep" }
				else if ( type.toLowerCase() == CircleAttackState.CIRCLE_ATTACK_TYPE ) { b = "circle" }
				obj["bitmap"] = b;
				var action:Action = factory.makeActionFromJson( obj, data.playerAtBat, Actor.PLAYER_TEAM, data.playerAtBat.stats );
				data.playerAtBat.attachAction( action, slot );
				return("Weapon attached");
			}
			return "Invalid command: new_weap " + a.join(" ");
		}
		
		public function setActionStats( a:Array ):String
		{
			if ( a.length == 3 )
			{
				var slot:Number = parseInt(a[0]);
				var stat:String = a[1];
				var val:Number = parseFloat(a[2]);
				if ( isNaN(slot) || isNaN(val) )
				{
					return "Invalid command: set_action_stat " + a.join(" ");
				}
				if ( data.playerAtBat.actions[slot] == null )
				{
					return "No action is attached to slot " + slot;
				}
				if ( ( data.playerAtBat.actions[slot].hasOwnProperty(stat) ) && ( data.playerAtBat.actions[slot][stat] is Number ) )
				{
					data.playerAtBat.actions[slot][stat] = val;
					return("Action adjusted");
					
				} else if ( ( data.playerAtBat.actions[slot].mStates["perform"].hasOwnProperty(stat) ) && ( data.playerAtBat.actions[slot].mStates["perform"][stat] is Number ) )
				{
					data.playerAtBat.actions[slot].mStates["perform"][stat] = val;
					return("Action adjusted");
				}
				return "Action in slot " + slot + " does not have numeric stat " + stat;
				
			}
			return "Invalid command: set_action_stat " + a.join(" ");		
		}
		
		public function setWeapStats( a:Array ):String
		{ 
			if ( a.length == 3 )
			{
				var slot:Number = parseInt(a[0]);
				var stat:String = a[1];
				var val:Number = parseFloat(a[2]);
				if ( isNaN(slot) || isNaN(val) )
				{
					return "Invalid command: set_weap_stat " + a.join(" ");
				}
				if ( data.playerAtBat.actions[slot] == null )
				{
					return "No action is attached to slot " + slot;
				} else if ( !(data.playerAtBat.actions[slot].mStates["perform"] is IWeaponStatAcceptor) )
				{
					return "Action in slot " + slot + " is not a weapon";
				}
				switch( stat )
				{
					case "damage":
						(data.playerAtBat.actions[slot].mStates["perform"] as IWeaponStatAcceptor).weaponData.damage = val;
						break;
					case "stun":
						(data.playerAtBat.actions[slot].mStates["perform"] as IWeaponStatAcceptor).weaponData.stunTime = val;
						break;
					case "knockback":
						(data.playerAtBat.actions[slot].mStates["perform"] as IWeaponStatAcceptor).weaponData.knockback = val;
						break;
					default:
						return "Invalid command: set_weap_stat " + a.join(" ");
				}
				return("Weapon adjusted");
			}
			return "Invalid command: set_weap_stat " + a.join(" ");
		}
		
		public function dumpWeaponInfo( a:Array ):String
		{
			var str:String = ""
			for ( var i:int = 0; i < data.playerAtBat.actions.length; ++i)
			{
				if ( i != 0 )
				{
					str += "\n";
				}
				str += "Slot " + i + ": " + data.playerAtBat.actions[i];
			}
			return str;
		}
		
		public function spawn( a:Array ):String
		{
			var x:Number;
			var y:Number;
			var hp:Number;
			
			if ( a.length < 2 )
			{
				return "invalid command: spawn " + a.join(" ");
			}
			x = parseFloat(a[0]);
			y = parseFloat(a[1]);
			hp = ( a.length >= 3 ) ? parseFloat(a[2]) : 4.0;
			
			if ( isNaN(x) || isNaN(y) || isNaN(hp) )
			{
				return "invalid command: spawn " + a.join(" ");
			}
			
			var critter:Creature = new Creature( data.resMgr.getBitmap("scootaspider"), Actor.ENEMY_TEAM, 0, x, y);
			data.sceneMgr.gameLayer.addChild(critter);
			data.sceneMgr.tree.insert( critter.node );
			var ctlr:MonsterController = new MonsterController(critter, hp, false, data);
			data.tickList.push(ctlr);
			
			return "Spider spawned";
		}
		
		private var isInvincible:Boolean = false;
		public function invincible( a:Array ):String
		{
			if ( a.length == 0 )
			{
				if ( isInvincible )
				{
					data.party.invincibilityDown();
					isInvincible = false;
					return "Invincibility off";
				}
				else
				{
					data.party.invincibilityUp();
					isInvincible = true;
					return "Invincibility on";
				}
			}
			else if ( ( a[0] as String ) == "true" )
			{
				if ( !isInvincible )
				{
					data.party.invincibilityUp();
					isInvincible = true;
				}
				return "Invincibility on";
			}
			else if ( ( a[0] as String ) == "false")
			{
				if ( isInvincible )
				{
					data.party.invincibilityDown();
					isInvincible = false;
				}
				return "Invincibility off";
			}
			
			
			return "invalid command: invincible " + a.join(" ");
		}
		
		public function drawWalls( a:Array ):String
		{
			var truth:Boolean;
			var retval:String;
			if ( ( a[0] as String ) == "true" )
			{
				truth = true;
				retval =  "Invisible walls shown";
			}
			else if ( ( a[0] as String ) == "false")
			{
				truth = false;
				retval =  "invisible walls hidden";
			} else 
			{
				return "invalid command: draw_walls " + a.join(" ");
			}
			
			for ( var i:int = 0; i < data.activeLevel.walls.length; i++ )
			{
				(data.activeLevel.walls[ i ] as Actor).visible = truth;
			}
			
			return retval;
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( drawQuadtree )
			{
				dbgDrawTree.graphics.clear();
				data.sceneMgr.tree.draw( dbgDrawTree.graphics );
			}
		}
		
	}

}