package org.xangarath.gemini.stats 
{
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.interfaces.IDestructable;
	
	// TODO: Move invincibility token stack here
	
	/**
	 * Reflects stats for a creature
	 * @author Jeff Cochran
	 */
	public class StatPage implements IDestructable
	{
		private static const TAG:String = "StatPage";
		public var _moveSpeed:NumericStat;
		public var _baseHP:NumericStat;
		public var _transientHP:NumericStat;
		public var _currentHP:NumericStat;
		public var _baseAttack:NumericStat;
		public var _transientAttack:NumericStat;
		public var _baseDefense:NumericStat;
		public var _transientDefense:NumericStat;
		public var _vsMonsterAttack:NumericStat;
		public var _vsHumanAttack:NumericStat;
		public var armored:Boolean = false;
		private var isConstructed:Boolean = true;
		public function StatPage() 
		{
			_moveSpeed = new NumericStat();
			_baseHP = new NumericStat();
			_transientHP = new NumericStat();
			_currentHP = new NumericStat();
			_baseAttack = new NumericStat();
			_transientAttack = new NumericStat();
			_baseDefense = new NumericStat();
			_transientDefense = new NumericStat();
			_vsMonsterAttack = new NumericStat();
			_vsHumanAttack = new NumericStat();
		}
		
		public function get maxHP():Number
		{
			return _baseHP.value + _transientHP.value;
		}
		
		public function calculateEffectiveAttack( target:Actor ):Number
		{
			if ( target == null )
			{
				return _baseAttack.value + _transientAttack.value;
			}
			
			return _baseAttack.value + _transientAttack.value;
		}
		
		public function get effectiveDefense():Number
		{
			return _baseDefense.value + _transientDefense.value;
		}
		
		/**
		 * This method is used to bypass an overlay
		 */
		public function get contributingBaseHP():Number 
		{ 
			return _baseHP.realValue; 
		}
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			_baseAttack.destroy();
			_baseDefense.destroy();
			_baseHP.destroy();
			_currentHP.destroy();
			_moveSpeed.destroy();
			_transientAttack.destroy();
			_transientDefense.destroy();
			_transientHP.destroy();
			_vsHumanAttack.destroy();
			_vsMonsterAttack.destroy();
			
			_baseAttack = null;
			_baseDefense = null;
			_baseHP = null;
			_currentHP = null;
			_moveSpeed = null;
			_transientAttack = null;
			_transientDefense = null;
			_transientHP = null;
			_vsHumanAttack = null;
			_vsMonsterAttack = null;
			
			isConstructed = false;
		}
		
		// ======== Numeric Stat Wrappers ======== //
		
		public function get moveSpeed():Number { return _moveSpeed.value; }
		public function set moveSpeed(x:Number):void { _moveSpeed.value = x; }
		public function get baseHP():Number { return _baseHP.value; }
		public function set baseHP(x:Number):void { _baseHP.value = x; }
		public function get transientHP():Number { return _transientHP.value; }
		public function set transientHP(x:Number):void { _transientHP.value = x; }
		public function get currentHP():Number { return _currentHP.value; }
		public function set currentHP(x:Number):void { _currentHP.value = x; }
		public function get baseAttack():Number { return _baseAttack.value; }
		public function set baseAttack(x:Number):void { _baseAttack.value = x; }
		public function get transientAttack():Number { return _transientAttack.value; }
		public function set transientAttack(x:Number):void { _transientAttack.value = x; }
		public function get baseDefense():Number { return _baseDefense.value; }
		public function set baseDefense(x:Number):void { _baseDefense.value = x; }
		public function get transientDefense():Number { return _transientDefense.value; }
		public function set transientDefense(x:Number):void { _transientDefense.value = x; }
		public function get vsMonsterAttack():Number { return _vsMonsterAttack.value; }
		public function set vsMonsterAttack(x:Number):void { _vsMonsterAttack.value = x; }
		public function get vsHumanAttack():Number { return _vsHumanAttack.value; }
		public function set vsHumanAttack(x:Number):void { _vsHumanAttack.value = x; }
	}

}