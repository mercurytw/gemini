package org.xangarath.gemini.stats 
{
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	
	// NTH: detection of cycles in overlay
	/**
	 * Represents a number whose value can be masked by some other value... kind of hard to explain
	 * 
	 * value = ((_value * premulitplicands) + addends) * postmultiplicands
	 * 
	 * @author Jeff Cochran
	 */
	public class NumericStat extends Object implements IDestructable
	{
		private static const TAG:String = "NumericStat";
		private var _value:Number;
		private var overlay:NumericStat = null;
		private var premultiplicands:Array = new Array();
		private var addends:Array = new Array();
		private var postmultiplicands:Array = new Array();
		private var isConstructed:Boolean = true;
		public function NumericStat( n:Number = 0.0 ) 
		{
			_value = n;
		}
		
		public function addAddend( stat:NumericStat ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( addends.indexOf( stat ) != -1 )
			{
				DebugLog.getSingleton().w( TAG, " tried to re-add a numeric stat contributor" );
				return;
			}
			
			addends.push( stat );
		}
		
		public function removeAddend( stat:NumericStat ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( addends.indexOf( stat ) == -1 )
			{
				DebugLog.getSingleton().w( TAG, " tried to remove a numeric stat contributor that doesn't exist" );
				return;
			}
			
			addends.splice( addends.indexOf( stat ), 1 );
		}
		
		public function addPremultiplicand( stat:NumericStat ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( premultiplicands.indexOf( stat ) != -1 )
			{
				DebugLog.getSingleton().w( TAG, " tried to re-add a numeric stat premultiplicand" );
				return;
			}
			
			premultiplicands.push( stat );
		}
		
		public function removePremultiplicand( stat:NumericStat ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( premultiplicands.indexOf( stat ) == -1 )
			{
				DebugLog.getSingleton().w( TAG, " tried to remove a numeric stat premultiplicand that doesn't exist" );
				return;
			}
			
			premultiplicands.splice( premultiplicands.indexOf( stat ), 1 );
		}
		
		public function addPostmultiplicand( stat:NumericStat ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( postmultiplicands.indexOf( stat ) != -1 )
			{
				DebugLog.getSingleton().w( TAG, " tried to re-add a numeric stat postmultiplicand" );
				return;
			}
			
			postmultiplicands.push( stat );
		}
		
		public function removePostmultiplicand( stat:NumericStat ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( postmultiplicands.indexOf( stat ) == -1 )
			{
				DebugLog.getSingleton().w( TAG, " tried to remove a numeric stat contributor that doesn't exist" );
				return;
			}
			
			postmultiplicands.splice( postmultiplicands.indexOf( stat ), 1 );
		}
		
		public function setOverlay( stat:NumericStat ):NumericStat
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var prev:NumericStat = overlay;
			overlay = stat;
			return prev;
		}
		
		public function clearOverlay():NumericStat
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var prev:NumericStat = overlay;
			overlay = null;
			return prev;
		}
		
		public function get realValue():Number
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return _value;
		}
		
		public function get value():Number
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var ret:Number = _value;
			var stat:NumericStat;
			
			if ( overlay != null )
			{
				return overlay.value;
			}
			
			for each ( stat in premultiplicands )
			{
				ret *= stat.value;
			}
			
			for each ( stat in addends )
			{
				ret += stat.value;
			}
			
			for each ( stat in postmultiplicands )
			{
				ret *= stat.value;
			}
			
			return ret;
		}
		
		public function set value( rhs:Number ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			
			if (overlay != null)
			{
				overlay.value = rhs;
			}
			else
			{
				_value = rhs;
			}
		}
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			clearOverlay();
			
			addends.length = 0;
			addends = null;
			
			postmultiplicands.length = 0;
			postmultiplicands = null;
			
			premultiplicands.length = 0;
			premultiplicands = null;
			
			isConstructed = false;
		}
		
		public function toString():String
		{
			if ( !isConstructed )
			{
				return "Destroyed";
			}
			
			return "" + value;
		}
	}

}