package org.xangarath.gemini 
{
	import flash.display.Stage;
	import flash.geom.Rectangle;
	import org.xangarath.engine.SharedData;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.actors.Party;
	import org.xangarath.gemini.items.ItemEncyclopedia;
	import org.xangarath.gemini.save.SaveData;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	import org.xangarath.gemini.world.Level;
	
	/**
	 * Data specific to this game
	 * @author Jeffrey Cochran
	 */
	public class GeminiData extends SharedData 
	{
		public var config:ConfigData;
		public var conner:Creature;
		public var annie:Creature = null;
		// TODO: Remove these, and rely only on the party, which will always be correct
		public var playerAtBat:Creature;
		public var playerOnDeck:Creature = null;
		public var activeLevel:Level;
		public var masterItemList:ItemEncyclopedia;
		public var saveData:SaveData;
		public var jump:Action = null;
		public var party:Party = null;
		private var _screenspace:Rectangle = null;
		public function GeminiData( stage:Stage ) 
		{
			super(stage, new GeminiTerrainInterpreter());
			config = new ConfigData();
			saveData = new SaveData();
		}
		
		public function get screenspace():Rectangle
		{
			if ( _screenspace == null )
			{
				_screenspace = new Rectangle();
				_screenspace.width = playArea.width;
				_screenspace.height = playArea.height;
			}
			_screenspace.x = playerAtBat.x - ( _screenspace.width / 2.0 );
			_screenspace.y = playerAtBat.y - ( _screenspace.height / 2.0 );
			
			return _screenspace;
		}
		
	}

}