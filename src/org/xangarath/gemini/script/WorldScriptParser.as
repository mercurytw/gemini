package org.xangarath.gemini.script 
{
	import flash.geom.Point;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.gemini.GeminiData;
	/**
	 * Parses .wld files.
	 * @author Jeffrey Cochran
	 */
	public class WorldScriptParser 
	{
		private static const TAG:String = "WorldScriptParser";
		private var data:GeminiData;
		private var jsonObj:Object;
		private var playerStart:Point = null;
		private var isBuilt:Boolean = false;
		private var decor:Array;
		private var items:Array;
		private var monsters:Array;
		public function WorldScriptParser( data:GeminiData ) 
		{
			this.data = data;
			this.decor = new Array();
			this.items = new Array();
			this.monsters = new Array();
		}
		
		public function getPlayerStart():Point
		{
			if ( !isBuilt )
			{
				return null;
			}
			
			return playerStart;
		}
		
		public function getDecor():Array
		{
			if ( !isBuilt )
			{
				return null;
			}
			
			return decor;
		}
		
		public function getItems():Array
		{
			if ( !isBuilt )
			{
				return null;
			}
			
			return items;
		}
		
		public function getMonsters():Array
		{
			if ( !isBuilt )
			{
				return null;
			}
			
			return monsters;
		}
		
		public function init( jsonString:String ):void
		{
			jsonObj = JSON.parse( jsonString );
			if ( jsonObj == null )
			{
				trace( TAG + "init: unable to parse JSON!" );
				return;
			}
			
			// just build everything here. 
			buildMeta();
			buildObjects();
			
			isBuilt = true;
		}
		
		private function buildMeta():void
		{
			var metaObj:Object = Argonaut.getAndCheck( TAG, jsonObj, "Meta" );
			playerStart = Argonaut.getAndCheckPoint( TAG, Argonaut.getAndCheck( TAG, metaObj, "player_start" ), "position" );
		}
		
		private function buildObjects():void
		{
			var decorObj:Object = Argonaut.getAndCheck( TAG, jsonObj, "Decor" );
			
			for ( var i:int = 0; decorObj[ i ] != null; i++ )
			{
				var obj:Object = decorObj[ i ];
				decor.push( obj );
			}
			
			var itemObj:Object = Argonaut.getAndCheck( TAG, jsonObj, "Item" );
			
			for ( i = 0; itemObj[ i ] != null; i++ )
			{
				obj = itemObj[ i ];
				items.push( obj );
			}
			
			var monsterObj:Object = Argonaut.getAndCheck( TAG, jsonObj, "Monster" );
			
			for ( i = 0; monsterObj[ i ] != null; i++ )
			{
				obj = monsterObj[ i ];
				monsters.push( obj );
			}
		}
	}
}