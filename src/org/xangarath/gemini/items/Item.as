package org.xangarath.gemini.items 
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * An item. 
	 * @author Jeffrey Cochran
	 */
	public class Item extends Actor
	{
		private static const TAG:String = "Item";
		public var properties:ItemProperties;
		public function Item( props:ItemProperties, drawable:Bitmap, x:Number, y:Number ) 
		{
			super( true, drawable, NO_TEAM, x, y );
			this.setName( props.name );
			this.properties = props;
			this.ignoreEvents = true;
		}
		
		public function pickUp( destination:Inventory, data:GeminiData ):void
		{
			if ( destination.add( properties ) == true )
			{
				if ( node.isInTree )
				{
					data.sceneMgr.tree.remove( node );
				}
				if ( data.sceneMgr.gameLayer.contains( this ) )
				{
					data.sceneMgr.gameLayer.removeChild( this );
				}
				properties = null;
			}
		}
		
		override public function destroy():void 
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( properties != null )
			{
				properties.destroy();
				properties = null;
			}
			
			super.destroy();
		}
		
	}

}