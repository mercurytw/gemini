package org.xangarath.gemini.items 
{
	import com.sociodox.theminer.data.InternalEventEntry;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IEffect;
	
	// TODO: Make me destructable
	/**
	 * Properties that make an item unique
	 * @author Jeffrey Cochran
	 */
	public class ItemProperties implements IDestructable
	{
		private static const TAG:String = "ItemProperties";
		
		public static const UNKNOWN:uint = (0x1 << 31);
		
		public static const CONNER:uint = (0x1 << 0);
		public static const ANNIE:uint = (0x1 << 1);
		public static const QUEST:uint = (0x1 << 2);
		public static const KEY:uint = (0x1 << 3);
		public static const CONSUMABLE:uint = (0x1 << 4);
		public static const BADGE:uint = (0x1 << 5);
		public var name:String;
		public var type:String;
		public var qualifiers:uint
		public var description:String;
		public var iconKey:String;
		public var effects:Array;
		public var bonusEffects:Array;
		// The below two are used for badges only
		public var brief:String;
		public var bonusRequirement:String;
		public var bonusActive:Boolean = false;
		private var isConstructed:Boolean = true;
		public function ItemProperties( name:String, type:String, qualifiers:uint, iconKey:String, description:String, effects:Array ) 
		{
			this.name = name;
			this.type = type;
			this.qualifiers = qualifiers;
			this.iconKey = iconKey;
			this.description = description;
			this.effects = effects
			this.bonusEffects = new Array();
			this.brief = "";
			this.bonusRequirement = null;
		}
		
		// this is a hack to check that health-only items can't be consumed by accident - Past Jeff
		// wait what? - Present Jeff
		// lol noobs - Future Jeff
		public function isValid( target:Creature ):Boolean
		{
			if ( effects.length == 1 )
			{
				return (effects[ 0 ] as IEffect).isValid( target );
			}
			return true;
		}
		
		// clone ctor
		public function clone( other:ItemProperties ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			this.name = other.name;
			this.type = other.type;
			this.qualifiers = other.qualifiers;
			this.iconKey = other.iconKey;
			this.description = other.description;
			this.effects = new Array();
			this.bonusEffects = new Array();
			
			for ( var i:int = 0; i < other.effects.length; i++ )
			{
				effects.push ( (other.effects[ i ] as IEffect).clone() );
			}
			
			for ( var j:int = 0; j < other.bonusEffects.length; j++ )
			{
				bonusEffects.push( ( other.bonusEffects[ j ] as IEffect).clone() );
			}
			
			this.brief = other.brief;
			this.bonusRequirement = other.bonusRequirement;
		}
		
		public function destroy():void
		{
			var i:int = 0;
			var len:int = 0;
			var eft:IEffect = null;
			if ( !isConstructed )
			{
				return;
			}
			
			len = bonusEffects.length;
			for ( i = 0; i < len; i++ )
			{
				eft = ErrorUtil.dbgSafeCast( TAG, bonusEffects[ i ], IEffect );
				
				if ( eft is IDestructable )
				{
					(eft as IDestructable).destroy();
				}
			}
			bonusEffects.length = 0;
			bonusEffects = null;
			
			bonusRequirement = null;
			brief = null;
			description = null;
			
			len = effects.length;
			for ( i = 0; i < len; i++ )
			{
				eft = ErrorUtil.dbgSafeCast( TAG, effects[ i ], IEffect );
				
				if ( eft is IDestructable )
				{
					(eft as IDestructable).destroy();
				}
			}
			effects.length = 0;
			effects = null;
			iconKey = null;
			name = null;
			type = null;
			
			isConstructed = false;
		}
		
		public static function getQualifierFromString( str:String ):uint
		{
			switch(str.toLowerCase())
			{
				case ("conner"):
					return CONNER;
				case ("annie"):
					return ANNIE;
				case ("quest"):
					return QUEST;
				case ("key"):
					return KEY;
				case ("consumable"):
					return CONSUMABLE;
				case ("capsule"):
				case ("badge"):
					return BADGE;
				default:
					DebugLog.getSingleton().e(TAG, "Encountered unknown item qualifier: " + str);
					return UNKNOWN;
			}
		}
	}

}