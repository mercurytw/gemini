package org.xangarath.gemini.items 
{
	import flash.utils.Dictionary;
	import org.xangarath.gemini.GeminiData;
	
	import org.xangarath.engine.dbg.StatLogger;
	import org.xangarath.gemini.effects.EffectArray;
	import org.xangarath.gemini.factories.EffectFactory;
	import org.xangarath.gemini.interfaces.IEffect;
	
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	/**
	 * Contains all the ItemProperty archetypes for the game, as parsed from JSON. If you can
	 * think up a better name for it, then be my guest.
	 * @author jcochran
	 */
	public class ItemEncyclopedia 
	{
		private static const TAG:String = "ItemEncyclopedia";
		
		private var itemLUT:Dictionary;
		private var fact:EffectFactory;
		public function ItemEncyclopedia( data:GeminiData ) 
		{
			itemLUT = new Dictionary();
			fact = new EffectFactory( data );
		}
		
		private function buildBonusFromJson( jsobj:Object, target:ItemProperties ):void
		{
			var req:String = Argonaut.getAndCheck( TAG, jsobj, "req" );
			var effects:Object = Argonaut.getAndCheck( TAG, jsobj, "effects" );
			
			for ( var i:int = 0; effects[ i ] != null; i++ )
			{
				var e:IEffect = fact.buildEffectFromJSON( effects[ i ] );
				if ( e != null )
				{
					target.bonusEffects.push( e );
				}
			}
			
			target.bonusRequirement = req;
		}
		
		public function parseItem( jsobj:Object ):void
		{
			var i:int;
			var type:String = Argonaut.getAndCheck( TAG, jsobj, "type" );
			type = type.toLowerCase();
			var name:String = Argonaut.getAndCheck( TAG, jsobj, "name" );
			
			var quals:Object = Argonaut.getAndCheck( TAG, jsobj, "qualifiers" );
			var qualifiers:uint = 0x0;
			
			for ( i = 0; quals[ i ] != null; i++ )
			{
				qualifiers |= ItemProperties.getQualifierFromString( quals[ i ] as String );
			}
			
			var icon:String = Argonaut.getAndCheck( TAG, jsobj, "icon" );
			var descr:String = Argonaut.getAndCheck( TAG, jsobj, "description" );
			var behav:Object = Argonaut.getAndCheck( TAG, jsobj, "behavior" );
			var effectArray:Array = new Array();
			
			for ( i = 0; behav[ i ] != null; i++ )
			{
				var e:IEffect = fact.buildEffectFromJSON( behav[ i ] );
				if ( e != null )
				{
					effectArray.push( e );
				}
			}
			
			itemLUT[ type ] = new ItemProperties( name, type, qualifiers, icon, descr, effectArray );
			
			if ( ( qualifiers & ItemProperties.BADGE ) == ItemProperties.BADGE )
			{
				var brief:String = Argonaut.getAndCheck( TAG, jsobj, "brief" );
				(itemLUT[ type ] as ItemProperties).brief = brief;
				
				if ( jsobj[ "bonus" ] != null )
				{
					buildBonusFromJson( jsobj[ "bonus" ], (itemLUT[ type ] as ItemProperties) );
				}
			}
			
			if ( ( qualifiers & ItemProperties.CONSUMABLE ) == ItemProperties.CONSUMABLE )
			{
				StatLogger.getSingleton().addStat( type, "" + type + "s used" );
			}
		}
		
		public function parseFromJSON( jsobj:Object ):void
		{
			for ( var i:int = 0; jsobj[ i ] != null; i++ )
			{
				parseItem( jsobj[ i ] );
			}
		}
		
		public function lookupItem( type:String ):ItemProperties
		{
			type = type.toLowerCase();
			if ( itemLUT[ type ] == null )
			{
				DebugLog.getSingleton().e( TAG, "Unable to find item type \"" + type + "\"" );
				return null;
			}
			return itemLUT[ type ];
		}
	}

}