package org.xangarath.gemini.items 
{
	import org.xangarath.engine.interfaces.IDestructable;
	/**
	 * An inventory item
	 * @author Jeffrey Cochran
	 */
	public class InventoryEntry implements IDestructable
	{
		public var props:ItemProperties;
		public function InventoryEntry( properties:ItemProperties )
		{
			props = properties;
		}
		
		public function destroy():void
		{
			if ( props != null )
			{
				props.destroy();
			}
			props = null;
		}
	}
}