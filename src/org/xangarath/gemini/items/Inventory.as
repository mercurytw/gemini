package org.xangarath.gemini.items 
{
	import flash.desktop.InvokeEventReason;
	import flash.utils.Dictionary;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.gemini.interfaces.IEffect;
	import org.xangarath.gemini.items.InventoryEntry;
	import org.xangarath.gemini.items.ItemProperties;
	
	/**
	 * Contains items held by an actor.
	 * @author Jeffrey Cochran
	 */
	public class Inventory implements IDestructable
	{
		private static const TAG:String = "Inventory";
		public var entries:Array;
		public var cap:uint;
		public var nonquestItems:uint = 0;
		private var isConstructed:Boolean = true;
		public function Inventory( capacity:uint = 0 ) 
		{
			entries = new Array();
			cap = capacity;
		}
		
		/**
		 * Checks for the location of props in the inventory
		 * @param	props The item in the inventory to check for
		 * @return The index of the item in the inventory, or -1 if it was not found
		 */
		public function indexOf( props:ItemProperties ):int
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			for ( var i:int = 0; i < entries.length; i++ )
			{
				var entry:InventoryEntry = entries[ i ];
				if ( entry.props == props )
				{
					return i;
				}
			}
			
			return -1;
		}

		/**
		 * Adds an item to the inventory at the specified location
		 * @param	props An ItemProperties describing the item to add
		 * @param	index The index at which to insert the item
		 * @return true on Success, false otherwise
		 */
		public function addAt( props:ItemProperties, index:int ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( ( cap != 0 ) && ( nonquestItems >= cap ) && ( ( (props.qualifiers & ItemProperties.QUEST) != ItemProperties.QUEST ) &&
			                                                   ( (props.qualifiers & ItemProperties.BADGE) != ItemProperties.BADGE ) ) )
			{
				return false;
			}
			
			if ( ( (props.qualifiers & ItemProperties.QUEST) != ItemProperties.QUEST ) && 
				 ( (props.qualifiers & ItemProperties.BADGE) != ItemProperties.BADGE ) )
			{
				nonquestItems++;
			}
			
			index = (index < 0) ? 0 : index;
			index = (index > entries.length) ? entries.length : index;
			
			entries.splice(index, 0, new InventoryEntry( props ));
			return true;
		}
		
		/**
		 * Adds an item to the end of the inventory
		 * @param	props An ItemProperties describing the item to add
		 */
		public function add( props:ItemProperties ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( ( cap != 0 ) && ( nonquestItems >= cap ) && ( ( (props.qualifiers & ItemProperties.QUEST) != ItemProperties.QUEST ) &&
			                                                   ( (props.qualifiers & ItemProperties.BADGE) != ItemProperties.BADGE ) ) )
			{
				return false;
			}
			
			if ( ( (props.qualifiers & ItemProperties.QUEST) != ItemProperties.QUEST ) && 
				 ( (props.qualifiers & ItemProperties.BADGE) != ItemProperties.BADGE ) )
			{
				nonquestItems++;
			}
			
			entries.push( new InventoryEntry( props ));
			return true;
		}
		
		/**
		 * Adds an inventory entry to the end of the inventory
		 * @param	props An inventory entry
		 */
		public function addEntry( entry:InventoryEntry ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( ( cap != 0 ) && ( nonquestItems >= cap ) && ( ( (entry.props.qualifiers & ItemProperties.QUEST) != ItemProperties.QUEST ) &&
			                                                   ( (entry.props.qualifiers & ItemProperties.BADGE) != ItemProperties.BADGE ) ) )
			{
				return false;
			}
			
			if ( ( (entry.props.qualifiers & ItemProperties.QUEST) != ItemProperties.QUEST ) && 
				 ( (entry.props.qualifiers & ItemProperties.BADGE) != ItemProperties.BADGE ) )
			{
				nonquestItems++;
			}
			
			entries.push( entry );
			return true;
		}
		
		/**
		 * looks up an item in the inventory.
		 * @param	type The type of the item to retrieve
		 * @return The requested ItemProperties, or null if the item was not in the inventory
		 */
		public function getEntry( type:String ):ItemProperties
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			type = type.toLowerCase();
			
			for ( var i:int = 0; i < entries.length; i++ )
			{
				var entry:InventoryEntry = entries[ i ];
				if ( entry.props.type == type )
				{
					return entry.props;
				}
			}
			return null;
		}
		
		public function removeEntry( ent:InventoryEntry ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			for ( var i:int = 0; i < entries.length; i++ )
			{
				var entry:InventoryEntry = entries[ i ];
				if ( entry == ent )
				{
					if ( ( (entry.props.qualifiers & ItemProperties.QUEST) != ItemProperties.QUEST ) && 
					     ( (entry.props.qualifiers & ItemProperties.BADGE) != ItemProperties.BADGE ) )
					{
						nonquestItems--;
					}
					entries.splice( i, 1 );
					return true;
				}
			}
			
			return false;
		}
		
		public function removeByProp( ent:ItemProperties ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			for ( var i:int = 0; i < entries.length; i++ )
			{
				var entry:InventoryEntry = entries[ i ];
				if ( entry.props == ent )
				{
					if ( ( (entry.props.qualifiers & ItemProperties.QUEST) != ItemProperties.QUEST ) && 
					     ( (entry.props.qualifiers & ItemProperties.BADGE) != ItemProperties.BADGE ) )
					{
						nonquestItems--;
					}
					entries.splice( i, 1 );
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * Tries to remove an item of type "type" from the inventory
		 * @return True/False reflecting success
		 **/
		public function remove( type:String ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			type = type.toLowerCase();
			
			for ( var i:int = 0; i < entries.length; i++ )
			{
				var entry:InventoryEntry = entries[ i ];
				if ( entry.props.type == type )
				{
					if ( ( (entry.props.qualifiers & ItemProperties.QUEST) != ItemProperties.QUEST ) && 
					     ( (entry.props.qualifiers & ItemProperties.BADGE) != ItemProperties.BADGE ) )
					{
						nonquestItems--;
					}
					entries.splice( i, 1 );
					return true;
				}
			}
			return false;
		}
		
		/**
		 * Tries to remove an item at the given index from the inventory
		 * @return True/False reflecting success
		 **/
		public function removeAt( index:int ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( entries.length >= index )
			{
				return false;
			}
			
			var entry:InventoryEntry = entries[ index ];
			
			if ( ( (entry.props.qualifiers & ItemProperties.QUEST) != ItemProperties.QUEST ) && 
				 ( (entry.props.qualifiers & ItemProperties.BADGE) != ItemProperties.BADGE ) )
			{
				nonquestItems--;
			}
			entries.splice( index, 1 );
			return true;
		}
		
		public function removeAll():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			entries.length = 0;
		}
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			for each ( var entry:InventoryEntry in entries )
			{
				entry.destroy();
			}
			entries.length = 0;
			entries = null;
			
			
			isConstructed = false;
		}
	}
	
	

}