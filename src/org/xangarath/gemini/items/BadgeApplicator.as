package org.xangarath.gemini.items 
{
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.gemini.GeminiData;

	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.interfaces.IEffect;
	/**
	 * Takes a badge and attaches it to the creature in the given slot, or removs it.
	 * Either way, this class abstracts all the badge effect specific setup and shutdown required
	 * @author ...
	 */
	public class BadgeApplicator 
	{
		private static const TAG:String = "BadgeApplicator";
		private var data:GeminiData;
		public function BadgeApplicator( data:GeminiData )
		{
			this.data = data;
		}
		
		public function applyBadge( badge:ItemProperties, slot:int, target:Creature ):void
		{
			var i:int;
			var e:IEffect;
			var otherSlot:int = (slot) ? 0 : 1;
			var bonusBadge:ItemProperties = target.badgeSlots[ otherSlot ];
			var annieBaseHpBefore:int = 0;
			
			if ( badge == null )
			{
				return;
			}
			
			if ( data.annie != null )
			{
				annieBaseHpBefore = data.annie.stats.baseHP;
			}
			
			if ( target.badgeSlots[ slot ] != null )
			{
				DebugLog.getSingleton().w( TAG, "Badge already in target slot. Removing..." );
				detatchBadge( slot, target );
			}
			
			if ( bonusBadge != null )
			{
				if ( badge.bonusRequirement == bonusBadge.type )
				{
					bonusBadge = badge;
				}
				else if ( bonusBadge.bonusRequirement != badge.type )
				{
					bonusBadge = null;
				}
				
				if ( bonusBadge != null )
				{
					bonusBadge.bonusActive = true;
					for ( i = 0; i < bonusBadge.bonusEffects.length; i++ )
					{
						e = bonusBadge.bonusEffects[ i ] as IEffect;
						target.attachEffect( e );
					}
				}
			}
			
			for ( i = 0; i < badge.effects.length; i++ )
			{
				e = badge.effects[ i ] as IEffect;
				target.attachEffect(e);
			}
			
			target.badgeSlots[ slot ] = badge;
			
			var annieBaseHpAfter:int = 0;
			if ( data.annie != null )
			{
				annieBaseHpAfter = data.annie.stats.baseHP;
				//data.conner.stats.baseHP += (annieBaseHpAfter - annieBaseHpBefore);
			}
		}
		
		public function detatchBadge( slot:int, target:Creature ):void
		{
			var i:int;
			var e:IEffect;
			var badge:ItemProperties = target.badgeSlots[ slot ];
			var otherBadge:ItemProperties = target.badgeSlots[ (slot) ? 0 : 1 ];
			var annieBaseHpBefore:int = 0;
			
			if (badge == null)
				return;
				
			if ( data.annie != null )
			{
				annieBaseHpBefore = data.annie.stats.baseHP;
			}
				
			if ( otherBadge != null )
			{
				if ( !otherBadge.bonusActive )
					otherBadge = null;
				
				if ( badge.bonusActive )
					otherBadge = badge;
					
				if ( otherBadge != null )
				{
					for ( i = 0; i < otherBadge.bonusEffects.length; i++ )
					{
						e = otherBadge.bonusEffects[ i ] as IEffect;
						target.detatchEffect( e );
					}
					otherBadge.bonusActive = false;
				}
			}
				
			for ( i = 0; i < badge.effects.length; i++ )
			{
				e = badge.effects[ i ] as IEffect;
				target.detatchEffect( e );
			}
			
			target.badgeSlots[ slot ] = null;
			
			var annieBaseHpAfter:int = 0;
			if ( data.annie != null )
			{
				annieBaseHpAfter = data.annie.stats.baseHP;
				//data.conner.stats.baseHP += (annieBaseHpAfter - annieBaseHpBefore);
			}
		}
	}

}