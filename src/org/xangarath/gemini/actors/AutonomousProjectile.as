package org.xangarath.gemini.actors 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.Hotspot;
	import org.xangarath.engine.interfaces.IPoolable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.quadtree.QuadTree;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	/**
	 * A self-controlling actor that reclaims itself when it leaves the screen
	 * @author Jeffrey Cochran
	 */
	public class AutonomousProjectile extends Actor implements ITickable, IPoolable 
	{
		private static const TAG:String = "AutonomousProjectile";
		private static const MOVE_DELAY:Number = 0.25;
		private var _isActive:Boolean;
		private var _weaponData:WeaponData;
		private var data:GeminiData;
		public var direction:Vector.<Number>;
		private var layer:Sprite;
		private var shouldStep:Boolean;
		private var shouldPenetrate:Boolean;
		private var speed:Number;
		
		private var hitList:Array;
		private var shouldDeactivate:Boolean;
		public function AutonomousProjectile( data:GeminiData, penetrate:Boolean, speed:Number, wdat:WeaponData, shouldStep:Boolean, layer:Sprite, drawable:Bitmap, team:uint, xPosition:Number=0.0, yPosition:Number=0.0 ) 
		{
			super( false, drawable, team, xPosition, yPosition);
			_isActive = false;
			this.data = data;
			this.layer = layer;
			direction = new Vector.<Number>(2);
			this.shouldStep = shouldStep;
			this._weaponData = wdat;
			this.shouldPenetrate = penetrate;
			this.speed = speed;
			hitList = new Array();
			this.name = "AutonomousProjectile#" + id;
			
		}
		
		private function handleCollision( e:Event ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var evt:CollisionEvent = e as CollisionEvent;
			if ( _isActive )
			{
				if ( ( evt.instigator is Actor ) && ( evt.instigator.team == NO_TEAM ) )
				{
					shouldDeactivate = true;
				} else if (evt.instigator.team != _weaponData.team)
				{
					if ( hitList.indexOf(evt.instigator) == -1 )
					{
						evt.instigator.notifyHolder(new HealthEvent(HealthEvent.DAMAGE_TYPE, this, _weaponData.damage + _weaponData.stats.calculateEffectiveAttack( evt.instigator ),
						                                            _weaponData.armorPenetration, _weaponData.knockback, _weaponData.stunTime));
						hitList.push(evt.instigator);
						if ( !this.shouldPenetrate )
						{
							shouldDeactivate = true;
						}
					}
				}
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IPoolable */
		
		public function clone():IPoolable
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var bmp:Bitmap = new Bitmap( ( drawable as Bitmap).bitmapData );
			var other:AutonomousProjectile = new AutonomousProjectile( data, shouldPenetrate, speed, _weaponData, shouldStep, layer, bmp, team, this.x, this.y );
			other.visible = this.visible;
			return other;
		}
		
		public function isActive():Boolean 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return _isActive;
		}
		
		public function activate():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( !_isActive )
			{
				_isActive = true;
				layer.addChild( this );
				data.tickList.push( this );
				this.visible = true;
				this.addEventListener( CollisionEvent.COLLISION, handleCollision );
				shouldDeactivate = false;
				data.sceneMgr.tree.insert( this.node );
			}
		}
		
		public function deactivate():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( _isActive )
			{
				_isActive = false;
				layer.removeChild( this );
				data.tickList.splice( data.tickList.indexOf( this ), 1 );
				this.visible = false;
				while ( hitList.length > 0 )
				{
					hitList.pop();
				}
				this.removeEventListener( CollisionEvent.COLLISION, handleCollision );
				data.sceneMgr.tree.remove( this.node );
			}
		}
		
		override public function destroy():void 
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( _isActive )
			{
				deactivate();
			}
			
			direction.length = 0;
			direction = null;
			_weaponData.destroy();
			_weaponData = null;
			
			hitList = null; // was already emptied by the call to deactivate()
			
			layer = null;
			
			data = null;
			
			super.destroy();
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		private var timeAgg:Number = 0.0;
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed )
			{
				return;
			}
			if ( _isActive )
			{
				if ( shouldDeactivate )
				{
					deactivate();
					return;
				}
				if ( !shouldStep || ( ( timeAgg += deltaTime ) >= MOVE_DELAY ) )
				{
					timeAgg = 0.0;
					if ( !shouldStep )
					{
						move( direction[0] * data.GRID_UNIT_SIZE * speed * deltaTime, direction[1] * data.GRID_UNIT_SIZE * speed * deltaTime );
					}
					else
					{
						move( direction[0] * data.GRID_UNIT_SIZE, direction[1] * data.GRID_UNIT_SIZE );
					}
				}
				
				var terr:uint = data.terrainMgr.getTerrainForRectangle(getBoundingVolume());
			
				if ((terr & GeminiTerrainInterpreter.TERRAIN_TYPE_WALL) == GeminiTerrainInterpreter.TERRAIN_TYPE_WALL)
				{
					shouldDeactivate = true;
				}
			}
		}
		
	}

}