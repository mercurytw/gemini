package org.xangarath.gemini.actors 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.GameObjectRegistry;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.events.InventoryEvent;
	import org.xangarath.gemini.events.PartyEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.items.*;
	import org.xangarath.gemini.stats.StatPage;
	import org.xangarath.engine.dbg.DebugLog;
	
	/**
	 * An actor container that contains all the logic for flocking characters in a party, appropriately routing health and damage, etc.
	 * 
	 * Take note that the logic of this code currently DOES NOT handle changes to a character's move speed
	 * @author Jeff Cochran
	 */
	public class Party extends EventDispatcher implements ITickable
	{
		private static const TAG:String = "Party";
		private var members:Vector.<Creature>;
		private var _atBat:Creature = null;
		private var _id:uint;
		public var stats:StatPage;
		public var inventory:Inventory;
		public var name:String = TAG;
		private var data:GeminiData;
		private var invincibilityStack:int = 0;
		public function Party( data:GeminiData ) 
		{
			this.data = data;
			_id = GameObjectRegistry.getSingleton().register( this );
			members = new Vector.<Creature>();
			stats = new StatPage();
			// TODO: Make sure to properly set the inventory cap 
			inventory = new Inventory();
		}
		
		// move the whole party at once. Clean up later to distribute characters nicely
		public function setPos( x:Number, y:Number ):void
		{
			for each ( var char:Creature in members )
			{
				char.setPos( x, y );
			}
		}
		
		public function setDirection( x:Number, y:Number ):void
		{
			for each ( var char:Creature in members )
			{
				char.setDirection( x, y );
			}
		}
		
		public function invincibilityUp():void
		{
			invincibilityStack++;
		}
		
		public function invincibilityDown():void
		{
			if ( invincibilityStack == 0 )
			{
				return;
			}
			invincibilityStack--;
		}
		
		public function isInvincible():Boolean
		{
			return invincibilityStack > 0;
		}
		
		public function repeatEvent( e:Event ):void
		{
			this.dispatchEvent( new PartyEvent( e ) );
		}
		
		public function get id():uint
		{
			return _id;
		}
		
		public function get size():int
		{
			return members.length;
		}
		
		public function get atBat():Creature
		{
			return _atBat;
		}
		
		public function set atBat( rhs:Creature ):void
		{
			if ( members.indexOf( rhs ) == -1 )
			{
				DebugLog.getSingleton().e( TAG, "Attempted to set a creature to be at bat who wasn't in the party!" );
				return;
			}
			rhs.stats._moveSpeed.clearOverlay();
			
			if ( (_atBat != null) && ( rhs.id != _atBat.id ) )
			{
				var newIdx:int = members.indexOf( rhs );
				var oldPos:Point = _atBat.pos;
				members[0] = rhs;
				_atBat.setPos( rhs.x, rhs.y );
				rhs.setPos( oldPos.x, oldPos.y );
				members[ newIdx ] = _atBat;
				rhs.isSolid = true;
				_atBat.isSolid = false;
			}
			_atBat = rhs;
			data.playerAtBat = atBat;
			data.playerOnDeck = onDeck;
			
			for ( var i:int = 1; i < members.length; i++ )
			{
				members[ i ].stats._moveSpeed.setOverlay( _atBat.stats._moveSpeed );
			}
			
			this.stats._moveSpeed.value = _atBat.stats._moveSpeed.value;
		}
		
		public function get onDeck():Creature
		{
			if (members.length < 2)
			{
				return null;
			}
			return members[1];
		}
		
		public function containsActor( a:Creature ):Boolean
		{
			return members.indexOf( a ) != -1;
		}
		
		public function addActor( a:Creature ):Boolean
		{
			if ( members.indexOf(a) != -1 )
			{
				DebugLog.getSingleton().w( TAG, "Tried to add creature " + a.name + " but they were already present." );
				return false;
			}
			
			stats._baseHP.addAddend( a.stats._baseHP );
			stats.currentHP += a.stats.currentHP;
			a.stats._currentHP.setOverlay(stats._currentHP);
			stats._transientHP.addAddend( a.stats._transientHP );
			
			a.damageFunctionOverride = this.damageOverride;
			
			a.addEventListener( HealthEvent.DAMAGE_TYPE, repeatEvent );
			a.addEventListener( HealthEvent.HEAL_TYPE, repeatEvent );
			a.addEventListener( InventoryEvent.ATTACH, repeatEvent );
			a.addEventListener( InventoryEvent.DETATCH, repeatEvent );
			a.addEventListener( CollisionEvent.COLLISION, repeatEvent );
			
			for each (var it:InventoryEntry in a.inventory)
			{
				inventory.addEntry(it);
			}
			a.inventory.removeAll();
			a.setInventoryOverlay( inventory );
			
			members.push(a);
			if ( members.length == 1 )
			{
				atBat = a;
			}
			else
			{
				if ( data.playerOnDeck == null )
				{
					data.playerOnDeck = onDeck;
				}
				a.isSolid = false;
			}
			DebugLog.getSingleton().i( TAG, "" + a.name + " joined the party" );
			return true;
		}
		
		public function removeActor( a:Creature ):Boolean
		{
			if ( members.indexOf(a) == -1 )
			{
				DebugLog.getSingleton().w( TAG, "Tried to remove creature " + a.name + " but they were not present." );
				return false;
			}
			
			stats._baseHP.removeAddend( a.stats._baseHP );
			a.stats._currentHP.clearOverlay();
			stats._transientHP.removeAddend( a.stats._transientHP );
			
			a.removeEventListener( HealthEvent.DAMAGE_TYPE, repeatEvent );
			a.removeEventListener( HealthEvent.HEAL_TYPE, repeatEvent );
			a.removeEventListener( InventoryEvent.ATTACH, repeatEvent );
			a.removeEventListener( InventoryEvent.DETATCH, repeatEvent );
			a.removeEventListener( CollisionEvent.COLLISION, repeatEvent );
			
			// TODO: How should this be calculated?
			if ( stats.currentHP > stats.maxHP )
			{
				stats.currentHP = stats.maxHP;
			}
			
			if ( a.stats.currentHP > a.stats.maxHP )
			{
				a.stats.currentHP = a.stats.maxHP;
			}
			
			a.clearInventoryOverlay();
			
			a.damageFunctionOverride = null;
			
			if ( members.indexOf(a) == 0 )
			{
				atBat = onDeck;
			}
			
			members.splice(members.indexOf(a), 1);
			
			if ( members.length == 0 )
			{
				atBat = null;
			}
			
			DebugLog.getSingleton().i( TAG, "" + a.name + " left the party" );
			return true;
		}
		
		public function damageOverride( amt:Number, recipient:Creature ):void
		{
			if ( isInvincible() )
			{
				return;
			}
			
			if ( amt > 0.)
			{
				amt -= recipient.stats.effectiveDefense;
				amt = Math.max( amt, 0.0 );
			}
			stats.currentHP = Math.max(Math.min( stats.currentHP - amt, stats.maxHP ), 0.);
		}
		
		public function removeAllFromScene():void
		{
			for each( var c:Creature in members )
			{
				data.sceneMgr.gameLayer.removeChild( c );
				data.sceneMgr.tree.remove( c.node );
			}
		}
		
		// TODO: Improve the follow code to use A*
		private function follow( deltaTime:Number ):void
		{
			var currentPos:Point = new Point();
			var facingVect:Point = new Point();
			var stopDistance:Number;
			for ( var i:int = 1; i < members.length; i++ )
			{
				var c:Creature = members[ i ];
				if ( c.getBoundingVolume().intersects( atBat.getBoundingVolume() ) )
				{
					continue;
				}
				
				stopDistance =  ((c.getBoundingVolume().height * Maths.SQRT_TWO) / 2) + 
								((_atBat.getBoundingVolume().height * Maths.SQRT_TWO) / 2);
				
				// In reality, this distance function will only be called once per tick
				if ( Maths.distance( c.pos, atBat.pos ) <= stopDistance )
				{
					continue;
				}
				
				currentPos.copyFrom( c.pos );
				c.face( atBat );
				facingVect.setTo( c.dirX, c.dirY );
				c.move( facingVect.x * this.stats.moveSpeed * data.GRID_UNIT_SIZE * deltaTime,
						facingVect.y * this.stats.moveSpeed * data.GRID_UNIT_SIZE * deltaTime );
				
				if ( !c.getBoundingVolume().intersects( atBat.getBoundingVolume() ) )
				{
					continue;
				}
				
				facingVect.x = -facingVect.x;
				facingVect.y = -facingVect.y;
				c.setPos( atBat.pos.x, atBat.pos.y );
				c.move( stopDistance * facingVect.x,
						stopDistance * facingVect.y );
				
				c.face( _atBat );
			}
		}
		
		public function tick( deltaTime:Number ):void
		{
			follow( deltaTime );
			
			for each( var c:Creature in members )
			{
				for each ( var a:Action in c.actions )
				{
					if ( a != null )
					{
						a.tick( deltaTime );
					}
				}
				c.effects.tick( deltaTime );
			}
		}
	}

}