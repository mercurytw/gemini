package org.xangarath.gemini.actors 
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.display.Sprite;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.interfaces.IPoolable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.ActionFactory;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.stats.StatPage;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	/**
	 * Represents an autonomous projectile which can be passed a special action to perform upon collision
	 * @author Jeffrey Cochran
	 */
	public class FancyAutonomousProjectile extends Actor implements ITickable, IPoolable 
	{
		private static const TAG:String = "FancyAutonomousProjectile";
		private static const MOVE_DELAY:Number = 0.25;
		private var _isActive:Boolean;
		private var data:GeminiData;
		public var direction:Point;
		private var layer:Sprite;
		private var shouldStep:Boolean;
		private var speed:Number;
		
		private var hitList:Array;
		private var shouldDeactivate:Boolean;
		
		private var perform:Action = null;
		private var performJson:Object = null;
		private var stats:StatPage;
		public function FancyAutonomousProjectile( data:GeminiData, speed:Number, shouldStep:Boolean, drawable:Bitmap, team:uint, stats:StatPage, xPosition:Number=0.0, yPosition:Number=0.0 ) 
		{
			super( false, drawable, team, xPosition, yPosition);
			_isActive = false;
			this.stats = stats;
			this.data = data;
			this.layer = layer;
			direction = new Point();
			this.shouldStep = shouldStep;
			this.speed = speed;
			layer = data.sceneMgr.gameLayer;
			this.name = "FancyAutonomousProjectile#" + id;
		}
		
		public function setPerformAction( json:Object, weaponData:WeaponData ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var perfFact:ActionFactory = new ActionFactory( data );
			perform = perfFact.makeActionFromJson( json, this, team, stats );
			if ( perform != null )
			{
				( perform.mStates["perform"] as IWeaponStatAcceptor ).weaponData = weaponData;
				performJson = json;
				perform.addActionListener( ActionEvent.ACTION_COMPLETE, completionHandler );
			}
			else
			{
				throw new Error( TAG + " Failed to construct appropriate action" );
			}
		}
		
		private function completionHandler( e:Event ):void
		{
			// deactivate
			shouldDeactivate = true;
		}
		
		private function handleCollision( e:Event ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var evt:CollisionEvent = e as CollisionEvent;
			if ( _isActive )
			{
				if ( !(evt.instigator is Actor ) )
				{
					return;
				}
				if ( ( evt.instigator.team != this.team ) || ( evt.instigator.team == NO_TEAM ) )
				{	
					if ( perform != null )
					{
						perform.perform();
					}
				}
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IPoolable */
		
		public function clone():IPoolable 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var bmp:Bitmap = new Bitmap( ( drawable as Bitmap).bitmapData );
			var other:FancyAutonomousProjectile = new FancyAutonomousProjectile( data, speed, shouldStep, bmp, team, stats, this.x, this.y );
			other.visible = this.visible;
			
			if ( perform != null )
			{
				other.setPerformAction( performJson, ( perform.mStates["perform"] as IWeaponStatAcceptor ).weaponData );
			}
			return other;
		}
		
		public function isActive():Boolean 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return _isActive;
		}
		
		public function activate():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( !_isActive )
			{
				_isActive = true;
				layer.addChild( this );
				data.tickList.push( this );
				this.visible = true;
				this.addEventListener( CollisionEvent.COLLISION, handleCollision );
				shouldDeactivate = false;
				data.sceneMgr.tree.insert( this.node );
			}
		}
		
		public function deactivate():void 
		{
			if ( _isActive )
			{
				_isActive = false;
				layer.removeChild( this );
				data.tickList.splice( data.tickList.indexOf( this ), 1 );
				this.visible = false;
				this.removeEventListener( CollisionEvent.COLLISION, handleCollision );
				data.sceneMgr.tree.remove( this.node );
			}
		}
		
		override public function destroy():void 
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( _isActive )
			{
				deactivate();
			}
			
			direction = null;
			
			while ( hitList.length > 0 )
			{
				hitList.pop();
			}
			hitList = null;
			layer = null;
			
			if ( perform != null )
			{
				perform.destroy();
				perform = null;
			}
			performJson = null;
			
			stats = null;
			
			data = null;
			
			super.destroy();
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		private var timeAgg:Number = 0.0;
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( _isActive )
			{
				if ( shouldDeactivate )
				{
					deactivate();
					return;
				}
				if ( !shouldStep || ( ( timeAgg += deltaTime ) >= MOVE_DELAY ) )
				{
					timeAgg = 0.0;
					if ( !shouldStep )
					{
						move( direction.x * data.GRID_UNIT_SIZE * speed * deltaTime, direction.y * data.GRID_UNIT_SIZE * speed * deltaTime );
					}
					else
					{
						move( direction.x * data.GRID_UNIT_SIZE, direction.y * data.GRID_UNIT_SIZE );
					}
				}
				
				var terr:uint = data.terrainMgr.getTerrainForRectangle(getBoundingVolume());
			
				if ((terr & GeminiTerrainInterpreter.TERRAIN_TYPE_WALL) == GeminiTerrainInterpreter.TERRAIN_TYPE_WALL)
				{
					perform.perform();
				}
				
				perform.tick( deltaTime );
			}
		}
		
	}

}