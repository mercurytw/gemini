package org.xangarath.gemini.actors 
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.drm.AuthenticationMethod;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.anim.AnimatedSprite;
	import org.xangarath.engine.anim.AnimationManager;
	import org.xangarath.engine.anim.AnimationSequence;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.quadtree.QuadTree;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.effects.EffectArray;
	import org.xangarath.gemini.events.InventoryEvent;
	import org.xangarath.gemini.interfaces.IEffect;
	import org.xangarath.gemini.items.Inventory;
	import org.xangarath.gemini.items.ItemProperties;
	import org.xangarath.gemini.stats.StatPage;
	/**
	 * Any living creature
	 * @author Jeffrey Cochran
	 */
	public class Creature extends Actor
	{
		private static const TAG:String = "Creature";
		public var actions:Vector.<Action>;
		private var overrideVol:Rectangle = null;
		private var invincibilityTokens:uint = 0;
		private var _inventory:Inventory;
		private var inventoryOverlay:Inventory = null;
		public var effects:EffectArray;
		
		public var isAttacking:Boolean = false;
		
		// TODO: Extend this class to have a "qualities" dictionary that would allow us to classify groups of creatures, such as monster, tech poacher, etc, arbitrarily
		
		// Animations
		public static const ANIM_RIGHT:int = 0;
		public static const ANIM_LEFT:int = 1;
		public static const ANIM_UP:int = 2;
		public static const ANIM_DOWN:int = 3;
		public var badgeSlots:Vector.<ItemProperties>;
		public var stats:StatPage;
		public var damageFunctionOverride:Function = null;
		public function Creature( drawable:DisplayObject, team:uint, actionSlots:int, xPosition:Number=0.0, yPosition:Number=0.0, rotateTogether:Boolean = false) 
		{
			super( true, drawable, team, xPosition, yPosition, rotateTogether );
			
			actions = new Vector.<Action>( actionSlots );
			for (var i:int = 0; i < actionSlots; ++i)
			{
				actions[i] = null;
			}
			this.name = "Creature#" + id;
			
			_inventory = new Inventory();
			effects = new EffectArray();
			badgeSlots = new Vector.<ItemProperties>( 2 );
			stats = new StatPage();
		}
		
		public function get inventory():Inventory
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return (inventoryOverlay == null) ? _inventory : inventoryOverlay;
		}
		
		/**
		 * Allows you to have this creature's inventory be overridden by another. Implemented for use in the Party code
		 * @param	inv
		 * @return The previous inventory overlay
		 */
		public function setInventoryOverlay( inv:Inventory ):Inventory
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var old:Inventory = inventoryOverlay;
			inventoryOverlay = inv;
			return old;
		}
		
		public function clearInventoryOverlay():Inventory
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return setInventoryOverlay(null);
		}
		
		private var cardinalDirection:Point = new Point();
		public function getCardinalDirection():int
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			cardinalDirection.setTo( dirX, dirY );
			
			if ( Math.abs( cardinalDirection.x ) >= Math.abs( cardinalDirection.y ) )
			{
				if ( cardinalDirection.x >= 0.0 )
				{
					return ANIM_RIGHT;
				}
				else
				{
					return ANIM_LEFT;
				}
			}
			else
			{
				if ( cardinalDirection.y >= 0.0 )
				{
					return ANIM_DOWN;
				}
				else
				{
					return ANIM_UP;
				}
			}
		}
		
		/**
		 * Remove an invicibility token from the creature
		 */
		public function invincibilityDown():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( invincibilityTokens == 0 )
			{
				return;
			}
			invincibilityTokens--;
		}
		
		/**
		 * Place a new invincibility token on the creature
		 */
		public function invincibilityUp():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			invincibilityTokens++;
		}
		
		public function isInvincible():Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return invincibilityTokens > 0;
		}
	
		public function overrideBoundingVolume( vol:Rectangle ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			overrideVol = vol;
		}
		
		public function get health():Number
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return stats.currentHP;
		}
		
		public function heal( amt:Number ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			damage( -amt );
		}
		
		public function damage( amt:Number ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			//notify here?
			if (isInvincible() && amt > 0.)
			{
				return;
			}
			
			// TODO: move the invincibility check to the stat page
			if (damageFunctionOverride != null)
			{
				damageFunctionOverride( amt, this );
				return;
			}
			
			if ( amt > 0.)
			{
				amt -= stats.effectiveDefense;
				amt = Math.max( amt, 0.0 );
			}
			stats.currentHP = Math.max(Math.min( stats.currentHP - amt, stats.maxHP ), 0.);
		}
		
		public function attachAction( action:Action, slot:int ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if (actions[slot] != null)
			{
				this.notifyHolder( new InventoryEvent( InventoryEvent.DETATCH, this, slot));
			}
			actions[slot] = action;
			if ( action.bmp != null )
			{
				attachBitmap( action.bmp );
				action.bmp.x = action.bmpPlacement.x;
				action.bmp.y = action.bmpPlacement.y;
			}
			this.notifyHolder( new InventoryEvent( InventoryEvent.ATTACH, this, slot ));
		}
		
		public function detatchAction( slot:int ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( ( slot > 0 ) && ( slot < actions.length ) )
			{
				if (actions[slot] != null)
				{
					if ( actions[slot].bmp != null )
					{
						detatchBitmap( actions[slot].bmp );
					}
					this.notifyHolder( new InventoryEvent( InventoryEvent.DETATCH,  this, slot ) );
					actions[slot] = null;
				}
			}
		}
		
		public function attachEffect( e:IEffect ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( effects.push( e ) )
			{
				e.setup( this );
			}
		}
		
		public function detatchEffect( e:IEffect ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( effects.remove( e ) )
			{
				e.teardown();
			}
		}
		
		public override function get boundingVolume():Rectangle
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( overrideVol != null )
			{
				overrideVol.x = this.x - ( overrideVol.width / 2.0 );
				overrideVol.y = this.y - ( overrideVol.height / 2.0 );
				return overrideVol;
			}
			return super.boundingVolume;
		}
		
		public override function destroy():void
		{
			var i:int = 0;
			var len:int = 0;
			
			if ( !isConstructed )
			{
				return;
			}
			
			effects.destroy();
			effects = null;
			
			len = actions.length;
			for ( i = 0; i < len; i++ )
			{
				var actn:Action = actions[ i ];
				
				if ( actn != null )
				{
					actn.destroy();
				}
			}
			actions.length = 0;
			actions = null;
			
			len = badgeSlots.length;
			for ( i = 0; i < len; i++ )
			{
				var badge:ItemProperties = badgeSlots[ i ];
				
				if ( badge != null )
				{
					badge.destroy();
				}
				badgeSlots[ i ] = null;
			}
			badgeSlots.length = 0;
			badgeSlots = null;
			
			damageFunctionOverride = null;
			
			stats.destroy();
			stats = null;
			
			cardinalDirection = null;
			inventoryOverlay = null;
			overrideVol = null;
			
			_inventory.destroy();
			_inventory = null;
			
			super.destroy();
		}
	}
}