package org.xangarath.gemini
{	
	import com.sociodox.theminer.TheMiner;
	import flash.desktop.NativeApplication;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.system.Capabilities;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.StatLogger;
	import org.xangarath.engine.DebugConsole;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.script.ScriptManager;
	import org.xangarath.engine.tiled.TiledTilesetManager;
	import org.xangarath.gemini.controllers.PlayerController;
	import org.xangarath.gemini.dbg.CheatManager;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.factories.ActorFactory;
	import org.xangarath.gemini.factories.ControllerFactory;
	import org.xangarath.gemini.ui.HUD;
	import org.xangarath.gemini.ui.InventoryMenu;
	import org.xangarath.gemini.ui.PauseScreen;
	
	
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class Main extends Sprite 
	{
		private static const TAG:String = "Main";
		private static const MILLIS_TO_SECONDS:Number = 1 / 1000;
		private var scenario:Scenario;
		private var cheats:CheatManager;
		private var data:GeminiData;
		private var timeOfLastFrame:Number = -1;
		private var filesToOpen:int;
		private var timeOfFirstFrame:Number = -1;
		private var numberOfFrames:uint = 0;
		private var cons:DebugConsole;
		private var pauseScrn:PauseScreen;
		private var inventoryScrn:InventoryMenu;
		private var hud:HUD;
		private var version:String;
		
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{	
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			CONFIG::debug
			{
				addChild( new TheMiner() );
			}
			
			DebugLog.getSingleton().setLogfile( "logfile.txt" );
			DebugLog.getSingleton().init();
			stage.nativeWindow.addEventListener( Event.CLOSING, exitFunc );
																
			DebugLog.getSingleton().setLoglevel( DebugLog.INFO );
			DebugLog.getSingleton().i( "Main", "/***********************************************/" );
			DebugLog.getSingleton().i( "Main", "/*********** let's light this candle ***********/" );
			DebugLog.getSingleton().i( "Main", "/***********************************************/" );
			
			var xml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = xml.namespace();
			version = xml.ns::versionNumber;
			
			DebugLog.getSingleton().i( TAG, "Gemini version " + version );
			DebugLog.getSingleton().i( TAG, "Flash Player version " + Capabilities.version );
			
			var seed:int;
			if(USER::PACKAGE_BUILD)
			{
				seed = int(new Date().getTime());
			}
			else
			{
				seed = int(new Date().getTime());
			}
			
			Maths.seed_random( seed );
			DebugLog.getSingleton().i( TAG, "Random seed: " + seed );
			
			data = new GeminiData(stage);
			buildResources();
			
			var uiLayer:Sprite = new Sprite();
			uiLayer.name = "uiLayer";
			stage.addChild(uiLayer);
			
			data.animMgr.loadAnimations();
			
			filesToOpen = 1;
			
			hud = new HUD(data);
			uiLayer.addChild(hud);
			
			setupScript();
			
			TiledTilesetManager.getSingleton().init( "data/tilesets/", data.resMgr );
			
			scenario = new Scenario( data );
			scenario.addEventListener( Event.COMPLETE, registerFileOpen );
			scenario.setup();
		}
		
		private function exitFunc( e:Event ):void
		{
			USER::NOT_FINAL_RELEASE
			{
				printStats();
			}
			DebugLog.getSingleton().close(); 
		}
		
		private function registerFileOpen( e:Event ):void
		{
			if ( --filesToOpen <= 0 )
			{
				finishInt();
			}
		}
		
		private function setupScript():void
		{
			USER::ENABLE_SCRIPTING
			{
				var geminiEnvironment:Object = new Object();
				geminiEnvironment.actFact = new ActorFactory( data );
				geminiEnvironment.ctlrFact = new ControllerFactory( data );
				geminiEnvironment.data = data;
				data.scriptMgr.addLuaGlobal("GEMINI_ENVIRONMENT", geminiEnvironment);
				data.scriptMgr.doFile(ScriptManager.VFS_SCRIPT_ROOT + "gemini_init.lua");
			}
		}
		
		private function finishInt():void
		{
			scenario.removeEventListener( Event.COMPLETE, finishInt );
			
			USER::NOT_FINAL_RELEASE
			{
				cons = new DebugConsole(data);
				(stage.getChildByName("uiLayer") as Sprite).addChild(cons);
				cons.init();
			}
			
			USER::FINAL_RELEASE
			{
				cons = null;
			}
			
			var ctlr:PlayerController = null;
			for ( var i:int = 0; i < data.tickList.length; i++)
			{
				if ( data.tickList[i] is PlayerController )
				{
					ctlr = data.tickList[i];
				}
			}
			
			USER::NOT_FINAL_RELEASE
			{
				cheats = new CheatManager( cons, data, ctlr );
			}
			
			USER::FINAL_RELEASE
			{
				cheats = null;
			}
			
			pauseScrn = new PauseScreen( data );
			(stage.getChildByName("uiLayer") as Sprite).addChild(pauseScrn);
			pauseScrn.init();
			
			inventoryScrn = new InventoryMenu( data );
			
			(stage.getChildByName("uiLayer") as Sprite).addChild(inventoryScrn);
			inventoryScrn.init();
			
			setupScript();
			scenario.startGame();
			
			addEventListener( Event.ENTER_FRAME, tick );
			stage.addEventListener(KeyboardEvent.KEY_UP, keycatch);
		}
		
		private function printStats():void
		{
			var timeSec:Number;
			var timeMin:Number;
			var timeHrs:Number;
			var timeDiff:Number = timeOfLastFrame - timeOfFirstFrame;
			
			timeHrs = Math.floor( timeDiff / ( 60.0 * 60.0 ) );
			timeDiff = timeDiff - ( timeHrs * 60.0 * 60.0 );
			timeMin = Math.floor( timeDiff / 60.0 );
			timeSec = timeDiff - ( timeMin * 60.0 );
			
			DebugLog.getSingleton().i( TAG, "Playtime: " + timeHrs + "h " + timeMin + "m " + timeSec + "s" );
			DebugLog.getSingleton().i( TAG, "Average FPS: " + ( numberOfFrames / ( timeOfLastFrame - timeOfFirstFrame ) ) );
			StatLogger.getSingleton().printStats();
		}
		
		private var dbgDrawTree:Shape;
		private var doOnce:Boolean = true;
		private function tick(e:Event = null):void
		{
			var timeSinceLastFrameSeconds:Number;
			
			try
			{
				if (data.requestExit)
				{
					exitFunc(null);
					NativeApplication.nativeApplication.exit(0);
					return;
				}
				data.sceneMgr.debugLayer.graphics.clear();
				numberOfFrames++;
				var timeNow:Number = getTimer() * MILLIS_TO_SECONDS;
				if ( timeOfFirstFrame == -1 )
				{
					timeOfFirstFrame = timeNow;
				}
				if ( timeOfLastFrame == -1 )
				{
					timeSinceLastFrameSeconds = 0.;
				}
				else 
				{
					timeSinceLastFrameSeconds = timeNow - timeOfLastFrame;
				}
				timeOfLastFrame = timeNow;
				
				if ( !data.gameIsPaused )
				{
					// want to make sure we don't do setup/teardown of a level mid-ticklist update, just to be clean
					scenario.tick( timeSinceLastFrameSeconds );
				}
				
				for ( var i:int = 0; i < data.tickList.length; i++ )
				{
					if ( !data.gameIsPaused )
					{
						( data.tickList[i] as ITickable ).tick( timeSinceLastFrameSeconds );
					}
				}
				data.sceneMgr.applyCollisions( data );
				data.sceneMgr.trackActor( data.playerAtBat );
				
				hud.tick( timeSinceLastFrameSeconds );
				
				if ( cheats != null )
				{
					cheats.tick( timeSinceLastFrameSeconds );
				}
				
				// TODO: Am I using this?
				for each ( var destructable:IDestructable in data.trashcan )
				{
					if ( destructable != null )
					{
						destructable.destroy();
					}
				}
				data.trashcan.length = 0;
			}
			catch (err:Error)
			{
				DebugLog.getSingleton().e( TAG, "!!! LAST CHANCE ERROR: " + err.message + "\n" + err.getStackTrace() );
				throw err;
			}
			
			if ( doOnce == true )
			{
				// TODO: FIX ME This should be event-driven for when the window opens
				if ( NativeApplication.nativeApplication.activeWindow != null )
				{
					NativeApplication.nativeApplication.activeWindow.title = NativeApplication.nativeApplication.activeWindow.title + " v" + version;
					doOnce = false;
				}
			}
		}
		
		private var isOpen:Boolean = true;
		private function debugBehavior():void
		{
			CONFIG::debug
			{
				
			}
		}
		
		public function keycatch(e:KeyboardEvent):void
		{
			e.preventDefault();
			switch(e.keyCode)
			{
				case Keyboard.ESCAPE:
					if (!cons.visible && !inventoryScrn.visible)
					{
						togglePauseMenu();
					}
					break;
				case Keyboard.TAB:
					if (!cons.visible && !pauseScrn.visible)
					{
						toggleInventory();
					}
					break;
				case Keyboard.EQUAL:
					CONFIG::debug
					{
						debugBehavior();
					}
					break;
			}
		}
		
		private function togglePauseMenu():void
		{
			var shouldShow:Boolean = !data.gameIsPaused;
			
			data.gameIsPaused = shouldShow;
			pauseScrn.toggle();
		}
		
		private function toggleInventory():void
		{
			var shouldShow:Boolean = !data.gameIsPaused;
			data.gameIsPaused = shouldShow;
			inventoryScrn.toggle();
			data.conner.dispatchEvent( new HealthEvent( HealthEvent.HEAL_TYPE, null, 0 ) );
		}
		
		private function buildResources():void
		{
			[Embed(source="../../../../assets/item-sheet.png")]
			var itemSheet:Class;
			
			var file:File = File.applicationDirectory.resolvePath("data/atlases/item-sheet.json");
			if ( file == null )
			{
				DebugLog.getSingleton().e( TAG, "unable to find item sheet JSON!" );
				throw new Error( TAG + "unable to find item sheet JSON!" );
			}
			var fs:FileStream = new FileStream();
			fs.open( file, FileMode.READ );
			var itemString:String = fs.readUTFBytes( fs.bytesAvailable );
			fs.close();
			
			data.resMgr.addBitmapsFromSpritesheet( new itemSheet(), itemString );
			
			[Embed(source="../../../../assets/status-sheet.png")]
			var statusSheet:Class;
			
			file = File.applicationDirectory.resolvePath("data/atlases/status-sheet.json");
			if ( file == null )
			{
				DebugLog.getSingleton().e( TAG, "unable to find status sheet JSON!" );
				throw new Error( TAG + "unable to find status sheet JSON!" );
			}
			fs = new FileStream();
			fs.open( file, FileMode.READ );
			var statusString:String = fs.readUTFBytes( fs.bytesAvailable );
			fs.close();
			
			data.resMgr.addBitmapsFromSpritesheet( new statusSheet(), statusString );
			
			[Embed(source="../../../../assets/badge-sheet.png")]
			var badgeSheet:Class;
			
			file = File.applicationDirectory.resolvePath("data/atlases/badge-sheet.json");
			if ( file == null )
			{
				DebugLog.getSingleton().e( TAG, "unable to find badge sheet JSON!" );
				throw new Error( TAG + "unable to find badge sheet JSON!" );
			}
			fs = new FileStream();
			fs.open( file, FileMode.READ );
			var badgeString:String = fs.readUTFBytes( fs.bytesAvailable );
			fs.close();
			
			data.resMgr.addBitmapsFromSpritesheet( new badgeSheet(), badgeString );
			
			
			[Embed(source="../../../../assets/ui-sheet.png")]
			var uiSheet:Class;
			
			file = File.applicationDirectory.resolvePath("data/atlases/ui-sheet.json");
			if ( file == null )
			{
				DebugLog.getSingleton().e( TAG, "unable to find UI sheet JSON!" );
				throw new Error( TAG + "unable to find UI sheet JSON!" );
			}
			fs = new FileStream();
			fs.open( file, FileMode.READ );
			var uiString:String = fs.readUTFBytes( fs.bytesAvailable );
			fs.close();
			
			data.resMgr.addBitmapsFromSpritesheet( new uiSheet(), uiString );
			
			[Embed(source="../../../../assets/annie.jpg")]
			var a:Class;
			data.resMgr.addBitmap("annie", new a());
			[Embed(source = "../../../../assets/blue.png")]
			var b:Class;
			data.resMgr.addBitmap("blue", new b());
			[Embed(source = "../../../../assets/circle.png")]
			var c:Class;
			data.resMgr.addBitmap("circle", new c());
			[Embed(source = "../../../../assets/conner.jpg")]
			var d:Class;
			data.resMgr.addBitmap("conner", new d());
			[Embed(source = "../../../../assets/scootaspider.jpg")]
			var e:Class;
			data.resMgr.addBitmap("scootaspider", new e());
			[Embed(source = "../../../../assets/sweep.png")]
			var f:Class;
			data.resMgr.addBitmap("sweep", new f());
			[Embed(source = "../../../../assets/sword2.png")]
			var g:Class;
			data.resMgr.addBitmap("sword", new g());
			[Embed(source = "../../../../assets/red.png")]
			var h:Class;
			data.resMgr.addBitmap("red", new h());
			[Embed(source = "../../../../assets/pelletSmall.png")]
			var i:Class;
			data.resMgr.addBitmap("pellet", new i());
			[Embed(source = "../../../../assets/wizard.png")]
			var j:Class;
			data.resMgr.addBitmap("wizard", new j() );
			[Embed(source = "../../../../assets/bull.png")]
			var k:Class;
			data.resMgr.addBitmap("bull", new k());
			[Embed(source = "../../../../assets/tentacleGuy.png")]
			var l:Class;
			data.resMgr.addBitmap("tentacle", new l());
			[Embed(source = "../../../../assets/ghost.png")]
			var m:Class;
			data.resMgr.addBitmap("ghost", new m());
			[Embed(source="../../../../assets/dog.png")]
			var n:Class;
			data.resMgr.addBitmap("dog", new n());
			[Embed(source = "../../../../assets/snowDoorVert.jpg")]
			var o:Class;
			data.resMgr.addBitmap( "snowDoorVert", new o() );
			[Embed(source = "../../../../assets/terminalDoorHoriz.jpg")]
			var p:Class;
			data.resMgr.addBitmap( "terminalDoorHoriz", new p() );
			[Embed(source = "../../../../assets/explosion.png")]
			var q:Class;
			data.resMgr.addBitmap( "explosion", new q() );
			[Embed(source = "../../../../assets/tri30ex.png")]
			var r:Class;
			data.resMgr.addBitmap( "triex", new r() );
			[Embed(source = "../../../../assets/explosionAnim.png")]
			var s:Class;
			data.resMgr.addBitmap( "explosionAnim.png", new s() );
			[Embed(source = "../../../../assets/shooter.png")]
			var t:Class;
			data.resMgr.addBitmap( "shooter.png", new t() );
			[Embed(source = "../../../../assets/anniePortrait.jpg")]
			var u:Class;
			data.resMgr.addBitmap( "anniePortrait.jpg", new u() );
			[Embed(source = "../../../../assets/70x70_placeholder.jpg")]
			var v:Class;
			data.resMgr.addBitmap( "70x70_placeholder.jpg", new v() );
			[Embed(source = "../../../../assets/testNPC.png")]
			var w:Class;
			data.resMgr.addBitmap( "testNPC.png", new w() );
			[Embed(source = "../../../../assets/postmodern.png")]
			var x:Class;
			data.resMgr.addBitmap( "postmodern.png", new x() );
			[Embed(source = "../../../../assets/TerminalMap.jpg")]
			var y:Class;
			data.resMgr.addBitmap( "TerminalMap.jpg", new y() );
		}
	}
}