package org.xangarath.gemini.factories 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IObstacle;
	import org.xangarath.gemini.obstacles.LockedDoor;
	import org.xangarath.engine.tiled.TiledObject;
	/**
	 * Constructs IObstacles from TiledObjects
	 * @author Jeffrey Cochran
	 */
	public class ObstacleFactory 
	{
		private static const TAG:String = "ObstacleFactory";
		private var data:GeminiData;
		public function ObstacleFactory( data:GeminiData ) 
		{
			this.data = data;
		}
		
		/**
		 * @deprecated in favor of getObstacleFromJSON
		 * Returns a new Obstacle based on a TiledObject, or null if the type couldn't be resolved
		 * @param	obj The TiledObject to parse
		 * @return A newly created Obstacle or null if the object type was unknown
		 */
		public function getObstacleFromTiled( obj:TiledObject, scriptPath:String  ):IObstacle
		{
			var obs:IObstacle = null;
			if ( obj.type == "lockedDoorHoriz" )
			{
				obs = LockedDoor.getLockedDoorFromTiled( obj, data, scriptPath);
			}
			else
			{
				trace( TAG + " warning: found unknown obstacle type: " + obj.type );
			}
			
			return obs;
		}
		
		USER::NEVER {
		/**
		 * Returns a new Obstacle based on a JSON object, or null if the type couldn't be resolved
		 * @param	obj The JSON object to parse
		 * @return A newly created Obstacle or null if the object type was unknown
		 */
		public function getObstacleFromJson( obj:Object, scriptPath:String ):IObstacle
		{
			var obs:IObstacle = null;
			var type:String = Argonaut.getAndCheck( TAG, obj, "type" );
			if ( type == "door" )
			{
				/// obs = LockedDoor.getLockedDoorFromTiled( obj, data, scriptPath);
				DebugLog.getSingleton().e(TAG, "Get obstacle from JSON is no longer supported");
			}
			else
			{
				trace( TAG + " warning: found unknown obstacle type: " + type );
			}
			return obs;
		}
		}
	}

}