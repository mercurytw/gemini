package org.xangarath.gemini.factories 
{
	import flash.display.Bitmap;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.anim.AnimatedSprite;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.tiled.TiledObject;
	import org.xangarath.gemini.actors.*;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.obstacles.*;
	
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class ActorFactory 
	{
		private static const TAG:String = "ActorFactory";
		private var data:GeminiData;
		public function ActorFactory( data:GeminiData ) 
		{
			this.data = data;
		}
		
		public function getGenericActor( type:String, name:String="", scriptPath:String = null ):Actor
		{
			var sprite:AnimatedSprite = null;
			var bmp:Bitmap = null;
			var actor:Actor;
			if ( type == "scootaspider" )
			{
				bmp = data.resMgr.getBitmap("scootaspider");
				actor = new Creature( bmp, Actor.ENEMY_TEAM, 0, (bmp.width / 2.0), -(bmp.height / 2.0) );
				actor.setName( "scootaspider#" + actor.id );
			} else if ( type == "wizard" )
			{
				sprite = data.animMgr.getSprite( "shooter" );
				actor = new Creature( sprite, Actor.ENEMY_TEAM, 1, (sprite.width / 2.0), - (sprite.height / 2.0) );
				actor.setName( "wizard#" + actor.id );
			} else if ( type == "bull" )
			{
				bmp = data.resMgr.getBitmap( "bull" );
				actor = new Creature( bmp, Actor.ENEMY_TEAM, 0, (bmp.width / 2.0), -(bmp.height / 2.0), true );
				actor.setName( "rocketloo#" + actor.id );
			} else if ( type == "tentacle" )
			{
				bmp = data.resMgr.getBitmap( "tentacle" );
				actor = new Creature( bmp, Actor.ENEMY_TEAM, 0, (bmp.width / 2.0), -(bmp.height / 2.0) );
				actor.setName( "tentacle#" + actor.id );
			} else if ( type == "haunter" )
			{
				bmp = data.resMgr.getBitmap( "ghost" );
				actor = new Creature( bmp, Actor.ENEMY_TEAM, 1, (bmp.width / 2.0), -(bmp.height / 2.0) );
				actor.setName( "haunter#" + actor.id );
			} else if ( type == "dog" )
			{
				bmp = data.resMgr.getBitmap( "dog" );
				actor = new Creature( bmp, Actor.ENEMY_TEAM, 1, (bmp.width / 2.0), -(bmp.height / 2.0) );
				actor.setName( "dog#" + actor.id );
			} else if ( type == "lockedDoorHoriz" )
			{
				bmp = data.resMgr.getBitmap( "terminalDoorHoriz" );
				actor = new LockedDoor( data, bmp, (bmp.width / 2.0), -(bmp.height / 2.0) );
				actor.setName( "lockedDoor#" + actor.id );
			} else if ( type == "npc" )
			{
				DebugLog.getSingleton().w( TAG, "NPCs not supported as generics yet" );
				return null;
			}
			else
			{
				DebugLog.getSingleton().w( TAG, "Requested unknown creature type: " + type );
				return null;
			}
			if (name != "")
			{
				actor.setName( name );
			}
			return actor;
		}
		
		public function getActorFromTiled(tiledObj:TiledObject, scriptPath:String = null):Actor
		{
			var bmp:Bitmap = null;
			var sprite:AnimatedSprite = null;
			var actor:Actor;
			if ( tiledObj.type == "scootaspider" )
			{
				bmp = data.resMgr.getBitmap("scootaspider");
				actor = new Creature( bmp, Actor.ENEMY_TEAM, 0, tiledObj.x + (bmp.width / 2.0), tiledObj.y  - (bmp.height / 2.0) );
				actor.setName( "scootaspider#" + actor.id );
			} else if ( tiledObj.type == "wizard" )
			{
				sprite = data.animMgr.getSprite( "shooter" );
				actor = new Creature( sprite, Actor.ENEMY_TEAM, 1, tiledObj.x + (sprite.width / 2.0), tiledObj.y  - (sprite.height / 2.0) );
				actor.setName( "wizard#" + actor.id );
			} else if ( tiledObj.type == "bull" )
			{
				bmp = data.resMgr.getBitmap( "bull" );
				actor = new Creature( bmp, Actor.ENEMY_TEAM, 0, tiledObj.x + (bmp.width / 2.0), tiledObj.y  - (bmp.height / 2.0), true );
				actor.setName( "rocketloo#" + actor.id );
			} else if ( tiledObj.type == "tentacle" )
			{
				bmp = data.resMgr.getBitmap( "tentacle" );
				actor = new Creature( bmp, Actor.ENEMY_TEAM, 0, tiledObj.x + (bmp.width / 2.0), tiledObj.y  - (bmp.height / 2.0) );
				actor.setName( "tentacle#" + actor.id );
			} else if ( tiledObj.type == "haunter" )
			{
				bmp = data.resMgr.getBitmap( "ghost" );
				actor = new Creature( bmp, Actor.ENEMY_TEAM, 1, tiledObj.x + (bmp.width / 2.0), tiledObj.y  - (bmp.height / 2.0) );
				actor.setName( "haunter#" + actor.id );
			} else if ( tiledObj.type == "dog" )
			{
				bmp = data.resMgr.getBitmap( "dog" );
				actor = new Creature( bmp, Actor.ENEMY_TEAM, 1, tiledObj.x + (bmp.width / 2.0), tiledObj.y  - (bmp.height / 2.0) );
				actor.setName( "dog#" + actor.id );
			} else if ( tiledObj.type == "npc" )
			{
				var archetype:String = Argonaut.getAndCheck( TAG, tiledObj.properties, "sprite" );
				sprite = data.animMgr.getSprite( archetype );
				actor = new Creature( sprite, Actor.NO_TEAM, 0, tiledObj.x + (sprite.width / 2.0), tiledObj.y  - (sprite.height / 2.0) );
				actor.setName( archetype + "#" + actor.id );
			} else if ( tiledObj.type == "lockedDoorHoriz" )
			{
				actor = LockedDoor.getLockedDoorFromTiled( tiledObj, data, scriptPath );
				actor.setName( "lockedDoor#" + actor.id );
			}
			else
			{
				DebugLog.getSingleton().w( TAG, "Encountered unknown creature type: " + tiledObj.type );
				return null;
			}
			if (tiledObj.name != "")
			{
				actor.setName( tiledObj.name );
			}
			return actor;
		}
	}

}