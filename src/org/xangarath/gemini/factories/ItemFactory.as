package org.xangarath.gemini.factories 
{
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import flash.geom.Point;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.items.Item;
	import org.xangarath.gemini.items.ItemProperties;
	import org.xangarath.engine.tiled.TiledObject;
	/**
	 * Builds items by name.
	 * @author Jeffrey Cochran
	 */
	public class ItemFactory 
	{
		private static const TAG:String = "ItemFactory";
		private var data:GeminiData;
		public function ItemFactory( data:GeminiData ) 
		{
			this.data = data;
		}
		
		/**
		 * Returns a new Item based on a TiledObject, or null if the type couldn't be resolved
		 * @param	obj The TiledObject to parse
		 * @return A newly created Item or null if the object type was unknown
		 */
		public function getItemFromTiled( obj:TiledObject ):Item
		{
			var item:Item;
			var props:ItemProperties;
			var img:Bitmap;
			
			var archetype:ItemProperties = data.masterItemList.lookupItem( obj.type );
			if ( archetype == null )
			{
				DebugLog.getSingleton().w( TAG, "found unknown item type: " + obj.type );
				return null;
			}
			
			props = new ItemProperties( "", "", 0, "", "", null );
			props.clone( archetype );
			img = data.resMgr.getBitmap( props.iconKey );
			
			item = new Item( props, img, obj.x + ( img.width / 2.0 ), obj.y - ( img.height / 2.0 ) );
			if (obj.name != "")
			{
				(item as Actor).setName(obj.name);
			}
			return item;
		}
		
		/**
		 * Returns a new Item based on a JSON Object, or null if the type couldn't be resolved
		 * @param	obj The Json Object to parse
		 * @return A newly created Item or null if the object type was unknown
		 */
		public function getItemFromJson( obj:Object ):Item
		{
			var item:Item;
			var props:ItemProperties;
			var img:Bitmap;
			var name:String;
			var pos:Point = Argonaut.getAndCheckPoint( TAG, obj, "position" );
			var type:String = Argonaut.getAndCheck( TAG, obj, "type" );
			if ( type == "small_key" )
			{
				props = new ItemProperties( type, type, ItemProperties.KEY | ItemProperties.QUEST, "key", "", null );
				img = data.resMgr.getBitmap( "key" );
			}
			else
			{
				trace( TAG + " warning: found unknown item type: " + type );
				return null;
			}
			item = new Item( props, img, pos.x + ( img.width / 2.0 ), pos.y - ( img.height / 2.0 ) );
			return item;
		}
	}

}