package org.xangarath.gemini.factories 
{
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IEffect;
	
	import org.xangarath.gemini.effects.*;
	
	/**
	 * Shit for making other shit
	 * @author ...
	 */
	public class EffectFactory 
	{
		private static const TAG:String = "EffectFactory";
		
		private var data:GeminiData;
		public function EffectFactory( data:GeminiData ) 
		{
			this.data = data;
		}
		
		public function buildEffectFromJSON( jsobj:Object ):IEffect
		{
			var result:IEffect = null;
			var timeClass:String = Argonaut.getAndCheck( TAG, jsobj, "time-class" );
			var type:String;
			var amt:Number;
			var dur:Number;
			
			switch (timeClass.toLowerCase())
			{
				case ImmediateEffect.TIME_CLASS:
					type = Argonaut.getAndCheck( TAG, jsobj, "type" );
					amt = ( jsobj[ "amount" ] == null ) ? 0.0 : Argonaut.getAndCheckN( TAG, jsobj, "amount" );
					result = new ImmediateEffect( ImmediateEffect.getType( type ), amt, data );
					break;
				case GradualEffect.TIME_CLASS:
					type = Argonaut.getAndCheck( TAG, jsobj, "type" );
					amt = Argonaut.getAndCheckN( TAG, jsobj, "amount" );
					dur = Argonaut.getAndCheckN( TAG, jsobj, "duration" );
					result = new GradualEffect( data, GradualEffect.getType( type ), amt, dur );
					break;
				case TimedEffect.TIME_CLASS:
					type = Argonaut.getAndCheck( TAG, jsobj, "type" );
					amt = Argonaut.getAndCheckN( TAG, jsobj, "amount" );
					dur = Argonaut.getAndCheckN( TAG, jsobj, "duration" );
					result = new TimedEffect( data, TimedEffect.getType( type ), amt, dur );
					break;
				case SemipermanentEffect.TIME_CLASS:
					type = Argonaut.getAndCheck( TAG, jsobj, "type" );
					amt = ( jsobj[ "amount" ] == null ) ? 0.0 : Argonaut.getAndCheckN( TAG, jsobj, "amount" );
					result = new SemipermanentEffect( SemipermanentEffect.getType( type ), amt );
					break;
				default:
					DebugLog.getSingleton().e( TAG, "Encountered unknown effect class: " + timeClass );
					return null;
			}
			
			DebugLog.getSingleton().d( TAG, "Got effect: " + result.toString() );
			return result;
		}
		
	}

}