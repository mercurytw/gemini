package org.xangarath.gemini.factories 
{
	import flash.utils.Dictionary;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.controllers.*;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.engine.tiled.TiledObject;
	/**
	 * Factory which builds monsters from TiledObject descriptions
	 * @author Jeffrey Cochran
	 */
	public class ControllerFactory 
	{
		private static const TAG:String = "ControllerFactory";
		private var data:GeminiData;
		private static var beastiary:Dictionary = new Dictionary();
		private static var npcLogic:Dictionary = new Dictionary();
		public function ControllerFactory( data:GeminiData ) 
		{
			this.data = data;
		}
		
		/**
		 * Should be called first to setup the beastiary
		 * @param	beasts
		 */
		public static function initControllerProperties( beasts:Object ):void
		{
			var i:int;
			for ( i = 0; beasts[ i ] != null; i++ )
			{
				beastiary[ beasts[ i ]["type"] ] = beasts[ i ];
			}
		}
		
		public function getController( actor:Creature, type:String, additionalProps:Object = null ):ITickable
		{
			var ctlr:ITickable;
			var props:Object;
			if ( type == "scootaspider" )
			{
				props = beastiary[ "scootaspider" ];
				ctlr = new MonsterController( actor, 
											  Argonaut.getAndCheckN( TAG, props, "health" ),
											  Argonaut.getAndCheckB( TAG, props, "armored" ),
											  data );
			} else if (type == "wizard" )
			{
				props = beastiary[ "wizard" ];
				ctlr = new WizardController( actor,
											 Argonaut.getAndCheckN( TAG, props, "health" ),
											 Argonaut.getAndCheckB( TAG, props, "armored" ),
											 data );
			} else if (type == "bull" )
			{
				props = beastiary[ "bull" ];
				ctlr = new BullController( Argonaut.getAndCheckN( TAG, props, "speed" ),
										   actor,
										   Argonaut.getAndCheckN( TAG, props, "health" ),
										   Argonaut.getAndCheckB( TAG, props, "armored" ),
										   data );
			} else if ( type == "tentacle" )
			{
				props = beastiary[ "tentacle" ];
				ctlr = new TentacleMonsterController( Argonaut.getAndCheckN( TAG, props, "damage" ),
													  Argonaut.getAndCheckN( TAG, props, "speed" ),
													  actor,
													  Argonaut.getAndCheckN( TAG, props, "health" ),
													  Argonaut.getAndCheckB( TAG, props, "armored" ),
										              data );
			}  else if ( type == "dog" )
			{
				props = beastiary[ "dog" ];
				ctlr = new TentacleMonsterController( Argonaut.getAndCheckN( TAG, props, "damage" ),
													  Argonaut.getAndCheckN( TAG, props, "speed" ),
													  actor,
													  Argonaut.getAndCheckN( TAG, props, "health" ),
													  Argonaut.getAndCheckB( TAG, props, "armored" ),
													  data,
													  false );
			} else if ( type == "haunter" )
			{
				props = beastiary[ "haunter" ];
				ctlr = new HaunterController( Argonaut.getAndCheckN( TAG, props, "damage" ),
											  Argonaut.getAndCheckN( TAG, props, "graceTime" ),
											  Argonaut.getAndCheckN( TAG, props, "minLurkTime" ),
											  Argonaut.getAndCheckN( TAG, props, "maxLurkTime" ),
											  actor,
											  Argonaut.getAndCheckN( TAG, props, "health" ),
											  Argonaut.getAndCheckB( TAG, props, "armored" ),
										      data );
			} else if ( type == "npc" )
			{
				ctlr = new NPCController( data );
				(ctlr as NPCController).possess( actor, additionalProps );
			}
			else 
			{
				DebugLog.getSingleton().w( TAG, "Encountered unknown creature type \"" + type + "\"" );
			}
			return ctlr;
		}
		
		public function getControllerFromTiled( actor:Creature, obj:TiledObject ):ITickable 
		{
			return getController( actor, obj.type, obj.properties );
		}
		
		[Deprecated(replacement="package org.xangarath.gemini.factories.ControllerFactory.getControllerFromTiled()", since="0.0.23")]
		public function getControllerFromJson( actor:Creature, obj:Object ):ITickable 
		{
			var type:String = Argonaut.getAndCheck( TAG, obj, "type" );
			return getController( actor, type );
		}
	}

}