package org.xangarath.gemini.events 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.gemini.dbg.CheatManager;
	
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class GameMilestoneBus extends EventDispatcher
	{
		private static const TAG:String = "GameMilestoneBus";
		public static const CONNER_GET_Q:String = "conner_get_q";
		public static const CONNER_GET_W:String = "conner_get_w";
		public static const CONNER_GET_E:String = "conner_get_e";
		public static const CONNER_GET_R:String = "conner_get_r";
		
		public static const ANNIE_GET_Q:String = "annie_get_q";
		public static const ANNIE_GET_W:String = "annie_get_w";
		public static const ANNIE_GET_E:String = "annie_get_e";
		public static const ANNIE_GET_R:String = "annie_get_r";
		
		private static var singleton:GameMilestoneBus = null;
		public function GameMilestoneBus() 
		{
			
		}
		
		public static function getSingleton():GameMilestoneBus
		{
			if (singleton == null)
			{
				singleton = new GameMilestoneBus();
			}
			return singleton;
		}
		
		public function trigger( event:String ):void
		{
			DebugLog.getSingleton().i( TAG, "Triggering milestone: " + event );
			dispatchEvent( new Event(event) );
		}
		
	}

}