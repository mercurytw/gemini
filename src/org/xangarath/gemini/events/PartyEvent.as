package org.xangarath.gemini.events 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class PartyEvent extends Event 
	{
		public static const PARTY_EVENT:String = "Gemini_PartyEvent";
		public var containedEvent:Event;
		public function PartyEvent( evt:Event ) 
		{
			super( PARTY_EVENT );
			containedEvent = evt;
		}
		
	}

}