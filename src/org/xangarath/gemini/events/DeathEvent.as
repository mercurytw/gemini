package org.xangarath.gemini.events 
{
	import flash.events.Event;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.gemini.actors.Creature;
	
	/**
	 * Event a critter throws when it dies
	 * @author Jeffrey Cochran
	 */
	public class DeathEvent extends Event
	{
		public static const DEATH_TYPE:String = "death";
		// maybe put team here?
		public var victim:Creature;
		public function DeathEvent(type:String, victim:Creature, bubbles:Boolean = false, cancelable:Boolean = false) 
		{ 
			super(type, bubbles, cancelable);
			this.victim = victim;
		} 
		
		public override function clone():Event 
		{ 
			return new DeathEvent(type, victim, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("DeathEvent", "type", "victim", "bubbles", "cancelable", "eventPhase"); 
		}
	}
	
}