package org.xangarath.gemini.events 
{
	import flash.events.Event;
	import org.xangarath.engine.Actor;
	
	/**
	 * Event an actor throws when damage has been dealt to it
	 * @author Jeff Cochran
	 */
	public class HealthEvent extends Event 
	{
		public static const DAMAGE_TYPE:String = "Damage";
		public static const HEAL_TYPE:String = "Heal";
		public var amount:Number;
		public var stunTime:Number;
		public var knockback:Number;
		public var instigator:Actor;
		public var armorPen:Number;
		public function HealthEvent(type:String, instigator:Actor, damage:Number, armorPen:Number = 1.0, knockback:Number=0.0, stun:Number=0.0, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			amount = damage;
			stunTime = stun;
			this.knockback = knockback;
			this.instigator = instigator;
			this.armorPen = armorPen;
		} 
		
		public override function clone():Event 
		{ 
			return new HealthEvent(type, instigator, amount, armorPen, knockback, stunTime, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("HealthEvent", "type", "instigator", "amount", "knockback", "stunTime", "armorPen", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}