package org.xangarath.gemini.events 
{
	import flash.events.Event;
	import org.xangarath.engine.Actor;
	import org.xangarath.gemini.actors.Creature;
	
	/**
	 * Represents an item being attached or detatched from a creature
	 * @author Jeffrey Cochran
	 */
	public class InventoryEvent extends Event 
	{
		public static const ATTACH:String = "attach";
		public static const DETATCH:String = "detatch";
		public var slot:int;
		public var recipient:Creature;
		//public var type:ItemType -- down the line
		public function InventoryEvent(type:String, recipient:Creature, slot:int = 0, bubbles:Boolean = false, cancelable:Boolean = false) 
		{ 
			super(type, bubbles, cancelable);
			this.slot = slot;
			this.recipient = recipient;
		} 
		
		public override function clone():Event 
		{ 
			return new InventoryEvent(type, recipient, slot, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("InventoryEvent", "type", "slot", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}