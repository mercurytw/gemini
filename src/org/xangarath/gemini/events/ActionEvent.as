package org.xangarath.gemini.events 
{
	import flash.events.Event;
	
	/**
	 * Informs interested parties about important parts of actions
	 * @author Jeffrey Cochran
	 */
	public class ActionEvent extends Event 
	{
		public static const ACTION_COMPLETE:String = "complete";
		public static const ACTION_BEGIN:String = "begin";
		public function ActionEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ActionEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ActionEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}