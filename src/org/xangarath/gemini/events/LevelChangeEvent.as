package org.xangarath.gemini.events 
{
	import flash.events.Event;
	import org.xangarath.engine.interfaces.IDestructable;
	
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class LevelChangeEvent extends Event implements IDestructable 
	{
		public static const LEVEL_CHANGE:String = "level_change";
		public var playerX:Number = NaN;
		public var playerY:Number = NaN;
		public var facingX:Number = NaN;
		public var facingY:Number = NaN;
		public var levelName:String;
		private var isConstructed:Boolean = true;
		public function LevelChangeEvent( levelName:String,
										  playerX:Number=NaN,
										  playerY:Number=NaN,
										  facingX:Number=NaN,
										  facingY:Number=NaN )
		{
			super(LEVEL_CHANGE, false, false);
			this.levelName = levelName;
			this.playerX = playerX;
			this.playerY = playerY;
			this.facingX = facingX;
			this.facingY = facingY;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void 
		{
			if ( !isConstructed )
			{
				return;
			}
			
			levelName = null;
			playerX = NaN;
			playerY = NaN;
			facingX = NaN;
			facingY = NaN;
			
			isConstructed = false;
		}
		
	}

}