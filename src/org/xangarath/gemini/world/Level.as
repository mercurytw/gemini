package org.xangarath.gemini.world 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.Hotspot;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.tiled.TiledObject;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.factories.ActorFactory;
	import org.xangarath.gemini.factories.ControllerFactory;
	import org.xangarath.gemini.factories.ItemFactory;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.gizmos.TeleportHotspot;
	import org.xangarath.gemini.items.Item;
	import org.xangarath.gemini.tiled.TiledJsonParser;
	
	/**
	 * A level contains all the information necessary to populate the world with
	 * one game level, and set up the player. This includes
	 * > Invisible walls
	 * > Player start position
	 * > Event trigger areas
	 * > Terrain information
	 * @author Jeffrey Cochran
	 */
	public class Level extends EventDispatcher implements IState
	{
		private static const TAG:String = "Level";
		public var walls:Array;
		public var map:Sprite;
		public var nameInternal:String;
		public var playerStart:Point;
		public var name:String;
		private var data:GeminiData;
		private var parser:TiledJsonParser;
		private var jsonString:String;
		private var ctlrFact:ControllerFactory;
		private var actorFact:ActorFactory;
		private var teardownList:Array = new Array();
		private var tickables:Array = new Array();
		private var isSetup:Boolean = false;
		public function Level( json:String, data:GeminiData, factory:ControllerFactory, actFactory:ActorFactory, internalName:String ) 
		{
			super();
			this.data = data;
			
			parser = new TiledJsonParser( data );
			parser.addEventListener( Event.COMPLETE, finishLevelBuild );
			jsonString = json;
			ctlrFact = factory;
			actorFact = actFactory;
			nameInternal = internalName;
		}
		
		public function build():void
		{
			parser.init( jsonString );
		}
		
		public function getScriptPath():String
		{
			return nameInternal + "/";
		}
		
		private function finishLevelBuild( e:Event ):void
		{
			parser.removeEventListener( Event.COMPLETE, finishLevelBuild );
			playerStart = parser.getPlayerStart();
			name = parser.getMapName();
			dispatchEvent( new Event( Event.COMPLETE ) );
		}
		
		private function buildCreatures():void
		{
			var monsters:Array = parser.getMonsters();
			
			for ( var i:int = 0; i < monsters.length; i++ )
			{
				var tiledObj:TiledObject = monsters[ i ] as TiledObject;
				var actor:Creature = actorFact.getActorFromTiled( tiledObj ) as Creature;
				if ( actor == null )
				{
					continue;
				}
				
				actor.spawn( data );
				var ctlr:ITickable = ctlrFact.getControllerFromTiled( actor, tiledObj );
				data.tickList.push( ctlr );
				ErrorUtil.dbgAssert( TAG, ctlr is IDestructable, "Forgot to implement a controller as destructable" );
				teardownList.push( ctlr );
				teardownList.push( actor );
			}
		}
		
		private function buildNPCs():void
		{
			var npcs:Array = parser.getNPCs();
			
			for ( var i:int = 0; i < npcs.length; i++ )
			{
				var tiledObj:TiledObject = npcs[ i ] as TiledObject;
				var actor:Creature = actorFact.getActorFromTiled( tiledObj ) as Creature;
				if ( actor == null )
				{
					continue;
				}
				
				actor.spawn( data );
				var ctlr:ITickable = ctlrFact.getControllerFromTiled( actor, tiledObj );
				data.tickList.push( ctlr );
				teardownList.push(ctlr);
				teardownList.push(actor);
			}
		}
		
		private function buildItems():void
		{
			var itemFactory:ItemFactory = new ItemFactory( data );
			var items:Array = parser.getItems();

			if ( items == null )
			{
				return;
			}
			
			var item:Item;
			var tiledObj:TiledObject;
			for ( var i:int = 0; i < items.length; i++ )
			{
				tiledObj = items[ i ] as TiledObject;
				item = itemFactory.getItemFromTiled( tiledObj );
				if ( item != null )
				{
					data.sceneMgr.gameLayer.addChild( item );
					data.sceneMgr.tree.insert( item.node );
				}
				
				teardownList.push( item );
			}
		}
		
		private function buildObstacles():void
		{
			var obstacles:Array = parser.getObstacles();

			if ( obstacles == null )
			{
				return;
			}
			
			var obs:Actor;
			var tiledObj:TiledObject;
			for ( var i:int = 0; i < obstacles.length; i++ )
			{
				tiledObj = obstacles[ i ] as TiledObject;
				obs = actorFact.getActorFromTiled( tiledObj, getScriptPath() );
				if ( obs != null )
				{
					obs.spawn( data );
					teardownList.push( obs );
				}
			}
		}
		
		private function buildHotpsots():void
		{
			var hotspots:Array = parser.getHotspots();
			
			var spot:Hotspot;
			var tiledObj:TiledObject;
			for ( var i:int = 0; i < hotspots.length; i++ )
			{
				tiledObj = hotspots[ i ] as TiledObject;
				if ( tiledObj.type == "hotspot" )
				{
					spot = Hotspot.getHotspotFromTiled( tiledObj, data, nameInternal + "/" );
				}
				else 
				{
					spot = TeleportHotspot.getTeleportFromTiled( tiledObj, data );
				}
				spot.spawn();
			}
			
			teardownList.push( spot );
		}
		
		/**
		 * Call this function to register objects that have been dynamically created if they 
		 * need to be destroyed when the level is torn down
		 * @param	obj
		 */
		public function registerDynamicDestructable( obj:IDestructable ):void
		{
			if ( !isSetup )
			{
				return;
			}
			
			if ( teardownList.indexOf( obj ) == -1 )
			{
				teardownList.push( obj );
			}
			else
			{
				DebugLog.getSingleton().d( TAG, "Duplicate add of object " + obj );
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			if ( isSetup )
			{
				return;
			}
			
			map = parser.buildMapLayer();
			
			data.sceneMgr.backgroundLayer.addChild( map );
			data.sceneMgr.setQuadtreeSize( map.width + 100, map.height + 100 ); // walls may extend outside map size in Tiled
			
			walls = parser.getWalls();
			
			for ( var i:int = 0; i < walls.length; i++ )
			{
				data.sceneMgr.gameLayer.addChild( (walls[i] as Actor) );
				data.sceneMgr.tree.insert( (walls[i] as Actor).node );
			}
			
			
			if ( data.annie != null )
			{
				data.annie.setPos( playerStart.x, playerStart.y );
				data.annie.spawn( data );
			}
			else
			{
				DebugLog.getSingleton().w( TAG, "Annie was NULL" );
			}
			
			if ( data.conner != null )
			{
				data.conner.setPos( playerStart.x, playerStart.y );
				data.conner.spawn( data );
			}
			else
			{
				DebugLog.getSingleton().w( TAG, "Conner was NULL" );
			}
			
			buildCreatures();
			
			buildNPCs();
			
			buildItems();
			
			buildObstacles();
			
			buildHotpsots();
			
			data.activeLevel = this;
			parser.initializeTerrainManager( data.terrainMgr );
			isSetup = true;
		}
		
		public function teardown():void 
		{
			if ( !isSetup )
			{
				return;
			}
			
			data.sceneMgr.backgroundLayer.removeChild( map );
			map = null;
			
			for ( var i:int = 0; i < walls.length; i++ )
			{
				data.sceneMgr.tree.remove( (walls[i] as Actor).node );
				data.sceneMgr.gameLayer.removeChild( (walls[i] as Actor) );
			}
			
			walls = null;
			
			for each ( var dstr:IDestructable in teardownList )
			{
				dstr.destroy();
			}
			teardownList.length = 0;
			
			if ( data.annie != null )
			{
				data.annie.despawn( data );
			}
			else
			{
				DebugLog.getSingleton().w( TAG, "Annie was NULL" );
			}
			
			if ( data.conner != null )
			{
				data.conner.despawn( data );
			}
			else
			{
				DebugLog.getSingleton().w( TAG, "Conner was NULL" );
			}

			
			data.activeLevel = null;
			
			isSetup = false;
		}
	}

}