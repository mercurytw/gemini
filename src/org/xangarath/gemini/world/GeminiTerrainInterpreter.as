package org.xangarath.gemini.world 
{
	import org.xangarath.engine.interfaces.ITerrainInterpreter;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.engine.dbg.DebugLog;
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class GeminiTerrainInterpreter implements ITerrainInterpreter 
	{
		private static const TAG:String = "GeminiTerrainInterpreter";
		public static const TERRAIN_TYPE_ACID:uint = (0x1 << (TerrainManager.USER_TERRAIN_SHIFT));
		public static const TERRAIN_TYPE_WALL:uint = (0x1 << (TerrainManager.USER_TERRAIN_SHIFT + 1));
		public static const TERRAIN_TYPE_WATER:uint = (0x1 << (TerrainManager.USER_TERRAIN_SHIFT + 2));
		public function GeminiTerrainInterpreter() 
		{
			
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITerrainInterpreter */
		
		public function getTerrainType( terrain:String ):uint 
		{
			switch ( terrain.toLowerCase() )
			{
				case "wall":
					return TERRAIN_TYPE_WALL;
				case "acid":
					return TERRAIN_TYPE_ACID;
				case "water":
					return TERRAIN_TYPE_WATER;
				default:
					DebugLog.getSingleton().e( TAG, "encountered unknown terrain enum: " + terrain );
					return TerrainManager.TERRAIN_TYPE_UNKNOWN;
			}
		}
		
	}

}