package org.xangarath.gemini.explosion 
{
	import com.junkbyte.console.addons.displaymap.DisplayMapAddon;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import org.xangarath.engine.anim.AnimatedSprite;
	import org.xangarath.engine.interfaces.IPoolable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * Represents a simple explosion that plays for the given amount of time
	 * @author Jeffrey Cochran
	 */
	public class Explosion extends Sprite implements ITickable, IPoolable 
	{
		public static const TAG:String = "Explosion";
		private var playTime:Number;
		private var playTimeRemaining:Number;
		private var data:GeminiData;
		private var _active:Boolean = false;
		private var drawable:AnimatedSprite;
		private var offt:Point;
		public function Explosion( animation:AnimatedSprite, offset:Point, data:GeminiData ) 
		{
			super();
			drawable = animation;
			this.addChild( drawable );
			if ( offset != null )
			{
				drawable.x += offset.x;
				drawable.y += offset.y;
			}
			offt = offset;
			this.visible = false;
			this.data = data;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IPoolable */
		
		public function clone():IPoolable 
		{
			var other:IPoolable = new Explosion( drawable.clone(), offt, data );
			return other;
		}
		
		public function isActive():Boolean 
		{
			return _active;
		}
		
		public function activate():void 
		{
			if ( !_active )
			{
				_active = true;
				this.visible = true;
				data.sceneMgr.fxLayer.addChild( this );
				data.tickList.push( this );
				drawable.play();
			}
		}
		
		public function deactivate():void 
		{
			if ( _active )
			{
				_active = false;
				this.visible = false;
				data.sceneMgr.fxLayer.removeChild( this );
				data.tickList.splice( data.tickList.indexOf( this ), 1 );
				this.scaleX = this.scaleY = 1.0;
				this.rotation = 0.0;
				drawable.stop();
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			drawable.tick( deltaTime );
			if ( drawable.isDonePlaying )
			{
				deactivate();
				return;
			}
		}
		
	}

}