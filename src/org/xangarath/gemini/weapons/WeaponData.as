package org.xangarath.gemini.weapons 
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.GeolocationEvent;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.SharedData;
	import org.xangarath.gemini.stats.StatPage;
	/**
	 * Data bout weapins
	 * @author Jeff Cochran
	 */
	public class WeaponData extends Object implements IDestructable
	{
		public static const TAG:String = "WeaponData";
		public static const UNDEF_TYPE:int = 0;
		public static const STAB_TYPE:int = 1;
		public static const SWEEP_TYPE:int = 2;
		public static const CIRCLE_TYPE:int = 3;
		public var damage:Number;
		public var stunTime:Number;
		public var knockback:Number;
		public var bmp:Bitmap;
		public var armorPenetration:Number;
		public var stats:StatPage = null;
		public var type:int;
		public var name:String;
		public var team:uint;
		private var isConstructed:Boolean = true;
		public function WeaponData(name:String, type:int, team:uint, dmg:Number, stunTimeSeconds:Number, knockback:Number, armorPenetration:Number, bmp:Bitmap = null) 
		{
			this.name = name;
			this.type = type;
			this.team = team;
			this.damage = dmg;
			this.stunTime = stunTimeSeconds;
			this.knockback = knockback;
			this.armorPenetration = Math.max(armorPenetration, 0.0);
			this.armorPenetration = Math.min(this.armorPenetration, 1.0);
			this.bmp = bmp;
		}
		
		private static function checkValid( jsonOb:Object ):Boolean
		{
			if (( jsonOb["name"] == null ) ||
			    ( jsonOb["type"] == null ) ||
				( jsonOb["damage"] == null ) ||
				( jsonOb["stun"] == null ) ||
				( jsonOb["knockback"] == null ) ||
				( jsonOb["armorPen"] == null ))
				{
					return false;
				}
			return true;
		}
		
		public static function fromJson(jsonOb:Object, sdata:SharedData, team:uint = 1):WeaponData
		{
			if ( !checkValid( jsonOb ) )
			{
				DebugLog.getSingleton().w( TAG, "fromJson encountered invalid json: " + jsonOb.toString() );
				return null;
			}
			var bmp:Bitmap = null; 
			var o:Object = jsonOb["bitmap"];
			if (o != null)
			{
				bmp = sdata.resMgr.getBitmap(o as String);
			}
			var data:WeaponData = new WeaponData(jsonOb["name"], 
												 WeaponData.getTypeFromString( jsonOb["type"] ),
												 team,
												 jsonOb["damage"],
												 jsonOb["stun"],
												 jsonOb["knockback"] * sdata.GRID_UNIT_SIZE,
												 jsonOb["armorPen"],
												 bmp);
			DebugLog.getSingleton().i( TAG, "Got weapon: " + data.toString() );
			return data;
		}
		
		public static function getTypeFromString( typeString:String ):int
		{
			if (typeString.toLowerCase() == "stab")
			{
				return STAB_TYPE;
			}
			else if (typeString.toLowerCase() == "sweep")
			{
				return SWEEP_TYPE;
			}
			else if (typeString.toLowerCase() == "circle")
			{
				return CIRCLE_TYPE;
			}
			DebugLog.getSingleton().w( TAG, "found invalid type: " + typeString );
			return UNDEF_TYPE;
		}
		
		private function formatToString(...args):String
		{
			var s:String = "";
			for ( var i:int = 0; i < args.length; i++ )
			{
				if ( i != 0 )
				{
					s += ", ";
				}
				
				s += (args[i] as String);
				
				if ( (args[i] as String) in this )
				{
					s += ':' + this[args[i] as String];
				}
			}
			return s;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			armorPenetration = Number.NEGATIVE_INFINITY;
			damage = Number.NEGATIVE_INFINITY;
			knockback = Number.NEGATIVE_INFINITY;
			bmp = null;
			name = null;
			stats.destroy();
			stats = null;
			stunTime = Number.NEGATIVE_INFINITY;
			type = UNDEF_TYPE;
			
			isConstructed = false;
		}
		
		public function toString():String 
		{ 
			if ( !isConstructed )
			{
				return "Destroyed";
			}
			return formatToString("name", "team", "type", "damage", "stunTime", "knockback", "armorPenetration"); 
		}
	}

}