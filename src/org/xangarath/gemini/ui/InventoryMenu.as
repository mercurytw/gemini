package org.xangarath.gemini.ui 
{
	import flash.display.Sprite;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import org.xangarath.engine.statemachine.StateMachine;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.ui.pack.PackBadgeScreen;
	import org.xangarath.gemini.ui.pack.PackItemScreen;
	import org.xangarath.gemini.ui.pack.PackMapScreen;
	import org.xangarath.gemini.ui.pack.PackStatsScreen;
	
	/**
	 * Controls setup and show of inventory screens
	 * @author ...
	 */
	public class InventoryMenu extends Sprite
	{
		private static const TAG:String = "InventoryMenu";
		
		private static const BG_COLOR:uint = 0xF4D873;
		private static const HIGHLIGHT_COLOR:uint = 0x00D200;
		
		private static const TAB_Y:Number = 0.1;
		private static const TAB_HEIGHT:Number = 0.05;
		private static const TAB_BORDER_PX:Number = 2.0;
		
		private static const PANEL_Y:Number = TAB_Y + (TAB_HEIGHT / 2.0);
		private static const PANEL_HEIGHT:Number = 0.8;
		private static const PANEL_WIDTH:Number = 0.9;
		private static const TAB_FONT_SIZE:int = 30;
		
		private var statScreen:PackStatsScreen;
		private var itemScreen:PackItemScreen;
		private var badgeScreen:PackBadgeScreen;
		private var mapScreen:PackMapScreen;
		
		private static const STATS_LABEL:String = "STATS";
		private static const ITEMS_LABEL:String = "ITEMS";
		private static const BADGES_LABEL:String = "BADGES";
		private static const MAP_LABEL:String = "MAP";
		
		
		private var isActive:Boolean = false;
		private var data:GeminiData;
		private var sm:StateMachine;
		private var panel:Sprite;
		private var tabTextFmt:TextFormat;
		private var activeTab:Sprite = null;
		private var isInitialized:Boolean = false;
		public function InventoryMenu( data:GeminiData ) 
		{
			super();
			sm = new StateMachine();
			this.data = data;
			this.visible = false;
			
			panel = new Sprite();
		}
		
		private static function getTabName( tab:Sprite ):String
		{
			return (tab.getChildByName("label") as TextField).text;
		}
		
		public function init():void
		{
			if (isInitialized)
			{
				return;
			}
			isInitialized = true;
			this.graphics.beginFill( 0x000000, 0.8 );
			this.graphics.drawRect( 0., 0., stage.stageWidth, stage.stageHeight );
			this.graphics.endFill();
			
			initText();
			
			panel.graphics.beginFill( BG_COLOR );
			panel.graphics.drawRect( 0., 0., stage.stageWidth * PANEL_WIDTH, stage.stageHeight * PANEL_HEIGHT );
			panel.graphics.endFill();
			panel.x = (stage.stageWidth * (1 - PANEL_WIDTH)) / 2.;
			panel.y = stage.stageHeight * PANEL_Y;
			this.addChild(panel);
			panel.visible = true;
			
			var tab:Sprite = createTab(STATS_LABEL);
			this.addChild(tab);
			
			statScreen = new PackStatsScreen( data );
			panel.addChild(statScreen);
			statScreen.init();	
			sm.addState(InventoryMenu.getTabName(tab), statScreen);
			setActiveTab(tab);
			
			tab = createTab(ITEMS_LABEL);
			this.addChild(tab);
			
			itemScreen = new PackItemScreen( data );
			panel.addChild( itemScreen );
			itemScreen.init();
			sm.addState(InventoryMenu.getTabName(tab), itemScreen);
			
			tab = createTab(BADGES_LABEL);
			this.addChild(tab);
			
			badgeScreen = new PackBadgeScreen( data );
			panel.addChild( badgeScreen );
			badgeScreen.init();
			sm.addState( InventoryMenu.getTabName(tab), badgeScreen );
			
			tab = createTab(MAP_LABEL);
			this.addChild(tab);
			
			mapScreen = new PackMapScreen( data );
			panel.addChild( mapScreen );
			mapScreen.init();
			sm.addState( InventoryMenu.getTabName(tab), mapScreen );
			isInitialized = true;
		}
		
		private function setActiveTab( tab:Sprite ):void
		{
			var newTab:Sprite;
			if (activeTab != null)
			{
				newTab = recreateTab( activeTab, false );
				this.addChild(newTab);
			}
			
			newTab = recreateTab( tab, true );
			this.addChild(newTab);
			activeTab = newTab;
			sm.enterState(InventoryMenu.getTabName(tab));
		}
		
		private function initText():void
		{
			tabTextFmt = new TextFormat();
			tabTextFmt.size = TAB_FONT_SIZE;
			tabTextFmt.color = 0x0;
			tabTextFmt.align = "center";
		}
		
		private var tabPos:Number = 0.10;
		private var tabWidth:Number = 0.17;
		private var TAB_SPACING:Number = 0.04;
		private var calcFirstTab:Boolean = true;
		private function createTab( txt:String ):Sprite
		{
			if (calcFirstTab)
			{
				calcFirstTab = false;
				tabPos *= stage.stageWidth;
				TAB_SPACING *= stage.stageWidth;
				tabWidth *= stage.stageWidth;
			}
			
			var tab:Sprite = new Sprite();
			tab.graphics.beginFill( 0x0 );
			tab.graphics.drawRect( 0, 0, tabWidth, TAB_HEIGHT * stage.stageWidth );
			tab.graphics.endFill();
			
			tab.graphics.beginFill( BG_COLOR );
			tab.graphics.drawRect( TAB_BORDER_PX, TAB_BORDER_PX, tabWidth - (2 * TAB_BORDER_PX), (TAB_HEIGHT * stage.stageWidth) - (2 * TAB_BORDER_PX) );
			tab.graphics.endFill();
			
			var field:TextField = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = tabTextFmt;
			field.text = txt;
			field.width = tabWidth - TAB_BORDER_PX;
			field.height = TAB_HEIGHT * stage.stageWidth;
			field.x = TAB_BORDER_PX;
			field.y = TAB_BORDER_PX;
			field.name = "label";
			tab.addChild(field);
			field.selectable = false;
			
			tab.x = tabPos;
			tab.y = TAB_Y * stage.stageHeight;
			
			tabPos += tabWidth + TAB_SPACING;
			
			tab.addEventListener(MouseEvent.CLICK, clickListener);
			tab.addEventListener(FocusEvent.FOCUS_IN, passFocus);
			return tab;
		}
		
		private function passFocus(e:FocusEvent):void
		{
			stage.assignFocus(stage, "none" );
		}
		
		private function recreateTab( oldTab:Sprite, isHighlighted:Boolean ):Sprite
		{
			var pt:Point = new Point( oldTab.x, oldTab.y );
			
			var tab:Sprite = new Sprite();
			if (isHighlighted)
			{
				tab.graphics.beginFill( HIGHLIGHT_COLOR );
			}
			else
			{
				tab.graphics.beginFill( 0x0 );
			}
			tab.graphics.drawRect( 0, 0, tabWidth, TAB_HEIGHT * stage.stageWidth );
			tab.graphics.endFill();
			
			tab.graphics.beginFill( BG_COLOR );
			tab.graphics.drawRect( TAB_BORDER_PX, TAB_BORDER_PX, tabWidth - (2 * TAB_BORDER_PX), (TAB_HEIGHT * stage.stageWidth) - (2 * TAB_BORDER_PX) );
			tab.graphics.endFill();
			
			var field:TextField = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = tabTextFmt;
			field.text = (oldTab.getChildByName("label") as TextField).text;
			field.width = tabWidth - TAB_BORDER_PX;
			field.height = TAB_HEIGHT * stage.stageWidth;
			field.x = TAB_BORDER_PX;
			field.y = TAB_BORDER_PX;
			field.name = "label";
			tab.addChild(field);
			field.selectable = false;
			
			tab.x = pt.x;
			tab.y = pt.y;
			
			this.removeChild(oldTab);
			oldTab.removeEventListener(MouseEvent.CLICK, clickListener);
			oldTab.removeEventListener(FocusEvent.FOCUS_IN, passFocus );
			tab.addEventListener(MouseEvent.CLICK, clickListener);
			tab.addEventListener(FocusEvent.FOCUS_IN, passFocus);
			return tab;
		}
		
		public function clickListener(event:MouseEvent):void
		{
			var target:Object = event.target;
			var targetTab:Object;
			if (!isActive)
				return;
				
			if (!(target is TextField))
			{
				return;
			}
			
			targetTab = (target as TextField).parent;
			
			if (targetTab == activeTab)
			{
				return;
			}
				
			if ((target as TextField).text == STATS_LABEL)
			{
				sm.enterState(STATS_LABEL);
				setActiveTab( targetTab as Sprite );
			}
			else if ((target as TextField).text == ITEMS_LABEL)
			{
				sm.enterState(ITEMS_LABEL);
				setActiveTab( targetTab as Sprite );
			}
			else if ((target as TextField).text == BADGES_LABEL)
			{
				sm.enterState(BADGES_LABEL);
				setActiveTab( targetTab as Sprite );
			}
			else if ((target as TextField).text == MAP_LABEL)
			{
				sm.enterState( MAP_LABEL );
				setActiveTab( targetTab as Sprite );
			}
		}
		
		public function toggle():void
		{
			if (isActive)
			{
				// remove
				sm.mActiveState.teardown();
			}
			else
			{
				// add event listeners
				sm.mActiveState.setup();
			}
			this.visible = isActive = !isActive;
		}
	}
}
