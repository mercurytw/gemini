package org.xangarath.gemini.ui 
{
	import flash.display.Sprite;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.GeminiData;
	import flash.events.Event;
	import ReadinessWidget;
	import ReadinessWidgetSmall;
	/**
	 * The player's hud
	 * @author Jeff Cochran
	 */
	public class HUD extends Sprite implements ITickable 
	{
		private static const TAG:String = "HUD";
		private var readyUILeft:ReadinessWidget;
		private var readyUIRight:ReadinessWidgetSmall;
	
		private var centerUI:CenterUIWidgets;
		private var data:GeminiData;
		public function HUD(data:GeminiData) 
		{
			super();
			this.name = "HUD";
			this.data = data;
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void
		{
			readyUILeft = new ReadinessWidget();
			addChild(readyUILeft);
			readyUILeft.x = 0;
			readyUILeft.y = 0;
			readyUILeft.name = "ReadinessUIAtBat";
			readyUILeft.visible = false;
			
			readyUIRight = new ReadinessWidgetSmall();
			addChild(readyUIRight);
			readyUIRight.x = stage.stageWidth;
			readyUIRight.y = 0;
			readyUIRight.name = "ReadinessUIOnDeck";
			readyUIRight.visible = false;
			
			centerUI = new CenterUIWidgets();
			addChild(centerUI);
			centerUI.x = stage.stageWidth / 2;
			centerUI.name = "CenterUIWidgets";
			centerUI.Jump.visible = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick(deltaTime:Number):void 
		{
			var i:int;
			var percent:Number;
			
			if (readyUILeft.visible == false)
			{
				readyUILeft.visible = true;
			}
			
			readyUIRight.visible = data.party.onDeck != null;
			
			if (data.jump != null)
			{
				centerUI.Jump.visible = true;
			}
			
			centerUI.Health.currHealth = (data.party.stats.currentHP > 19.0) ? Math.floor(data.party.stats.currentHP) : Math.ceil(data.party.stats.currentHP); 
			centerUI.Health.maxHealth = data.party.stats.maxHP;
			
			if ((data.playerAtBat.id == data.annie.id) && !(readyUILeft.isShowingAnnie))
			{
				readyUILeft.showAnnie();
				readyUIRight.showConner();
			}
			else if ((data.playerAtBat.id == data.conner.id) && (readyUILeft.isShowingAnnie))
			{
				readyUILeft.showConner();
				readyUIRight.showAnnie();
			}
			
			if (data.playerAtBat.id == data.annie.id)
			{
				for ( i = 0; i < 4; i++ )
				{
					percent = data.annie.actions[i].percentReady * 100.0;
					if (((readyUILeft.circleSetAnnie[i].visible = data.annie.actions[i].enabled) == true) &&
						( readyUILeft.circleSetAnnie[i].readiness != percent ))
					{
						readyUILeft.circleSetAnnie[i].setReadiness( percent );
					}
					
					percent = data.conner.actions[i].percentReady * 100.0;
					if (((readyUIRight.circleSetConner[i].visible = data.conner.actions[i].enabled) == true) && 
						( readyUIRight.circleSetConner[i].readiness != percent ))
					{
						readyUIRight.circleSetConner[i].setReadiness( percent );
					}
				}
			}
			else
			{
				for ( i = 0; i < 4; i++ )
				{
					percent = data.conner.actions[i].percentReady * 100.0;
					if (((readyUILeft.circleSetConner[i].visible = data.conner.actions[i].enabled) == true) && 
						( readyUILeft.circleSetConner[i].readiness != percent ))
					{
						readyUILeft.circleSetConner[i].setReadiness( percent );
					}
					
					percent = data.annie.actions[i].percentReady * 100.0;
					if (((readyUIRight.circleSetAnnie[i].visible = data.annie.actions[i].enabled) == true) &&
						( readyUIRight.circleSetAnnie[i].readiness != percent ))
					{
						readyUIRight.circleSetAnnie[i].setReadiness( percent );
					}
				}
			}
			
			if (data.jump != null)
			{
				if (!data.jump.enabled)
				{
					centerUI.Jump.readiness = 0.0;
				}
				else
				{
					centerUI.Jump.readiness = data.jump.percentReady;
				}
			}
		}
		
	}

}