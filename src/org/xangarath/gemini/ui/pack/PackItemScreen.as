package org.xangarath.gemini.ui.pack 
{
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.events.FocusEvent;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.StatLogger;
	
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.InventoryEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IEffect;
	import org.xangarath.gemini.items.InventoryEntry;
	import org.xangarath.gemini.items.Item;
	import org.xangarath.gemini.items.ItemProperties;
	
	/**
	 * The screen of the Inventory Menu responsible for showing consumable & quest items
	 * @author JCochran
	 */
	public class PackItemScreen extends Sprite implements IState 
	{
		private static const TAG:String = "PackItemScreen";
		
		private static const PANEL_COLOR:uint = 0xF7E9B7; 
		private static const BOTTOM_BUTTON_COLOR:uint = 0xA99756;
		private static const BUTTON_HIGHLIGHT_COLOR:uint = 0x25A6AB;
		
		private static var BLOCK_WIDTH:Number = 0.30;
		private static var LIST_WIDTH:Number;
		private static var MARGIN_TOP:Number = 0.10;
		private static var MARGIN_BOTTOM:Number = 0.02;
		private static var BLOCK_MARGIN:Number = 0.045;
		private static var V_SPACING:Number = 0.02;
		private static var H_SPACING:Number = 0.5;
		private static var BLOCK_HEIGHT:Number;
		private static var DESCR_HEIGHT:Number;
		private static var BUTTON_HEIGHT:Number = 0.1;
		private static var SIDE_BUTTON_WIDTH:Number;
		private static var BOTTOM_BUTTON_WIDTH:Number = 0.25;
		private static var FONT_HEIGHT:Number;
		private static var DESCR_FONT_HEIGHT:Number = 0.70;
		
		private var itemDescription:TextField;
		private var centeredTextFormat:TextFormat;
		private var whiteTextFormat:TextFormat;
		private var descrTextFormat:TextFormat;
		
		private var regItemsList:ItemList;
		private var questItemsList:ItemList;
		
		private var data:GeminiData;
		
		private var activeItemSet:Sprite = null;
		
		private var useOnConnerButton:Sprite;
		private static const USE_ON_CONNER_BUTTON_LABEL:String = "USE ON C";
		private var useOnAnnieButton:Sprite;
		private static const USE_ON_ANNIE_BUTTON_LABEL:String = "USE ON A";
		private var discardButton:Sprite;
		private static const DISCARD_BUTTON_LABEL:String = "DISCARD";
		
		
		private var gearButton:Sprite;
		private static const GEAR_BUTTON_LABEL:String = "GEAR";
		private var questButton:Sprite;
		private static const QUEST_BUTTON_LABEL:String = "QUEST";
		
		private var parentWidth:Number;
		
		public function PackItemScreen( data:GeminiData ) 
		{
			super();
			this.data = data;
			this.visible = false;
		}
		
		public function init():void
		{
			BLOCK_WIDTH *= this.parent.width;
			MARGIN_TOP *= this.parent.height;
			MARGIN_BOTTOM *= this.parent.height;
			BLOCK_MARGIN *= this.parent.width;
			BLOCK_HEIGHT = parent.height - (MARGIN_TOP + MARGIN_BOTTOM);
			LIST_WIDTH = ( parent.width - (BLOCK_MARGIN * 2.) ) * 0.8;
			V_SPACING *= this.parent.height;
			H_SPACING *= V_SPACING;
			BUTTON_HEIGHT *= BLOCK_HEIGHT; 
			FONT_HEIGHT = BUTTON_HEIGHT - 20.;
			DESCR_FONT_HEIGHT *= FONT_HEIGHT;
			DESCR_HEIGHT = (BLOCK_HEIGHT * 0.333) - ( (V_SPACING * 1.5) + BUTTON_HEIGHT );
			BOTTOM_BUTTON_WIDTH *= LIST_WIDTH;
			SIDE_BUTTON_WIDTH = (parent.width - ( (BLOCK_MARGIN * 2.) + H_SPACING ) ) - LIST_WIDTH;
			
			this.graphics.beginFill( 0x000000, 0.0 );
			this.graphics.drawRect( 0., 0., parent.width, parent.height );
			this.graphics.endFill();
			
			initText();
			initButtons();
			
			regItemsList = new ItemList( data, new Point( LIST_WIDTH, (BLOCK_HEIGHT * 0.667) - (V_SPACING / 2.) ), 5, 3, PANEL_COLOR );
			this.addChild(regItemsList);
			regItemsList.init();
			regItemsList.x = BLOCK_MARGIN;
			regItemsList.y = MARGIN_TOP;
			regItemsList.addEventListener( ItemList.ITEM_FOCUS_CHANGE_EVENT_TYPE, itemEventListener );
			
			questItemsList = new ItemList( data, new Point( LIST_WIDTH, (BLOCK_HEIGHT * 0.667) - (V_SPACING / 2.) ), 5, 3, PANEL_COLOR );
			this.addChild(questItemsList);
			questItemsList.init();
			questItemsList.x = BLOCK_MARGIN;
			questItemsList.y = MARGIN_TOP;
			questItemsList.addEventListener( ItemList.ITEM_FOCUS_CHANGE_EVENT_TYPE, itemEventListener );
			questItemsList.visible = false;
			
			
			itemDescription.x = BLOCK_MARGIN;
			itemDescription.y = (BLOCK_HEIGHT * 0.667) + (V_SPACING / 2.) + MARGIN_TOP;
			parentWidth = parent.width;
		}
		
		private function initText():void
		{
			centeredTextFormat = new TextFormat();
			centeredTextFormat.size = FONT_HEIGHT;
			centeredTextFormat.color = 0x0;
			centeredTextFormat.align = "center";
			
			descrTextFormat = new TextFormat();
			descrTextFormat.size = DESCR_FONT_HEIGHT;
			descrTextFormat.color = 0x0;
			descrTextFormat.align = "center";
			
			whiteTextFormat = new TextFormat();
			whiteTextFormat.size = FONT_HEIGHT;
			whiteTextFormat.color = 0xFFFFFF;
			whiteTextFormat.align = "center";
			
			itemDescription = new TextField();
			itemDescription.wordWrap = true;
			itemDescription.defaultTextFormat = descrTextFormat;
			itemDescription.text = "";
			itemDescription.width = LIST_WIDTH;
			itemDescription.height = DESCR_HEIGHT;
			itemDescription.x = 0.;
			itemDescription.y = 0.;
			itemDescription.name = "item-description";
			this.addChild(itemDescription);
			itemDescription.selectable = false;
			itemDescription.background = true;
			itemDescription.backgroundColor = PANEL_COLOR;
		}
		
		private function createButton( label:String, isSideButton:Boolean, isHighlighted:Boolean ):Sprite
		{
			var button:Sprite = new Sprite();
			
			var size:Point = new Point( (isSideButton) ? SIDE_BUTTON_WIDTH : BOTTOM_BUTTON_WIDTH, BUTTON_HEIGHT );
			var pt:Point = new Point( 0., 0. );
			
			if (isHighlighted)
			{
				button.graphics.beginFill( BUTTON_HIGHLIGHT_COLOR, 1. );
				button.graphics.drawRect( pt.x, pt.y, size.x, size.y );
				button.graphics.endFill();
				
				pt.x = 2.;
				pt.y = 2.;
				
				size.x -= 4.;
				size.y -= 4.;
				
				button.graphics.beginFill( (isSideButton) ? PANEL_COLOR : BOTTOM_BUTTON_COLOR, 1. );
				button.graphics.drawRect( pt.x, pt.y, size.x, size.y );
				button.graphics.endFill();
			}
			else
			{
				button.graphics.beginFill( (isSideButton) ? PANEL_COLOR : BOTTOM_BUTTON_COLOR, 1. );
				button.graphics.drawRect( pt.x, pt.y, size.x, size.y );
				button.graphics.endFill();
			}
			
			var field:TextField = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = (isSideButton) ? centeredTextFormat : whiteTextFormat;
			field.text = label;
			field.width = size.x;
			field.height = size.y;
			field.x = 0.;
			field.y = 2.;
			field.name = "label";
			button.addChild(field);
			field.selectable = false;
			
			var disableOverlay:Shape = new Shape();
			disableOverlay.graphics.beginFill( 0x0, 0.7 );
			disableOverlay.graphics.drawRect( 0., 0., size.x, size.y );
			disableOverlay.graphics.endFill();
			disableOverlay.width = size.x;
			disableOverlay.height = size.y;
			disableOverlay.name = "overlay";
			button.addChild(disableOverlay);
			disableOverlay.visible = false;
			
			this.addChild(button);
			return button;
		}
		
		private function activateButton( btn:Sprite ):void
		{
			btn.getChildByName("overlay").visible = false;
			
			btn.removeEventListener( MouseEvent.CLICK, clickListener );
			btn.addEventListener( MouseEvent.CLICK, clickListener );
		}
		
		private function deactivateButton( btn:Sprite ):void
		{
			btn.getChildByName("overlay").visible = true;
			btn.removeEventListener( MouseEvent.CLICK, clickListener );
		}
		
		private function initButtons():void
		{
			var botButtonSpacing:Number = (LIST_WIDTH - (BOTTOM_BUTTON_WIDTH * 3.)) / 4.;
			
			useOnConnerButton = createButton( USE_ON_CONNER_BUTTON_LABEL, false, false );
			useOnConnerButton.x = BLOCK_MARGIN + botButtonSpacing;
			useOnConnerButton.y = parent.height - (MARGIN_BOTTOM + BUTTON_HEIGHT);
			useOnAnnieButton = createButton( USE_ON_ANNIE_BUTTON_LABEL, false, false );
			useOnAnnieButton.x = useOnConnerButton.x + botButtonSpacing + BOTTOM_BUTTON_WIDTH;
			useOnAnnieButton.y = useOnConnerButton.y;
			
			discardButton = createButton( DISCARD_BUTTON_LABEL, false, false );
			discardButton.x = useOnAnnieButton.x + botButtonSpacing + BOTTOM_BUTTON_WIDTH;
			discardButton.y = useOnAnnieButton.y;
			
			deactivateButton(useOnConnerButton);
			deactivateButton(discardButton);
			
			gearButton = createButton( GEAR_BUTTON_LABEL, true, true );
			gearButton.x = parent.width - (BLOCK_MARGIN + SIDE_BUTTON_WIDTH);
			gearButton.y = MARGIN_TOP;
			gearButton.addEventListener(MouseEvent.CLICK, clickListener);
			questButton = createButton( QUEST_BUTTON_LABEL, true, false );
			questButton.x = gearButton.x;
			questButton.y = gearButton.y + BUTTON_HEIGHT + V_SPACING;
			questButton.addEventListener(MouseEvent.CLICK, clickListener);
			
			activeItemSet = gearButton;
		}
		
		public function itemEventListener(e:Event):void
		{
			var item:ItemProperties = null;
			if ( e.type == ItemList.ITEM_FOCUS_CHANGE_EVENT_TYPE )
			{
				if ( activeItemSet == gearButton )
				{
					item = regItemsList.activeItem;
				}
				else if ( activeItemSet == questButton )
				{
					item = questItemsList.activeItem;
				}
				
				if ( item != null )
				{
					itemDescription.text = item.name + ":\n" + item.description;
					
					deactivateButton( useOnConnerButton );
					deactivateButton( useOnAnnieButton );
					
					if ( ( item.qualifiers & ItemProperties.CONSUMABLE ) == ItemProperties.CONSUMABLE )
					{	
						if ( ( item.qualifiers & ItemProperties.ANNIE ) == ItemProperties.ANNIE )
						{
							if ( item.isValid( data.conner ) )
							{
								activateButton( useOnAnnieButton );
							}
						}
						
						if ( ( item.qualifiers & ItemProperties.CONNER ) == ItemProperties.CONNER )
						{
							if ( item.isValid( data.conner ) )
							{
								activateButton( useOnConnerButton );
							}
						}
					}
					
					if ( ( item.qualifiers & ItemProperties.QUEST ) != ItemProperties.QUEST )
					{
						activateButton( discardButton );
					}
					else
					{
						deactivateButton( discardButton );
					}
				}
			}
		}
		
		public function clickListener(e:MouseEvent):void
		{
			var target:Object = null;
			var item:ItemProperties = null;
			
			if ( e.target is TextField )
			{
				target = (e.target as TextField).parent;
			}
			else
			{
				target = e.target;
			}
			
			if ( target == activeItemSet )
			{
				return;
			}
			else if ( target == gearButton )
			{
				itemDescription.text = "";
				
				removeChild(questButton);
				questButton.removeEventListener(MouseEvent.CLICK, clickListener);
				removeChild(gearButton);
				gearButton.removeEventListener(MouseEvent.CLICK, clickListener);
				questButton = createButton ( (questButton.getChildByName("label") as TextField).text, true, false );	
				activeItemSet = gearButton = createButton( (gearButton.getChildByName("label") as TextField).text, true, true );
				gearButton.x = parentWidth - (BLOCK_MARGIN + SIDE_BUTTON_WIDTH);
				gearButton.y = MARGIN_TOP;
				questButton.x = gearButton.x;
				questButton.y = gearButton.y + BUTTON_HEIGHT + V_SPACING;
				questButton.addEventListener(MouseEvent.CLICK, clickListener);
				gearButton.addEventListener(MouseEvent.CLICK, clickListener);
				questItemsList.visible = false;
				regItemsList.visible = true;
				setup();
				stage.assignFocus( stage, "none" );
			}
			else if ( target == questButton )
			{
				itemDescription.text = "";
				
				removeChild(questButton);
				questButton.removeEventListener(MouseEvent.CLICK, clickListener);
				removeChild(gearButton);
				gearButton.removeEventListener(MouseEvent.CLICK, clickListener);
				gearButton = createButton ( (gearButton.getChildByName("label") as TextField).text, true, false );	
				activeItemSet = questButton = createButton( (questButton.getChildByName("label") as TextField).text, true, true );
				gearButton.x = parentWidth - (BLOCK_MARGIN + SIDE_BUTTON_WIDTH);
				gearButton.y = MARGIN_TOP;
				questButton.x = gearButton.x;
				questButton.y = gearButton.y + BUTTON_HEIGHT + V_SPACING;
				questButton.addEventListener(MouseEvent.CLICK, clickListener);
				gearButton.addEventListener(MouseEvent.CLICK, clickListener);
				questItemsList.visible = true;
				regItemsList.visible = false;
				setup();
				stage.assignFocus( stage, "none" );
			}
			else if ( target == discardButton )
			{
				item = null;
				
				if ( activeItemSet == gearButton )
				{
					item = regItemsList.activeItem;
				}
				else
				{
					item = questItemsList.activeItem;
				}
				
				if (item == null)
				{
					return;
				}
				
				data.annie.inventory.removeByProp( item );
				
				setup();
			}
			else if ( ( target == useOnConnerButton ) || ( target == useOnAnnieButton ) )
			{
				item = null;
				var recipient:Creature = ( target == useOnConnerButton ) ? data.conner : data.annie;
				
				if ( activeItemSet == gearButton )
				{
					item = regItemsList.activeItem;
				}
				else
				{
					item = questItemsList.activeItem;
				}
				
				if (item == null)
				{
					return;
				}
				
				if ( CONFIG::debug )
				{
					StatLogger.getSingleton().addValue( item.type, 1 );
				}
				
				data.annie.inventory.removeByProp( item );
				setup();
				
				for ( var i:int = 0; i < item.effects.length; i++ )
				{
					recipient.attachEffect( item.effects[i] as IEffect );
				}
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			regItemsList.clear();
			questItemsList.clear();
			
			for ( var i:int = 0; i < data.conner.inventory.entries.length; i++ )
			{
				var item:ItemProperties = (data.conner.inventory.entries[ i ] as InventoryEntry).props;
				
				if ( (item.qualifiers & ItemProperties.BADGE) == ItemProperties.BADGE )
				{
					continue;
				}
				
				if ( (item.qualifiers & ItemProperties.QUEST) == ItemProperties.QUEST )
				{
					questItemsList.addItem( item );
				}
				else
				{
					regItemsList.addItem( item );
				}
			}
			
			itemDescription.text = "";
			
			deactivateButton( useOnConnerButton );
			deactivateButton( useOnAnnieButton );
			deactivateButton( discardButton );
			
			this.visible = true;
		}
		
		public function teardown():void 
		{
			this.visible = false;
		}
		
	}

}