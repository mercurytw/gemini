package org.xangarath.gemini.ui.pack 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.gemini.GeminiData;
	
	import org.xangarath.engine.dbg.ErrorUtil;
	
	/**
	 * Le map screen
	 * @author Jeff Cochran
	 */
	public class PackMapScreen extends Sprite implements IState 
	{
		private static const TAG:String = "PackMapScreen";
		private static const HIGHLIGHT_COLOR:uint =  0x25A6AB;
		private static const BUTTON_COLOR:uint = 0xAC6A3E;
		private static const PLAYER_POS_COLOR:uint = 0xFF0000;
		
		private static const PLAYER_POS_RADIUS:int = 3.5;
		
		private static const HIGHLIGHT_BORDER_PX:Number = 3.0;
		
		private static const HIGHLIGHT_NAME:String = "highlight";
		private static const HIGHLIGHT_INDEX:int = 0;
		
		private static const LOCAL_BUTTON_ICON:String = "local-button-icon.png"
		private static const GLOBAL_BUTTON_ICON:String = "world-view-icon.png"
		
		private static const MAP_NAME_MARGIN_LEFT_PX:Number = 18.0;
		private static const MAP_NAME_MARGIN_BOTTOM_PX:Number = 10.0;
		
		private var MARGIN_TOP:Number;
		private var MARGIN_BOTTOM:Number;
		private var BLOCK_MARGIN_RIGHT:Number;
		private var BLOCK_MARGIN_LEFT:Number;
		private var BUTTON_WIDTH:Number;
		private var BUTTON_HEIGHT:Number;
		private var BUTTON_SPACING:Number;
		private var BLOCK_SPACING:Number;
		private var MAP_WIDTH:Number;
		private var FONT_HEIGHT:Number;
		
		private var MAP_LAYER_INDEX:int = 0;
		private var MAP_NAME_INDEX:int = 1;
		private var PLAYER_POS_INDEX:int = 3;
		
		private var localButton:Sprite;
		private var globalButton:Sprite;
		private var mapSprite:Sprite;
		private var worldMap:Sprite;
		private var mapName:String = "";
		private var playerPosSprite:Sprite;
		
		private var localMapImageWidth:Number;
		private var localMapImageHeight:Number;
		
		private var data:GeminiData;
		public function PackMapScreen( data:GeminiData ) 
		{
			super();
			this.data = data;
			this.visible = false;
		}
		
		public function init():void
		{
			MARGIN_TOP             = this.parent.height * 0.1;
			BLOCK_MARGIN_RIGHT     = this.parent.width * 0.0334;
			BLOCK_MARGIN_LEFT      = this.parent.width * 0.0056;
			BLOCK_SPACING          = this.parent.width * 0.00972;
			BUTTON_WIDTH           = this.parent.width * 0.0486;
			BUTTON_HEIGHT          = this.parent.height * 0.0937;
			MARGIN_BOTTOM          = this.parent.height * 0.023;
			MAP_WIDTH              = this.parent.width - (BLOCK_MARGIN_LEFT + BLOCK_MARGIN_RIGHT + BLOCK_SPACING + BUTTON_WIDTH);
			BUTTON_SPACING         = this.parent.height * 0.0334;
			FONT_HEIGHT            = (parent.height - ( MARGIN_TOP + MARGIN_BOTTOM )) * 0.05395;
			
			this.graphics.beginFill( 0x000000, 0.0 );
			this.graphics.drawRect( 0., 0., parent.width, parent.height );
			this.graphics.endFill();
			
			mapSprite = new Sprite();
			
			this.addChild(mapSprite);
			mapSprite.x = BLOCK_MARGIN_LEFT + BUTTON_WIDTH + BLOCK_SPACING;
			mapSprite.y = MARGIN_TOP;
			
			worldMap = new Sprite();
			worldMap.graphics.beginFill( 0xFF80C0 );
			worldMap.graphics.drawRect( 0., 0., MAP_WIDTH, (parent.height - ( MARGIN_TOP + MARGIN_BOTTOM )) );
			worldMap.graphics.endFill();
			
			this.addChild(worldMap);
			worldMap.x = BLOCK_MARGIN_LEFT + BUTTON_WIDTH + BLOCK_SPACING;
			worldMap.y = MARGIN_TOP;
			worldMap.visible = false;
			
			localButton = new Sprite();
			buildButton( localButton, data.resMgr.getBitmap( LOCAL_BUTTON_ICON ) );
			this.addChild( localButton );
			localButton.x = BLOCK_MARGIN_LEFT;
			localButton.y = MARGIN_TOP;
			deactivateButton( localButton );
			localButton.name = "local-button";
			
			globalButton = new Sprite();
			buildButton( globalButton, data.resMgr.getBitmap( GLOBAL_BUTTON_ICON ) );
			this.addChild( globalButton );
			globalButton.x = BLOCK_MARGIN_LEFT;
			globalButton.y = localButton.y + BUTTON_HEIGHT + BUTTON_SPACING;
			activateButton( globalButton );
			globalButton.name = "global-button";
		}
		
		private function buildButton( s:Sprite, bmp:Bitmap ):void
		{
			ErrorUtil.assertNonNull( TAG + ":buildButton", bmp );
			
			var newHeight:Number;
			var newWidth:Number;
			var tmp:Sprite = new Sprite();
			tmp.graphics.beginFill( HIGHLIGHT_COLOR );
			tmp.graphics.drawRect( 0., 0., BUTTON_WIDTH + (2.0 * HIGHLIGHT_BORDER_PX), BUTTON_HEIGHT + (2.0 * HIGHLIGHT_BORDER_PX) );
			tmp.graphics.endFill();
			tmp.name = HIGHLIGHT_NAME;
			
			s.addChild(tmp);
			tmp.x = -HIGHLIGHT_BORDER_PX;
			tmp.y = -HIGHLIGHT_BORDER_PX;
			s.setChildIndex(tmp, HIGHLIGHT_INDEX);
			
			tmp = new Sprite();
			tmp.graphics.beginFill( BUTTON_COLOR );
			tmp.graphics.drawRect( 0., 0., BUTTON_WIDTH, BUTTON_HEIGHT );
			tmp.graphics.endFill();
			s.addChild(tmp);
			s.setChildIndex(tmp, 1);
			
			var dat:BitmapData = new BitmapData( bmp.width, bmp.height );
			dat.copyPixels( bmp.bitmapData, new Rectangle( 0., 0., bmp.width, bmp.height ), new Point() );
			var tmpBmp:Bitmap = new Bitmap(dat);
			
			var heightToWidthRatio:Number = bmp.width / bmp.height;
			var widthToHeightRatio:Number = bmp.height / bmp.width;
			
			var offtX:Number;
			var offtY:Number;
			
			// set to width
			newWidth = tmp.width;
			newHeight = newWidth * widthToHeightRatio;
			if ( newHeight > tmp.height )
			{
				newHeight = tmp.height;
				newWidth = newHeight * heightToWidthRatio;
			}
			
			offtX = (newWidth < tmp.width) ? (tmp.width - newWidth) / 2.0 : 0.0;
			offtY = (newHeight < tmp.height) ? (tmp.height - newHeight) / 2.0 : 0.0;
			
			tmpBmp.width = newWidth;
			tmpBmp.height = newHeight;
			
			s.addChild( tmpBmp );
			tmpBmp.x = offtX;
			tmpBmp.y = offtY;
		}
		
		private function buildMap( s:Sprite ):void
		{
			var mapWidth:Number = data.activeLevel.map.width;
			var mapHeight:Number = data.activeLevel.map.height;
			var spriteWidth:Number = MAP_WIDTH;
			var spriteHeight:Number = parent.height - ( MARGIN_TOP + MARGIN_BOTTOM );
			// as in, height * h2w = width
			var heightToWidthRatio:Number = mapWidth / mapHeight;
			var widthToHeightRatio:Number = mapHeight / mapWidth;
			var offtX:Number;
			var offtY:Number;
			
			if ( mapName != data.activeLevel.name )
			{
				s.removeChildren();
				s.graphics.clear();
				
				s.graphics.beginFill( 0x0 );
				s.graphics.drawRect( 0., 0., spriteWidth, spriteHeight );
				s.graphics.endFill();
				
				
				var dat:BitmapData =  new BitmapData( mapWidth, mapHeight );
				dat.copyPixels( (data.activeLevel.map.getChildAt(0) as Bitmap).bitmapData, new Rectangle(0, 0, mapWidth, mapHeight), new Point() );
				var tmp:Bitmap = new Bitmap(dat);
				
				// set to width
				localMapImageWidth = spriteWidth;
				localMapImageHeight = localMapImageWidth * widthToHeightRatio;
				if ( localMapImageHeight > spriteHeight )
				{
					localMapImageHeight = spriteHeight;
					localMapImageWidth = localMapImageHeight * heightToWidthRatio;
				}
				
				offtX = (localMapImageWidth < spriteWidth) ? (spriteWidth - localMapImageWidth) / 2.0 : 0.0;
				offtY = (localMapImageHeight < spriteHeight) ? (spriteHeight - localMapImageHeight) / 2.0 : 0.0;
				
				tmp.width = localMapImageWidth;
				tmp.height = localMapImageHeight;
				
				s.addChild(tmp);
				tmp.x = offtX;
				tmp.y = offtY;			
				
				mapName = data.activeLevel.name;
				
				var fmt:TextFormat = new TextFormat();
				fmt.size = FONT_HEIGHT;
				fmt.color = 0xFFFFFF;
				fmt.align = "left";
				var mapNameField:TextField = new TextField();
				mapNameField.wordWrap = false;
				mapNameField.defaultTextFormat = fmt;
				mapNameField.text = mapName;
				mapNameField.width = MAP_WIDTH - MAP_NAME_MARGIN_LEFT_PX;
				mapNameField.height = FONT_HEIGHT + MAP_NAME_MARGIN_BOTTOM_PX;
				mapNameField.name = "map-name-field";
				s.addChild( mapNameField );
				mapNameField.selectable = false;
				mapNameField.x = MAP_NAME_MARGIN_LEFT_PX;
				mapNameField.y = spriteHeight - ( FONT_HEIGHT + MAP_NAME_MARGIN_BOTTOM_PX );
				
				playerPosSprite = new Sprite();
				playerPosSprite.graphics.beginFill( 0x0, 0. );
				playerPosSprite.graphics.drawRect( 0., 0., localMapImageWidth, localMapImageHeight );
				playerPosSprite.graphics.endFill();
				
				s.addChild( playerPosSprite );
				playerPosSprite.x = offtX;
				playerPosSprite.y = offtY;
			}
			
			playerPosSprite.graphics.clear();
			
			playerPosSprite.graphics.beginFill( PLAYER_POS_COLOR );
			playerPosSprite.graphics.drawCircle( (data.playerAtBat.pos.x / mapWidth) * localMapImageWidth, 
												 (data.playerAtBat.pos.y / mapHeight) * localMapImageHeight,
												 PLAYER_POS_RADIUS );
			playerPosSprite.graphics.endFill();
		}
		
		private function setHighlightVisible( btn:Sprite, truth:Boolean ):void
		{
			btn.getChildAt(HIGHLIGHT_INDEX).visible = truth;
		}
		
		private function activateButton( btn:Sprite ):void
		{
			setHighlightVisible( btn, false );
			btn.removeEventListener( MouseEvent.CLICK, clickListener );
			btn.addEventListener( MouseEvent.CLICK, clickListener );
		}
		
		private function deactivateButton( btn:Sprite ):void
		{
			setHighlightVisible( btn, true );
			btn.removeEventListener( MouseEvent.CLICK, clickListener );
		}
		
		public function clickListener( e:MouseEvent ):void
		{
			var target:DisplayObject = e.target as DisplayObject;
			
			if ( !(e.target is DisplayObject ) )
				return;
			
			if ( localButton.contains( target ) )
			{
				mapSprite.visible = true;
				worldMap.visible = false;
				activateButton( globalButton );
				deactivateButton( localButton );
			}
			else
			{
				worldMap.visible = true;
				mapSprite.visible = false;
				activateButton( localButton );
				deactivateButton( globalButton );
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			buildMap( mapSprite );
			this.visible = true;
			if ( worldMap.visible == true )
			{
				activateButton( localButton );
			}
			else
			{
				activateButton( globalButton );
			}
		}
		
		public function teardown():void 
		{
			this.visible = false;
			
			deactivateButton( localButton );
			deactivateButton( globalButton );
		}
		
	}

}