package org.xangarath.gemini.ui.pack 
{
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	/**
	 * stat block biatch
	 * @author ...
	 */
	public class StatBlock extends Sprite
	{
		private var _name:String;
		private var _hpNum:Number;
		private var _hpDenom:Number;
		private var _atkMod:Number;
		private var _defMod:Number;
		private var size:Point;
		
		private var act:Creature;
		private var hp:Number;
		
		private var FONT_HEIGHT:Number;
		private var FONT_SPACING:Number;
		
		private var data:GeminiData;
		
		public function StatBlock( data:GeminiData, name:String, act:Creature, hp:Number, size:Point ) 
		{
			super();
			_name = name;
			this.act = act;
			this.hp = hp;
			this.size = size;
			this.data = data;
		}
		
		public function init():void
		{
			this.graphics.beginFill( 0x000000, 0.0 );
			this.graphics.drawRect( 0., 0., size.x, size.y );
			this.graphics.endFill();
			
			FONT_HEIGHT = ((size.y * 0.9) / 4.0) - 10;
			FONT_SPACING = (size.y * 0.1) / 3.0;
			
			initText();
			this.visible = true;
		}
		
		public function recalc():void
		{
			this.removeChildren();
			initText();
		}
		
		private function initText():void
		{
			var center:TextFormat = new TextFormat();
			center.size = FONT_HEIGHT;
			center.color = 0x0;
			center.align = "center";
			
			var left:TextFormat = new TextFormat();
			left.size = FONT_HEIGHT;
			left.color = 0x0;
			left.align = "left";
			
			var right:TextFormat = new TextFormat();
			right.size = FONT_HEIGHT;
			right.color = 0x0;
			right.align = "right";
			
			var field:TextField = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = center;
			field.text = _name;
			field.width = size.x;
			field.x = 0.;
			field.y = 0.;
			field.name = "name";
			this.addChild(field);
			field.selectable = false;
			
			field = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = left;
			field.text = "MAX HP";
			field.width = ((size.x * 2.) / 3.) + 10.;
			field.x = 0.;
			field.y = FONT_HEIGHT + FONT_SPACING;
			field.name = "HP-title";
			this.addChild(field);
			field.selectable = false;
			
			field = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = right;
			var tmp:int = act.stats.maxHP;
			field.text = "" + tmp;
			field.width = size.x / 2.;
			field.x = size.x / 2.;
			field.y = FONT_HEIGHT + FONT_SPACING;
			field.name = "HP-value";
			this.addChild(field);
			field.selectable = false;
			
			field = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = left;
			field.text = "ATK";
			field.width = size.x / 2.;
			field.x = 0.;
			field.y = (FONT_HEIGHT + FONT_SPACING) * 2.;
			field.name = "ATK-title";
			this.addChild(field);
			field.selectable = false;
			
			field = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = right;
			field.text = (act.stats.calculateEffectiveAttack( null ) >= 0.) ? "+" + act.stats.calculateEffectiveAttack( null ) : "" +  act.stats.calculateEffectiveAttack( null );
			field.width = size.x / 2.;
			field.x = size.x / 2.;
			field.y = (FONT_HEIGHT + FONT_SPACING) * 2.;
			field.name = "ATK-value";
			this.addChild(field);
			field.selectable = false;
			
			field = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = left;
			field.text = "DEF";
			field.width = size.x / 2.;
			field.x = 0.;
			field.y = (FONT_HEIGHT + FONT_SPACING) * 3.;
			field.name = "DEF-title";
			this.addChild(field);
			field.selectable = false;
			
			field = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = right;
			field.text = (act.stats.effectiveDefense >= 0.) ? "+" + act.stats.effectiveDefense : "" + act.stats.effectiveDefense;
			field.width = size.x / 2.;
			field.x = size.x / 2.;
			field.y = (FONT_HEIGHT + FONT_SPACING) * 3.;
			field.name = "DEF-value";
			this.addChild(field);
			field.selectable = false;
		}
		
	}

}