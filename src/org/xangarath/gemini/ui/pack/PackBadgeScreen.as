package org.xangarath.gemini.ui.pack 
{
	import com.google.analytics.data.X10;
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TransformGestureEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.items.ItemProperties;
	import org.xangarath.gemini.items.InventoryEntry;
	import org.xangarath.gemini.items.BadgeApplicator;
	
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.StatLogger;
	import org.xangarath.engine.interfaces.IState;
	
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * A screen used to show available badges and let the user select which are active
	 * @author jeff cochran
	 */
	public class PackBadgeScreen extends Sprite implements IState 
	{
		private static const TAG:String = "PackBadgeScreen";
		
		private static const PANEL_COLOR:uint = 0xE2DAC0;
		private static const SIDE_BUTTON_COLOR:uint = 0xAC6A3E;
		private static const BOTTOM_BUTTON_COLOR:uint = 0xA99756;
		private static const BUTTON_HIGHLIGHT_COLOR:uint = 0x25A6AB;
		
		private static const BAD_CHANGE_COLOR:uint = 0xFF0000;
		private static const GOOD_CHANGE_COLOR:uint = 0x00FF00;
		
		private var V_SPACING:Number = 0.02;
		private var H_SPACING:Number = 0.5;
		private var MARGIN_TOP:Number = 0.10;
		private var MARGIN_BOTTOM:Number = 0.02;
		private var BUTTON_HEIGHT:Number = 0.1;
		private var BLOCK_MARGIN:Number = 0.045;
		private var BLOCK_HEIGHT:Number;
		private var BLOCK_WIDTH:Number;
		private var DESCR_HEIGHT:Number;
		private var LIST_WIDTH:Number = 0.75;
		private var SIDEBAR_WIDTH:Number;
		private var SIDEBAR_HEIGHT:Number;
		private var SIDE_BUTTON_WIDTH:Number = 0.667;
		private var BOTTOM_BUTTON_WIDTH:Number = 0.25;
		private var FONT_HEIGHT:Number;
		private var SIDE_FONT_HEIGHT:Number;
		private var DESCR_FONT_HEIGHT:Number = 0.70;
		private var SIDE_BUTTON_HEIGHT:Number = 1. / 14.;
		private static const DIVIDER_WIDTH_PX:Number = 2.0;
		private var SIDEBAR_MARGIN:Number = 0.01;
		private static const HIGHLIGHT_BORDER_PX:Number = 3.0;
		private var BADGE_SLOT_SIZE:Number = 0.33;
		private var BADGE_BRIEF_WIDTH:Number = 0.66;
		private var BADGE_BRIEF_HEIGHT:Number;
		private var SIDEBAR_LOWER_H_SPACING:Number = 0.01;
		
		private var itemDescription:TextField;
		private var centeredTextFormat:TextFormat;
		private var whiteTextFormat:TextFormat;
		private var smallWhiteTextFormat:TextFormat;
		private var descrTextFormat:TextFormat;
		private var briefTextFormat:TextFormat;
		private var briefCenterTextFormat:TextFormat;
		private var briefRightTextFormat:TextFormat;
		
		private var sidebar:Sprite;
		
		private var assignButton:Sprite;
		private static const ASSIGN_BUTTON_LABEL_ASSIGN:String = "ASSIGN";
		private static const ASSIGN_BUTTON_LABEL_SWAP:String = "SWAP";
		private var discardButton:Sprite;
		private static const DISCARD_BUTTON_LABEL:String = "REMOVE";
		private var switchCharButton:Sprite;
		private static const SWITCH_CHAR_BUTTON_LABEL_CONNER:String = ">> Conner";
		private static const SWITCH_CHAR_BUTTON_LABEL_ANNIE:String = ">> Annie";
		
		private var activeCreature:Creature = null;
		private var activeSlot:Sprite = null;
		private var activeCreaturePortrait:Sprite = null;
		private var activeCreatureName:TextField;
		private var activeBadge:ItemProperties = null;
		
		private var slotOneIcon:Sprite;
		private var slotOneBrief:TextField;
		private var slotOneBadge:ItemProperties;
		private var slotTwoIcon:Sprite;
		private var slotTwoBrief:TextField;
		private var slotTwoBadge:ItemProperties;
		
		private var hpCurrent:TextField;
		private var hpChange:TextField;
		private var defCurrent:TextField;
		private var defChange:TextField;
		private var atkCurrent:TextField;
		private var atkChange:TextField;
		
		private var badgeList:ItemList;
		private var data:GeminiData;
		private var bDontCalculate:Boolean = false;
		
		private var applicator:BadgeApplicator;
		public function PackBadgeScreen( data:GeminiData ) 
		{
			super();
			this.data = data;
			this.visible = false;
			applicator = new BadgeApplicator( data );
		}
		
		public function init():void
		{
			MARGIN_TOP *= this.parent.height;
			MARGIN_BOTTOM *= this.parent.height;
			BLOCK_HEIGHT = parent.height - (MARGIN_TOP + MARGIN_BOTTOM);
			BLOCK_MARGIN *= this.parent.width;
			BLOCK_WIDTH = this.parent.width - ( 2. * BLOCK_MARGIN );
			V_SPACING *= this.parent.width;
			H_SPACING *= V_SPACING;
			BUTTON_HEIGHT *= BLOCK_HEIGHT; 
			LIST_WIDTH *= BLOCK_WIDTH;
			FONT_HEIGHT = BUTTON_HEIGHT - 20.;
			DESCR_FONT_HEIGHT *= FONT_HEIGHT;
			DESCR_HEIGHT = (BLOCK_HEIGHT * 0.333) - ( (V_SPACING * 1.5) + BUTTON_HEIGHT );
			SIDEBAR_WIDTH = BLOCK_WIDTH - ( LIST_WIDTH + H_SPACING );
			SIDEBAR_HEIGHT = ((BLOCK_HEIGHT * 0.667) + (V_SPACING / 2.)) + DESCR_HEIGHT;
			SIDE_BUTTON_WIDTH *= SIDEBAR_WIDTH;
			BOTTOM_BUTTON_WIDTH *= LIST_WIDTH;
			SIDE_BUTTON_HEIGHT *= BLOCK_HEIGHT;
			SIDE_FONT_HEIGHT = SIDE_BUTTON_HEIGHT - 12.;
			SIDEBAR_MARGIN *= BLOCK_HEIGHT;
			BADGE_SLOT_SIZE *= (SIDEBAR_WIDTH) - ( 2. * SIDEBAR_MARGIN);
			BADGE_BRIEF_HEIGHT = BADGE_SLOT_SIZE;
			BADGE_BRIEF_WIDTH *= (SIDEBAR_WIDTH) - ( 2. * SIDEBAR_MARGIN);
			SIDEBAR_LOWER_H_SPACING *= SIDEBAR_WIDTH;
			
			
			graphics.beginFill( 0x0, 0.0 );
			graphics.drawRect( 0., 0., parent.width, parent.height );
			graphics.endFill();
			
			sidebar = new Sprite();
			addChild( sidebar );
			sidebar.graphics.beginFill( PANEL_COLOR );
			sidebar.graphics.drawRect( 0., 0., SIDEBAR_WIDTH, SIDEBAR_HEIGHT );
			sidebar.graphics.endFill();
			sidebar.x = parent.width - ( SIDEBAR_WIDTH + BLOCK_MARGIN );
			sidebar.y = MARGIN_TOP;
			
			activeCreature = data.party.atBat;
			
			initText();
			initButtons();
			 
			badgeList = new ItemList( data, new Point( LIST_WIDTH, (BLOCK_HEIGHT * 0.667) - (V_SPACING / 2.) ), 4, 3, PANEL_COLOR );
			this.addChild(badgeList);
			badgeList.init();
			badgeList.x = BLOCK_MARGIN;
			badgeList.y = MARGIN_TOP;
			badgeList.addEventListener( ItemList.ITEM_FOCUS_CHANGE_EVENT_TYPE, itemEventListener );
			
			itemDescription.x = BLOCK_MARGIN;
			itemDescription.y = (BLOCK_HEIGHT * 0.667) + (V_SPACING / 2.) + MARGIN_TOP;
			
			var sidebarHeightDivTwo:Number = sidebar.height / 2.;
			sidebar.graphics.beginFill( SIDE_BUTTON_COLOR );
			sidebar.graphics.drawRect( 0., ((sidebar.height / 2.) - ( DIVIDER_WIDTH_PX / 2.)), sidebar.width, DIVIDER_WIDTH_PX );
			sidebar.graphics.drawRect( (sidebar.width / 2.) + ( SIDEBAR_MARGIN), 
			                           sidebarHeightDivTwo + ( sidebarHeightDivTwo / 3. ),
									   DIVIDER_WIDTH_PX,
									   (sidebarHeightDivTwo * 2. ) / 3.);
			sidebar.graphics.endFill();
			
			slotOneIcon = buildSlotSprite();
			slotOneIcon.x = SIDEBAR_MARGIN;
			slotOneIcon.y = SIDEBAR_MARGIN;
			slotOneIcon.addEventListener( MouseEvent.CLICK, clickListener );
			
			slotOneBrief.x = SIDEBAR_WIDTH - ( SIDEBAR_MARGIN + slotOneBrief.width );
			slotOneBrief.y = SIDEBAR_MARGIN;
			
			slotTwoIcon = buildSlotSprite();
			slotTwoIcon.x = SIDEBAR_WIDTH - ( SIDEBAR_MARGIN + slotTwoIcon.width );
			slotTwoIcon.y = SIDEBAR_MARGIN + slotOneIcon.height + SIDEBAR_MARGIN;
			slotTwoIcon.addEventListener( MouseEvent.CLICK, clickListener );
			
			slotTwoBrief.x = SIDEBAR_MARGIN;
			slotTwoBrief.y = slotTwoIcon.y;
			
			activeCreaturePortrait = buildPortrait();
			activeCreaturePortrait.x = SIDEBAR_MARGIN;
			activeCreaturePortrait.y = (sidebar.height / 2. ) + DIVIDER_WIDTH_PX + SIDEBAR_MARGIN;
			
			activeCreatureName.x = sidebar.width - ( SIDEBAR_MARGIN + activeCreatureName.width );
			activeCreatureName.y = activeCreaturePortrait.y + ( (activeCreaturePortrait.height / 2.) - (activeCreatureName.height / 2.));
			
			activeCreature = null;
			setActiveCreature( data.conner );
			this.visible = false;
		}
		
		private function buildSlotSprite():Sprite
		{
			var icon:Sprite = new Sprite();
			sidebar.addChild( icon );
			icon.graphics.beginFill( 0x0, 0.0 );
			icon.graphics.drawRect( 0., 0., BADGE_SLOT_SIZE, BADGE_SLOT_SIZE );
			icon.graphics.endFill();
			
			var highlight:Sprite = new Sprite();
			icon.addChild( highlight );
			highlight.graphics.beginFill( 0x25A6AB );
			highlight.graphics.drawRect( 0., 0., BADGE_SLOT_SIZE, BADGE_SLOT_SIZE );
			highlight.graphics.endFill();
			highlight.name = "highlight";
			highlight.visible = false;
			
			var image:Bitmap = data.resMgr.getBitmap( "70x70_placeholder.jpg" );
			if ( image == null )
			{
				DebugLog.getSingleton().e( TAG, "Could not find placeholder icon for badge icon slots" );
				return null;
			}
			
			image.width = BADGE_SLOT_SIZE - (HIGHLIGHT_BORDER_PX * 2);
			image.height = BADGE_SLOT_SIZE - (HIGHLIGHT_BORDER_PX * 2);
			
			icon.addChild( image );
			image.name = "image";
			image.x += HIGHLIGHT_BORDER_PX;
			image.y += HIGHLIGHT_BORDER_PX;
			
			return icon;
		}
		
		private function buildPortrait():Sprite
		{
			var icon:Sprite = new Sprite();
			sidebar.addChild( icon );
			icon.graphics.beginFill( 0x0, 0.0 );
			icon.graphics.drawRect( 0., 0., (sidebar.width / 2.) - ((SIDEBAR_MARGIN * 2 ) + (SIDEBAR_LOWER_H_SPACING / 2.)), (sidebar.width / 2.) - ((SIDEBAR_MARGIN * 2 ) + (SIDEBAR_LOWER_H_SPACING / 2.)) );
			icon.graphics.endFill();
			
			var image:Bitmap = data.resMgr.getBitmap( "70x70_placeholder.jpg" );
			if ( image == null )
			{
				DebugLog.getSingleton().e( TAG, "Could not find placeholder icon for character portrait" );
				return null;
			}
			
			image.width = icon.width;
			image.height = icon.width;
			
			icon.addChild( image );
			image.name = "image";
			
			activeCreatureName = new TextField();
			activeCreatureName.wordWrap = true;
			activeCreatureName.defaultTextFormat = briefTextFormat;
			activeCreatureName.text = "CONNER";
			activeCreatureName.width = (sidebar.width / 2.) - ((SIDEBAR_MARGIN * 2 ));
			activeCreatureName.height = briefTextFormat.size + 5.;
			activeCreatureName.x = 0.;
			activeCreatureName.y = 0.;
			sidebar.addChild( activeCreatureName );
			activeCreatureName.selectable = false;
			
			return icon;
		}
		
		private function initText():void
		{
			centeredTextFormat = new TextFormat();
			centeredTextFormat.size = FONT_HEIGHT;
			centeredTextFormat.color = 0x0;
			centeredTextFormat.align = "center";
			
			descrTextFormat = new TextFormat();
			descrTextFormat.size = DESCR_FONT_HEIGHT;
			descrTextFormat.color = 0x0;
			descrTextFormat.align = "center";
			
			whiteTextFormat = new TextFormat();
			whiteTextFormat.size = FONT_HEIGHT;
			whiteTextFormat.color = 0xFFFFFF;
			whiteTextFormat.align = "center";
			
			smallWhiteTextFormat = new TextFormat();
			smallWhiteTextFormat.size = SIDE_FONT_HEIGHT;
			smallWhiteTextFormat.color = 0xFFFFFF;
			smallWhiteTextFormat.align = "center";
			
			briefTextFormat = new TextFormat();
			briefTextFormat.size = SIDE_FONT_HEIGHT - 2.;
			briefTextFormat.color = 0x0;
			briefTextFormat.align = "left";
			
			briefRightTextFormat = new TextFormat();
			briefRightTextFormat.size = SIDE_FONT_HEIGHT - 2.;
			briefRightTextFormat.color = 0x0;
			briefRightTextFormat.align = "right";
			
			briefCenterTextFormat = new TextFormat();
			briefCenterTextFormat.size = SIDE_FONT_HEIGHT - 2.;
			briefCenterTextFormat.color = 0x0;
			briefCenterTextFormat.align = "center";
			
			itemDescription = new TextField();
			itemDescription.wordWrap = true;
			itemDescription.defaultTextFormat = descrTextFormat;
			itemDescription.text = "";
			itemDescription.width = LIST_WIDTH;
			itemDescription.height = DESCR_HEIGHT;
			itemDescription.x = 0.;
			itemDescription.y = 0.;
			itemDescription.name = "item-description";
			this.addChild(itemDescription);
			itemDescription.selectable = false;
			itemDescription.background = true;
			itemDescription.backgroundColor = PANEL_COLOR;
			
			slotOneBrief = new TextField();
			slotOneBrief.wordWrap = true;
			slotOneBrief.defaultTextFormat = briefTextFormat;
			slotOneBrief.text = "Available\nAvailable";
			slotOneBrief.width = BADGE_BRIEF_WIDTH;
			slotOneBrief.height = BADGE_BRIEF_HEIGHT;
			slotOneBrief.x = 0.;
			slotOneBrief.y = 0.;
			sidebar.addChild( slotOneBrief );
			slotOneBrief.selectable = false;
			
			slotTwoBrief = new TextField();
			slotTwoBrief.wordWrap = true;
			slotTwoBrief.defaultTextFormat = briefTextFormat;
			slotTwoBrief.text = "Available\nAvailable";
			slotTwoBrief.width = BADGE_BRIEF_WIDTH;
			slotTwoBrief.height = BADGE_BRIEF_HEIGHT;
			slotTwoBrief.x = 0.;
			slotTwoBrief.y = 0.;
			sidebar.addChild( slotTwoBrief );
			slotTwoBrief.selectable = false;
			
			var lastY:Number;
			
			var tf:TextField = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefRightTextFormat;
			tf.text = "current";
			tf.width = (sidebar.width / 2.) - (SIDEBAR_MARGIN);
			tf.height = briefRightTextFormat.size + 6.;
			tf.x = SIDEBAR_MARGIN;
			tf.y = (sidebar.height * 3.) / 4.;
			sidebar.addChild( tf );
			tf.selectable = false;
			
			tf = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefCenterTextFormat;
			tf.text = "change";
			tf.width = (sidebar.width / 2.) - ((DIVIDER_WIDTH_PX / 2.) + (3. * SIDEBAR_MARGIN ));
			tf.height = briefTextFormat.size + 6.;
			tf.x = (sidebar.width / 2.) + SIDEBAR_MARGIN + DIVIDER_WIDTH_PX + SIDEBAR_MARGIN;
			tf.y = (sidebar.height * 3.) / 4.;
			sidebar.addChild( tf );
			tf.selectable = false;
			
			lastY = tf.y + tf.height;
			
			tf = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefTextFormat;
			tf.text = "HP";
			tf.width = (sidebar.width / 2.);
			tf.height = briefTextFormat.size + 6.;
			tf.x = SIDEBAR_MARGIN;
			tf.y = lastY + 2.;
			sidebar.addChild( tf );
			tf.selectable = false;
			
			tf = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefTextFormat;
			tf.text = "42";
			tf.width = (sidebar.width / 4.);
			tf.height = briefTextFormat.size + 6.;
			tf.x = ((sidebar.width * 1.) / 4.) - (SIDEBAR_MARGIN / 2.);
			tf.y = lastY + 2.;
			sidebar.addChild( tf );
			tf.selectable = false;
			hpCurrent = tf;
			
			tf = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefCenterTextFormat;
			tf.text = "42";
			tf.width = (sidebar.width / 2.) - ((DIVIDER_WIDTH_PX / 2.) + (3. * SIDEBAR_MARGIN ));
			tf.height = briefCenterTextFormat.size + 6.;
			tf.x = (sidebar.width / 2.) + SIDEBAR_MARGIN + DIVIDER_WIDTH_PX + SIDEBAR_MARGIN;
			tf.y = lastY + 2.;
			sidebar.addChild( tf );
			tf.selectable = false;
			hpChange = tf;
			
			lastY = tf.y + tf.height;
			
			tf = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefTextFormat;
			tf.text = "ATK";
			tf.width = (sidebar.width / 2.);
			tf.height = briefTextFormat.size + 6.;
			tf.x = SIDEBAR_MARGIN;
			tf.y = lastY + 2.;
			sidebar.addChild( tf );
			tf.selectable = false;
			
			tf = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefTextFormat;
			tf.text = "42";
			tf.width = (sidebar.width / 4.);
			tf.height = briefTextFormat.size + 6.;
			tf.x = ((sidebar.width * 1.) / 4.) - (SIDEBAR_MARGIN / 2.);
			tf.y = lastY + 2.;
			sidebar.addChild( tf );
			tf.selectable = false;
			atkCurrent = tf;
			
			tf = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefCenterTextFormat;
			tf.text = "42";
			tf.width = (sidebar.width / 2.) - ((DIVIDER_WIDTH_PX / 2.) + (3. * SIDEBAR_MARGIN ));
			tf.height = briefCenterTextFormat.size + 6.;
			tf.x = (sidebar.width / 2.) + SIDEBAR_MARGIN + DIVIDER_WIDTH_PX + SIDEBAR_MARGIN;
			tf.y = lastY + 2.;
			sidebar.addChild( tf );
			tf.selectable = false;
			atkChange = tf;
			
			lastY = tf.y + tf.height;
			
			tf = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefTextFormat;
			tf.text = "DEF";
			tf.width = (sidebar.width / 2.);
			tf.height = briefTextFormat.size + 6.;
			tf.x = SIDEBAR_MARGIN;
			tf.y = lastY + 2.;
			sidebar.addChild( tf );
			tf.selectable = false;
			
			tf = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefTextFormat;
			tf.text = "42";
			tf.width = (sidebar.width / 4.);
			tf.height = briefTextFormat.size + 6.;
			tf.x = ((sidebar.width * 1.) / 4.) - (SIDEBAR_MARGIN / 2.);
			tf.y = lastY + 2.;
			sidebar.addChild( tf );
			tf.selectable = false;
			defCurrent = tf;
			
			tf = new TextField();
			tf.wordWrap = false;
			tf.defaultTextFormat = briefCenterTextFormat;
			tf.text = "42";
			tf.width = (sidebar.width / 2.) - ((DIVIDER_WIDTH_PX / 2.) + (3. * SIDEBAR_MARGIN ));
			tf.height = briefCenterTextFormat.size + 6.;
			tf.x = (sidebar.width / 2.) + SIDEBAR_MARGIN + DIVIDER_WIDTH_PX + SIDEBAR_MARGIN;
			tf.y = lastY + 2.;
			sidebar.addChild( tf );
			tf.selectable = false;
			defChange = tf;
		}
		
		private function createButton( label:String, isSideButton:Boolean, isHighlighted:Boolean ):Sprite
		{
			var button:Sprite = new Sprite();
			
			var size:Point = new Point();
			if (isSideButton)
			{
				size.x = SIDE_BUTTON_WIDTH;
				size.y = SIDE_BUTTON_HEIGHT;
			}
			else
			{
				size.x = BOTTOM_BUTTON_WIDTH;
				size.y = BUTTON_HEIGHT;
			}
			var pt:Point = new Point( 0., 0. );
			
			if (isHighlighted)
			{
				button.graphics.beginFill( BUTTON_HIGHLIGHT_COLOR, 1. );
				button.graphics.drawRect( pt.x, pt.y, size.x, size.y );
				button.graphics.endFill();
				
				pt.x = 2.;
				pt.y = 2.;
				
				size.x -= 4.;
				size.y -= 4.;
				
				button.graphics.beginFill( (isSideButton) ? SIDE_BUTTON_COLOR : BOTTOM_BUTTON_COLOR, 1. );
				button.graphics.drawRect( pt.x, pt.y, size.x, size.y );
				button.graphics.endFill();
			}
			else
			{
				button.graphics.beginFill( (isSideButton) ? SIDE_BUTTON_COLOR : BOTTOM_BUTTON_COLOR, 1. );
				button.graphics.drawRect( pt.x, pt.y, size.x, size.y );
				button.graphics.endFill();
			}
			
			var field:TextField = new TextField();
			field.wordWrap = false;
			if ( isSideButton )
			{
				field.defaultTextFormat = smallWhiteTextFormat;
			}
			else
			{
				field.defaultTextFormat = whiteTextFormat;
			}
			field.text = label;
			field.width = size.x;
			field.height = size.y;
			field.x = 0.;
			field.y = 2.;
			field.name = "label";
			button.addChild(field);
			field.selectable = false;
			
			var disableOverlay:Shape = new Shape();
			disableOverlay.graphics.beginFill( 0x0, 0.7 );
			disableOverlay.graphics.drawRect( 0., 0., size.x, size.y );
			disableOverlay.graphics.endFill();
			disableOverlay.width = size.x;
			disableOverlay.height = size.y;
			disableOverlay.name = "overlay";
			button.addChild(disableOverlay);
			disableOverlay.visible = false;
			
			if ( isSideButton )
			{
				sidebar.addChild( button );
			}
			else
			{
				this.addChild(button);
			}
			return button;
		}
		
		private function swapActiveBadge():void
		{
			var selected:ItemProperties = activeBadge;
			var whichSlot:int = (activeSlot == slotOneIcon) ? 0 : 1;
			var old:ItemProperties = activeCreature.badgeSlots[ whichSlot ];
			var badgeIndex:int = activeCreature.inventory.indexOf(selected);
			
			if ( old != null )
			{
				applicator.detatchBadge( whichSlot, activeCreature );
				activeCreature.inventory.addAt( old, badgeIndex );
			}
			
			applicator.applyBadge( selected, whichSlot, activeCreature );
			activeCreature.inventory.removeByProp( selected );
			activeBadge = null;
			
			setup();
			
			if ( old != null )
			{
				badgeList.activeItem = old;
			}
			else
			{
				hpChange.textColor = atkChange.textColor = defChange.textColor = 0x0;
				itemDescription.text = "";
			}
			
			activateButton( discardButton );
			
			redrawBadgeSlots();
		}
		
		private function removeActiveBadge( index:int = -1 ):void
		{
			var whichSlot:int = (activeSlot == slotOneIcon) ? 0 : 1;
			var badge:ItemProperties = activeCreature.badgeSlots[ whichSlot ];
			var badgeIndex:int = ( index == -1 ) ? activeCreature.inventory.entries.length : index;
			
			applicator.detatchBadge( whichSlot, activeCreature );
			activeCreature.inventory.addAt( badge, badgeIndex );
			
			setup();
			
			badgeList.activeItem = badge;
			
			deactivateButton( discardButton );
			
			redrawBadgeSlots();
		}
		
		public function clickListener( e:MouseEvent ):void
		{
			var target:Object = null;
			
			if ( e.target is Sprite )
			{
				target = e.target;
			}
			else if ( e.target is TextField )
			{
				target = (e.target as TextField).parent;
			}
			
			if ( target == switchCharButton )
			{
				setActiveCreature( ( activeCreature == data.conner ) ? data.annie : data.conner );
			}
			else if ( ( target == slotOneIcon ) || ( target == slotTwoIcon ) )
			{
				selectBadgeSlot( target as Sprite );
				calculateStatChange();
			}
			else if ( target == assignButton )
			{
				swapActiveBadge();
			}
			else if ( target == discardButton )
			{
				removeActiveBadge();
				calculateStatChange();
				deactivateButton( discardButton );
			}
		}
		
		private function activateButton( btn:Sprite ):void
		{
			btn.getChildByName("overlay").visible = false;
			btn.removeEventListener( MouseEvent.CLICK, clickListener );
			btn.addEventListener( MouseEvent.CLICK, clickListener );
		}
		
		private function deactivateButton( btn:Sprite ):void
		{
			btn.getChildByName("overlay").visible = true;
			btn.removeEventListener( MouseEvent.CLICK, clickListener );
		}
		
		private function initButtons():void
		{
			var botButtonSpacing:Number = (LIST_WIDTH - (BOTTOM_BUTTON_WIDTH * 2.)) / 3.;
			
			assignButton = createButton( ASSIGN_BUTTON_LABEL_ASSIGN, false, false );
			assignButton.x = BLOCK_MARGIN + botButtonSpacing;
			assignButton.y = parent.height - (MARGIN_BOTTOM + BUTTON_HEIGHT);
			discardButton = createButton( DISCARD_BUTTON_LABEL, false, false );
			discardButton.x = assignButton.x + botButtonSpacing + BOTTOM_BUTTON_WIDTH;
			discardButton.y = assignButton.y;
			
			deactivateButton(assignButton);
			deactivateButton(discardButton);
			
			switchCharButton = createButton( SWITCH_CHAR_BUTTON_LABEL_ANNIE, true, false );
			switchCharButton.x = sidebar.width - switchCharButton.width;
			switchCharButton.y = ((sidebar.height / 2.) - (switchCharButton.height)) + 2.0;;
			activateButton(switchCharButton);
		}
		
		private function selectBadgeSlot( slot:Sprite ):void
		{	
			var slotNum:int;
			if ( slot == slotOneIcon )
			{
				slotTwoIcon.getChildByName( "highlight" ).visible = false;
				slotOneIcon.getChildByName( "highlight" ).visible = true;
				
				if ( slotOneBadge == null )
				{
					(assignButton.getChildByName( "label" ) as TextField).text = ASSIGN_BUTTON_LABEL_ASSIGN;
				}
				else
				{
					(assignButton.getChildByName( "label" ) as TextField).text = ASSIGN_BUTTON_LABEL_SWAP;
				}
				
				slotNum = 0;
			}
			else
			{
				slotOneIcon.getChildByName( "highlight" ).visible = false;
				slotTwoIcon.getChildByName( "highlight" ).visible = true;
				
				if ( slotTwoBadge == null )
				{
					(assignButton.getChildByName( "label" ) as TextField).text = ASSIGN_BUTTON_LABEL_ASSIGN;
				}
				else
				{
					(assignButton.getChildByName( "label" ) as TextField).text = ASSIGN_BUTTON_LABEL_SWAP;
				}
				
				slotNum = 1;
			}
			
			activeSlot = slot;
			
			if ( activeCreature.badgeSlots[ slotNum ] != null )
			{
				activateButton( discardButton );
			}
			else
			{
				deactivateButton( discardButton );
			}
		}
		
		private function redrawBadgeSlots():void
		{
			var img:Bitmap = null;
			var descr:String = "";
			var props:ItemProperties;
			var imgOld:Bitmap;
			if ( activeCreature.badgeSlots[ 0 ] == null )
			{
				img = data.resMgr.getBitmap( "70x70_placeholder.jpg" );
				descr = "Available";
				slotOneBadge = null;
			}
			else
			{
				props = (activeCreature.badgeSlots[ 0 ] as ItemProperties);
				img = data.resMgr.getBitmap( props.iconKey );
				descr = props.brief;
				slotOneBadge = props;
			}
			
			imgOld = slotOneIcon.getChildByName( "image" ) as Bitmap;
			img.width = imgOld.width;
			img.height = imgOld.height;
			slotOneIcon.addChild( img );
			img.x = imgOld.x;
			img.y = imgOld.y;
			slotOneIcon.removeChild( imgOld );
			img.name = "image";
			slotOneBrief.text = descr;
			
			if ( activeCreature.badgeSlots[ 1 ] == null )
			{
				img = data.resMgr.getBitmap( "70x70_placeholder.jpg" );
				descr = "Available";
				slotTwoBadge = null;
			}
			else
			{
				props = (activeCreature.badgeSlots[ 1 ] as ItemProperties);
				img = data.resMgr.getBitmap( props.iconKey );
				descr = props.brief;
				slotTwoBadge = props;
			}
			
			imgOld = slotTwoIcon.getChildByName( "image" ) as Bitmap;
			img.width = imgOld.width;
			img.height = imgOld.height;
			slotTwoIcon.addChild( img );
			img.x = imgOld.x;
			img.y = imgOld.y;
			slotTwoIcon.removeChild( imgOld );
			img.name = "image";
			slotTwoBrief.text = descr;
			
			var hp:Number = activeCreature.stats.baseHP;
			var atk:Number = activeCreature.stats.baseAttack;
			var def:Number = activeCreature.stats.baseDefense;
			
			if ( activeCreature.badgeSlots[ 0 ] != null )
			{
				// TODO: check for badge modification of stats
			}
			if ( activeCreature.badgeSlots[ 1 ] != null )
			{
				// TODO: check for badge modification of stats
			}
			
			hpCurrent.text = hpChange.text = "" + hp;
			atkCurrent.text = atkChange.text = "" + atk;
			defCurrent.text = defChange.text = "" + def;
		}
		
		private function getColor( before:int, after:int ):uint
		{
			if ( before < after )
				return GOOD_CHANGE_COLOR;
			else if ( before > after )
				return BAD_CHANGE_COLOR;
			
			return 0x0;
		}
		
		private function calculateStatChange():void
		{
			var hpDiff:int;
			var atkDiff:int;
			var defDiff:int;
			var whichSlot:int = (activeSlot == slotOneIcon) ? 0 : 1;
			
			if ( bDontCalculate )
				return;
			
			if ( activeBadge == null )
			{
				hpDiff = activeCreature.stats.baseHP;
				hpChange.text = "" + hpDiff;
				atkChange.text = "" + activeCreature.stats.baseAttack;
				defChange.text = "" + activeCreature.stats.baseDefense; 
				
				hpChange.textColor = atkChange.textColor = defChange.textColor = 0x0;
			}
			else if ( activeCreature.badgeSlots[ whichSlot ] == null )
			{
				bDontCalculate = true;
				var slot:int = activeCreature.inventory.indexOf( activeBadge );
				swapActiveBadge();	
				hpDiff = activeCreature.stats.baseHP;
				atkDiff = activeCreature.stats.baseAttack;
				defDiff = activeCreature.stats.baseDefense;
				
				removeActiveBadge( slot );
				
				hpChange.textColor = getColor(activeCreature.stats.baseHP, hpDiff);
				atkChange.textColor = getColor(activeCreature.stats.baseAttack, atkDiff);
				defChange.textColor = getColor(activeCreature.stats.baseDefense, defDiff);
				bDontCalculate = false;

				hpChange.text = "" + hpDiff;
				atkChange.text = "" + atkDiff;
				defChange.text = "" + defDiff; 
			}
			else
			{
				bDontCalculate = true;
				swapActiveBadge();	
				hpDiff = activeCreature.stats.baseHP;
				atkDiff = activeCreature.stats.baseAttack;
				defDiff = activeCreature.stats.baseDefense;
				swapActiveBadge();
				hpChange.textColor = getColor(activeCreature.stats.baseHP, hpDiff);
				atkChange.textColor = getColor(activeCreature.stats.baseAttack, atkDiff);
				defChange.textColor = getColor(activeCreature.stats.baseDefense, defDiff);
				bDontCalculate = false;

				hpChange.text = "" + hpDiff;
				atkChange.text = "" + atkDiff;
				defChange.text = "" + defDiff; 
			}	
		}
		
		private function setActiveCreature( c:Creature ):void
		{
			if ( c == activeCreature )
			{
				return;
			}
			
			slotTwoIcon.getChildByName( "highlight" ).visible = false;
			activeBadge = null;
			badgeList.clear();
			
			for ( var i:int = 0; i < data.conner.inventory.entries.length; i++ )
			{
				var item:ItemProperties = (data.conner.inventory.entries[ i ] as InventoryEntry).props;
				
				if ( (item.qualifiers & ItemProperties.BADGE) == ItemProperties.BADGE )
				{
					badgeList.addItem( item );
				}
			}
			
			itemDescription.text = "";
			
			if ( c == data.annie )
			{
				(switchCharButton.getChildByName( "label" ) as TextField).text = SWITCH_CHAR_BUTTON_LABEL_CONNER;
				activeCreatureName.text = "ANNIE";
			}
			else
			{
				(switchCharButton.getChildByName( "label" ) as TextField).text = SWITCH_CHAR_BUTTON_LABEL_ANNIE;
				activeCreatureName.text = "CONNER";
			}
			activeCreature = c;
			
			redrawBadgeSlots();
			
			selectBadgeSlot( slotOneIcon );
			
			deactivateButton( assignButton );
			deactivateButton( discardButton );
			
			hpChange.textColor = atkChange.textColor = defChange.textColor = 0x0;
			
			activeCreature = c;
			
			calculateStatChange();
			
			if ( activeCreature.badgeSlots[ 0 ] != null )
			{
				activateButton( discardButton );
			}
		}
		
		public function itemEventListener( e:Event ):void
		{	
			var item:ItemProperties = null;
			var whichSlot:int = (activeSlot == slotOneIcon) ? 0 : 1;
			
			if ( e.type == ItemList.ITEM_FOCUS_CHANGE_EVENT_TYPE )
			{
				activeBadge = badgeList.activeItem;
				itemDescription.text = activeBadge.name + "\n" + activeBadge.description;
				activateButton( assignButton );
				calculateStatChange();
				
				if (activeCreature.badgeSlots[ whichSlot ] == null)
				{
					(assignButton.getChildByName("label") as TextField).text = ASSIGN_BUTTON_LABEL_ASSIGN;
				}
				else
				{
					(assignButton.getChildByName("label") as TextField).text = ASSIGN_BUTTON_LABEL_SWAP;
				}
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			this.visible = true;
			badgeList.clear();
			deactivateButton( assignButton );
			
			for ( var i:int = 0; i < data.conner.inventory.entries.length; i++ )
			{
				var item:ItemProperties = (data.conner.inventory.entries[ i ] as InventoryEntry).props;
				
				if ( (item.qualifiers & ItemProperties.BADGE) == ItemProperties.BADGE )
				{
					badgeList.addItem( item );
				}
			}
			
			activeBadge = null;
		}
		
		public function teardown():void 
		{
			this.visible = false;
		}
		
	}

}