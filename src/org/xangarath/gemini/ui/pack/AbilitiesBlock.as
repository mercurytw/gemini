package org.xangarath.gemini.ui.pack 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display3D.textures.Texture;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * ...
	 * @author ...
	 */
	public class AbilitiesBlock extends Sprite 
	{
		private var size:Point;
		
		private var LOCKED_ABILITY_COLOR:uint = 0x00FF00;
		
		private var TITLE_FONT_HEIGHT:Number;
		private var DESCR_FONT_HEIGHT:Number;
		private var LOCKED_FONT_HEIGHT:Number;
		private var MAJOR_SPACING:Number;
		private var MINOR_SPACING:Number;
		private var BLOCK_HEIGHT:Number;
		private var BLOCK_WIDTH:Number;
		
		private var q:Sprite;
		private var w:Sprite;
		private var e:Sprite;
		private var r:Sprite;
		
		private var blockTextFormat:TextFormat;
		private var lockedTextFormat:TextFormat;
		private var titleTextFormat:TextFormat;
		
		private var data:GeminiData;
		
		public function AbilitiesBlock( size:Point, data:GeminiData ) 
		{
			super();
			this.size = size;
			
			q = new Sprite();
			q.name = "Q";
			w = new Sprite();
			w.name = "W";
			e = new Sprite();
			e.name = "E";
			r = new Sprite();
			r.name = "R";
			
			this.data = data;
		}
		
		public function init( actor:Creature ):void
		{
			q.removeChildren();
			w.removeChildren();
			e.removeChildren();
			r.removeChildren();
			this.removeChildren();
			this.graphics.beginFill( 0x000000, 0.0 );
			this.graphics.drawRect( 0., 0., size.x, size.y );
			this.graphics.endFill();
			
			TITLE_FONT_HEIGHT = ((size.y) * .18);
			MAJOR_SPACING = (size.y) * 0.08;
			MINOR_SPACING = size.x * 0.02;
			BLOCK_HEIGHT = (size.y) * .33;
			BLOCK_WIDTH = (size.x - MAJOR_SPACING) / 2.0;
			DESCR_FONT_HEIGHT = BLOCK_HEIGHT * 0.22;
			LOCKED_FONT_HEIGHT = BLOCK_HEIGHT * 0.4;
			
			initText();
			if (actor.id == data.conner.id)
			{
				makeBlock(data.saveData.connerHasQ ? actor.actions[0] : null, q, new Point(0, 0));	
				makeBlock(data.saveData.connerHasW ? actor.actions[1] : null, w, new Point(0, 1));
				makeBlock(data.saveData.connerHasE ? actor.actions[2] : null, e, new Point(1, 0));
				makeBlock(data.saveData.connerHasR ? actor.actions[3] : null, r, new Point(1, 1));
			}
			else
			{
				makeBlock(data.saveData.annieHasQ ? actor.actions[0] : null, q, new Point(0, 0));	
				makeBlock(data.saveData.annieHasW ? actor.actions[1] : null, w, new Point(0, 1));
				makeBlock(data.saveData.annieHasE ? actor.actions[2] : null, e, new Point(1, 0));
				makeBlock(data.saveData.annieHasR ? actor.actions[3] : null, r, new Point(1, 1));
			}
			this.visible = true;
			
		}
		
		private function initText():void
		{
			titleTextFormat = new TextFormat();
			titleTextFormat.size = TITLE_FONT_HEIGHT;
			titleTextFormat.color = 0x0;
			titleTextFormat.align = "left";
			
			blockTextFormat = new TextFormat();
			blockTextFormat.size = DESCR_FONT_HEIGHT;
			blockTextFormat.color = 0x0;
			blockTextFormat.align = "left";
			
			lockedTextFormat = new TextFormat();
			lockedTextFormat.size = LOCKED_FONT_HEIGHT;
			lockedTextFormat.color = 0x0;
			lockedTextFormat.align = "left";
			
			var field:TextField = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = titleTextFormat;
			field.text = "ABILITIES";
			field.width = size.x;
			field.x = 0.;
			field.y = 0.;
			field.name = "title";
			this.addChild(field);
			field.selectable = false;
		}
		
		private function makeBlock( action:Action, unit:Sprite, pos:Point ):void
		{
			unit.graphics.beginFill( 0x000000, 0.0 );
			unit.graphics.drawRect( 0., 0., BLOCK_WIDTH, BLOCK_HEIGHT);
			unit.graphics.endFill();
			
			addChild(unit);
			
			var field:TextField = new TextField();
			field.wordWrap = true;
			var icon:Sprite;
			if (action != null)
			{
				if (action.icon == null )
				{
					icon = new Sprite();
					icon.graphics.beginFill( 0x0, 1.0 );
					icon.graphics.drawRect( 0.0, 0.0, BLOCK_HEIGHT, BLOCK_HEIGHT );
					icon.graphics.endFill();
					
					unit.addChild(icon);
				}
				else
				{
					var bmp:Bitmap = action.icon;
					bmp.width = BLOCK_HEIGHT;
					unit.addChild(bmp);
				}
				
				field.defaultTextFormat = blockTextFormat;
				field.text = unit.name + " [" + action.actionName + "]\n" + action.description;
				field.y = 0.;
			}
			else
			{
				icon = new Sprite();
				icon.graphics.beginFill( LOCKED_ABILITY_COLOR, 1.0 );
				icon.graphics.drawRect( 0.0, 0.0, BLOCK_HEIGHT, BLOCK_HEIGHT );
				icon.graphics.endFill();
				
				unit.addChild(icon);
				field.defaultTextFormat = lockedTextFormat;
				field.text = "LOCKED";
				field.y = (BLOCK_HEIGHT / 2.) - (LOCKED_FONT_HEIGHT / 2.);
			}
			field.width = BLOCK_WIDTH - (BLOCK_HEIGHT + MINOR_SPACING);
			field.height = BLOCK_HEIGHT;
			field.x = BLOCK_HEIGHT + MINOR_SPACING;
			
			field.name = unit.name + "-field";
			unit.addChild(field);
			field.selectable = false;
			
			unit.x = pos.x * (MAJOR_SPACING + BLOCK_WIDTH);
			unit.y = (pos.y * (MAJOR_SPACING + BLOCK_HEIGHT)) + TITLE_FONT_HEIGHT + MAJOR_SPACING;
		}
		
		
	}

}