package org.xangarath.gemini.ui.pack 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.util.Pair;
	
	import org.xangarath.engine.dbg.DebugLog;
	
	import org.xangarath.gemini.events.InventoryEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.items.Inventory;
	import org.xangarath.gemini.items.ItemProperties;
	
	import org.xangarath.gemini.items.InventoryEntry;
	
	/**
	 * ...
	 * @author ...
	 */
	public class ItemList extends Sprite 
	{
		private static const TAG:String = "ItemList";
		
		private var size:Point;
		
		public static const ITEM_FOCUS_CHANGE_EVENT_TYPE:String = "ItemFocusChangeEvent";
		
		private static const SCALE_FACTOR:Number = 1.25;
		private var HORIZONTAL_SLOTS:int = 0;
		private var VERTICAL_SLOTS:int = 0;
		private var MARGIN:Number = 1. / 22.;
		private var ICON_SIZE:Number = 1. / 11.;
		private var V_SPACING:Number;
		private var H_SPACING:Number;
		
		private var _activeItemSprite:Sprite = null;
		private var bgColor:uint;
		
		// map bitmaps to inventory entries
		private var items:Array;
		
		private var itemLoc:Point;
		private var data:GeminiData;
		private var numEntries:uint = 0;
		public function ItemList( data:GeminiData, sz:Point, hSlots:int, vSlots:int, color:uint )
		{
			super();
			size = sz;
			HORIZONTAL_SLOTS = hSlots;
			VERTICAL_SLOTS = vSlots;
			bgColor = color;
			this.data = data;
			items = new Array();
			itemLoc = new Point();
		}
		
		public function init():void
		{
			MARGIN *= size.x;
			ICON_SIZE *= size.x;
			H_SPACING = size.x - (( 2. * MARGIN ) + ( ICON_SIZE * HORIZONTAL_SLOTS ));
			H_SPACING = H_SPACING / (HORIZONTAL_SLOTS - 1);
			V_SPACING = size.y - (( 2. * MARGIN ) + ( ICON_SIZE * VERTICAL_SLOTS ));
			V_SPACING = V_SPACING / (VERTICAL_SLOTS - 1);
			
			
			this.graphics.beginFill( bgColor, 1.0 );
			this.graphics.drawRect( 0., 0., size.x, size.y );
			this.graphics.endFill();
			
			clear();
		}
		
		public function get activeItem():ItemProperties
		{
			if (_activeItemSprite == null)
			{
				return null;
			}
			
			for ( var i:int = 0; i < items.length; i++ )
			{
				var p:Pair = items[i] as Pair;
				
				if ( p.second == _activeItemSprite )
				{
					return p.first as ItemProperties;
				}
			}
			
			DebugLog.getSingleton().w( TAG, "Could not find active item in item list" );
			return null;   
		}
		
		public function set activeItem( rhs:ItemProperties ):void
		{	
			for ( var i:int = 0; i < items.length; i++ )
			{
				var p:Pair = items[i] as Pair;
				
				if ( p.first == rhs )
				{
					setActiveItem( p.second as Sprite );
				}
			}
		}
		
		public function addItem( props:ItemProperties ):void
		{
			ErrorUtil.dbgAssertNonNull( TAG, props );
			var button:Sprite = new Sprite();
			button.graphics.beginFill( 0x0, 1.0 );
			button.graphics.drawRect( 0., 0., ICON_SIZE, ICON_SIZE );
			button.graphics.endFill();
			
			var bmp:Bitmap = data.resMgr.getBitmap( props.iconKey );
			if ( bmp == null )
			{
				DebugLog.getSingleton().e( TAG, "Could not find icon for key " + props.iconKey );
				throw new Error( TAG + " Could not find icon for key " + props.iconKey );
				return;
			}
			
			button.addChild(bmp);
			
			if ( bmp.width > bmp.height )
			{
				bmp.scaleX = ICON_SIZE / bmp.width;
				bmp.scaleY = bmp.scaleX;
			}
			else
			{
				bmp.scaleY = ICON_SIZE / bmp.height;
				bmp.scaleX = bmp.scaleY;
			}
			
			this.addChild(button);
			button.x = itemLoc.x;
			button.y = itemLoc.y;
			
			if (((++numEntries) % HORIZONTAL_SLOTS) == 0)
			{
				itemLoc.x = MARGIN;
				itemLoc.y += ICON_SIZE + V_SPACING;
			}
			else
			{
				itemLoc.x += ICON_SIZE + H_SPACING;
			}
			button.addEventListener(MouseEvent.CLICK, clickListener);
			items.push( new Pair( props, button ) );
		}
		
		private function setActiveItem( btn:Sprite ):void
		{
			if ( btn == _activeItemSprite )
			{
				return;
			}
			
			var widthDiff:Number;
			var heightDiff:Number;
			
			if ( _activeItemSprite != null )
			{
				widthDiff = _activeItemSprite.width;
				heightDiff = _activeItemSprite.height;
			
				_activeItemSprite.scaleX = 1.;
				_activeItemSprite.scaleY = 1.;
				
				widthDiff = widthDiff - _activeItemSprite.width;
				heightDiff = heightDiff - _activeItemSprite.height;
				
				_activeItemSprite.x += widthDiff / 2.;
				_activeItemSprite.y += heightDiff / 2.;
			}
			
			if ( btn == null )
			{
				_activeItemSprite = null;
				return;
			}
			
			widthDiff = btn.width;
			heightDiff = btn.height;
			
			btn.scaleX = SCALE_FACTOR;
			btn.scaleY = SCALE_FACTOR;
			
			widthDiff = btn.width - widthDiff;
			heightDiff = btn.height - heightDiff;
			
			btn.x -= widthDiff / 2.;
			btn.y -= heightDiff / 2.;
			
			_activeItemSprite = btn;
			
			this.dispatchEvent( new Event( ITEM_FOCUS_CHANGE_EVENT_TYPE ) );
		}
		
		public function clickListener(evt:MouseEvent):void
		{
			for ( var i:int = 0; i < items.length; i++ )
			{
				var btn:Sprite = (items[ i ] as Pair).second as Sprite;
				if ( btn == evt.target )
				{
					if ( btn != _activeItemSprite )
					{
						setActiveItem(btn);
					}
					return;
				}
			}
		}
		
		public function clear():void
		{	
			while ( items.length > 0 )
			{
				var p:Pair = items.pop();
				
				(p.second as Sprite).removeEventListener(MouseEvent.CLICK, clickListener);
			}
			_activeItemSprite = null;
			
			this.removeChildren();
			numEntries = 0;
			
			itemLoc.x = MARGIN;
			itemLoc.y = MARGIN;
		}
	}

}