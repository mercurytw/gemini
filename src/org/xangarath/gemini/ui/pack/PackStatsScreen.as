package org.xangarath.gemini.ui.pack 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * ...
	 * @author me, bitch
	 */
	public class PackStatsScreen extends Sprite implements IState 
	{
		private static const TAG:String = "PackStatsScreen";
		
		private static const BUTTON_NORM_COLOR:uint = 0x17ACBE;
		
		private static var BLOCK_WIDTH:Number = 0.30;
		private static var MARGIN_TOP:Number = 0.10;
		private static var MARGIN_BOTTOM:Number = 0.02;
		private static var BLOCK_MARGIN:Number = 0.045;
		private static var BUTTON_HEIGHT:Number = 0.2;
		private static var BUTTON_MARGIN:Number = 0.02;
		private static var BLOCK_SPACING:Number;
		private static var BLOCK_HEIGHT:Number;
		
		private var connorStats:StatBlock;
		private var annieStats:StatBlock = null;
		
		private var connorButton:SimpleButton;
		private var annieButton:SimpleButton;
		
		private var abilities:AbilitiesBlock;
		
		private var data:GeminiData;
		private var anniePortraitBmp:Bitmap;
		private var connorPortraitBmp:Bitmap;
		
		private var showingConner:Boolean = false;
		public function PackStatsScreen( data:GeminiData ) 
		{
			super();
			this.data = data;
			
			this.visible = false;
		}
		
		public function init():void
		{
			BLOCK_WIDTH *= this.parent.width;
			MARGIN_TOP *= this.parent.height;
			MARGIN_BOTTOM *= this.parent.height;
			BLOCK_MARGIN *= this.parent.width;
			BLOCK_SPACING = this.parent.width - ((BLOCK_WIDTH * 3.) + (BLOCK_MARGIN * 2.0));
			BLOCK_HEIGHT = parent.height - (MARGIN_TOP + MARGIN_BOTTOM);
			BUTTON_HEIGHT *= BLOCK_HEIGHT / 2.;
			BUTTON_MARGIN *= BLOCK_WIDTH;
			
			this.graphics.beginFill( 0x000000, 0.0 );
			this.graphics.drawRect( 0., 0., parent.width, parent.height );
			this.graphics.endFill();
			
			anniePortraitBmp = data.resMgr.getBitmap("anniePortrait.jpg");
			var aspectRatio:Number = BLOCK_WIDTH / BLOCK_HEIGHT;
			var newWidth:Number = aspectRatio * anniePortraitBmp.height;
			var offt:Number = (anniePortraitBmp.width - newWidth) / 2.;
			var dat:BitmapData = new BitmapData( newWidth, anniePortraitBmp.height);
			dat.copyPixels(anniePortraitBmp.bitmapData, new Rectangle( offt, 0., newWidth, anniePortraitBmp.height ), new Point(0.0, 0.0));
			var newBmp:Bitmap = new Bitmap( dat );
			anniePortraitBmp = newBmp;
			connorPortraitBmp = null;
			
			if (connorPortraitBmp == null)
			{
				this.graphics.beginFill( 0x000000 );
				this.graphics.drawRect( BLOCK_MARGIN, MARGIN_TOP, BLOCK_WIDTH, BLOCK_HEIGHT );
				this.graphics.endFill();
			}
			else
			{
				addChild(connorPortraitBmp);
				connorPortraitBmp.x = BLOCK_MARGIN;
				connorPortraitBmp.y = MARGIN_TOP;
			}
			
			addChild( anniePortraitBmp );
			anniePortraitBmp.x = BLOCK_MARGIN;
			anniePortraitBmp.y = MARGIN_TOP;
			anniePortraitBmp.visible = false;
			
			var numAmt:Number;
			numAmt = data.conner.stats.maxHP;
			
			connorStats = new StatBlock( data,
										 "CONNER",
										 data.conner,
										 numAmt,
										 new Point( BLOCK_WIDTH, BLOCK_HEIGHT / 2. ) );
			addChild(connorStats);
			connorStats.init();
			connorStats.x = BLOCK_MARGIN + BLOCK_WIDTH + BLOCK_SPACING;
			connorStats.y = MARGIN_TOP;
			
			initButtons();
			connorButton.x = BLOCK_MARGIN + BLOCK_WIDTH + BLOCK_SPACING + BUTTON_MARGIN;
			connorButton.y = (MARGIN_TOP + (BLOCK_HEIGHT / 4.0)) - (BUTTON_HEIGHT / 2.0);
			connorButton.visible = false;
			
			annieButton.x = BLOCK_MARGIN + ((BLOCK_WIDTH + BLOCK_SPACING) * 2) + BUTTON_MARGIN;
			annieButton.y = (MARGIN_TOP + (BLOCK_HEIGHT / 4.0)) - (BUTTON_HEIGHT / 2.0);
			annieButton.visible = false;
			
			this.abilities = new AbilitiesBlock(new Point((BLOCK_WIDTH * 2) + BLOCK_SPACING, (BLOCK_HEIGHT / 2.)), data);
			this.addChild(abilities);
			abilities.init(data.conner);
			
			abilities.x = BLOCK_MARGIN + BLOCK_WIDTH + BLOCK_SPACING;
			abilities.y = MARGIN_TOP + BLOCK_HEIGHT / 2.;
			abilities.visible = true;
			
			showingConner = true;
		}
		
		public function initButtons():void
		{	
			var connorBtnShape:Sprite = new Sprite();
			connorBtnShape.graphics.beginFill( BUTTON_NORM_COLOR );
			connorBtnShape.graphics.drawRect( 0., 0., BLOCK_WIDTH - (BUTTON_MARGIN * 2.0), BUTTON_HEIGHT );
			connorBtnShape.graphics.endFill();
			
			var center:TextFormat = new TextFormat();
			center.size = BUTTON_HEIGHT - (BUTTON_HEIGHT * 0.2);
			center.color = 0x0;
			center.align = "center";
			
			var field:TextField = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = center;
			field.text = ">CONNER";
			field.width = BLOCK_WIDTH - (BUTTON_MARGIN * 2.0);
			field.height = BUTTON_HEIGHT;
			field.x = 0.;
			field.y = 0.;
			field.name = "connorBtnText";
			connorBtnShape.addChild(field);
			field.selectable = false;
			
			connorButton = new SimpleButton( connorBtnShape, connorBtnShape, connorBtnShape, connorBtnShape );
			this.addChild(connorButton);
			
			connorBtnShape = new Sprite();
			connorBtnShape.graphics.clear();
			connorBtnShape.graphics.beginFill( BUTTON_NORM_COLOR );
			connorBtnShape.graphics.drawRect( 0., 0., BLOCK_WIDTH - (BUTTON_MARGIN * 2.0), BUTTON_HEIGHT );
			connorBtnShape.graphics.endFill();
			
			field = new TextField();
			field.wordWrap = false;
			field.defaultTextFormat = center;
			field.text = ">ANNIE";
			field.width = BLOCK_WIDTH - (BUTTON_MARGIN * 2.0);
			field.height = BUTTON_HEIGHT;
			field.x = 0.;
			field.y = 0.;
			field.name = "annieBtnText";
			connorBtnShape.addChild(field);
			field.selectable = false;
			
			annieButton = new SimpleButton( connorBtnShape, connorBtnShape, connorBtnShape, connorBtnShape );
			this.addChild(annieButton);
		}
		
		public function showConnorStats( e:MouseEvent ):void
		{
			connorButton.removeEventListener(MouseEvent.CLICK, showConnorStats);
			annieButton.addEventListener(MouseEvent.CLICK, showAnnieStats);
			annieStats.visible = false;
			connorStats.visible = true;
			connorButton.visible = false;
			annieButton.visible = true
			annieStats.recalc();
			
			abilities.init(data.conner);
			
			anniePortraitBmp.visible = false;
			
			if (connorPortraitBmp != null)
			{
				connorPortraitBmp.visible = true;
			}
			
			showingConner = true;
		}
		
		public function showAnnieStats( e:MouseEvent ):void
		{
			annieButton.removeEventListener(MouseEvent.CLICK, showAnnieStats);
			connorButton.addEventListener(MouseEvent.CLICK, showConnorStats);
			connorStats.visible = false;
			annieStats.visible = true;
			annieButton.visible = false;
			connorButton.visible = true;
			connorStats.recalc();
			
			abilities.init( (data.party.atBat == data.conner) ? data.party.onDeck : data.party.atBat );
			
			if (connorPortraitBmp != null)
			{
				connorPortraitBmp.visible = false;
			}
			
			showingConner = false;
			
			anniePortraitBmp.visible = true;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			connorStats.recalc();
			
			if ( ( annieStats == null ) && ( data.party.onDeck != null ) )
			{
				var a:Creature = ( data.party.onDeck != data.conner ) ? data.party.onDeck : data.party.atBat;
				annieStats = new StatBlock( data,
											"ANNIE",
											a,
											a.stats.maxHP,
											new Point( BLOCK_WIDTH, BLOCK_HEIGHT / 2. ) );
				addChild(annieStats);
				annieStats.init();
				annieStats.x = BLOCK_MARGIN + ( ( BLOCK_WIDTH + BLOCK_SPACING ) * 2.0 );
				annieStats.y = MARGIN_TOP;
				
				annieStats.visible = false;
				annieButton.visible = true;
				annieButton.addEventListener(MouseEvent.CLICK, showAnnieStats);
			}
			else if ( annieStats != null )
			{
				annieStats.recalc();
			}
			
			showingConner ? abilities.init(data.conner) : abilities.init(data.annie);
			
			this.visible = true;
		}
		
		public function teardown():void 
		{
			this.visible = false;
		}
		
	}

}