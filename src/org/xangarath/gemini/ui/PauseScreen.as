package org.xangarath.gemini.ui 
{
	import flash.display.Sprite;
	import flash.display3D.textures.Texture;
	import flash.events.MouseEvent;
	import flash.text.StaticText;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import org.xangarath.gemini.GeminiData;
	/**
	 * It's the pause screen bitches
	 * @author Jeff
	 */
	public class PauseScreen extends Sprite 
	{
		private static const TAG:String = "PauseScreen";
		private var isActive:Boolean = false;
		private var title:TextField;
		private var save:TextField;
		private var load:TextField;
		private var options:TextField;
		private var exit:TextField;
		private var data:GeminiData;
		
		private const TITLE_FONT_SIZE:int = 40;
		private const OPT_FONT_SIZE:int = 30;
		
		private var normalFmt:TextFormat;
		private var selectedFmt:TextFormat;
		private var unavailFmt:TextFormat;
		public function PauseScreen( data:GeminiData ) 
		{
			this.visible = false;
			this.data = data;
		}
		
		public function init():void
		{
			this.graphics.beginFill( 0x000000, 0.8 );
			this.graphics.drawRect( 0., 0., stage.stageWidth, stage.stageHeight );
			this.graphics.endFill();
			
			initText();
			exit.setTextFormat(selectedFmt);
		}
		
		private function initText():void
		{
			title = new TextField();
			var fmt:TextFormat = title.getTextFormat();
			fmt.size = TITLE_FONT_SIZE;
			fmt.color = 0xFFFFFF;
			fmt.align = "center";
			
			normalFmt = new TextFormat();
			normalFmt.size = OPT_FONT_SIZE;
			normalFmt.color = 0xFFFFFF;
			normalFmt.align = "center";
			
			selectedFmt = new TextFormat();
			selectedFmt.size = OPT_FONT_SIZE;
			selectedFmt.color = 0xFFFFFF;
			selectedFmt.align = "center";
			selectedFmt.bold = true;
			
			unavailFmt = new TextFormat();
			unavailFmt.size = OPT_FONT_SIZE;
			unavailFmt.color = 0xA4A4A4;
			unavailFmt.align = "center";
			
			
			title.wordWrap = false;
			title.defaultTextFormat = fmt;
			title.text = "PAUSED";
			title.width = stage.stageWidth + 2.;
			title.y = stage.stageHeight / 10;
			this.addChild(title);
			title.selectable = false;
			
			var initOfft:int = title.y + ( fmt.size + fmt.leading );
			initOfft *= 2;
			
			save = new TextField();
			load = new TextField();
			options = new TextField();
			exit = new TextField();
			save.defaultTextFormat = load.defaultTextFormat = options.defaultTextFormat = unavailFmt;
			exit.defaultTextFormat = normalFmt;
			
			save.text = "Save Game";
			load.text = "Load Game";
			options.text = "Options";
			exit.text = "Exit Game";
			
			save.width = load.width = options.width = exit.width = stage.stageWidth + 2.;
			save.y = initOfft;
			load.y = initOfft + (2 * (unavailFmt.size + unavailFmt.leading));
			options.y = initOfft + (4 * (unavailFmt.size + unavailFmt.leading));
			exit.y = initOfft + (6 * (unavailFmt.size + unavailFmt.leading));
			
			this.addChild(save);
			this.addChild(load);
			this.addChild(options);
			this.addChild(exit);
			save.selectable = load.selectable = options.selectable = exit.selectable = false;
		}
		
		public function clickListener(event:MouseEvent):void
		{
			if (event.target == exit)
			{
				data.requestExit = true;
			}
		}
		
		public function toggle():void
		{
			if (isActive)
			{
				exit.removeEventListener(MouseEvent.MOUSE_UP, clickListener);
			}
			else
			{
				exit.addEventListener(MouseEvent.MOUSE_UP, clickListener);
			}
			this.visible = isActive = !isActive;
		}
	}

}