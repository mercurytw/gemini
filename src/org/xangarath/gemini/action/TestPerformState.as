package org.xangarath.gemini.action 
{
	import flash.events.EventDispatcher;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	
	/**
	 * State for testing. delete this
	 * @author Jeff Cochran
	 */
	public class TestPerformState extends EventDispatcher implements IState, ITickable
	{
		public function TestPerformState() 
		{
		}
		
		public function setup():void
		{
			trace("Bleep bloop!");
			this.dispatchEvent(new StateChangeEvent("cooldown", "StateChangeEvent"));
		}
		
		public function teardown():void
		{
			
		}
		
		public function tick():void
		{
			
		}
	}

}