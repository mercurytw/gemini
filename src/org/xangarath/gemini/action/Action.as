package org.xangarath.gemini.action 
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.geom.Point;
	import org.xangarath.engine.ActionWidget;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.util.Serializer;
	import org.xangarath.engine.statemachine.StateMachine;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.interfaces.IActionState;
	
	/**
	 * Represents an action that a creature can take.
	 * @author Jeffrey Cochran
	 */
	public class Action extends StateMachine implements ITickable
	{
		private var name:String = "";
		private var _widget:ActionWidget = null;
		private var readiness:Object;
		private var _cost:Number;
		public var bmp:DisplayObject;
		public var bmpPlacement:Point;
		public var description:String;
		public var icon:Bitmap;
		private var _enabled:Boolean = true;
		public function Action(n:String, delay:Number, cost:Number, perfState:IState, cooldown:Number, data:GeminiData, b:DisplayObject = null, bmpPlacement:Point = null) 
		{
			super();
			this.name = n;
			if ( cost < 0.0 ) { cost = 0.0; }
			else if ( cost > 1.0 ) { cost = 1.0; }
			this._cost = cost;
			this.bmp = b;
			this.bmpPlacement = bmpPlacement;
			this.icon = null;
			this.description = "";
			readiness = new Object();
			readiness.value = 1.0;
			addState("cooldown", new CooldownState( cooldown, null, readiness, data, b ), true);
			addState("delay", new DelayState(delay, _cost, readiness, data, b));
			addState("perform", perfState);	
		}
		
		public function get actionName():String
		{
			return name;
		}
		
		public function set cost( v:Number ):void
		{
			_cost = v;
			(mStates["delay"] as DelayState).cost = v;
		}
		
		public function get cost():Number
		{
			return _cost;
		}
		
		public function set delay( v:Number ):void
		{
			(mStates["delay"] as DelayState).delayTime = v;
		}
		
		public function get delay():Number
		{
			return (mStates["delay"] as DelayState).delayTime;
		}
		
		public function set cooldown( v:Number ):void
		{
			(mStates["cooldown"] as CooldownState).cooldownTime = v;
		}
		
		public function get cooldown():Number
		{
			return (mStates["cooldown"] as CooldownState).cooldownTime;
		}
		
		public function clearCooldown():void
		{
			readiness.value = 1.0;
			if ( _widget != null )
			{
				_widget.barWidth = 1.0;
			}
		}
		
		public function isReady():Boolean
		{
			return ( mActiveState == mStates["cooldown"] ) && ( readiness.value >= _cost );
		}
		
		public function get percentReady():Number
		{
			return readiness.value;
		}
		
		public function isBlocking():Boolean
		{
			return (mActiveState as IActionState).getIsBlocking();
		}
		
		public function getDoesAllowMovement():Number
		{
			return (mActiveState as IActionState).getDoesAllowMovement();
		}
		
		public function perform():Boolean
		{
			if ( ( mActiveState == mStates["cooldown"] ) && ( readiness.value >= _cost ) )
			{
				enterState("delay");
				return true;
			}
			return false;
		}
		
		public function addActionListener( type:String, callback:Function ):void
		{
			(mStates[ "perform" ] as EventDispatcher).addEventListener( type, callback );
		}
		
		public function removeActionListener( type:String, callback:Function ):void
		{
			(mStates[ "perform" ] as EventDispatcher).removeEventListener( type, callback );
		}
		
		public function get widget():ActionWidget
		{
			return _widget;
		}
		
		public function set widget(w:ActionWidget):void
		{
			_widget = w;
			if ( w != null)
			{	
				_widget.title = name;
			}
			(mStates["delay"] as DelayState).widget = w;
			(mStates["cooldown"] as CooldownState).widget = w;
		}
		
		// throw an event
		public function set enabled( b:Boolean ):void
		{
			if (_widget != null)
			{
				_widget.visible = b;
			}
			_enabled = b;
		}
		
		public function get enabled():Boolean
		{
			return _enabled;
		}
		
		public function toString():String
		{
			return Serializer.seralizeFormatted(this, "Action", "delay", "cost", "cooldown") + ", " + mStates["perform"];
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public override function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			bmp = null;
			bmpPlacement = null;
			description = null;
			icon = null;
			_widget = null;
			name = null;
			readiness = null;
			
			super.destroy();
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed )
			{
				return;
			}
			
			(mActiveState as ITickable).tick( deltaTime );
		}
		
	}

}