package org.xangarath.gemini.action 
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	import org.xangarath.engine.ActionWidget;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IActionState;
	
	
	/**
	 * Represents delay before an action is taken. In the real game, this would start an animation.
	 * For us, we're just going to optionally make a bitmap visible
	 * @author Jeffrey Cochran
	 */
	public class DelayState extends EventDispatcher implements IState, ITickable, IActionState, IDestructable 
	{
		private static const TAG:String = "DelayState";
		public var delayTime:Number;
		private var b:DisplayObject;
		private var timePassed:Number = 0.0;
		private var data:GeminiData;
		public var widget:ActionWidget = null;
		public var cost:Number;
		public var readinessHolder:Object;
		private var isConstructed:Boolean = true;
		public function DelayState( time:Number, cost:Number, readiness:Object, data:GeminiData, bmp:DisplayObject = null ) 
		{
			this.delayTime = time;
			this.cost = cost;
			b = bmp;
			this.data = data;
			this.readinessHolder = readiness;
		}
		
		/* INTERFACE gemini.IActionState */
		
		public function getDoesAllowMovement():Number
		{
			return 0.0;
		}
		
		public function getIsBlocking():Boolean
		{
			return true;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			
			timePassed = 0.0;
			if ( b != null )
			{
				b.visible = true;
			}
			readinessHolder.value -= cost;
			if ( widget != null )
			{
				widget.barWidth = readinessHolder.value;
			}
		}
		
		public function teardown():void 
		{
			
		}
		
		/* INTERFACE engine.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			readinessHolder = null;
			widget = null;
			b = null;
			data = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( ( timePassed += deltaTime ) >= delayTime )
			{
				this.dispatchEvent(new StateChangeEvent("perform", "StateChangeEvent"));
			}
		}
	}

}