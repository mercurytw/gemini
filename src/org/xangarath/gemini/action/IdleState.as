package org.xangarath.gemini.action 
{
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.interfaces.IActionState;
	
	/**
	 * Do nothing state
	 * @author Jeff Cochran
	 */
	public class IdleState implements IState, ITickable, IActionState
	{
		public function IdleState() 
		{
		}
		
		/* INTERFACE gemini.IActionState */
		
		public function getDoesAllowMovement():Number
		{
			return 1.0;
		}
		
		public function getIsBlocking():Boolean
		{
			return false;
		}
		
		public function setup():void
		{
			
		}
		
		public function teardown():void
		{
			
		}
		
		public function tick():void
		{
			
		}
	}

}