package org.xangarath.gemini.action 
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	import org.xangarath.engine.ActionWidget;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.SharedData;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.gemini.interfaces.IActionState;
	
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class CooldownState extends EventDispatcher implements IState, ITickable, IActionState, IDestructable 
	{
		private static const TAG:String = "CooldownState";
		public var cooldownTime:Number;
		private var cooldownAgg:Number;
		private var data:SharedData;
		public var widget:ActionWidget = null;
		private var bmp:DisplayObject;
		public var readinessHolder:Object;
		private var isConstructed:Boolean = true;
		public function CooldownState(time:Number, widget:ActionWidget, readiness:Object, data:SharedData, b:DisplayObject = null) 
		{
			cooldownTime = time;
			this.widget = widget;
			this.data = data;
			bmp = b;
			this.readinessHolder = readiness;
		}
		
		/* INTERFACE gemini.IActionState */
		
		public function getDoesAllowMovement():Number
		{
			return 1.0;
		}
		
		public function getIsBlocking():Boolean
		{
			return false;
		}
		
		/* INTERFACE engine.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			cooldownAgg = readinessHolder.value * cooldownTime;
			if ( bmp != null )
			{
				bmp.visible = false;
			}
		}
		
		public function teardown():void 
		{
			
		}
		
		/* INTERFACE engine.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			readinessHolder = null;
			widget = null;
			bmp = null;
			data = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE engine.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{		
			if ( !isConstructed )
			{
				return;
			}
			
			if ( readinessHolder.value < 1.0 )
			{
				cooldownAgg += deltaTime;
				readinessHolder.value = cooldownAgg / cooldownTime;
				if ( widget != null )
				{
					widget.barWidth = readinessHolder.value;
				}
			}
			else
			{
				readinessHolder.value = 1.0;
			}
		}
	}

}