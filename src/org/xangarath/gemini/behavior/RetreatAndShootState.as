package org.xangarath.gemini.behavior 
{
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	/**
	 * Retreat while attacking
	 * @author Jeffrey Cochran
	 */
	public class RetreatAndShootState extends EventDispatcher implements ITickable, IState, IDestructable
	{
		private static const TAG:String = "RetreatAndShootState";
		private var attack:Function;
		private var moveSpeed:Number;
		private var actor:Creature;
		private var data:GeminiData;
		private var screenRect:Rectangle;
		private var stopPercent:Number = 0.75;
		private var isConstructed:Boolean = true;
		public function RetreatAndShootState( speed:Number, attack:Function, actor:Creature, data:GeminiData ) 
		{
			super();
			this.attack = attack;
			this.actor = actor;
			this.data = data;
			this.moveSpeed = speed;
			screenRect = new Rectangle();
			screenRect.copyFrom( data.screenspace );
			screenRect.width *= stopPercent;
			screenRect.height *= stopPercent;
		}
		
		public function setup():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
		}
		
		public function teardown():void
		{
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			actor = null;
			attack = null;
			
			screenRect = null;
			
			data = null;
			
			isConstructed = false;
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( !isConstructed )
			{
				return;
			}
			screenRect.x = data.playerAtBat.x - ( screenRect.width / 2.0 );
			screenRect.y = data.playerAtBat.y - ( screenRect.width / 2.0 );
			
			if ( screenRect.containsRect( actor.getBoundingVolume() ) )
			{
				var oldx:Number = actor.x;
				var oldy:Number = actor.y;
				var newx:Number = actor.x - data.playerAtBat.x;
				var newy:Number = actor.y - data.playerAtBat.y;
				var mag:Number = Math.sqrt( Maths.distanceSq( actor.pos, data.playerAtBat.pos ) );
				newx /= mag;
				newy /= mag;
				actor.move( newx * moveSpeed * data.GRID_UNIT_SIZE * deltaTime, newy * moveSpeed * data.GRID_UNIT_SIZE * deltaTime );
			}
			
			attack( deltaTime );
		}
		
	}

}