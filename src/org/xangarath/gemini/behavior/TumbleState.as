package org.xangarath.gemini.behavior 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * Makes mosters tumble away from the attacker, then enters a stun state when applicable
	 * @author Jeffrey Cochran
	 */
	public class TumbleState extends EventDispatcher implements IState, ITickable, IDestructable 
	{
		private static const TAG:String = "TumbleState";
		private static const TUMBLE_SPEED:Number = 400.0;
		private var stunStateName:String = null;
		public var stunTime:Number; // time for stun
		public var direction:Point = new Point();
		public var distanceSquared:Number;
		private var act:Creature;
		private var initalPos:Point = new Point();
		private var stunState:StunState;
		private var lastPos:Point;
		private var data:GeminiData;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function TumbleState( actor:Creature, data:GeminiData, stunStateName:String = null, state:StunState = null ) 
		{
			super();
			act = actor;
			this.stunStateName = stunStateName;
			this.stunState = state;
			this.data = data;
			lastPos = new Point();
		}
		
		private function handleCollision( e:Event ):void
		{
			var collision:CollisionEvent = e as CollisionEvent;
			
			if ( collision.instigator.team != act.team )
			{
				exitState();
			}
		}
		
		private function exitState():void
		{
			if ( ( stunStateName != null ) && ( stunTime > 0.0 ) )
			{
				dispatchEvent( new StateChangeEvent( stunStateName, StateChangeEvent.STATE_CHANGE ) );
				stunState.stunTimeRemaining = stunTime;
			}
			else
			{
				dispatchEvent( new StateChangeEvent( "active", StateChangeEvent.STATE_CHANGE ) );
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "Tried to setup destroyed state" );
			
			if ( isSetup )
			{
				return;
			}
			
			initalPos.setTo( act.pos.x, act.pos.y );
			act.addEventListener( CollisionEvent.COLLISION, handleCollision );
			stunTime = 0.0;
			
			isSetup = true;
		}
		
		public function teardown():void 
		{
			if ( !isSetup )
			{
				return;
			}
			
			act.removeEventListener( CollisionEvent.COLLISION, handleCollision );
			
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			stunStateName = null;
			direction = null;
			act = null;
			initalPos = null;
			stunState = null;
			lastPos = null;
			data = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed || !isSetup )
			{
				return;
			}
			
			lastPos.copyFrom(act.pos);
			act.move( direction.x * TUMBLE_SPEED * deltaTime, direction.y * TUMBLE_SPEED * deltaTime );
			
			if ( Maths.distanceSq( act.pos, initalPos ) >= distanceSquared )
			{
				exitState();
			}
			else if ( (data.terrainMgr.getTerrainForRectangle(act.getBoundingVolume()) & act.badWallMask ) != 0 )
			{
				exitState();
			}
		}
		
	}

}