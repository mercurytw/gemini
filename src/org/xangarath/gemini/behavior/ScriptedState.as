package org.xangarath.gemini.behavior 
{
	import flash.events.Event;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.script.ScriptManager;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * A behavior state whose logic is implemented in lua
	 * @author Jeff Cochran
	 */
	public class ScriptedState implements IState, ITickable, IDestructable
	{
		private static const TAG:String = "ScriptedState";
		public var setupCback:Function = null;
		public var teardownCback:Function = null;
		public var tickCback:Function = null;
		public var eventCback:Function = null;
		public var userData:Object = null;
		private var shouldReset:Boolean = false;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		private var script:String = null;
		private var data:GeminiData = null;
		public function ScriptedState( data:GeminiData, udata:Object = null ) 
		{
			this.data = data;
			userData = udata;
		}
		
		public function applyScript( luaScript:String, data:GeminiData ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			ErrorUtil.dbgAssertNonNull( TAG, luaScript );
			
			this.setupCback = null;
			this.teardownCback = null;
			this.tickCback = null;
			this.eventCback = null;
			if (luaScript != null)
			{
				data.scriptMgr.doReloadableFile( ScriptManager.VFS_SCRIPT_ROOT + luaScript, this );
				script = luaScript;
			}
		}
		
		public function scriptRebindCallback():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( isSetup )
			{
				shouldReset = true;
				teardown();
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			if ( !isSetup )
			{
				if ( setupCback != null )
				{
					setupCback.apply( this, null );
				}
				isSetup = true;
			}
		}
		
		public function teardown():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( isSetup )
			{
				if ( teardownCback != null )
				{
					teardownCback.apply( this, null );
				}
				isSetup = false;
			}
		}
		
		public function eventHandler(e:Event):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( eventCback != null )
			{
				eventCback.apply( this, [ e ] );
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			eventCback = null;
			setupCback = null;
			teardownCback = null;
			tickCback = null;
			
			if ( script != null )
			{
				data.scriptMgr.undoReloadableFile( ScriptManager.VFS_SCRIPT_ROOT + script );
				script = null;
			}
			
			data = null;
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void
		{
			CONFIG::debug
			{
				if ( !isSetup )
				{
					DebugLog.getSingleton().w( TAG, "Tick called while object was not set up" );
				}
			}
			
			if ( !isConstructed  )
			{
				return;
			}
			
			if ( shouldReset )
			{
				setup();
				shouldReset = false;
			}
			
			if ( tickCback != null )
			{
				tickCback.apply( this, [ deltaTime ] );
			}
		}
	}

}