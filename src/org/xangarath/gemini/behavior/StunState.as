package org.xangarath.gemini.behavior 
{
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * Behavior for a stunned enemy
	 * @author Jeffrey Cochran
	 */
	public class StunState extends EventDispatcher implements ITickable, IState, IDestructable 
	{
		private static const TAG:String = "StunState";
		public var stunTimeRemaining:Number = 0.0;
		private var actor:Creature;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function StunState( actor:Creature ) 
		{
			super();
			this.actor = actor;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "Tried to setup destroyed state" );
			isSetup = true;
		}
		
		public function teardown():void 
		{
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			actor = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed || !isSetup )
			{
				return;
			}
			
			if (stunTimeRemaining > 0.0)
			{
				stunTimeRemaining = Math.max( stunTimeRemaining - deltaTime, 0.0 );
			}
			if (stunTimeRemaining <= 0.0)
			{
				dispatchEvent( new StateChangeEvent( "active", StateChangeEvent.STATE_CHANGE ) );
			}
		}
		
	}

}