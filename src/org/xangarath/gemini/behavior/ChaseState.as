package org.xangarath.gemini.behavior 
{
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.engine.world.astar.AStarSearcher;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.graphics.RenderHelper;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	
	/**
	 * Behavior of a creature chasing the player
	 * @author Jeffrey Cochran
	 */
	public class ChaseState extends EventDispatcher implements ITickable, IState, IDestructable 
	{
		private static const TAG:String = "ChaseState";
		private const ASTAR_QUERY_FREQUENCY_SECONDS:Number = 1.0;
		private var moveSpeed:Number;
		private var actor:Creature;
		private var data:GeminiData;
		private var astarResultArray:Array = null;
		private var timeAggSeconds:Number = 0.0;
		private var pathfinder:AStarSearcher;
		private var mySize:Point;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function ChaseState( speed:Number, actor:Creature, data:GeminiData ) 
		{
			super();
			moveSpeed = speed;
			this.actor = actor;
			this.data = data;
			this.mySize = new Point( actor.getBoundingVolume().width, actor.getBoundingVolume().height );
			pathfinder = new AStarSearcher( data.GRID_UNIT_SIZE );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "Tried to setup destroyed state" );
			timeAggSeconds = 0.0;
			isSetup = true;
		}
		
		public function teardown():void 
		{
			if ( !isSetup )
			{
				return;
			}
			if ( astarResultArray != null )
			{
				astarResultArray.splice( 0, astarResultArray.length );
			}
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			if ( astarResultArray != null )
			{
				astarResultArray.length = 0;
			}
			astarResultArray = null;
			mySize = null;
			tmpRect = null;
			
			pathfinder.destroy();
			pathfinder = null;
			
			actor = null;
			data = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		private var tmpRect:Rectangle = new Rectangle();
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed || !isSetup )
			{
				return;
			}
			
			var tmpPoint:Point;
			var tmpArr:Array;
			
			if ( ( timeAggSeconds += deltaTime ) >= ASTAR_QUERY_FREQUENCY_SECONDS )
			{
				timeAggSeconds = 0.0;
				
				tmpArr = pathfinder.findPath( actor.pos, data.playerAtBat.pos, mySize, data.terrainMgr, actor.badWallMask );
				
				if ( tmpArr != null )
				{
					if ( astarResultArray != null )
					{
						astarResultArray.splice( 0, astarResultArray.length );
					}
				
					astarResultArray = tmpArr;
					
					if ( ( astarResultArray.length >= 2 ) && 
						( Maths.distanceSq( astarResultArray[0] as Point, data.playerAtBat.pos ) > Maths.distanceSq( actor.pos, data.playerAtBat.pos ) ) )
					{
						tmpPoint = astarResultArray[ 1 ] as Point;
						tmpRect.copyFrom( actor.getBoundingVolume() );
						tmpRect.offset( tmpPoint.x - actor.pos.x, tmpPoint.y - actor.pos.y );
						if ( ( data.terrainMgr.getTerrainForAABBMove( actor.getBoundingVolume(), tmpRect ) & actor.badWallMask ) == 0 )
						{
							astarResultArray.splice( 0, 1 );
						}
					}
				}
				
			}
			
			if ( ( astarResultArray != null ) && ( astarResultArray.length != 0 ) )
			{
				if ( CONFIG::debug )
				{
					RenderHelper.getSingleton().DrawLines( data.sceneMgr.debugLayer, 0xFF0000, 7.0, astarResultArray );
				}
				
				if ( Maths.distanceSq( actor.pos, astarResultArray[0] as Point ) < Math.pow( moveSpeed * data.GRID_UNIT_SIZE * 0.04 , 2) )
				{
					var tmpPt:Point = astarResultArray.splice( 0, 1 )[0] as Point;
					actor.setPos(tmpPt.x, tmpPt.y);
				}
				if ( astarResultArray.length != 0 )
				{
					actor.setDirection( (astarResultArray[ 0 ] as Point).x - actor.pos.x, (astarResultArray[ 0 ] as Point).y - actor.pos.y );
				}
			}
			else
			{
				actor.face( data.playerAtBat );
			}
			
			actor.move( actor.dirX * moveSpeed * data.GRID_UNIT_SIZE * deltaTime, actor.dirY * moveSpeed * data.GRID_UNIT_SIZE * deltaTime );
		}
		
	}

}