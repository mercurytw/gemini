package org.xangarath.gemini.behavior 
{
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * Encapsulates behavior of a creature when it is not activated, and waiting to activate
	 * @author Jeffrey Cochran
	 */
	public class InactiveState extends EventDispatcher implements ITickable, IState, IDestructable 
	{
		private var actor:Creature;
		private var data:GeminiData;
		private var wait:Boolean;
		private var activeStateName:String;
		private var isSetup:Boolean = false;
		public function InactiveState( waitForLineofSight:Boolean, act:Creature, data:GeminiData, name:String = "active" ) 
		{
			super();
			wait = waitForLineofSight;
			actor = act;
			this.data = data;
			activeStateName = name;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			isSetup = true;
		}
		
		public function teardown():void 
		{
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			teardown();
			actor = null;
			data = null;
			activeStateName = null;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( !isSetup )
			{
				return;
			}
			
			if ( data.screenspace.containsPoint( actor.pos ) )
			{
				dispatchEvent( new StateChangeEvent( activeStateName, StateChangeEvent.STATE_CHANGE ) );
			}
		}
		
	}

}