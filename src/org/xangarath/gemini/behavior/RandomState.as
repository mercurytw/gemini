package org.xangarath.gemini.behavior 
{
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * Behavior reflecting the creature moving toward the player, with some random movement as well
	 * @author Jeffrey Cochran
	 */
	public class RandomState extends EventDispatcher implements ITickable, IState, IDestructable 
	{
		private static const TAG:String = "RandomState";
		private var moveSpeed:Number;
		private var actor:Creature;
		private var data:GeminiData;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function RandomState( speed:Number, actor:Creature, data:GeminiData ) 
		{
			super();
			moveSpeed = speed;
			this.actor = actor;
			this.data = data;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "Tried to setup non-constructed state!" );
			
			isSetup = true;
		}
		
		public function teardown():void 
		{
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( isSetup )
			{
				teardown();
			}
			
			if ( !isConstructed )
			{
				return;
			}
			
			actor = null;
			data = null;
			dirMoving = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		private var dirMoving:Point = new Point();
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed || !isSetup )
			{
				return;
			}
			
			dirMoving.setTo( Math.random(), Math.random() ); 
			if ( Math.round( Math.random() ) == 1.0 )
			{
				dirMoving.x *= -1.0;
			}
			if ( Math.round( Math.random() ) == 1.0 )
			{
				dirMoving.y *= -1.0;
			}
			
			dirMoving.x += (data.playerAtBat.x - actor.pos.x) / 400.0;
			dirMoving.y += (data.playerAtBat.y - actor.pos.y) / 400.0;
			
			dirMoving.normalize( 1.0 );
			
			actor.move( dirMoving.x * moveSpeed * data.GRID_UNIT_SIZE * deltaTime, dirMoving.y * moveSpeed * data.GRID_UNIT_SIZE * deltaTime );
		}
		
	}

}