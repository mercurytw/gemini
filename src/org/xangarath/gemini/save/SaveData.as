package org.xangarath.gemini.save 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import org.xangarath.gemini.events.GameMilestoneBus;
	/**
	 * A bunch of serializable game state.
	 * @author Jeff Cochran
	 */
	public class SaveData extends EventDispatcher
	{
		public var connerHasQ:Boolean = false;
		public var connerHasW:Boolean = false;
		public var connerHasE:Boolean = false;
		public var connerHasR:Boolean = false;
		public var annieHasQ:Boolean = false;
		public var annieHasW:Boolean = false;
		public var annieHasE:Boolean = false;
		public var annieHasR:Boolean = false;
		public function SaveData()
		{
			GameMilestoneBus.getSingleton().addEventListener(GameMilestoneBus.ANNIE_GET_Q, handleMilestone);
			GameMilestoneBus.getSingleton().addEventListener(GameMilestoneBus.ANNIE_GET_W, handleMilestone);
			GameMilestoneBus.getSingleton().addEventListener(GameMilestoneBus.ANNIE_GET_E, handleMilestone);
			GameMilestoneBus.getSingleton().addEventListener(GameMilestoneBus.ANNIE_GET_R, handleMilestone);
			GameMilestoneBus.getSingleton().addEventListener(GameMilestoneBus.CONNER_GET_Q, handleMilestone);
			GameMilestoneBus.getSingleton().addEventListener(GameMilestoneBus.CONNER_GET_W, handleMilestone);
			GameMilestoneBus.getSingleton().addEventListener(GameMilestoneBus.CONNER_GET_E, handleMilestone);
			GameMilestoneBus.getSingleton().addEventListener(GameMilestoneBus.CONNER_GET_R, handleMilestone);
		}
		
		private function handleMilestone(e:Event):void
		{
			switch (e.type)
			{
				case GameMilestoneBus.ANNIE_GET_Q:
					annieHasQ = true;
					break;
				case GameMilestoneBus.ANNIE_GET_W:
					annieHasW = true;
					break;
				case GameMilestoneBus.ANNIE_GET_E:
					annieHasE = true;
					break;
				case GameMilestoneBus.ANNIE_GET_R:
					annieHasR = true;
					break;
					
				case GameMilestoneBus.CONNER_GET_Q:
					connerHasQ = true;
					break;
				case GameMilestoneBus.CONNER_GET_W:
					connerHasW = true;
					break;
					
				case GameMilestoneBus.CONNER_GET_E:
					connerHasE = true;
					break;
				case GameMilestoneBus.CONNER_GET_R:
					connerHasR = true;
					break;
			}
		}
		
	}

}