package org.xangarath.gemini.gizmos 
{
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.Hotspot;
	import org.xangarath.engine.SharedData;
	import org.xangarath.engine.tiled.TiledObject;
	import org.xangarath.gemini.events.LevelChangeEvent;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * A teleport hotspot simply takes a player from one map to another
	 * This should probably be removed when I figure out how to handle instancing in lua
	 * @author Jeff Cochran
	 */
	public class TeleportHotspot extends Hotspot 
	{
		private static const TAG:String = "TeleportHotspot";
		private var mapName:String;
		private var playerX:Number = NaN;
		private var playerY:Number = NaN;
		private var facingX:Number = NaN;
		private var facingY:Number = NaN;
		public function TeleportHotspot( data:GeminiData, 
										 cv:Rectangle,
										 mapName:String,
										 playerX:Number = NaN,
										 playerY:Number = NaN,
										 facingX:Number = NaN,
										 facingY:Number = NaN )
		{
			super(data, cv, null);
			this.mapName = mapName;
			this.playerX = playerX;
			this.playerY = playerY;
			this.facingX = facingX;
			this.facingY = facingY;
		}
		
		public static function getTeleportFromTiled( to:TiledObject, data:GeminiData ):TeleportHotspot
		{
			var ret:TeleportHotspot;
			var cv:Rectangle = new Rectangle( to.x, to.y, to.width, to.height );
			var mapName:String = null;
			var playerX:Number = NaN;
			var playerY:Number = NaN;
			var facingX:Number = NaN;
			var facingY:Number = NaN;
			
			if ( to.properties[ "map_name" ] != null )
			{
				mapName = to.properties[ "map_name" ];
			}
			
			if ( to.properties[ "player_x" ] != null )
			{
				playerX = Argonaut.getAndCheckN( TAG, to.properties, "player_x" );
				playerY = Argonaut.getAndCheckN( TAG, to.properties, "player_y" );
			}
			
			if ( to.properties[ "facing_x" ] != null )
			{
				facingX = Argonaut.getAndCheckN( TAG, to.properties, "facing_x" );
				facingY = Argonaut.getAndCheckN( TAG, to.properties, "facing_y" );
			}
			
			ret = new TeleportHotspot( data,
									   cv,
									   mapName,
									   playerX,
									   playerY,
									   facingX,
									   facingY );
			if (to.name != "")
			{
				ret.name = to.name;
			}
			return ret;
		}
		
		protected override function onActorEntry( a:Actor ):void
		{
			super.onActorEntry( a );
			
			var d:GeminiData = data as GeminiData;
			
			if ( d.party.atBat.id == a.id )
			{
				if ( mapName != null )
				{
					d.activeLevel.dispatchEvent( new LevelChangeEvent( mapName,
																	   playerX,
																	   playerY,
																	   facingX,
																	   facingY ) );
				}
				else
				{
					if ( !isNaN( playerX ) && !isNaN( playerY ) )
					{
						d.party.setPos( playerX, playerY );
					}
					
					if ( !isNaN( facingX ) && !isNaN( facingY ) )
					{
						d.party.setDirection( facingX, facingY );
					}
				}
			}
		}
		
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public override function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			mapName = null;
			playerX = NaN;
			playerY = NaN;
			facingX = NaN;
			facingY = NaN;
			
			super.destroy();
		}
		
	}

}