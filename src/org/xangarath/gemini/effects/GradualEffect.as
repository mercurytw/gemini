package org.xangarath.gemini.effects 
{
	import flash.display.Bitmap;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.GeminiData;
	
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.interfaces.IEffect;
	import org.xangarath.gemini.events.HealthEvent;
	
	/**
	 * An effect which applies to the target over time
	 * @author ...
	 */
	public class GradualEffect implements ITickable, IEffect 
	{
		private static const TAG:String = "GradualEffect";
		public static const TIME_CLASS:String = "grad";
		
		private static const HEALTH:uint = (0x1 << 0);
		private static const DAMAGE:uint = (0x1 << 1);
		
		public var type:uint;
		public var amt:Number;
		public var durationSec:Number;
		private var _isComplete:Boolean = false;
		private var amtApplied:Number = 0.0;
		private var target:Creature;
		private var data:GeminiData;
		private var overlay:Bitmap = null;
		private var isSetup:Boolean = false;
		public function GradualEffect( data:GeminiData, type:uint, mag:Number, dur:Number ) 
		{
			this.type = type;
			this.amt = mag;
			this.durationSec = dur;
			this.data = data;
			
			switch ( type )
			{
				case HEALTH:
					overlay = data.resMgr.getBitmap( "healing.png" );
					break;
				case DAMAGE:
					overlay = null;
					break;
				default:
					DebugLog.getSingleton().e( TAG, "Encountered unknown type" );
					return;
			}
			
		}
		
		public static function getType( str:String ):uint
		{
			switch ( str.toLowerCase() )
			{
				case "health":
				case "heal":
					return HEALTH;
				case "damage":
					return DAMAGE;
				default:
					return 0x0;
			}
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IEffect */
		
		public function isValid( target:Creature ):Boolean
		{
			if ( ( type == HEALTH ) && ( target.health == target.stats.maxHP ) )
			{
				return false;
			}
			return true;
		}
		
		public function setup( target:Creature ):void 
		{
			if (isSetup)
				return;
				
			isSetup = true;
			
			this.target = target;
			if ( overlay != null )
			{	
				switch ( type )
				{
					case HEALTH:
						target.overlays.addChild( overlay );
						overlay.y = target.overlays.height - overlay.height;
						break;
					case DAMAGE:
						target.overlays.addChild( overlay );
						overlay.x = target.overlays.width - overlay.width;
						overlay.y = target.overlays.height - overlay.height;
						break;
					default:
						DebugLog.getSingleton().e( TAG, "Encountered unknown type" );
						return;
				}
			}
		}
		
		public function teardown():void 
		{
			if (!isSetup)
				return;
				
			isSetup = false;
			
			if ( overlay != null )
			{
				overlay.parent.removeChild( overlay );
			}
		}
		
		public function get isComplete():Boolean 
		{
			return _isComplete;
		}
		
		public function clone():IEffect 
		{
			return new GradualEffect( data, type, amt, durationSec );
		}
		
		public function toString():String 
		{
			var res:String = "Repeating: ";
			
			switch (type)
			{
				case HEALTH:
					res += "heal ";
					break;
				case DAMAGE:
					res += "damage ";
					break;
				default: 
					res += "UNKNOWN ";
					break;
			}
			
			res += amt;
			
			res += " over " + durationSec + " seconds";
			return res;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick(deltaTime:Number):void 
		{
				
			if ( _isComplete || !isSetup )
			{
				return;
			}
			
			var amtThisTime:Number = (amt * deltaTime) / durationSec;
			
			if ( ( amtThisTime + amtApplied ) > amt )
			{
				amtThisTime = amt - amtApplied;
			}
			
			switch ( type )
			{
				case HEALTH:
					target.notifyHolder( new HealthEvent( HealthEvent.HEAL_TYPE, null, amtThisTime ) );
					break;
				case DAMAGE:
					target.notifyHolder( new HealthEvent( HealthEvent.DAMAGE_TYPE, null, amtThisTime, 1.0 ) );
					break;
				default:
					DebugLog.getSingleton().w( TAG, "Encountered unknown effect type" );
					break;
			}
			
			amtApplied += amtThisTime;
			
			if (amtApplied >= amt)
			{
				_isComplete = true;
			}
		}
		
	}

}