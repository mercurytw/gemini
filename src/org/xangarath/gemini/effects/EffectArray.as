package org.xangarath.gemini.effects 
{
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.interfaces.IEffect;
	
	/**
	 * An array that also manages ticking itself
	 * @author ...
	 */
	public class EffectArray implements ITickable, IDestructable 
	{
		private static const TAG:String = "EffectArray";
		
		private var effects:Array;
		private var dumpList:Array;
		private var isConstructed:Boolean = true;
		public function EffectArray() 
		{
			effects = new Array();
			dumpList = new Array();
		}
		
		// I should be extended for searching and removing by effect type
		
		public function push( e:IEffect ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( effects.indexOf( e ) < 0 )
			{
				effects.push( e );
				return true;
			}
			return false;
		}
		
		public function remove( e:IEffect ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( effects.indexOf( e ) >= 0 )
			{
				effects.splice( effects.indexOf(e), 1 );
				return true;
			}
			return false;
		}
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			for each ( var eft:IEffect in effects )
			{
				eft.teardown();
				
				if ( eft is IDestructable )
				{
					( eft as IDestructable ).destroy();
				}
				
				remove( eft );
			}
			
			effects.length = 0;
			effects = null;
			
			dumpList.length = 0;
			dumpList = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed )
			{
				return;
			}
			
			for ( var i:int = 0; i < effects.length; i++ )
			{
				var e:IEffect = effects[ i ] as IEffect;
				
				e.tick( deltaTime );
				
				if ( e.isComplete )
				{
					e.teardown();
					dumpList.push(e);
				}
			}
			
			while ( dumpList.length > 0 )
			{
				effects.splice( effects.indexOf( dumpList.pop() ), 1 );
			}
		}
		
	}

}