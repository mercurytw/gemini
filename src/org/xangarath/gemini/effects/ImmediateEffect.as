package org.xangarath.gemini.effects 
{
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.GeminiData;
	
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.interfaces.IEffect;
	
	/**
	 * An immediate effect is applied immediately to the target
	 * @author ...
	 */
	public class ImmediateEffect implements IEffect, ITickable
	{
		private static const TAG:String = "ImmediateEffect";
		public static const TIME_CLASS:String = "imm";
		
		private static const HEALTH:uint = (0x1 << 0);
		private static const DAMAGE:uint = (0x1 << 1);
		private static const CDR:uint    = (0x1 << 2);
		
		public var type:uint;
		public var amt:Number;
		private var _isComplete:Boolean = false;
		private var isSetup:Boolean = false;
		private var data:GeminiData;
		public function ImmediateEffect( type:uint, mag:Number, data:GeminiData ) 
		{
			this.type = type;
			this.amt = mag;
			this.data = data;
		}
		
		public static function getType( str:String ):uint
		{
			switch (str.toLowerCase())
			{
				case "health":
				case "heal":
					return HEALTH;
				case "damage":
					return DAMAGE;
				case "cdr":
					return CDR;
				default:
					return 0x0;
			}
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IEffect */
		
		public function isValid( target:Creature ):Boolean
		{
			if ( ( type == HEALTH ) )
			{
				if ( data.party.containsActor( target ) && ( data.party.stats.currentHP == data.party.stats.maxHP ) )
				{
					return false;
				}
				else if ( target.health == target.stats.maxHP )
				{
					return false;
				}
			}
			return true;
		}
		
		public function setup( target:Creature ):void 
		{
			if (isSetup)
				return;
				
			isSetup = true;
			
			switch ( type )
			{
				case ImmediateEffect.HEALTH:
					target.notifyHolder( new HealthEvent( HealthEvent.HEAL_TYPE, null, amt ) );
					break;
				case DAMAGE:
					target.notifyHolder( new HealthEvent( HealthEvent.DAMAGE_TYPE, null, amt, 1.0 ) );
					break;
				case CDR:
					for ( var i:int = 0; i < target.actions.length; i++ )
					{
						target.actions[i].clearCooldown();
					}
					break;
				default:
					DebugLog.getSingleton().w( TAG, "Encountered unknown effect type" );
					break;
			}
			_isComplete = true;
		}
		
		public function clone():IEffect
		{
			return new ImmediateEffect( type, amt, data );
		}
		
		public function teardown():void 
		{
			if (!isSetup)
				return;
				
			isSetup = false;
		}
		
		public function tick( deltaTime:Number ):void 
		{
			
		}
		
		public function get isComplete():Boolean 
		{
			return _isComplete;
		}
		
		public function toString():String
		{
			var res:String = "Immediate: ";
			
			switch (type)
			{
				case HEALTH:
					res += "heal ";
					break;
				case DAMAGE:
					res += "damage ";
					break;
				case CDR:
					res += "cdr ";
					break;
				default: 
					res += "UNKNOWN ";
					break;
			}
			
			res += amt;
			return res;
		}
	}

}