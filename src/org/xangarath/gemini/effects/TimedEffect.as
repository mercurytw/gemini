package org.xangarath.gemini.effects 
{
	import flash.display.Bitmap;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IEffect;
	
	/**
	 * An effect which is only active for a given period of time
	 * @author ...
	 */
	public class TimedEffect implements ITickable, IEffect 
	{
		private static const TAG:String = "TimedEffect";
		public static const TIME_CLASS:String = "time";
		
		private static const ATK:uint = (0x1 << 0);
		private static const DEF:uint = (0x1 << 1);
		
		public var type:uint;
		public var amt:Number;
		public var durationSec:Number;
		private var timeApplied:Number;
		private var _isComplete:Boolean = false;
		private var target:Creature;
		private var overlay:Bitmap;
		private var data:GeminiData;
		private var isSetup:Boolean;
		public function TimedEffect( data:GeminiData, type:uint, mag:Number, dur:Number ) 
		{
			this.type = type;
			this.amt = mag;
			this.durationSec = dur;
			this.data = data;
			
			switch ( type )
			{
				case ATK:
					overlay = data.resMgr.getBitmap( "atkBuff.png" );
					break;
				case DEF:
					overlay = data.resMgr.getBitmap( "defBuff.png" );
					break;
				default:
					DebugLog.getSingleton().e( TAG, "Encountered unknown effect type!");
					return;
			}
		}
		
		public static function getType( str:String ):uint
		{
			switch ( str.toLowerCase() )
			{
				case "attack":
					return ATK;
				case "defense":
					return DEF;
				default:
					return 0x0;
			}
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IEffect */
		
		public function isValid( target:Creature ):Boolean
		{
			return true;
		}
		
		public function setup( target:Creature ):void 
		{
			if (isSetup)
				return;
			
			timeApplied = 0.0;
			this.target = target;
			
			switch ( type )
			{
				case ATK:
					target.stats.transientAttack += amt;
					target.overlays.addChild( overlay );
					break;
				case DEF:
					target.stats.transientDefense += amt;
					target.overlays.addChild( overlay );
					overlay.x = target.overlays.width - overlay.width;
					break;
				default:
					DebugLog.getSingleton().e( TAG, "Encountered unknown type" );
					return;
			}
			
			isSetup = true;
		}
		
		public function teardown():void 
		{	
			if (!isSetup)
				return;
				
			switch ( type )
			{
				case ATK:
					target.stats.transientAttack -= amt;
					target.overlays.removeChild( overlay );
					break;
				case DEF:
					target.stats.transientDefense -= amt;
					target.overlays.removeChild( overlay );
					break;
				default:
					DebugLog.getSingleton().e( TAG, "Encountered unknown type" );
					return;
			}
			
			isSetup = false;
		}
		
		public function get isComplete():Boolean 
		{
			return _isComplete;
		}
		
		public function clone():IEffect 
		{
			return new TimedEffect( data, type, amt, durationSec );
		}
		
		public function toString():String 
		{
			var res:String = "Timed: ";
			
			switch (type)
			{
				case ATK:
					res += "attack buff +";
					break;
				case DEF:
					res += "defense buff +";
					break;
				default: 
					res += "UNKNOWN ";
					break;
			}
			
			res += amt;
			
			res += " for " + durationSec + " seconds";
			return res;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick(deltaTime:Number):void 
		{
			if ( _isComplete || !isSetup )
			{
				return;
			}
			
			
			
			if ( ( timeApplied += deltaTime ) >= durationSec )
			{
				_isComplete = true;
			}
		}
		
	}

}