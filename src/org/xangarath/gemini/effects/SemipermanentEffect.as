package org.xangarath.gemini.effects 
{
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.interfaces.ITickable;
	
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.interfaces.IEffect;
	
	/**
	 * A semipermanent effect is an effect that persists until some external force removes it
	 * @author ...
	 */
	public class SemipermanentEffect implements IEffect, ITickable 
	{
		private static const TAG:String = "SemipermanentEffect";
		public static const TIME_CLASS:String = "semi";
		
		private static const HEALTH:uint     = (0x1 << 0);
		private static const ATTACK:uint     = (0x1 << 1);
		private static const DEFENSE:uint    = (0x1 << 2);
		
		public var type:uint;
		public var amt:Number;
		private var _isComplete:Boolean = false;
		private var isSetup:Boolean = false;
		private var act:Creature;
		public function SemipermanentEffect( type:uint, mag:Number ) 
		{
			this.type = type;
			this.amt = mag;
		}
		
		public static function getType( str:String ):uint
		{
			switch (str.toLowerCase())
			{
				case "health":
					return HEALTH;
				case "attack":
					return ATTACK;
				case "defense":
					return DEFENSE;
				default:
					return 0x0;
			}
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IEffect */
		
		public function isValid( target:Creature ):Boolean
		{
			return true;
		}
		
		public function get isComplete():Boolean 
		{
			return _isComplete;
		}
		
		public function setup( target:Creature ):void 
		{
			if (isSetup)
				return;
			
			switch (type)
			{
				case HEALTH:
					target.stats.baseHP += amt;
					break;
				case ATTACK:
					target.stats.baseAttack += amt;
					break;
				case DEFENSE:
					target.stats.baseDefense += amt;
					break;
				default:
					return;
			}
			
			isSetup = true;
			act = target;
			return;
		}
		
		public function teardown():void 
		{
			if (!isSetup)
				return;
			
			switch (type)
			{
				case HEALTH:
					act.stats.baseHP -= amt;
					break;
				case ATTACK:
					act.stats.baseAttack -= amt;
					break;
				case DEFENSE:
					act.stats.baseDefense -= amt;
					break;
				default:
					return;
			}
			
			isSetup = false;
			
			return;
		}
		
		public function clone():IEffect 
		{
			return new SemipermanentEffect( type, amt );
		}
		
		public function toString():String 
		{
			var res:String = "Semipermanent: ";
			
			switch (type)
			{
				case HEALTH:
					res += "health ";
					break;
				case ATTACK:
					res += "attack ";
					break;
				case DEFENSE:
					res += "defense ";
					break;
				default: 
					res += "UNKNOWN ";
					break;
			}
			
			res += amt;
			return res;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			
		}
		
	}

}