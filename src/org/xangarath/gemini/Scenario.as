package org.xangarath.gemini
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileReference;
	import flash.net.FileFilter;
	import flash.utils.ByteArray;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.ResourceManager;
	import org.xangarath.engine.SceneManager;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.script.ScriptManager;
	import org.xangarath.engine.statemachine.StateMachine;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.engine.SharedData;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.actors.Party;
	import org.xangarath.gemini.controllers.*;
	import org.xangarath.gemini.events.LevelChangeEvent;
	import org.xangarath.gemini.factories.ActorFactory;
	import org.xangarath.gemini.factories.ControllerFactory;
	import org.xangarath.gemini.items.ItemEncyclopedia;
	import org.xangarath.gemini.tiled.TiledJsonParser;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.gemini.events.DeathEvent;
	import org.xangarath.gemini.world.Level;
	/**
	 * Represents a state machine reflecting all the information needed to initalize the game
	 * @author Jeffrey Cochran
	 */
	public class Scenario extends EventDispatcher implements ITickable
	{
		private static const TAG:String = "Scenario";
		private var data:GeminiData;
		private var ctlrFactory:ControllerFactory;
		private var actorFactory:ActorFactory;
		private var moveSpeed:Number;
		public var sm:StateMachine;
		private var mapSprite:Sprite;
		private var mapsToLoad:int = 0;
		private var levelChangeRequest:LevelChangeEvent = null;
		public function Scenario(data:GeminiData) 
		{
			this.data = data;
			sm = new StateMachine();
		}
		
		public function setup():void
		{
			var file:File = File.applicationDirectory.resolvePath("data/scenario.json");
			if ( file == null )
			{
				DebugLog.getSingleton().e( TAG, "unable to find scenario JSON!" );
				throw new Error( TAG + "unable to find scenario JSON!" );
				return;
			}
			var fs:FileStream = new FileStream();
			fs.open( file, FileMode.READ );
			
			var script:String = fs.readUTFBytes( fs.bytesAvailable );
			fs.close();
			
			buildScenarioFromJSON( script );			
		}
		
		private var maxHP:Number = -1;
		private var playerCtlr:PlayerController;
		private function initPlayer(obj:Object):void
		{
			var xPos:Number = 0.;
			var yPos:Number = 0.;
			DebugLog.getSingleton().i( TAG, "Initializing player..." );
			
			var act:Creature = new Creature( data.resMgr.getBitmap("conner"), Actor.PLAYER_TEAM, 4, 0.0, 0.0 );
			act.setName( "connor" );
			USER::ENABLE_SCRIPTING
			{
				data.scriptMgr.addLuaGlobal("__CONNER", act.id);
			}
			data.sceneMgr.gameLayer.addChild(act);
			data.playerAtBat = act;
			playerCtlr = new PlayerController(act, data);
			data.tickList.push(playerCtlr);
			
			if (obj["health"] == null)
			{
				DebugLog.getSingleton().w( TAG, "no health specified " );
			}
			else
			{
				var hp:Number = obj["health"];
				data.playerAtBat.stats.baseHP = hp / 2;
				data.playerAtBat.stats.currentHP = data.playerAtBat.stats.maxHP;
				maxHP = hp;
				data.playerAtBat.heal(hp);
				DebugLog.getSingleton().i( TAG, "got health: " + hp );
			}
			
			var inventoryCap:uint = Argonaut.getAndCheckUI( TAG, obj, "inventory-cap" );
			act.inventory.cap = inventoryCap;
			
			moveSpeed = Argonaut.getAndCheckN( TAG, obj, "moveSpeed" );
			act.stats.moveSpeed = moveSpeed;
			
			var factory:ActionFactory = new ActionFactory(data);
			for (var i:int = 0; obj["weapons"][i] != null; i++ )
			{
				if ( i > 3 )
				{
					DebugLog.getSingleton().w( TAG, "specified too many weapons!" );
					continue;
				}
				var a:Action = factory.makeActionFromJson( obj["weapons"][i], act, Actor.PLAYER_TEAM, act.stats );
				data.playerAtBat.attachAction( a, i );
			}
			if ( obj["special"] != null )
			{
				var a2:Action = factory.makeActionFromJson( obj["special"], act, Actor.PLAYER_TEAM, act.stats ); 
				playerCtlr.jump = a2;
				data.jump = a2;
			}
			data.conner = act;
			
			data.conner.actions[0].enabled = data.saveData.connerHasQ;
			data.conner.actions[1].enabled = data.saveData.connerHasW;
			data.conner.actions[2].enabled = data.saveData.connerHasE;
			data.conner.actions[3].enabled = data.saveData.connerHasR;
			
			
			DebugLog.getSingleton().i( TAG, "connor loaded" );
		}
		
		private function initAnnie( obj:Object ):void
		{
			var annie:Creature = new Creature( data.resMgr.getBitmap("annie"), Actor.PLAYER_TEAM, 4, 0.0, 0.0 );
			annie.setName( "annie" );
			data.annie = annie;
			USER::ENABLE_SCRIPTING
			{
				data.scriptMgr.addLuaGlobal("__ANNIE", data.annie.id);
			}
			//data.conner = conner;
			data.sceneMgr.gameLayer.addChild(annie);
			data.annie.stats.baseHP = (maxHP == -1) ? 1 : maxHP / 2.;
			data.annie.stats.currentHP = data.annie.stats.maxHP;
			data.annie.stats.moveSpeed = moveSpeed;
			playerCtlr.possessConnor( annie );
			var factory:ActionFactory = new ActionFactory(data);
			for (var i:int = 0; obj["weapons"][i] != null; i++ )
			{
				if ( i > 3 )
				{
					DebugLog.getSingleton().w( TAG, "specified too many weapons!" );
					continue;
				}
				var a:Action = factory.makeActionFromJson( obj["weapons"][i], annie, Actor.PLAYER_TEAM, annie.stats );
				annie.attachAction( a, i );
			}
			data.annie.actions[0].enabled = data.saveData.annieHasQ;
			data.annie.actions[1].enabled = data.saveData.annieHasW;
			data.annie.actions[2].enabled = data.saveData.annieHasE;
			data.annie.actions[3].enabled = data.saveData.annieHasR;
		}
		
		private var lvl:Level;
		private var deleteThisLevelName:String;
		// delete me, I'm just for testing.
		private function buildMap( mapValue:String ):void
		{
			deleteThisLevelName = mapValue;
			var map:String = mapValue + ".json";
            
			var file:File = File.applicationDirectory.resolvePath( "data/maps/" + map );
			if ( file == null )
			{
				DebugLog.getSingleton().e( TAG, "unable to find map JSON!" );
				throw new Error( TAG + "unable to find map JSON!" );
				return;
			}
			var fs:FileStream = new FileStream();
			fs.open( file, FileMode.READ );
			
			var mapJson:String = fs.readUTFBytes( fs.bytesAvailable );
			fs.close();
			
			mapValue = mapValue.toLowerCase();
			
			lvl = new Level( mapJson, data, ctlrFactory, actorFactory, mapValue );
			lvl.addEventListener( Event.COMPLETE, finishLevelBuild );
			lvl.addEventListener( LevelChangeEvent.LEVEL_CHANGE, registerLevelChange );
			sm.addState( mapValue, lvl, false );
			lvl.build();
		}
		
		private function registerLevelChange( evt:LevelChangeEvent ):void
		{
			if ( this.levelChangeRequest != null )
			{
				DebugLog.getSingleton().w( TAG, "Requested multiple level changes. Discarding old request for level " + levelChangeRequest.levelName );
				levelChangeRequest.destroy();
			}
			
			levelChangeRequest = evt;
		}
		
		public function finishLevelBuild( e:Event ):void
		{	
			( e.target as Level ).removeEventListener( Event.COMPLETE, finishLevelBuild );
			DebugLog.getSingleton().i( TAG, "Finished level build for " + ( e.target as Level ).name );
			if (--mapsToLoad == 0)
			{
				dispatchEvent( new Event( Event.COMPLETE ) );
			}
		}
		
		private function parseConfig( jsobj:Object ):void
		{
			data.config.acidDamage = Argonaut.getAndCheckN( TAG, jsobj, "acidDamage" );
		}
		
		private function parseMaps( jsobj:Object ):void
		{
			var i:int = 0;
			mapsToLoad = 1; // to count the entry map
			var entryMap:String = Argonaut.getAndCheck( TAG, jsobj, "entry" );
			
			var otherMaps:Object = Argonaut.getAndCheck( TAG, jsobj, "other" );
		
			for ( i = 0; otherMaps[i] != null; i++ )
			{
				mapsToLoad++;
			}
			
			for ( i = 0; otherMaps[i] != null; i++ )
			{
				buildMap( otherMaps[i] as String );
			}
			
			buildMap( entryMap );
		}
		
		private function buildScenarioFromJSON(s:String):void
		{
			DebugLog.getSingleton().i( TAG, "Game script loaded successfully. Now parsing..." );
			var jsobj:Object = JSON.parse( s );
			
			data.config.getConfigDataFromJson( Argonaut.getAndCheck( TAG, jsobj, "config" ) );
			if ( data.config.useDebugScriptPath )
			{
				ScriptManager.SCRIPT_PATH = data.config.debugScriptPath;
				ScriptManager.prependPath = false;
			}
			
			ControllerFactory.initControllerProperties( Argonaut.getAndCheck( TAG, jsobj, "beastiary" ) );
			ctlrFactory = new ControllerFactory( data );
			
			actorFactory = new ActorFactory( data );
			data.party = new Party( data );
			
			initPlayer( jsobj["player"] );
			data.party.addActor( data.conner );
			if ( jsobj["annie"] != null )
			{
				initAnnie( jsobj["annie"] );
				data.party.addActor( data.annie );
			}
			
			DebugLog.getSingleton().i( TAG, "Building item encyclopedia..." );
			var encyclopedia:ItemEncyclopedia = new ItemEncyclopedia( data );
			encyclopedia.parseFromJSON( Argonaut.getAndCheck( TAG, jsobj, "items" ) );
			data.masterItemList = encyclopedia;
			
			DebugLog.getSingleton().i( TAG, "Building map..." );
			
			parseMaps( Argonaut.getAndCheck( TAG, jsobj, "maps" ) );
		}
		
		public function loadLevel( name:String, playerX:Number=NaN, playerY:Number=NaN, facingX:Number=NaN, facingY:Number=NaN ):void
		{
			sm.enterState(name.toLowerCase());
			
			if ( !isNaN( playerX ) && !isNaN( playerY ) )
			{
				data.party.setPos( playerX * data.GRID_UNIT_SIZE, playerY * data.GRID_UNIT_SIZE );
			}
			
			if ( !isNaN( facingX ) && !isNaN( facingY ) )
			{
				data.party.setDirection( facingX, facingY );
			}
		}
		
		public function startGame():void
		{
			sm.enterState( deleteThisLevelName );
		}
		
		// TODO: Delete this
		public function closeLevel():void
		{
			if ( lvl != null )
			{
				lvl.teardown();
			}
		}
		
		public function openLevel():void
		{
			if ( lvl != null )
			{
				lvl.setup();
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( levelChangeRequest != null )
			{
				CONFIG::debug
				{
					if ( levelChangeRequest.levelName == data.activeLevel.name )
					{
						DebugLog.getSingleton().w( TAG, "Requested to load \"" + data.activeLevel.name + "\", but it is already active!" );
						return;
					}
				}
				loadLevel( levelChangeRequest.levelName,
						   levelChangeRequest.playerX,
						   levelChangeRequest.playerY,
						   levelChangeRequest.facingX,
						   levelChangeRequest.facingY );
				levelChangeRequest.destroy();
				levelChangeRequest = null;
			}
		}
	}

}