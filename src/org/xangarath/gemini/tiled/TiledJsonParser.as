package org.xangarath.gemini.tiled 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.util.Pair;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.tiled.*;
	/**
	 * Parses a JSON representation of a map from the Tiled map editor tool.
	 * Behaves like a state machine. Call init first, then you can request various aspects of the JSON
	 * @author Jeffrey Cochran
	 */
	public class TiledJsonParser extends EventDispatcher
	{
		private static const TAG:String = "TiledJsonParser";
		private var jsonObj:Object = null;
		private var data:GeminiData;
		private var tilesets:Dictionary;
		private var tileheight:int;
		private var tilewidth:int;
		private var height:int;
		private var width:int;
		private var layers:Dictionary;
		private var setsToLoad:int;
		private var tlut:Dictionary;
		private var mapLayer:TiledTileLayer = null;
		private var mapTileData:Array = null;
		public function TiledJsonParser( data:GeminiData ) 
		{
			this.data = data;
			tilesets = new Dictionary();
			layers = new Dictionary();
			tlut = new Dictionary();
		}
		
		private function constructTilesets( jsobj:Object ):void
		{
			if ( jsobj == null )
			{
				throw new SyntaxError( TAG + " encountered malformed JSON " );
			}
			
			tilesets[ 0 ] = null;
			var i:int;
			
			for ( i = 0; jsobj[i] != null; i++ )
			{
			}
			setsToLoad = i;
			
			for ( i = 0; jsobj[i] != null; i++ )
			{
				if ( jsobj[ i ][ "properties" ][ "ignored" ] != null )
				{
					setsToLoad--;
					continue;
				}
				
				var firstGid:int = Argonaut.getAndCheckI( TAG, jsobj[ i ], "firstgid" );
				
				var tileset:TiledTileset;
				if (  ( tileset = TiledTilesetManager.getSingleton().getTileset( Argonaut.getAndCheck( TAG, jsobj[i], "name" ) ) ) == null )
				{
					tileset = TiledTileset.getTilesetFromJson( jsobj[i] );
					TiledTilesetManager.getSingleton().addTileset( tileset );
					tileset.loadImage( data.resMgr );
					setsToLoad--;
				}
				else
				{
					setsToLoad--;
				}
				
				
				for ( var j:int = 0; j < tileset.numberOfTiles; j++ )
				{
					
					tilesets[ j + firstGid ] = new Pair(tileset, firstGid);
				}
				
				tileset.buildTlut(tlut, firstGid);
			}
			if ( setsToLoad == 0 )
			{
				registerNewTileset();
			}
		}
		
		// TODO: Remove this legacy asynchronous code
		private var alreadyCalled:Boolean = false;
		private function registerNewTileset( e:Event = null ):void
		{
			if ( --setsToLoad <= 0 )
			{
				if ( !alreadyCalled )
				{
					constructLayers( Argonaut.getAndCheck( TAG, jsonObj, "layers" ) );
					dispatchEvent( new Event( Event.COMPLETE ) );
					alreadyCalled = true;
				}
			}
		}
		
		private function constructLayers( jsobj:Object ):void
		{
			if ( jsobj == null )
			{
				throw new SyntaxError( TAG + " encountered malformed JSON " );
			}
			
			for ( var i:int = 0; jsobj[ i ] != null; i++ )
			{
				var type:String = Argonaut.getAndCheck( TAG, jsobj[ i ], "type" );
				if ( type == "tilelayer" )
				{
					var layer:TiledTileLayer = TiledTileLayer.getTileLayerFromJson( jsobj[ i ] );
					layers[ layer.name ] = layer;
				} else if ( type == "objectgroup" )
				{
					var objgroup:TiledObjectLayer = new TiledObjectLayer( jsobj[ i ] );
					layers[ objgroup.name ] = objgroup;
				}
			}
			
			mapLayer = ( layers[ "map" ] as TiledTileLayer ); 
			mapTileData = mapLayer.buildGIDMap();
			//data.terrainMgr.init( gidArray, new Point(l.width, l.height), new Point(tilewidth, tileheight), tlut );
		}
		
		public function initializeTerrainManager( mgr:TerrainManager ):void
		{
			ErrorUtil.dbgAssertNonNull( TAG, mapLayer );
			data.terrainMgr.init( mapTileData, new Point(mapLayer.width, mapLayer.height), new Point(tilewidth, tileheight), tlut );
		}
		
		/**
		 * Initialize this parser. Call this before calling other functions
		 * @param	jsonString The string to be parsed into a JSON object
		 */
		public function init( jsonString:String ):void
		{
			jsonObj = JSON.parse( jsonString );
			if ( jsonObj == null )
			{
				trace( TAG + "init: unable to parse JSON!" );
				return;
			}
			
			tileheight = Argonaut.getAndCheckI( TAG, jsonObj, "tileheight" );
			tilewidth = Argonaut.getAndCheckI( TAG, jsonObj, "tilewidth" );
			height = Argonaut.getAndCheckI( TAG, jsonObj, "height" );
			width = Argonaut.getAndCheckI( TAG, jsonObj, "width" );
			
			constructTilesets( Argonaut.getAndCheck( TAG, jsonObj, "tilesets" ) );
			
		}
		
		/**
		 * Builds the map layer of the level
		 * @return The map, built from tiles, as a Sprite
		 */
		public function buildMapLayer():Sprite
		{
			var mapSprite:Sprite = new Sprite();
			
			var b:Bitmap = ( layers[ "map" ] as TiledTileLayer ).buildBmp( tilesets, tilewidth, tileheight );
			mapSprite.addChild( b ); // bmps add at 0,0, from the TLC of the bmp
			return mapSprite;
		}
		
		/**
		 * Gets the starting position of the player
		 * @return A point representing the player start point, in plxels
		 */
		public function getPlayerStart():Point
		{
			var pt:Point = new Point();
			
			var propertyJson:Object = Argonaut.getAndCheck( TAG, jsonObj, "properties" );
			pt.x = tilewidth * Argonaut.getAndCheckI( TAG, propertyJson, "player_start_x" );
			pt.y = tileheight * Argonaut.getAndCheckI( TAG, propertyJson, "player_start_y" );
			
			// Actors are centered, let's play nice
			pt.x += tilewidth / 2.0;
			pt.y += tileheight / 2.0;
			
			return pt;
		}
		
		/**
		 * Gets the user-friendly name of the map
		 * @return A new string representing the map
		 */
		public function getMapName():String
		{
			var res:String = Argonaut.getAndCheck( TAG, Argonaut.getAndCheck( TAG, jsonObj, "properties" ), "name" );
			return res;
		}
		
		private function buildWallFromObject( layer:TiledObjectLayer, obj:TiledObject ):Actor 
		{
			if ((obj.width == 0) || (obj.height == 0))
			{
				DebugLog.getSingleton().w( TAG, "Encountered misshapen wall" );
				return null;
			}
			try
			{
				var bmpDat:BitmapData = new BitmapData( obj.width, obj.height, true, layer.color );
			
				if (bmpDat == null)
				{
					DebugLog.getSingleton().w( TAG, "Encountered misshapen wall" );
					return null;
				}
				var bmp:Bitmap = new Bitmap( bmpDat );
			}
			catch (ex:Error)
			{
				DebugLog.getSingleton().e( TAG, "Failed to build wall");
				return null;
			}
			if (bmp == null)
			{
				DebugLog.getSingleton().e( TAG, "Failed to build wall!");
				return null;
			}
			bmp.alpha = layer.opacity;
			var visible:Boolean = layer.visible;
			if ( visible )
			{
				visible = obj.visible;
			}
			bmp.visible = visible;
			var act:Actor = new Actor( true, bmp, Actor.NO_TEAM, obj.x + ( obj.width / 2.0 ), obj.y + ( obj.height / 2.0 ), true );
			act.setName( "wall" );
			return act;
		}
		
		/**
		 * Get the invisible walls which keep the player in the play area
		 * @return An array of Actors
		 */
		public function getWalls():Array
		{
			var walls:Array = new Array();
			var objLayer:TiledObjectLayer = (layers[ "walls" ] as TiledObjectLayer);
			var wallDescriptors:Array = objLayer.objects;
			
			if (wallDescriptors == null)
			{
				return walls;
			}
			
			for ( var i:int = 0; wallDescriptors[ i ] != null; i++ )
			{
				var a:Actor = buildWallFromObject( objLayer, wallDescriptors[ i ] );
				if (a != null)
				{
					walls.push( a );
				}
			}
			
			wallDescriptors = null;
			(layers[ "walls" ] as TiledObjectLayer).deflate();
			
			return walls;
		}
		
		/**
		 * Returns hotspost as TiledObjects
		 * @return
		 */
		public function getHotspots():Array
		{
			if (layers[ "hotspots" ] == null)
				return null;
				
			return (layers[ "hotspots" ] as TiledObjectLayer).objects;
		}
		
		/**
		 * Returns NPCs as TiledObjects
		 * @return
		 */
		public function getNPCs():Array
		{
			if (layers[ "npc" ] == null)
				return null;
				
			return (layers[ "npc" ] as TiledObjectLayer).objects;
		}
		
		/**
		 * Returns monster placements as TiledObjects
		 * @return
		 */
		public function getMonsters():Array
		{
			return (layers[ "monsters" ] as TiledObjectLayer).objects;
		}
		
		/**
		 * Returns items as TiledObjects
		 * @return an Array of TiledObjects
		 */
		public function getItems():Array
		{
			if ( layers[ "pickups" ] == null )
			{
				trace( TAG, " warning: no pickups were supplied in the level JSON" );
				return null;
			}
			return ( layers[ "pickups" ] as TiledObjectLayer ).objects;
		}
		
		/**
		 * Returns obstacles as TiledObjects
		 * @return an Array of TiledObjects
		 */
		public function getObstacles():Array
		{
			if ( layers[ "obstacles" ] == null )
			{
				trace( TAG, " warning: no obstacles were supplied in the level JSON" );
				return null;
			}
			return ( layers[ "obstacles" ] as TiledObjectLayer ).objects;
		}
	}

}