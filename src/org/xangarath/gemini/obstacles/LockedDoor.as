package org.xangarath.gemini.obstacles 
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.geom.Point;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.script.ScriptManager;
	import org.xangarath.engine.SharedData;
	import org.xangarath.engine.tiled.TiledObject;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IObstacle;
	/**
	 * Represents a locked door that can be opened if it collides with an actor holding the key
	 * @author Jeffrey Cochran
	 */
	public class LockedDoor extends Actor implements IObstacle 
	{
		private static const TAG:String = "LockedDoor";
		private var data:GeminiData;
		private var isActive:Boolean = false;
		public var scriptOnCollision:Function = null;
		private var script:String;
		private var startSpawned:Boolean = true;
		private var hasFirstSpawn:Boolean = false;
		public function LockedDoor( data:GeminiData, drawable:Bitmap, xPosition:Number=0.0, yPosition:Number=0.0, script:String = null ) 
		{
			super(true, drawable, Actor.NO_TEAM, xPosition, yPosition, false);
			this.data = data;
			
			applyScript( script );
		}
		
		public static function getLockedDoorFromTiled( obj:TiledObject, data:GeminiData, scriptPath:String = "" ):LockedDoor
		{
			var ret:LockedDoor;
			
			// TODO: un hardcode this
			var img:Bitmap = data.resMgr.getBitmap( "terminalDoorHoriz" );
			var pos:Point = new Point( obj.x + (img.width / 2.0), obj.y  - (img.height / 2.0) );
			
			var script:String = obj.properties[ "script" ];
			if ( script != null )
			{
				script = scriptPath + script;
			}
			
			ret = new LockedDoor( data, img, pos.x, pos.y, script );
			if ( obj.name != "" )
			{
				ret.setName( obj.name );
			}
			var startSpawned:Boolean = Argonaut.getAndCheckB( TAG, obj.properties, "start_closed" );
			if (startSpawned == false)
			{
				ret.startSpawned = false
			}
			return ret;
		}
		
		public function applyScript( p:String ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			this.script = p;
			this.scriptOnCollision = null;
			if (script != null)
			{
				data.scriptMgr.doReloadableFile( ScriptManager.VFS_SCRIPT_ROOT + script, this );
			}
		}
		
		public function handleCollisionEvent( e:Event ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( e is CollisionEvent )
			{
				if ( scriptOnCollision != null )
				{
					scriptOnCollision.apply( this, [ e as CollisionEvent ] );
				}
			}
		}
		
		public override function spawn( data:SharedData ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( !isActive )
			{
				if (!hasFirstSpawn && !startSpawned)
				{
					hasFirstSpawn = true;
					return;
				}
				super.spawn( data );
				this.addEventListener( CollisionEvent.COLLISION, handleCollisionEvent );
				isActive = true;
			}
		}
		
		public override function despawn( data:SharedData ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( isActive )
			{
				isActive = false;
				this.removeEventListener( CollisionEvent.COLLISION, handleCollisionEvent );
				super.despawn( data );
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public override function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isActive )
			{
				despawn( data );
			}
			
			scriptOnCollision = null;
			
			if ( script != null )
			{
				data.scriptMgr.undoReloadableFile( ScriptManager.VFS_SCRIPT_ROOT + script );
				script = null;
			}
			
			data = null;
			super.destroy();
		}
		
	}

}