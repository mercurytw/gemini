package org.xangarath.gemini 
{
	import flash.automation.ActionGenerator;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.interfaces.IPoolable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.actors.FancyAutonomousProjectile;
	import org.xangarath.gemini.performs.*;
	import org.xangarath.gemini.performs.projectile.*;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.gemini.stats.StatPage;
	/**
	 * Makes actions
	 * @author Jeffrey Cochran
	 */
	public class ActionFactory 
	{
		private static const TAG:String = "ActionFactory";
		private var data:GeminiData;
		private var jsonIsValid:Boolean = true;
		public function ActionFactory(data:GeminiData) 
		{
			this.data = data;
		}
		
		private function getJson( jsobj:Object, property:String ):*
		{
			if ( jsobj[property] == null )
			{
				jsonIsValid = false;
				DebugLog.getSingleton().w( TAG, "ActionFactory: could not find property " + property + " in json object " + jsobj );
			}
			return jsobj[property];
		}
		
		public function makeActionFromJson( jsobj:Object, actor:Actor, team:int, stats:StatPage ):Action
		{
			var perfState:IState = null;
			var res:Action = null;
			var wdat:WeaponData;
			var p:Point;
			var penetrate:Boolean;
			var bmp:Bitmap = null;
			var proj:FancyAutonomousProjectile = null;
			jsonIsValid = true;
			if ( jsobj["type"] == null )
			{
				DebugLog.getSingleton().e( TAG, "ActionFactory: encountered invalid json: " + jsobj );
				return null;
			}
			switch( (jsobj["type"] as String).toLowerCase() )
			{
				case StabAttackState.STAB_ATTACK_TYPE:
					wdat = WeaponData.fromJson( jsobj, data, team );
					if ( wdat == null ) { return null; }
					wdat.stats = stats;
					perfState = new StabAttackState( actor, wdat, data );
					wdat.bmp.visible = false;
					p = new Point( -data.HALF_GRID_UNIT_SIZE, -(data.HALF_GRID_UNIT_SIZE + data.GRID_UNIT_SIZE));
					res = new Action(wdat.name, 
									 getJson( jsobj, "delay" ), 
									 getJson( jsobj, "cost" ), 
									 perfState, 
									 getJson( jsobj, "cooldown" ), 
									 data, wdat.bmp, p);
					if ( !jsonIsValid ) { return null; }
					break;
				case SweepAttackState.SWEEP_ATTACK_TYPE:
					wdat = WeaponData.fromJson( jsobj, data, team );
					if ( wdat == null ) { return null; }
					wdat.stats = stats;
					perfState = new SweepAttackState( actor, wdat, data );
					wdat.bmp.visible = false;
					p = new Point( -(data.HALF_GRID_UNIT_SIZE + data.GRID_UNIT_SIZE) , -(data.HALF_GRID_UNIT_SIZE + data.GRID_UNIT_SIZE));
					res = new Action(wdat.name, 
									 getJson( jsobj, "delay" ), 
									 getJson( jsobj, "cost" ), 
									 perfState, 
									 getJson( jsobj, "cooldown" ), 
									 data, wdat.bmp, p);
					if ( !jsonIsValid ) { return null; }
					break;
				case CircleAttackState.CIRCLE_ATTACK_TYPE:
					wdat = WeaponData.fromJson( jsobj, data, team );
					if ( wdat == null ) { return null; }
					wdat.stats = stats;
					perfState = new CircleAttackState( actor, wdat, data );
					wdat.bmp.visible = false;
					p = new Point( -(data.HALF_GRID_UNIT_SIZE + data.GRID_UNIT_SIZE) , -(data.HALF_GRID_UNIT_SIZE + data.GRID_UNIT_SIZE));
					res = new Action(wdat.name, 
									 getJson( jsobj, "delay" ), 
									 getJson( jsobj, "cost" ), 
									 perfState, 
									 getJson( jsobj, "cooldown" ), 
									 data, wdat.bmp, p);
					if ( !jsonIsValid ) { return null; }
					break;
				case ProjectileWaveState.PROJECTILE_WAVE_TYPE:
					wdat = WeaponData.fromJson( jsobj, data, team );
					if ( wdat == null ) { return null; }
					wdat.stats = stats;
					penetrate = false;
					if ( jsobj["penetration"] != null )
					{
						if ( jsobj["penetration"] == "yes" ) { penetrate = true; }
					}
					if ( jsobj["bmp"] != null )
					{
						bmp = data.resMgr.getBitmap( Argonaut.getAndCheck( TAG, jsobj, "bmp" ) );
					}
					else
					{
						bmp = data.resMgr.getBitmap( "pellet" );
					}
					if ( jsobj["special"] != null )
					{
						proj = new FancyAutonomousProjectile( data, Argonaut.getAndCheckN( TAG, jsobj, "speed" ), false, bmp, team, stats );
						proj.setPerformAction( jsobj["special"], wdat );
					}
					perfState = new ProjectileWaveState( actor, penetrate, getJson( jsobj, "speed" ), data, wdat, team, bmp, proj );
					res = new Action(wdat.name, 
									 getJson( jsobj, "delay" ), 
									 getJson( jsobj, "cost" ), 
									 perfState, 
									 getJson( jsobj, "cooldown" ), 
									 data);
					if ( !jsonIsValid ) { return null; }
					break;
				case TripleShotState.TRIPLE_SHOT_TYPE:
					wdat = WeaponData.fromJson( jsobj, data, team );
					if ( wdat == null ) { return null; }
					wdat.stats = stats;
					penetrate = false;
					if ( jsobj["penetration"] != null )
					{
						if ( jsobj["penetration"] == "yes" ) { penetrate = true; }
					}
					if ( jsobj["bmp"] != null )
					{
						bmp = data.resMgr.getBitmap( Argonaut.getAndCheck( TAG, jsobj, "bmp" ) );
					}
					else
					{
						bmp = data.resMgr.getBitmap( "pellet" );
					}
					if ( jsobj["special"] != null )
					{
						proj = new FancyAutonomousProjectile( data, Argonaut.getAndCheckN( TAG, jsobj, "speed" ), false, bmp, team, stats );
						proj.setPerformAction( jsobj["special"], wdat );
					}
					perfState = new TripleShotState( actor, data, getJson( jsobj, "speed" ), wdat, team, penetrate, proj );
					res = new Action(wdat.name, 
									 getJson( jsobj, "delay" ), 
									 getJson( jsobj, "cost" ), 
									 perfState, 
									 getJson( jsobj, "cooldown" ), 
									 data, wdat.bmp, p);
				    if ( !jsonIsValid ) { return null; }
					break;
				case JumpState.JUMP_STATE_TYPE:
					perfState = new JumpState( getJson( jsobj, "distance" ), data);
					res = new Action("jump", 
									 getJson( jsobj, "delay" ), 
									 getJson( jsobj, "cost" ), 
									 perfState, 
									 getJson( jsobj, "cooldown" ), 
									 data );
					if ( !jsonIsValid ) { return null; }
					break;
				case OneTwoThreeState.TYPE:
					wdat = WeaponData.fromJson( jsobj, data, team );
					if ( wdat == null ) { return null; }
					wdat.stats = stats;
					perfState = new OneTwoThreeState( actor, wdat, data );
					wdat.bmp.visible = false;
					p = new Point( -data.HALF_GRID_UNIT_SIZE, -(data.HALF_GRID_UNIT_SIZE + data.GRID_UNIT_SIZE));
					res = new Action(wdat.name, 
									 getJson( jsobj, "delay" ), 
									 getJson( jsobj, "cost" ), 
									 perfState, 
									 getJson( jsobj, "cooldown" ), 
									 data, wdat.bmp, p);
					if ( !jsonIsValid ) { return null; }
					break;
				case ShieldBombState.TYPE:
					wdat = WeaponData.fromJson( jsobj, data, team );
					if ( wdat == null ) { return null; }
					wdat.stats = stats;
					perfState = new ShieldBombState( actor, 
													 Argonaut.getAndCheckN( TAG, jsobj, "radius" ),
													 Argonaut.getAndCheckPercent( TAG, jsobj, "movePercent" ),
													 Argonaut.getAndCheckPercent( TAG, jsobj, "dmgPercent" ),
													 Argonaut.getAndCheckN( TAG, jsobj, "shieldTime" ),
													 wdat, data );
					wdat.bmp.visible = false;
					p = new Point( -( wdat.bmp.width / 2.0 ) , -( wdat.bmp.height / 2.0 ) );
					res = new Action(wdat.name, 
									 getJson( jsobj, "delay" ), 
									 getJson( jsobj, "cost" ), 
									 perfState, 
									 getJson( jsobj, "cooldown" ), 
									 data, wdat.bmp, p);
					if ( !jsonIsValid ) { return null; }
					break;
				case ExplosiveRoundState.TYPE:
					perfState = new ExplosiveRoundState( actor, this, jsobj, stats );
					res = new Action("exp round", 
									 getJson( jsobj, "delay" ), 
									 getJson( jsobj, "cost" ), 
									 perfState, 
									 getJson( jsobj, "cooldown" ), 
									 data );
					if ( !jsonIsValid ) { return null; }
					break;
				case ExplosionState.TYPE:
					perfState = new ExplosionState( Argonaut.getAndCheckN( TAG, jsobj, "radius" ), actor, data );
					res = new Action( "explosion",
									  0, 1.0, 
									  perfState,
									  0.5, data, data.resMgr.getBitmap( "circle" ) );
					break;
				case ConicExplosionState.TYPE:
					perfState = new ConicExplosionState( Argonaut.getAndCheckN( TAG, jsobj, "exdamage" ),
														 Argonaut.getAndCheckN( TAG, jsobj, "radius" ),
														 Argonaut.getAndCheckN( TAG, jsobj, "theta" ),
														 actor, data );
					res = new Action( "explosion",
									  0, 1.0, 
									  perfState,
									  0.5, data, data.resMgr.getBitmap( "circle" ) );
					break;
				default:
					DebugLog.getSingleton().w( TAG, "Encountered unknown type: " + jsobj["type"] );
					break;
			}
			
			if (res != null)
			{
				if (jsobj["descr"] != null)
				{
					res.description = (jsobj["descr"] as String);
				}
				
				if (jsobj["icon"] != null)
				{
					res.icon = data.resMgr.getBitmap((jsobj["icon"] as String));
				}
			}
			return res;
		}
	}

}