package org.xangarath.gemini.interfaces 
{
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * An IObstacle represents something that is place in a level, but is not a creature, pickup, 
	 * tile, or invisible wall. It is able to add itself to any necessary collections such as draw, 
	 * tick, and quadtree
	 * @author Jeffrey Cochran
	 */
	public interface IObstacle 
	{
	}
	
}