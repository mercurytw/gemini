package org.xangarath.gemini.interfaces 
{
	import flash.display.Bitmap;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.actors.Creature;
	
	/**
	 * An effect affects a creature
	 * @author 
	 */
	public interface IEffect
	{
		function setup( target:Creature ):void;
		function teardown():void;
		function tick( deltaTime:Number ):void;
		function get isComplete():Boolean;
		function clone():IEffect;
		function toString():String;
		function isValid( target:Creature ):Boolean;
	}

}