package org.xangarath.gemini.interfaces 
{
	import org.xangarath.gemini.weapons.WeaponData;
	/**
	 * Represents a class that can have weapon data applied to it
	 * @author Jeffrey Cochran
	 */
	public interface IWeaponStatAcceptor 
	{
		function get weaponData():WeaponData;
		function set weaponData( data:WeaponData ):void;
	}
	
}