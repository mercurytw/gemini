package org.xangarath.gemini.interfaces 
{
	
	/**
	 * Represents some functions that ActionStates must implement to share information with the caller
	 * @author Jeffrey Cochran
	 */
	public interface IActionState 
	{
		/**
		 * Returns a number in the range 0.0-1.0 reflecting a percentage of allowed movement speed.
		 * For example, if this state completely disallows motion, return 0.0 ( 0% )
		 * for 50% motion, return 0.5
		 * or for full motion, return 1.0 ( 100% )
		 * @return
		 */
		function getDoesAllowMovement():Number;
		
		/**
		 * Should this state prevent the caller from performing other actions?
		 * @return
		 */
		function getIsBlocking():Boolean;
	}
	
}