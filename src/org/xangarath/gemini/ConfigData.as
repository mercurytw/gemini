package org.xangarath.gemini 
{
	import flash.filesystem.File;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	/**
	 * ...
	 * @author ...
	 */
	public class ConfigData 
	{
		private static const TAG:String = "ConfigData";
		public var acidDamage:Number = 0.0;
		public var debugScriptPath:String;
		public var useDebugScriptPath:Boolean;
		public var npcMoveSpeed:Number = 2.5;
		public function ConfigData() 
		{
			
		}
		
		public function getConfigDataFromJson( jsobj:Object ):void
		{
			acidDamage = Argonaut.getAndCheckN( TAG, jsobj, "acidDamage" );
			debugScriptPath = File.documentsDirectory.nativePath + File.separator + "gemini" + File.separator + (Argonaut.getAndCheck( TAG, jsobj, "scriptPath" ) as String);
			useDebugScriptPath =  Argonaut.getAndCheckB( TAG, jsobj, "useScriptPath" );
			npcMoveSpeed = Argonaut.getAndCheckN( TAG, jsobj, "npcMoveSpeed" );
			
			USER::PACKAGE_BUILD
			{
				useDebugScriptPath = false;
			}
			
			if (useDebugScriptPath)
			{
				DebugLog.getSingleton().i( TAG, "Using debug script path" );
			}
			else
			{
				DebugLog.getSingleton().i( TAG, "NOT using debug script path" );
			}
		}
		
	}

}