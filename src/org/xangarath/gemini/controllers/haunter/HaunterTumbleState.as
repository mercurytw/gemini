package org.xangarath.gemini.controllers.haunter 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * represents a haunter tumbling away after being hit
	 * @author Jeffrey Cochran
	 */
	public class HaunterTumbleState extends EventDispatcher implements ITickable, IState, IDestructable 
	{
		private static const TAG:String = "HaunterTumbleState";
		private static const INVISIBILITY_DISTANCE_SQUARED:Number = 90000.0;
		private static const MOVE_SPEED:Number = 400.0;
		private var actor:Creature;
		private var data:GeminiData;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function HaunterTumbleState( actor:Creature, data:GeminiData ) 
		{
			super();
			this.actor = actor;
			this.data = data;
		}
		
		private function handleCollision( e:Event ):void
		{	
			dispatchEvent( new StateChangeEvent( "lurk", StateChangeEvent.STATE_CHANGE ) );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		private var togetherness:Boolean;
		public function setup():void 
		{
			if ( isSetup )
			{
				return;
			}
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			
			actor.face( data.playerAtBat );
			actor.setDirection( -actor.dirX, -actor.dirY );
			togetherness = actor.rotateTogether;
			actor.rotateTogether = true;
			actor.addEventListener( CollisionEvent.COLLISION, handleCollision );
			
			isSetup = true;
		}
		
		public function teardown():void 
		{
			if ( !isSetup )
			{
				return;
			}
			
			actor.rotation = 0.0;
			actor.rotateTogether = togetherness;
			actor.alpha = 1.0;
			actor.removeEventListener( CollisionEvent.COLLISION, handleCollision );
			
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			actor = null;
			data = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed || !isSetup )
			{
				return;
			}
			
			var ds:Number = Maths.distanceSq( actor.pos, data.playerAtBat.pos );
			if ( ds >= INVISIBILITY_DISTANCE_SQUARED )
			{
				actor.alpha = 0.0;
			}
			else
			{
				actor.alpha = 1.0 - ( ds / INVISIBILITY_DISTANCE_SQUARED );
			}
			
			actor.move( actor.dirX * MOVE_SPEED * deltaTime, actor.dirY * MOVE_SPEED * deltaTime );
		}
		
	}

}