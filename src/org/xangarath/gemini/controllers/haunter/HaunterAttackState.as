package org.xangarath.gemini.controllers.haunter 
{
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * represents a haunter about to attack, then attacking
	 * @author Jeffrey Cochran
	 */
	public class HaunterAttackState extends EventDispatcher implements ITickable, IState, IDestructable 
	{
		private static const TAG:String = "HaunterAttackState";
		private static const SHAKE_AMT:Number = 2.0;
		private var actor:Creature;
		private var data:GeminiData;
		private var graceTimeRemaining:Number;
		private var graceTime:Number;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function HaunterAttackState( grace:Number, actor:Creature, data:GeminiData ) 
		{
			super();
			this.graceTime = grace;
			this.actor = actor;
			this.data = data;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			graceTimeRemaining = graceTime;
			isSetup = true;
		}
		
		public function teardown():void 
		{
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			actor = null;
			data = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		private var upOrDown:int = 1;
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed )
			{
				return;
			}
			if ( ( graceTimeRemaining -= data.secondsPerFrame ) <= 0.0 )
			{
				actor.actions[0].perform();
				actor.actions[0].tick( deltaTime );
				dispatchEvent( new StateChangeEvent( "lurk", StateChangeEvent.STATE_CHANGE ) );
			}
			upOrDown *= -1;
			actor.move( -actor.dirY * SHAKE_AMT * upOrDown, actor.dirX * SHAKE_AMT * upOrDown );
			actor.face( data.playerAtBat );
		}
		
	}

}