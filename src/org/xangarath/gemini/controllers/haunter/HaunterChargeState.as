package org.xangarath.gemini.controllers.haunter 
{
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * Represents behavior for a charging Haunter
	 * @author Jeffrey Cochran
	 */
	public class HaunterChargeState extends EventDispatcher implements ITickable, IState, IDestructable 
	{
		private static const TAG:String = "HaunterChargeState";
		private static const MOVE_SPEED:Number = 10.0;
		private var actor:Creature;
		private var data:GeminiData;
		private var lastPos:Point = new Point();
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function HaunterChargeState( actor:Creature, data:GeminiData ) 
		{
			super();
			this.actor = actor;
			this.data = data;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			isSetup = true;
		}
		
		public function teardown():void 
		{
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			actor = null;
			data = null;
			lastPos = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{	
			if ( !isConstructed )
			{
				return;
			}
			
			lastPos.copyFrom( actor.pos );
			actor.face( data.playerAtBat );
			actor.move( actor.dirX * MOVE_SPEED * data.GRID_UNIT_SIZE * deltaTime, actor.dirY * MOVE_SPEED * data.GRID_UNIT_SIZE * deltaTime );
			
			if ( actor.getBoundingVolume().intersects( data.playerAtBat.getBoundingVolume() ) )
			{
				actor.setPos( lastPos.x, lastPos.y );
				actor.face( data.playerAtBat );
				dispatchEvent( new StateChangeEvent( "attack", StateChangeEvent.STATE_CHANGE ) );
			}
		}
		
	}

}