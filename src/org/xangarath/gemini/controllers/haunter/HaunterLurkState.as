package org.xangarath.gemini.controllers.haunter 
{
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	
	/**
	 * Represents the lurk state for a haunter
	 * @author Jeffrey Cochran
	 */
	public class HaunterLurkState extends EventDispatcher implements ITickable, IState, IDestructable 
	{
		private static const TAG:String = "HaunterLurkState";
		private static const MOVE_SPEED:Number = 4.0;
		private var actor:Creature;
		private var data:GeminiData;
		private var lurkTimeRemaining:Number;
		private var radius:Number;
		private var maxLurkTime:Number = 15.0;
		private var minLurkTime:Number = 5.0;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function HaunterLurkState( minLurk:Number, maxLurk:Number, actor:Creature, data:GeminiData ) 
		{
			super();
			this.actor = actor;
			this.data = data;
			this.minLurkTime = minLurk;
			this.maxLurkTime = maxLurk;
			var maxExtent:Number = Math.max( actor.getBoundingVolume().width, actor.getBoundingVolume().height );
			maxExtent /= 2.0;
			var minWorldExtent:Number = Math.min( data.playArea.width, data.playArea.height );
			minWorldExtent /= 2.0;
			radius =  minWorldExtent - maxExtent;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			lurkTimeRemaining = Math.max((Math.random() * maxLurkTime), minLurkTime);
			isSetup = true;
		}
		
		public function teardown():void 
		{
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			actor = null;
			data = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{	
			if ( !isConstructed )
			{
				return;
			}
			if ( ( lurkTimeRemaining -= deltaTime ) <= 0.0 )
			{
				dispatchEvent( new StateChangeEvent( "charge", StateChangeEvent.STATE_CHANGE ) );
				return;
			}
			if ( Maths.distance( this.actor.pos, data.playerAtBat.pos ) > radius )
			{
				actor.setDirection( data.playerAtBat.pos.x - actor.pos.x, data.playerAtBat.pos.y - actor.pos.y );
				actor.move( actor.dirX * MOVE_SPEED * data.GRID_UNIT_SIZE * deltaTime, actor.dirY * MOVE_SPEED * data.GRID_UNIT_SIZE * deltaTime );
				return;
			}
			actor.setDirection( data.playerAtBat.pos.x - actor.pos.x, data.playerAtBat.pos.y - actor.pos.y );
			actor.move( -actor.dirY * MOVE_SPEED * data.GRID_UNIT_SIZE * deltaTime, actor.dirX * MOVE_SPEED * data.GRID_UNIT_SIZE * deltaTime );
		}
		
	}

}