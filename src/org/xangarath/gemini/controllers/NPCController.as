package org.xangarath.gemini.controllers 
{
	import flash.events.Event;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.statemachine.StateMachine;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.behavior.ScriptedState;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	
	/**
	 * A generic NPC controller, which is driven by lua
	 * @author Jeff Cochran
	 */
	public class NPCController implements ITickable, IDestructable 
	{
		private static const TAG:String = "NPCController";
		private static const NPC_PATH:String = "npc/";
		private var data:GeminiData;
		private var act:Creature = null;
		private var sm:StateMachine;
		private var hasEntered:Boolean = false;
		private var entryState:String = null;
		private var moveSpeed:Number;
		private var isConstructed:Boolean = true;
		public function NPCController( data:GeminiData ) 
		{
			this.data = data;
			moveSpeed = data.config.npcMoveSpeed;
			sm = new StateMachine();
		}
		
		public function possess( c:Creature, props:Object = null ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			
			CONFIG::debug
			{
				if ( act != null )
				{
					DebugLog.getSingleton().w( TAG, "controller already possesses an actor" );
				}
			}
			
			act = c;
			if ( act.animSprite != null )
			{
				act.animSprite.play();
			}
			
			if ( props != null )
			{
				applyProperties( props );
			}
			
			act.badWallMask = TerrainManager.TERRAIN_TYPE_UNCROSSABLE | 
							  GeminiTerrainInterpreter.TERRAIN_TYPE_WALL |
							  GeminiTerrainInterpreter.TERRAIN_TYPE_WATER;
							  
			act.stats.moveSpeed = moveSpeed;
			act.addEventListener( CollisionEvent.COLLISION, handleActorEvent );
		}
		
		private function handleActorEvent( e:Event ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( sm.mActiveState is ScriptedState )
			{
				( sm.mActiveState as ScriptedState ).eventHandler( e );
			}
		}
		
		private function applyProperties( props:Object ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			ErrorUtil.dbgAssertNonNull( TAG, act );
			
			var face:String = Argonaut.getAndCheck( TAG, props, "facing" );
			switch ( face )
			{
				case "l":
					act.setDirection( -1.0, 0.0 );
					break;
				case "r":
					act.setDirection( 1.0, 0.0 );
					break;
				case "u":
					act.setDirection( 0.0, -1.0 );
					break;
				case "d":
					act.setDirection( 0.0, 1.0 );
					break;
				default:
					DebugLog.getSingleton().e( TAG, "encountered unexpected direction \"" + face + "\" while posessing NPC " + act.name );
			};
			
			var states:String = Argonaut.getAndCheck( TAG, props, "states" );
			var stateNames:Array = states.split(",");
			
			for each ( var str:String in stateNames )
			{
				var behavior:ScriptedState = new ScriptedState( data, act );
				str = str.replace(" ", "");
				behavior.applyScript( NPC_PATH + str + ".lua", data );
				sm.addState( str, behavior );
			}
			
			entryState = Argonaut.getAndCheck( TAG, props, "entryState" );
			hasEntered = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( act != null )
			{
				act.removeEventListener( CollisionEvent.COLLISION, handleActorEvent );
				act.despawn( data );
				act.destroy();
				act = null;
				
				entryState = null;
				
				sm.destroy();
				sm = null;
				
				var idx:int = data.tickList.indexOf( this );
				if ( idx != -1 )
				{
					data.tickList.splice( idx, 1 );
				}
				data = null;
			}
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		private var complainOnce:Boolean = true;
		public function tick( deltaTime:Number ):void 
		{
			ErrorUtil.dbgAssertNonNull( TAG, act );
			
			if ( !isConstructed )
			{
				return;
			}
			
			if ( !hasEntered && ( entryState != null ) )
			{
				sm.enterState( entryState );
				hasEntered = true;
			}
			
			if ( sm.mActiveState != null )
			{
				(sm.mActiveState as ITickable).tick( deltaTime );
			}
			
			if ( act.animSprite != null )
			{
				act.animSprite.tick( deltaTime );
			}
		}
		
	}

}