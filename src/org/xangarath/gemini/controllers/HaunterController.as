package org.xangarath.gemini.controllers 
{
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.SceneManager;
	import org.xangarath.engine.SharedData;
	import org.xangarath.engine.statemachine.StateMachine;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.behavior.InactiveState;
	import org.xangarath.gemini.controllers.bull.BullChargeState;
	import org.xangarath.gemini.controllers.bull.BullIdleState;
	import org.xangarath.gemini.controllers.haunter.HaunterAttackState;
	import org.xangarath.gemini.controllers.haunter.HaunterChargeState;
	import org.xangarath.gemini.controllers.haunter.HaunterLurkState;
	import org.xangarath.gemini.controllers.haunter.HaunterTumbleState;
	import org.xangarath.gemini.events.DeathEvent;
	import org.xangarath.gemini.events.HealthEvent;
	import flash.events.Event;
	import flash.geom.Point;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.performs.StabAttackState;
	import org.xangarath.gemini.weapons.WeaponData;
	
	/**
	 * Represents a haunter-type. Nasty fellows.
	 * @author Jeffrey Cochran
	 */
	public class HaunterController implements ITickable, IDestructable 
	{
		private static const TAG:String = "HaunterController";
		private static const INVISIBILITY_DISTANCE_SQUARED:Number = 90000.0;
		private static const MAX_MOTION:Number = 2.0;
		private static const COLLISION_DAMAGE:Number = 3.0;
		public var actor:Creature;
		private var data:GeminiData;
		
		private var painAgg:Number = 0.0;
		private static const MAX_PAIN_TIME:Number = 1.0;
		private var painBmp:Bitmap;
		
		private var behavior:StateMachine;
		private var isConstructed:Boolean = true;
		public function HaunterController( damage:Number, grace:Number, minLurk:Number, maxLurk:Number, actor:Creature, health:Number, armored:Boolean, data:GeminiData) 
		{
			this.actor = actor;
			actor.stats.baseHP = health;
			actor.heal(health);
			this.data = data;
			this.actor.addEventListener(HealthEvent.DAMAGE_TYPE, handleActorEvent);
			actor.stats.armored = armored;
			painBmp = data.resMgr.getBitmap("red");
			painBmp.name = "painflash";
			actor.attachBitmap(painBmp);
			painBmp.x -= painBmp.width / 2.0;
			painBmp.y -= painBmp.height / 2.0;
			painBmp.visible = false;
			actor.facingOffset = 90.0;
			actor.overrideBoundingVolume( new Rectangle(0, 0, 75.0, 75.0) );
			var b:Bitmap = data.resMgr.getBitmap("sword");
			var wdat:WeaponData = new WeaponData("", WeaponData.STAB_TYPE, Actor.ENEMY_TEAM, damage, 0.0, 0.0, 1.0, b );
			wdat.bmp.visible = false;
			wdat.stats = actor.stats;
			var p:Point = new Point( -data.HALF_GRID_UNIT_SIZE, -(data.HALF_GRID_UNIT_SIZE + data.GRID_UNIT_SIZE));
			var perf:StabAttackState = new StabAttackState( actor, wdat, data );
			var a:Action = new Action("", 0.0, 1.0, perf, 0.0, data, b, p);
			actor.attachAction( a, 0 );
			actor.badWallMask = 0;
			
			behavior = new StateMachine();
			behavior.addState( "inactive", new InactiveState( false, actor, data, "lurk" ), true );
			behavior.addState( "lurk", new HaunterLurkState( minLurk, maxLurk, actor, data ) );
			behavior.addState( "charge", new HaunterChargeState( actor, data ) );
			behavior.addState( "attack", new HaunterAttackState( grace, actor, data ) );
			behavior.addState( "tumble", new HaunterTumbleState( actor, data ) );
		}
		
		public function handleActorEvent(e:Event):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if (e.type == HealthEvent.DAMAGE_TYPE)
			{
				if ( (e as HealthEvent).amount <= 0.0 )
				{
					return;
				}
				if ( actor.stats.armored )
				{
					if ( (e as HealthEvent).armorPen == 0.0 )
					{
						return;
					}
					actor.damage((e as HealthEvent).amount * (e as HealthEvent).armorPen);
				} else
				{
					actor.damage((e as HealthEvent).amount);
				}
				behavior.enterState("tumble");
				painAgg = MAX_PAIN_TIME;
				painBmp.visible = true;
				if (actor.stats.currentHP == 0)
				{
					actor.removeEventListener( HealthEvent.DAMAGE_TYPE, handleActorEvent );
					actor.removeEventListener( CollisionEvent.COLLISION, handleActorEvent );
					actor.dispatchEvent( new DeathEvent( DeathEvent.DEATH_TYPE, actor ) );
					data.sceneMgr.tree.remove( actor.node );
					data.sceneMgr.gameLayer.removeChild(actor);
					data.tickList.splice(data.tickList.indexOf(this), 1);
				}
			} 
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( actor != null )
			{
				behavior.destroy();
				behavior = null;
				
				actor.removeEventListener( HealthEvent.DAMAGE_TYPE, handleActorEvent );
				actor.despawn( data );
				actor.destroy();
				actor = null;
				
				painBmp = null;
				
				var idx:int = data.tickList.indexOf( this );
				if ( idx != -1 )
				{
					data.tickList.splice( idx, 1 );
				}
				data = null;
			}
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		private var multiplier:int = 1.0;
		public function tick( deltaTime:Number ):void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			var ds:Number = Maths.distanceSq( actor.pos, data.playerAtBat.pos );
			if ( ds >= INVISIBILITY_DISTANCE_SQUARED )
			{
				actor.alpha = 0.0;
			}
			else
			{
				actor.alpha = 1.0 - ( ds / INVISIBILITY_DISTANCE_SQUARED );
			}
			
			var oldPos:Point = actor.pos;
			if ( painAgg > 0.0 )
			{
				if ( (painAgg -= deltaTime ) <= 0.0 )
				{
					painBmp.visible = false;
					painAgg = 0.0;
				}
			}
			
			actor.actions[0].tick( deltaTime );
			(behavior.mActiveState as ITickable).tick( deltaTime );
		}
	}

}