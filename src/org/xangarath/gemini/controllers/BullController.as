package org.xangarath.gemini.controllers
{
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.CollisionTest;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.SceneManager;
	import org.xangarath.engine.SharedData;
	import org.xangarath.engine.statemachine.StateMachine;
	import org.xangarath.gemini.behavior.InactiveState;
	import org.xangarath.gemini.controllers.bull.BullChargeState;
	import org.xangarath.gemini.controllers.bull.BullIdleState;
	import org.xangarath.gemini.events.DeathEvent;
	import org.xangarath.gemini.events.HealthEvent;
	import flash.events.Event;
	import flash.geom.Point;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	/**
	 * It's a Bull-type
	 * @author Jeff Cochran
	 */
	public class BullController implements ITickable, IDestructable 
	{
		private static const TAG:String = "BullController";
		private static const MAX_MOTION:Number = 10.0;
		private static const COLLISION_DAMAGE:Number = 3.0;
		public var actor:Creature;
		private var data:GeminiData;
		
		private var painAgg:Number = 0.0;
		private static const MAX_PAIN_TIME:Number = 1.0;
		private var painBmp:Bitmap;
		
		private var behavior:StateMachine;
		private var lastPos:Point = new Point();
		
		private var collisionTester:CollisionTest;
		private var isConstructed:Boolean = true;
		public function BullController(speed:Number, actor:Creature, health:Number, armored:Boolean, data:GeminiData) 
		{
			this.actor = actor;
			actor.stats.baseHP = health;
			actor.heal(health);
			this.data = data;
			this.actor.addEventListener(HealthEvent.DAMAGE_TYPE, handleActorEvent);
			this.actor.addEventListener( CollisionEvent.COLLISION, handleActorEvent );
			actor.stats.armored = armored;
			painBmp = data.resMgr.getBitmap("red");
			painBmp.name = "painflash";
			actor.attachBitmap(painBmp);
			painBmp.x -= painBmp.width / 2.0;
			painBmp.y -= painBmp.height / 2.0;
			painBmp.visible = false;
			actor.facingOffset = 0.0;
			actor.overrideBoundingVolume( new Rectangle(0, 0, 75.0, 75.0) );
			actor.badWallMask = GeminiTerrainInterpreter.TERRAIN_TYPE_WALL | 
								GeminiTerrainInterpreter.TERRAIN_TYPE_WATER |
								TerrainManager.TERRAIN_TYPE_UNCROSSABLE;
			
			behavior = new StateMachine();
			behavior.addState( "inactive", new InactiveState( false, actor, data, "idle" ), true );
			behavior.addState( "idle", new BullIdleState( actor, data ) );
			behavior.addState( "charge", new BullChargeState( actor, data, speed ) );
			
			collisionTester = new CollisionTest();
		}
		
		public function handleActorEvent(e:Event):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if (e.type == HealthEvent.DAMAGE_TYPE)
			{
				if ( (e as HealthEvent).amount <= 0.0 )
				{
					return;
				}
				if ( actor.stats.armored )
				{
					if ( (e as HealthEvent).armorPen == 0.0 )
					{
						return;
					}
					actor.damage((e as HealthEvent).amount * (e as HealthEvent).armorPen);
				} else
				{
					actor.damage((e as HealthEvent).amount);
				}
				painAgg = MAX_PAIN_TIME;
				painBmp.visible = true;
				if (actor.stats.currentHP == 0)
				{
					actor.removeEventListener( HealthEvent.DAMAGE_TYPE, handleActorEvent );
					actor.removeEventListener( CollisionEvent.COLLISION, handleActorEvent );
					actor.dispatchEvent( new DeathEvent( DeathEvent.DEATH_TYPE, actor ) );
					data.sceneMgr.tree.remove( actor.node );
					data.sceneMgr.gameLayer.removeChild(actor);
					data.tickList.splice(data.tickList.indexOf(this), 1);
				}
			} else if ( e is CollisionEvent )
			{
				var instigator:Actor = (e as CollisionEvent).instigator;
				if ( ( instigator.team != actor.team ) && ( instigator != data.playerOnDeck ) )
				{
					if ( ( instigator.team == Actor.PLAYER_TEAM ) && ( collisionTester.complexActor( actor, instigator ) ) )
					{
						instigator.notifyHolder( new HealthEvent( HealthEvent.DAMAGE_TYPE, this.actor, COLLISION_DAMAGE + actor.stats.calculateEffectiveAttack( instigator ) ) );
					}
				}
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( actor != null )
			{
				behavior.destroy();
				behavior = null;
				
				actor.removeEventListener( HealthEvent.DAMAGE_TYPE, handleActorEvent );
				actor.removeEventListener( CollisionEvent.COLLISION, handleActorEvent );
				actor.despawn( data );
				actor.destroy();
				actor = null;
				
				collisionTester.destroy();
				collisionTester = null;
				
				lastPos = null;
				painBmp = null;
				
				var idx:int = data.tickList.indexOf( this );
				if ( idx != -1 )
				{
					data.tickList.splice( idx, 1 );
				}
				data = null;
			}
			isConstructed = false;
		}
		
		
		public function tick( deltaTime:Number ):void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			lastPos = actor.pos;
			if ( painAgg > 0.0 )
			{
				if ( ( painAgg -= deltaTime ) <= 0.0 )
				{
					painBmp.visible = false;
					painAgg = 0.0;
				}
			}
			
			
			var terr:uint = data.terrainMgr.getTerrainForRectangle(actor.getBoundingVolume());
			
			if ((terr & GeminiTerrainInterpreter.TERRAIN_TYPE_ACID) == GeminiTerrainInterpreter.TERRAIN_TYPE_ACID)
			{
				actor.dispatchEvent(new HealthEvent(HealthEvent.DAMAGE_TYPE, null, data.config.acidDamage));
			}
			
			(behavior.mActiveState as ITickable).tick( deltaTime );
		}
	}
}