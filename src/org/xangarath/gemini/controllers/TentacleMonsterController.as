package org.xangarath.gemini.controllers 
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.geom.Point;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.statemachine.StateMachine;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.behavior.ChaseState;
	import org.xangarath.gemini.behavior.InactiveState;
	import org.xangarath.gemini.behavior.StunState;
	import org.xangarath.gemini.events.DeathEvent;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	/**
	 * Represents a tentacle monster which creeps towards the player and deals heavy damage upon collision
	 * @author Jeffrey Cochran
	 */
	public class TentacleMonsterController implements ITickable, IDestructable
	{
		private static const TAG:String = "TentacleMonsterController";
		private var moveSpeed:Number = 2.0;
		private var damage:Number;
		public var actor:Creature;
		private var data:GeminiData;
		
		private var painAgg:Number = 0.0;
		private static const MAX_PAIN_TIME:Number = 1.0;
		private var painBmp:Bitmap;
		
		private var lastPos:Point = new Point();
		private var behavior:StateMachine;
		private var isConstructed:Boolean = true;
		public function TentacleMonsterController(damage:Number, speed:Number, actor:Creature, health:Number, armored:Boolean, data:GeminiData, walkThroughAcid:Boolean = true) 
		{
			this.moveSpeed = speed;
			this.damage = damage;
			this.actor = actor;
			actor.stats.baseHP = health;
			actor.heal(health);
			this.data = data;
			this.actor.addEventListener( HealthEvent.DAMAGE_TYPE, handleActorEvent );
			this.actor.addEventListener( CollisionEvent.COLLISION, handleActorEvent );
			actor.stats.armored = armored;
			painBmp = data.resMgr.getBitmap("red");
			painBmp.name = "painflash";
			actor.attachBitmap(painBmp);
			painBmp.x -= painBmp.width / 2.0;
			painBmp.y -= painBmp.height / 2.0;
			painBmp.visible = false;
			actor.badWallMask = TerrainManager.TERRAIN_TYPE_UNCROSSABLE | 
								GeminiTerrainInterpreter.TERRAIN_TYPE_WALL |
								GeminiTerrainInterpreter.TERRAIN_TYPE_WATER;
			if ( !walkThroughAcid )
			{
				actor.badWallMask |= GeminiTerrainInterpreter.TERRAIN_TYPE_ACID;
			}
			
			behavior = new StateMachine();
			behavior.addState( "inactive", new InactiveState( false, actor, data ), true );
			behavior.addState( "active", new ChaseState( speed, actor, data ) );
			behavior.addState( "stun", new StunState( actor ) );
		}
		
		public function handleActorEvent(e:Event):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if (e.type == HealthEvent.DAMAGE_TYPE)
			{
				if ( actor.stats.armored )
				{
					if ( (e as HealthEvent).armorPen == 0.0 )
					{
						return;
					}
					actor.damage((e as HealthEvent).amount * (e as HealthEvent).armorPen);
				} else
				{
					actor.damage((e as HealthEvent).amount);
				}
				painAgg = MAX_PAIN_TIME;
				painBmp.visible = true;
				if (actor.stats.currentHP == 0)
				{
					actor.removeEventListener( HealthEvent.DAMAGE_TYPE, handleActorEvent );
					actor.removeEventListener( CollisionEvent.COLLISION, handleActorEvent );
					actor.dispatchEvent( new DeathEvent( DeathEvent.DEATH_TYPE, actor ) );
					data.sceneMgr.tree.remove( actor.node );
					data.sceneMgr.gameLayer.removeChild(actor);
					data.tickList.splice(data.tickList.indexOf(this), 1);
				}
				
				// stun
				var stunTime:Number = (e as HealthEvent).stunTime;
				if (stunTime != 0.0)
				{
					behavior.enterState( "stun" );
					(behavior.mActiveState as StunState).stunTimeRemaining = Math.max(stunTime, (behavior.mActiveState as StunState).stunTimeRemaining);
				}
			} else if ( e is CollisionEvent )
			{
				var instigator:Actor = (e as CollisionEvent).instigator;
				if ( !(behavior.mActiveState is StunState) && ( instigator.team != actor.team ) )
				{
					instigator.notifyHolder( new HealthEvent( HealthEvent.DAMAGE_TYPE, this.actor, damage + actor.stats.calculateEffectiveAttack( instigator ) ) );
				}
				if ( ( instigator.team != Actor.ENEMY_TEAM ) && ( instigator is Actor ) ) // we've hit a wall
				{
					actor.pos = lastPos;
				}
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( actor != null )
			{
				behavior.destroy();
				behavior = null;
				
				actor.removeEventListener( HealthEvent.DAMAGE_TYPE, handleActorEvent );
				actor.removeEventListener( CollisionEvent.COLLISION, handleActorEvent );
				actor.despawn( data );
				actor.destroy();
				actor = null;
				
				lastPos = null;
				painBmp = null;
				
				var idx:int = data.tickList.indexOf( this );
				if ( idx != -1 )
				{
					data.tickList.splice( idx, 1 );
				}
				data = null;
			}
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick ( deltaTime:Number ):void 
		{
			if ( !isConstructed )
			{
				return;
			}
			lastPos = actor.pos;
			if ( painAgg > 0.0 )
			{
				if ( (painAgg -= data.secondsPerFrame) <= 0.0 )
				{
					painBmp.visible = false;
					painAgg = 0.0;
				}
			}
			
			(behavior.mActiveState as ITickable).tick( deltaTime );
			
			
			var terr:uint = data.terrainMgr.getTerrainForRectangle(actor.getBoundingVolume());
			
			if ( terr & actor.badWallMask )
			{
				actor.setPos( lastPos.x, lastPos.y );
			}
			
			if ( !(behavior.mActiveState is InactiveState) && !data.screenspace.containsPoint( actor.pos ) )
			{
				behavior.enterState("inactive");
			}
		}
		
	}

}