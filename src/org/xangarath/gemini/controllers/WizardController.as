package org.xangarath.gemini.controllers 
{
	import flash.events.Event;
	import flash.geom.Point;
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.anim.AnimatedSprite;
	import org.xangarath.engine.CollisionTest;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.statemachine.StateMachine;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.behavior.InactiveState;
	import org.xangarath.gemini.behavior.RetreatAndShootState;
	import org.xangarath.gemini.behavior.StunState;
	import org.xangarath.gemini.behavior.TumbleState;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.events.DeathEvent;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.performs.ProjectileWaveState;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	/**
	 * A wizard class that fires projectiles at the party
	 * @author Jeffrey Cochran
	 */
	public class WizardController implements ITickable, IDestructable 
	{
		private static const TAG:String = "WizardController";
		private static const WALK_SPEED:Number = 3.0;
		private var actor:Creature;
		private var data:GeminiData;
		
		// tumble vars
		private var isTumbling:Boolean = false;
		private var tumbleDir:Point = new Point();
		private var tumbleAmt:Number = 0.0;
		
		private var painAgg:Number = 0.0;
		private static const MAX_PAIN_TIME:Number = 1.0;
		private var painBmp:Bitmap;
		
		private var lastPos:Point = new Point();
		private var behavior:StateMachine;
		private var test:CollisionTest;
		
		private var deintersectDirection:Point;
		private var tmpPointA:Point;
		private var tmpPointB:Point;
		private var isConstructed:Boolean = true;
		public function WizardController( actor:Creature,  hp:Number, armored:Boolean, data:GeminiData ) 
		{
			this.actor = actor;
			this.data = data;
			actor.stats.baseHP = hp;
			actor.heal( actor.stats.maxHP );
			buildDaMagics();
			actor.addEventListener( HealthEvent.DAMAGE_TYPE, handleActorEvent );
			actor.addEventListener( CollisionEvent.COLLISION, handleActorEvent );
			actor.stats.armored = armored;
			painBmp = data.resMgr.getBitmap("red");
			painBmp.name = "painflash";
			actor.attachBitmap(painBmp);
			painBmp.x -= painBmp.width / 2.0;
			painBmp.y -= painBmp.height / 2.0;
			painBmp.visible = false;
			actor.badWallMask = TerrainManager.TERRAIN_TYPE_UNCROSSABLE | 
								GeminiTerrainInterpreter.TERRAIN_TYPE_WALL |
								GeminiTerrainInterpreter.TERRAIN_TYPE_ACID |
								GeminiTerrainInterpreter.TERRAIN_TYPE_WATER;
			
			behavior = new StateMachine();
			behavior.addState( "inactive", new InactiveState( false, actor, data ), true );
			behavior.addState( "active", new RetreatAndShootState( WALK_SPEED, attack, actor, data ) );
			var stun:StunState = new StunState( actor );
			behavior.addState( "stun", stun );
			behavior.addState( "tumble", new TumbleState( actor, data, "stun", stun ) );
			
			test = new CollisionTest();
			deintersectDirection = new Point();
		}
		
		private function handleActorEvent( e:Event ):void
		{
			if (e.type == HealthEvent.DAMAGE_TYPE)
			{
				if ( actor.stats.armored )
				{
					if ( (e as HealthEvent).armorPen == 0.0 )
					{
						return;
					}
					actor.damage((e as HealthEvent).amount * (e as HealthEvent).armorPen);
				} else
				{
					actor.damage((e as HealthEvent).amount);
				}
				painAgg = MAX_PAIN_TIME;
				painBmp.visible = true;
				if (actor.stats.currentHP == 0)
				{
					actor.removeEventListener( HealthEvent.DAMAGE_TYPE, handleActorEvent );
					actor.removeEventListener( CollisionEvent.COLLISION, handleActorEvent );
					actor.dispatchEvent( new DeathEvent( DeathEvent.DEATH_TYPE, actor ) );
					data.sceneMgr.tree.remove( actor.node );
					data.sceneMgr.gameLayer.removeChild(actor);
					data.tickList.splice(data.tickList.indexOf(this), 1);
				}
				var knockback:Number = (e as HealthEvent).knockback;
				if( knockback != 0.0 )
				{
					behavior.enterState( "tumble" );
					var tumble:TumbleState = ( behavior.mActiveState as TumbleState );
					tumble.stunTime = ( e as HealthEvent ).stunTime;
					tumble.direction.copyFrom( actor.pos.subtract((e as HealthEvent).instigator.pos) );
					tumble.direction.normalize( 1.0 );
					tumble.distanceSquared = knockback * knockback;
				}
				else if ((e as HealthEvent).stunTime != 0.0)
				{
					var stunTime:Number = ( e as HealthEvent ).stunTime;
					behavior.enterState( "stun" );
					(behavior.mActiveState as StunState).stunTimeRemaining = Math.max(stunTime, (behavior.mActiveState as StunState).stunTimeRemaining);
				}
			} else if ( e is CollisionEvent )
			{
				var instigator:Actor = (e as CollisionEvent).instigator;
				if ( ( instigator.team != Actor.ENEMY_TEAM ) && ( instigator is Actor ) ) // we've hit a wall
				{
					handleSliding( instigator );
				}
			}
		}
		
		private var newDir:Point = new Point();
		private function handleSliding( instigator:Actor ):void
		{
			deintersectDirection.setTo( 0.0, 0.0 );
					
			var lines:Array = test.getIntersectingLinesSimple( actor, instigator );
					
			while ( lines.length > 0 )
			{
				tmpPointB = lines.pop();
				tmpPointA = lines.pop();
						
				tmpPointA.x = tmpPointB.x - tmpPointA.x;
				tmpPointA.y = tmpPointB.y - tmpPointA.y;
						
				tmpPointA.normalize( 1.0 );
						
				tmpPointA.setTo( -tmpPointA.y, tmpPointA.x );
						
				deintersectDirection.setTo( deintersectDirection.x + tmpPointA.x, deintersectDirection.y + tmpPointA.y );
				deintersectDirection.normalize( 1.0 );
			}
			
			newDir.setTo( actor.pos.x - lastPos.x, actor.pos.y - lastPos.y );
			newDir.x = ( deintersectDirection.x != 0.0 ) ? 0.0 : newDir.x;
			newDir.y = ( deintersectDirection.y != 0.0 ) ? 0.0 : newDir.y;
			newDir.normalize( 1.0 );
			
			
			actor.setPos( lastPos.x, lastPos.y );
			
			actor.move( newDir.x * WALK_SPEED, newDir.y * WALK_SPEED );
		}
		
		private function buildDaMagics():void
		{
			var wdat:WeaponData = new WeaponData("", 0, Actor.ENEMY_TEAM, 0.25, 0.0, 0.0, 1.0 );
			wdat.stats = actor.stats;
			var perf:ProjectileWaveState = new ProjectileWaveState( actor, false, 4.0, data, wdat, Actor.ENEMY_TEAM );
			var action:Action = new Action("", 0.0, 1.0, perf, 3.0, data );
			actor.attachAction( action, 0 );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		private function isAttackInPlayback():Boolean
		{
			if ( actor.animSprite.animationName.search( "Attack" ) == -1 )
			{
				return false;
			}
			
			return !actor.animSprite.isDonePlaying;
		}
		
		private function getDirect():int
		{
			var direct:int = actor.getCardinalDirection();
			if ( direct == Creature.ANIM_DOWN ) { direct = Creature.ANIM_UP; }
			else if ( direct == Creature.ANIM_UP ) { direct = Creature.ANIM_DOWN; }
			else if ( direct == Creature.ANIM_LEFT ) { direct = Creature.ANIM_RIGHT; }
			else
			{
				direct = Creature.ANIM_LEFT;
			}
			return direct;
		}
		
		private function getDirectionString( dir:int ):String
		{
			switch ( dir )
			{
				case Creature.ANIM_DOWN:
					return "Down";
				case Creature.ANIM_LEFT:
					return "Left";
				case Creature.ANIM_RIGHT:
					return "Right";
				case Creature.ANIM_UP:
					return "Up";
			}
			return null;
		}
		
		private var startedAttackAnimation:Boolean = false;
		private function attack( deltaTime:Number ):void
		{
			deltaTime = 1.0 / 30.0;
			var direct:int = getDirect();
			
			if ( isAttackInPlayback() && startedAttackAnimation )
			{
				
			}
			else if ( !actor.actions[0].isBlocking() && ( actor.actions[0].isReady() ) )
			{
				actor.setDirection( data.party.atBat.x - actor.x, data.party.atBat.y - actor.y );
				actor.animSprite.setAnimation( getDirectionString( actor.getCardinalDirection() ) + "Attack" );
				actor.actions[0].perform();
				actor.animSprite.play();
				startedAttackAnimation = true;
			}
			else if ( ( lastPos.x == actor.pos.x ) && ( lastPos.y == actor.pos.y ) )
			{
				if ( actor.animSprite.animationName != ( getDirectionString( direct ) + "Idle" ) )
				{
					actor.setDirection( data.party.atBat.x - actor.x, data.party.atBat.y - actor.y );
					actor.animSprite.setAnimation( getDirectionString( actor.getCardinalDirection() ) + "Idle" );
					actor.animSprite.play();
					startedAttackAnimation = false;
				}
			}
			else
			{
				if ( actor.animSprite.animationName != ( getDirectionString( direct ) + "Walk" ) )
				{
					actor.setDirection( data.party.atBat.x - actor.x, data.party.atBat.y - actor.y );
					actor.animSprite.setAnimation( getDirectionString( actor.getCardinalDirection() ) + "Walk" );
					actor.animSprite.play();
					startedAttackAnimation = false;
				}
			}
			
			actor.actions[0].tick( deltaTime );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( actor != null )
			{
				behavior.destroy();
				behavior = null;
				
				deintersectDirection = null;
				lastPos = null;
				newDir = null;
				
				painBmp = null;
				
				test.destroy();
				test = null;
				
				tmpPointA = null;
				tmpPointB = null;
				tumbleDir = null;
				
				actor.despawn( data );
				actor.destroy();
				actor = null;
				
				var idx:int = data.tickList.indexOf( this );
				if ( idx != -1 )
				{
					data.tickList.splice( idx, 1 );
				}
				data = null;
			}
			
			isConstructed = false;
		}
		
		public function tick( deltaTime:Number ):void 
		{	
			if ( !isConstructed )
			{
				return;
			}
			lastPos = actor.pos;
			if ( painAgg > 0.0 )
			{
				if ( ( painAgg -= deltaTime ) <= 0.0 )
				{
					painBmp.visible = false;
					painAgg = 0.0;
				}
			}
			
			(behavior.mActiveState as ITickable).tick( deltaTime );
			
			var terr:uint = data.terrainMgr.getTerrainForRectangle(actor.getBoundingVolume());
			
			if ( ( terr & actor.badWallMask ) != 0 )
			{
				actor.setPos( lastPos.x, lastPos.y );
			}
			
			if ( !(behavior.mActiveState is InactiveState) && !data.screenspace.containsRect( actor.getBoundingVolume() ) )
			{
				actor.setPos( lastPos.x, lastPos.y );
				actor.animSprite.setAnimation( getDirectionString( actor.getCardinalDirection() ) + "Idle" );
				behavior.enterState("inactive");
				actor.animSprite.play();
			}
			
			if ( !( behavior.mActiveState is InactiveState ) )
			{
				(actor.drawable as AnimatedSprite).tick( deltaTime );
			}
		}
		
	}

}