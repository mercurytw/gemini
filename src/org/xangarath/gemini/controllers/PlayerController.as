package org.xangarath.gemini.controllers
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	import HealthBar;
	import org.xangarath.engine.ActionWidget;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.StatLogger;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.events.InventoryEvent;
	import org.xangarath.gemini.events.PartyEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.items.Item;
	import org.xangarath.gemini.stats.NumericStat;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	
	/**
	 * Player controller. Controlled by keys.
	 * @author Jeff Cochran
	 */
	public class PlayerController implements ITickable
	{
		private static const TAG:String = "PlayerController";
		private static const DAMAGE_INVINCIBILITY_TIME:Number = 1.0;
		private static const UP:int = 0;
		private static const LEFT:int = 1;
		private static const RIGHT:int = 2;
		private static const DOWN:int = 3;
		private static const MOVE_SPEED:Number = 2.5;
		
		public var act:Creature;
		private var dirs:Array = [false, false, false, false];
		private var timeSinceLastTrigger:Vector.<Number>;
		private var dirFacing:Vector3D = new Vector3D();
		private var lastPos:Point;
		
		private var attackKeyDown:Array;
		private var weaponWidgets:Array;
		private var secondaryWeaponWidgets:Array;
		private var swapKeyDown:Boolean;
		
		private var invincibilityTime:Number = 0.0;
		
		private var data:GeminiData;
		private var jumpAction:Action;
		private var dampedMoveSpeed:NumericStat = new NumericStat( 1.0 );
		
		public function PlayerController(actor:Creature, data:GeminiData) 
		{
			act = actor;
			lastPos = new Point( actor.x, actor.y );
			this.data = data;
			attackKeyDown = new Array();
			swapKeyDown = false;
			data.party.stats._moveSpeed.addPremultiplicand( dampedMoveSpeed );
			
			actor.badWallMask = TerrainManager.TERRAIN_TYPE_UNCROSSABLE | 
								GeminiTerrainInterpreter.TERRAIN_TYPE_WALL |
								GeminiTerrainInterpreter.TERRAIN_TYPE_WATER;
			
			attackKeyDown.push(false, false, false, false, false);
			timeSinceLastTrigger = new Vector.<Number>();
			timeSinceLastTrigger.push( 0.0, 0.0, 0.0, 0.0 );
			timeSinceLastTrigger.fixed = true;
			
			data.party.addEventListener( PartyEvent.PARTY_EVENT, handleActorEvent );
			actor.stage.addEventListener(KeyboardEvent.KEY_UP, keycatch);
			actor.stage.addEventListener(KeyboardEvent.KEY_DOWN, keycatch);
			actor.stage.nativeWindow.addEventListener( Event.DEACTIVATE, onFocusLoss );
		}
		
		// Unsets keydown flags when window loses focus
		private function onFocusLoss( e:Event ):void
		{
			DebugLog.getSingleton().d( TAG, "Game window lost focus" );
			dirs[ 0 ] = dirs[ 1 ] = dirs[ 2 ] = dirs[ 3 ] = false;
			swapKeyDown = false;
			
			for ( var i:int = 0; i < attackKeyDown.length; i++ )
			{
				attackKeyDown[ i ] = false;
			}
		}
		
		public function set jump( action:Action ):void
		{
			jumpAction = action;
		}
		
		private function handleActorEvent(e:Event):void
		{
			if ( e is PartyEvent )
			{
				e = (e as PartyEvent).containedEvent;
			}
			if (e is HealthEvent)
			{
				if (e.type == HealthEvent.HEAL_TYPE)
				{
					data.party.atBat.heal((e as HealthEvent).amount);
					return;
				}
				if ( (e as HealthEvent).target != data.party.atBat ) // only affect character at bat
				{
					return;
				}
				if ( data.party.atBat.isInvincible() )
				{
					return;
				}
				if ( (invincibilityTime > 0.0)  && ((e as HealthEvent).amount > 0.0) )
				{
					return;
				}
				var amt:Number = (e as HealthEvent).amount;

				data.party.atBat.damage(amt);
				registerDamage( (e as HealthEvent).amount, (e as HealthEvent).instigator );
				if ( data.party.stats.currentHP == 0 ) 
				{
					DebugLog.getSingleton().i( TAG, "Player died." );
					if ( data.party.inventory.getEntry( "feather" ) != null )
					{
						data.party.inventory.remove( "feather" );
						data.party.atBat.heal( 999999 );
						DebugLog.getSingleton().i( TAG, "used a feather");
						StatLogger.getSingleton().addValue( "feather", 1.0 );
					}
					else
					{
						data.party.removeAllFromScene();
						data.tickList.splice( data.tickList.indexOf( this ), 1 );
					}
				}
				if ((e as HealthEvent).amount > 0.0) // stun attacks shouldn't trigger invincibility
				{
					invincibilityTime = DAMAGE_INVINCIBILITY_TIME;
				}
			}
			else if ( e is InventoryEvent )
			{
				if (e.type == InventoryEvent.ATTACH)
				{
					//attachAction((e as InventoryEvent).slot, (e as InventoryEvent).recipient);
				}
				else
				{
					//detatchAction((e as InventoryEvent).slot, (e as InventoryEvent).recipient);
				}
			}
			else if ( e is CollisionEvent )
			{
				if ( e.target == data.party.onDeck )
				{
					return;
				}
				// TODO: is this code right???
				var collider:Actor = ( e as CollisionEvent ).instigator;
				if ( ( collider is Actor ) && ( collider != data.party.onDeck ) && ( collider.team != Actor.PLAYER_TEAM ) )
				{
					if ( collider is Item )
					{
						var item:Item = collider as Item;
						item.pickUp( data.party.inventory, data );
						DebugLog.getSingleton().i( TAG, item.name + " GET!" );
					}
					else
					{
						// comment this out for wallwalk
						act.setPos( lastPos.x, lastPos.y );
						act.setDirection( -act.dirX, -act.dirY );
					}
				}
			}
		}
		
		public function possessConnor( actr:Creature ):void
		{
			actr.badWallMask = data.party.atBat.badWallMask;
		}
		
		public function keycatch(e:KeyboardEvent):void
		{
			var dirstr:String = "";
			var dir:int = -1;
			var truth:Boolean = e.type == KeyboardEvent.KEY_DOWN;
			e.preventDefault();
			switch(e.keyCode)
			{
				case Keyboard.UP:
					dirs[UP] = truth;
					if (truth) { dir = UP; }
					dirstr = "UP";
					break;
				case Keyboard.LEFT:
					dirs[LEFT] = truth;
					if (truth) { dir = LEFT; }
					dirstr = "LEFT";
					break;
				case Keyboard.RIGHT:
					dirs[RIGHT] = truth;
					if (truth) { dir = RIGHT; }
					dirstr = "RIGHT";
					break;
				case Keyboard.DOWN:
					dirs[DOWN] = truth;
					if (truth) { dir = DOWN; }
					dirstr = "DOWN";
					break;
				case Keyboard.Q:
					attackKeyDown[0] = truth;
					break;
				case Keyboard.W:
					attackKeyDown[1] = truth;
					break;
				case Keyboard.E:
					attackKeyDown[2] = truth;
					break;
				case Keyboard.R:
					attackKeyDown[3] = truth;
					break;
				case Keyboard.F:
					swapKeyDown = truth;
					break;
				case Keyboard.SPACE:
					attackKeyDown[4] = truth;
					break;
				default:
					break;
			}
			
		}
		
		private var swapBlockAgg:Number = 0.0;
		private var flashTimeAgg:Number = 0.0;
		private var dirMoving:Point = new Point();
		public function tick( deltaTime:Number ):void
		{
			lastPos = act.pos;
			dirMoving.setTo( 0, 0 );
			if (dirs[UP]) { dirMoving.y -= MOVE_SPEED; }
			if (dirs[DOWN]) { dirMoving.y += MOVE_SPEED; }
			
			if (dirs[LEFT]) { dirMoving.x -= MOVE_SPEED; }
			if (dirs[RIGHT]) { dirMoving.x += MOVE_SPEED; }
			
			var isAnyActionBlocking:Boolean = false;
			dampedMoveSpeed.value = 1.0;
			for (var i:int = 0; i < act.actions.length; i++)
			{
				if (act.actions[i] == null)
				{
					continue;
				}
				if (act.actions[i].isBlocking())
				{
					isAnyActionBlocking = true;
				}
				dampedMoveSpeed.value = Math.min( dampedMoveSpeed.value, act.actions[ i ].getDoesAllowMovement() );
			}
			
			if ( ( jumpAction != null ) && ( !isAnyActionBlocking ) )
			{
				isAnyActionBlocking = jumpAction.isBlocking();
			}
			
			swapBlockAgg = Math.max( 0.0, swapBlockAgg - deltaTime);
			if ( !isAnyActionBlocking && ( swapKeyDown ) && ( swapBlockAgg == 0.0 ))
			{
				swapBlockAgg = 0.75;
				
				if ( data.party.size > 1 )
				{
					act = data.party.atBat = data.party.onDeck;
					data.party.onDeck.visible = true;
				}
			}
			
			dampedMoveSpeed.value = Math.min( dampedMoveSpeed.value, jumpAction.getDoesAllowMovement() );
			
			if ( dampedMoveSpeed.value != 0.0 )
			{
				dirMoving.normalize( 1.0 );
				
				act.move( dirMoving.x * MOVE_SPEED * data.GRID_UNIT_SIZE * deltaTime * dampedMoveSpeed.value, 
				          dirMoving.y * MOVE_SPEED * data.GRID_UNIT_SIZE * deltaTime * dampedMoveSpeed.value );
			}
			if ( ( dirMoving.x != 0.0 ) || ( dirMoving.y != 0.0 ) )
			{
				act.setDirection( dirMoving.x, dirMoving.y );
			}
			
			var foundAttack:Boolean = false;
			for ( i = 0; i < act.actions.length; i++ )
			{
				timeSinceLastTrigger[ i ] = Math.min( timeSinceLastTrigger[ i ] + deltaTime, 1.0 );
				if ((act.actions[i] == null) || !act.actions[i].enabled)
				{
					continue;
				}
				if ((foundAttack == false) && (attackKeyDown[i] == true) && (!isAnyActionBlocking) && ( timeSinceLastTrigger[ i ] > 0.25 ))
				{
					timeSinceLastTrigger[ i ] = 0.0;
					act.actions[i].perform();
					foundAttack = true;
					registerAttack( i );
				}
				act.actions[i].tick( deltaTime );
			}
			
			if ( ( jumpAction != null ) && ( foundAttack  == false ) && ( attackKeyDown[ 4 ] ) && ( !isAnyActionBlocking ) )
			{
				foundAttack = true;
				jumpAction.perform();
				StatLogger.getSingleton().addValue( StatLogger.JUMPS, 1 );
			}
			jumpAction.tick( deltaTime );
			
			var terr:uint = data.terrainMgr.getTerrainForRectangle(act.getBoundingVolume());
			
			if ((terr & GeminiTerrainInterpreter.TERRAIN_TYPE_ACID) == GeminiTerrainInterpreter.TERRAIN_TYPE_ACID)
			{
				act.dispatchEvent(new HealthEvent(HealthEvent.DAMAGE_TYPE, null, data.config.acidDamage));
			}
			// TODO: Add bad terrain mask to the actor where the cheat system will be able to get at it
			
			if ( ( terr & act.badWallMask ) != 0 )
			{
				act.setPos( lastPos.x, lastPos.y );
				act.setDirection( -act.dirX, -act.dirY );
			}
			
			if (invincibilityTime > 0.0)
			{
				invincibilityTime = Math.max(invincibilityTime - deltaTime, 0.0);
				
				if ((flashTimeAgg += data.secondsPerFrame) >= 0.1)
				{
					flashTimeAgg = 0.0;
					act.visible = !act.visible;
				}
				if (invincibilityTime <= 0.0)
				{
					act.visible = true;
				}
			}
			
			data.party.tick( deltaTime );
		}
		
		private function registerDamage( amt:Number, instigator:Actor ):void
		{
			if ( CONFIG::debug )
			{
				StatLogger.getSingleton().addValue( StatLogger.DAMAGE_TAKEN, amt );
				
				if (instigator == null)
				{
					DebugLog.getSingleton().i(TAG, "Player took " + amt + " points of general damage");
					return;
				}
				
				var type:String = instigator.name.slice( 0, instigator.name.indexOf("#") );
				switch ( type )
				{
					case "scootaspider":
						StatLogger.getSingleton().addValue( StatLogger.DAMAGE_SPIDER, amt );
						break;
					case "AutonomousProjectile":
						StatLogger.getSingleton().addValue( StatLogger.DAMAGE_WIZARD, amt );
						break;
					case "bull":
						StatLogger.getSingleton().addValue( StatLogger.DAMAGE_ROCKET, amt );
						break;
					case "haunter":
						StatLogger.getSingleton().addValue( StatLogger.DAMAGE_GHOST, amt );
						break;
					case "tentacle":
						StatLogger.getSingleton().addValue( StatLogger.DAMAGE_TENTACLE, amt );
						break;
					case "dog":
						StatLogger.getSingleton().addValue( StatLogger.DAMAGE_DOG, amt );
						break;
					default:
						DebugLog.getSingleton().w( TAG, "Got damage from unknown creature type \"" + type + "\", unable to log " );
				}
			}
		}
		
		private function registerAttack( index:int ):void
		{
			if ( CONFIG::debug )
			{
				if ( data.party.atBat.id == data.annie.id )
				{
					StatLogger.getSingleton().addValue( StatLogger.ANNIE_ATTACKS, 1 );
					switch( index )
					{
						case 0:
							StatLogger.getSingleton().addValue( StatLogger.ANNIE_Q, 1 );
							break;
						case 1:
							StatLogger.getSingleton().addValue( StatLogger.ANNIE_W, 1 );
							break;
						case 2:
							StatLogger.getSingleton().addValue( StatLogger.ANNIE_E, 1 );
							break;
						case 3:
							StatLogger.getSingleton().addValue( StatLogger.ANNIE_R, 1 );
							break;
					}
				}
				else
				{
					StatLogger.getSingleton().addValue( StatLogger.CONNOR_ATTACKS, 1 );
					switch( index )
					{
						case 0:
							if ( StatLogger.getSingleton().connorUlting )
								StatLogger.getSingleton().addValue( StatLogger.CONNOR_RQ, 1 );
							else
								StatLogger.getSingleton().addValue( StatLogger.CONNOR_Q, 1 );
							break;
						case 1:
							if ( StatLogger.getSingleton().connorUlting )
								StatLogger.getSingleton().addValue( StatLogger.CONNOR_RW, 1 );
							else
								StatLogger.getSingleton().addValue( StatLogger.CONNOR_W, 1 );
							break;
						case 2:
							if ( StatLogger.getSingleton().connorUlting )
								StatLogger.getSingleton().addValue( StatLogger.CONNOR_RE, 1 );
							else
								StatLogger.getSingleton().addValue( StatLogger.CONNOR_E, 1 );
							break;
						case 3:
							StatLogger.getSingleton().addValue( StatLogger.CONNOR_R, 1 );
							break;
					}
				}
			}
		}
	}
}