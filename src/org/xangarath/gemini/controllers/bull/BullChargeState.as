package org.xangarath.gemini.controllers.bull 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.CollisionTest;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	/**
	 * State representing bull charging behavior
	 * @author Jeffrey Cochran
	 */
	public class BullChargeState extends EventDispatcher implements ITickable, IState, IDestructable 
	{
		private static const TAG:String = "BullChargeState";
		private var actor:Creature;
		private var data:GeminiData;
		private var speed:Number;
		private var collisionTester:CollisionTest;
		private var lastPos:Point = new Point();
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function BullChargeState( actor:Creature, data:GeminiData, chargeSpeed:Number ) 
		{
			super();
			this.actor = actor;
			this.data = data;
			speed = chargeSpeed;
			queryRect.width = data.playArea.width * 1.3;
			queryRect.height = data.playArea.height * 1.3;
			collisionTester = new CollisionTest();
		}
		
		private function handleCollision( e:Event ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var instigator:Actor = (e as CollisionEvent).instigator;
			
			
			if ( ( instigator.team != actor.team ) && ( instigator != data.party.onDeck ) )
			{
				if ( ( instigator == data.party.atBat ) && !( collisionTester.complexActor( instigator, actor ) ) )
				{
					return;
				}
				actor.setDirection( -actor.dirX, -actor.dirY );
				if ( ( instigator.team == Actor.NO_TEAM ) && ( instigator is Actor ) )
				{
					do 
					{
						actor.move( actor.dirX * ( actor.getBoundingVolume().width / 2.0 ), actor.dirY * ( actor.getBoundingVolume().height / 2.0 ) );
					} while ( collisionTester.simple( actor, instigator ) )
				}
				else 
				{
					actor.move( actor.dirX * ( actor.getBoundingVolume().width / 2.0 ), actor.dirY * ( actor.getBoundingVolume().height / 2.0 ) );
				}
				dispatchEvent( new StateChangeEvent( "idle", StateChangeEvent.STATE_CHANGE ) );
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			actor.addEventListener( CollisionEvent.COLLISION, handleCollision );
			isSetup = true;
		}
		
		public function teardown():void 
		{
			actor.removeEventListener( CollisionEvent.COLLISION, handleCollision );
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			collisionTester.destroy();
			collisionTester = null;
			
			lastPos = null;
			queryRect = null;
			
			actor = null;
			data = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		private var queryRect:Rectangle = new Rectangle();
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed || !isSetup )
			{
				return;
			}
			
			var terrainCollide:Boolean = false;
			lastPos = actor.pos;
			
			/*
			if ( !data.sceneMgr.isValidMove( actor, data.party.atBat.x, data.party.atBat.y, data.terrainMgr ) )
			{
				dispatchEvent( new StateChangeEvent( "idle", StateChangeEvent.STATE_CHANGE ) );
				return;
			}
			*/
			
			actor.move( actor.dirX * speed * data.GRID_UNIT_SIZE * deltaTime, actor.dirY * speed * data.GRID_UNIT_SIZE * deltaTime );
			
			
			var terr:uint = data.terrainMgr.getTerrainForRectangle(actor.getBoundingVolume());
			
			if (((terr & GeminiTerrainInterpreter.TERRAIN_TYPE_WALL) == GeminiTerrainInterpreter.TERRAIN_TYPE_WALL) || 
			   	((terr & GeminiTerrainInterpreter.TERRAIN_TYPE_WATER) == GeminiTerrainInterpreter.TERRAIN_TYPE_WATER) ||
				((terr & TerrainManager.TERRAIN_TYPE_UNCROSSABLE) == TerrainManager.TERRAIN_TYPE_UNCROSSABLE))

			{
				actor.setDirection( -actor.dirX, -actor.dirY );
				do
				{
					actor.move( actor.dirX * ( actor.getBoundingVolume().width / 2.0 ), actor.dirY * ( actor.getBoundingVolume().height / 2.0 ) );
				} while (!data.sceneMgr.isEmptySpace( actor, data.terrainMgr ));
				dispatchEvent( new StateChangeEvent( "idle", StateChangeEvent.STATE_CHANGE ) );
			}
			
			queryRect.x = actor.x - ( (data.playArea.width * 1.3) / 2.0 );
			queryRect.y = actor.y - ( (data.playArea.height * 1.3) / 2.0 );
			
			if ( !queryRect.intersects( data.playerAtBat.getBoundingVolume() ) )
			{
				dispatchEvent( new StateChangeEvent( "idle", StateChangeEvent.STATE_CHANGE ) );
			}
		}
		
	}

}