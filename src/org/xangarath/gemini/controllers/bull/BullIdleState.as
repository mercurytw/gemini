package org.xangarath.gemini.controllers.bull 
{
	import flash.display3D.textures.RectangleTexture;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.CollisionTest;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.engine.world.TerrainManager;
	import org.xangarath.gemini.world.GeminiTerrainInterpreter;
	import org.xangarath.engine.dbg.DebugLog;
	
	/**
	 * Behavior for an idling bull. Tracks player position
	 * @author Jeffrey Cochran
	 */
	public class BullIdleState extends EventDispatcher implements IState, ITickable, IDestructable 
	{
		private static const TAG:String = "BullIdleState";
		private static const IDLE_TIME:Number = 2.0;
		private var idleTimeRemaining:Number;
		private var actor:Creature;
		private var data:GeminiData;
		private var shakeAmt:Number = 0.0;
		private var test:CollisionTest;
		private var deintersectDirection:Point;
		private var tmpPointA:Point;
		private var tmpPointB:Point;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function BullIdleState( act:Creature, data:GeminiData ) 
		{
			super();
			this.actor = act;
			this.data = data;
			test = new CollisionTest();
			deintersectDirection = new Point();
		}
		
		private function handleActorEvent( e:Event ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var counter:int = 0;
			if ( e is CollisionEvent )
			{
				var instigator:Actor = ( e as CollisionEvent ).instigator;
				if ( instigator.id == data.party.onDeck.id )
				{
					return;
				}
				
				if ( instigator.team == Actor.NO_TEAM )
				{
					deintersectDirection.setTo( 0.0, 0.0 );
					
					var lines:Array = test.getIntersectingLinesSimple( actor, instigator );
					
					while ( lines.length > 0 )
					{
						tmpPointB = lines.pop();
						tmpPointA = lines.pop();
						
						tmpPointA.x = tmpPointB.x - tmpPointA.x;
						tmpPointA.y = tmpPointB.y - tmpPointA.y;
						
						tmpPointA.normalize( 1.0 );
						
						tmpPointA.setTo( -tmpPointA.y, tmpPointA.x );
						
						deintersectDirection.setTo( deintersectDirection.x + tmpPointA.x, deintersectDirection.y + tmpPointA.y );
						deintersectDirection.normalize( 1.0 );
					}
					
					while ( actor.getBoundingVolume().intersects( instigator.getBoundingVolume() ) )
					{
						actor.move( deintersectDirection.x * Math.max( actor.width, actor.height ) / 2.0, 
									deintersectDirection.y * Math.max( actor.width, actor.height ) / 2.0 );
									
						CONFIG::debug
						{
							if ((++counter != 0) && (counter % 50 == 0))
							{
								DebugLog.getSingleton().w( TAG, "Tried to deintersect bull " + counter + " times!");
							}
						}
					}
				}
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			if ( isSetup )
			{
				return;
			}
			
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			idleTimeRemaining = IDLE_TIME;
			shakeAmt = 0.0;
			actor.addEventListener( CollisionEvent.COLLISION, handleActorEvent );
			isSetup = true;
		}
		
		public function teardown():void 
		{
			actor.removeEventListener( CollisionEvent.COLLISION, handleActorEvent );
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			deintersectDirection = null;
			test.destroy();
			test = null;
			tmpPointA = null;
			tmpPointB = null;
			
			actor = null;
			data = null;
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		private var upOrDown:int = 1.0;
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed || !isSetup )
			{
				return;
			}
			
			var target:Point = data.playerAtBat.pos;
			
			
			
			if ( !data.sceneMgr.isValidMove( actor, target.x, target.y, data.terrainMgr ) )
			{
				shakeAmt = 0.0;
				idleTimeRemaining = IDLE_TIME;
			}
			else
			{
				if ( ( idleTimeRemaining -= deltaTime ) <= 0.0 )
				{
					dispatchEvent( new StateChangeEvent("charge", StateChangeEvent.STATE_CHANGE) );
				}
				if ( idleTimeRemaining < 2.0 )
				{
					upOrDown *= -1;
					shakeAmt += 0.2;
					actor.move( -actor.dirY * shakeAmt * upOrDown, actor.dirX * shakeAmt * upOrDown );
				}
			}
			actor.face( data.playerAtBat );
		}
	}

}