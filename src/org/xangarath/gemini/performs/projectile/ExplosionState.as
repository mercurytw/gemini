package org.xangarath.gemini.performs.projectile 
{
	import flash.display.Bitmap;
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.anim.AnimatedSprite;
	import org.xangarath.engine.anim.AnimationSequence;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IPoolable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.pool.Pool;
	import org.xangarath.engine.quadtree.QuadTreeNode;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.explosion.Explosion;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IActionState;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.weapons.WeaponData;
	
	/**
	 * Represents an explosion from one of connor's explosive rounds
	 * @author Jeffrey Cochran
	 */
	public class ExplosionState extends EventDispatcher implements ITickable, IState, IActionState, IWeaponStatAcceptor 
	{
		public static const TYPE:String = "explosiontype";
		private static var explosionPool:Pool = null;
		private var _weaponData:WeaponData;
		private var actor:Actor;
		private var data:GeminiData;
		private var hitbox:Rectangle;
		private var radius:Number;
		public function ExplosionState( explosionRadius:Number, actor:Actor, data:GeminiData ) 
		{
			super();
			this.actor = actor;
			this.data = data;
			radius = explosionRadius * data.GRID_UNIT_SIZE;
			hitbox = new Rectangle( 0, 0, 2 * radius, 2 * radius );
		}
		
		private static function getExplosion( scale:Number, data:GeminiData ):Explosion
		{
			if ( explosionPool == null )
			{
				var explosion:Explosion = new Explosion( new AnimatedSprite( new <AnimationSequence>[data.animMgr.getSequence("explosion")], data.animMgr ), null, data );
				explosionPool = new Pool( explosion, 10 );
			}
			var ex:Explosion = explosionPool.getObject() as Explosion;
			ex.scaleX = ex.scaleY = scale;
			return ex;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IWeaponStatAcceptor */
		
		public function set weaponData( data:WeaponData ):void
		{
			_weaponData = data;
		}
		
		public function get weaponData():WeaponData
		{
			return _weaponData;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IActionState */
		
		public function getDoesAllowMovement():Number 
		{
			return 1.0;
		}
		
		public function getIsBlocking():Boolean 
		{
			return false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
		}
		
		public function teardown():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			var act:Actor;
			hitbox.x = actor.x - radius;
			hitbox.y = actor.y - radius;
			
			var colliders:Vector.<QuadTreeNode> = data.sceneMgr.rectSceneQuery( hitbox );
			while ( colliders.length > 0 )
			{
				var tmp:Object =  colliders.pop().owner;
				if ( !(tmp is Actor) )
				{
					continue;
				}
				
				act = tmp as Actor;
				var dmg:Number = (_weaponData.damage == 0) ? 0 : _weaponData.damage + _weaponData.stats.calculateEffectiveAttack( act );
				if (act.team != _weaponData.team)
				{
					act.notifyHolder(new HealthEvent(HealthEvent.DAMAGE_TYPE, actor, dmg, _weaponData.armorPenetration, _weaponData.knockback, _weaponData.stunTime));
				}
			}
			var ex:Explosion = getExplosion( ( radius / data.GRID_UNIT_SIZE ) * 2.0, data );
			ex.x = actor.x - ( ex.width / 2.0 );
			ex.y = actor.y - ( ex.height / 2.0 );
			dispatchEvent( new StateChangeEvent( "cooldown", StateChangeEvent.STATE_CHANGE ) );
		}
		
	}

}