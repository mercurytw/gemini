package org.xangarath.gemini.performs.projectile 
{
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.anim.AnimatedSprite;
	import org.xangarath.engine.anim.AnimationSequence;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.geom.Triangle;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.pool.Pool;
	import org.xangarath.engine.quadtree.QuadTreeNode;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.explosion.Explosion;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IActionState;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.weapons.WeaponData;
	
	/**
	 * Represents an explosion which deals damage to the target, and causes penetrating explosion
	 * damage behind the point of collision
	 * @author Jeffrey Cochran
	 */
	public class ConicExplosionState extends EventDispatcher implements ITickable, IState, IActionState, IWeaponStatAcceptor 
	{
		public static const TYPE:String = "conicexplosiontype";
		private static var explosionPool:Pool = null;
		private var _weaponData:WeaponData;
		private var actor:Actor;
		private var data:GeminiData;
		private var hitbox:Triangle;
		private var triCalcVect:Point;
		private var expDamage:Number;
		private var halfTheta:Number;
		private var radius:Number;
		public function ConicExplosionState( explosionDamage:Number, explosionRadius:Number, thetaDeg:Number, actor:Actor, data:GeminiData ) 
		{
			super();
			this.actor = actor;
			this.data = data;
			triCalcVect = new Point( 1.0, 0.0 );
			hitbox = new Triangle();
			halfTheta = thetaDeg / 2.0;
			expDamage = explosionDamage;
			radius = explosionRadius * data.GRID_UNIT_SIZE;
		}
		
		private static function getExplosion( scale:Number, data:GeminiData ):Explosion
		{
			if ( explosionPool == null )
			{
				var explosion:Explosion = new Explosion( new AnimatedSprite( new <AnimationSequence>[data.animMgr.getSequence("triex")], data.animMgr ), 
				                                         new Point( -data.HALF_GRID_UNIT_SIZE, -data.GRID_UNIT_SIZE ), data );
				explosionPool = new Pool( explosion, 10 );
			}
			var ex:Explosion = explosionPool.getObject() as Explosion;
			ex.scaleX = ex.scaleY = scale;
			return ex;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IWeaponStatAcceptor */
		
		public function get weaponData():WeaponData 
		{
			return _weaponData;
		}
		
		public function set weaponData(value:WeaponData):void 
		{
			_weaponData = value;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IActionState */
		
		public function getDoesAllowMovement():Number 
		{
			return 1.0;
		}
		
		public function getIsBlocking():Boolean 
		{
			return false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
			hitbox.B.setTo( 0.0, 0.0 );
			hitbox.A.setTo( 0.0, -1.0 );
			hitbox.C.setTo( 0.0, -1.0 );
			
			Maths.rotateVect( hitbox.A, actor.rotation - halfTheta );
			Maths.rotateVect( hitbox.C, actor.rotation + halfTheta );
			
			hitbox.A.x *= radius;
			hitbox.A.y *= radius;
			
			hitbox.C.x *= radius;
			hitbox.C.y *= radius;
			
			hitbox.A.copyFrom( hitbox.A.add( actor.pos ) );
			hitbox.B.copyFrom( hitbox.B.add( actor.pos ) );
			hitbox.C.copyFrom( hitbox.C.add( actor.pos ) );
		}
		
		public function teardown():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			var idx:int;
			var act:Actor;
			var tmp:Object;
			var node:QuadTreeNode;
			var colliders:Vector.<QuadTreeNode> = data.sceneMgr.rectSceneQuery( actor.getBoundingVolume() );
			var exColliders:Vector.<QuadTreeNode> = data.sceneMgr.triSceneQuery( hitbox );
			var dmg:Number = (_weaponData.damage == 0) ? 0 : _weaponData.damage + _weaponData.stats.calculateEffectiveAttack( act );
			while ( colliders.length > 0 )
			{
				node = colliders.pop();
				if (node == null)
				{
					continue;
				}
				
				tmp = node.owner;

				if ( !(tmp is Creature) )
				{
					continue;
				}
				
				act = tmp as Actor;
				if (act.team != _weaponData.team)
				{
					act.notifyHolder(new HealthEvent(HealthEvent.DAMAGE_TYPE, actor, dmg, _weaponData.armorPenetration, _weaponData.knockback, _weaponData.stunTime));
				}
				
				idx = exColliders.indexOf( act );
				if ( idx != -1 )
				{
					exColliders.splice( idx, 1 );
				}
			}
			
			while ( exColliders.length > 0 )
			{
				node = exColliders.pop();
				if (node == null)
				{
					continue;
				}
				
				tmp = node.owner;

				if ( !(tmp is Creature) )
				{
					continue;
				}
				
				act = tmp as Actor;
				if ( act.team != _weaponData.team )
				{
					act.notifyHolder(new HealthEvent(HealthEvent.DAMAGE_TYPE, actor, expDamage, _weaponData.armorPenetration, _weaponData.knockback, _weaponData.stunTime));
				}
			}
			var ex:Explosion = getExplosion( ( radius / data.GRID_UNIT_SIZE ), data );
			ex.x = hitbox.B.x;
			ex.y = hitbox.B.y;
			ex.rotation = actor.rotation;
			dispatchEvent( new StateChangeEvent( "cooldown", StateChangeEvent.STATE_CHANGE ) );
		}
		
	}

}