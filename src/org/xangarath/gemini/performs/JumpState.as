package org.xangarath.gemini.performs 
{
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import org.xangarath.engine.ActionWidget;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.util.Serializer;
	import org.xangarath.gemini.interfaces.IActionState;
	/**
	 * Annie's jump. Can be used for other controllers, probably
	 * @author Jeffrey Cochran
	 */
	public class JumpState extends EventDispatcher implements IState, ITickable, IActionState
	{
		public static const JUMP_STATE_TYPE:String = "jumptype";
		public static var SPEED_UPS:Number = 4.0;
		private var data:GeminiData;
		private var act:Creature;
		public var distanceSquared:Number;
		private var distanceMovedSquared:Number = 0.0;
		private var dirToJump:Point;
		public function JumpState( distance:Number, data:GeminiData ) 
		{
			this.data = data;
			this.distanceSquared = ( distance * distance ) * data.GRID_UNIT_SIZE;
			dirToJump = new Point();
		}
		
		/* INTERFACE gemini.IActionState */
		
		public function getDoesAllowMovement():Number
		{
			return 0.0;
		}
		
		public function getIsBlocking():Boolean
		{
			return true;
		}
		
		public function setup():void
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
			act = data.playerAtBat;
			//act.invincibilityUp();
			distanceMovedSquared = 0.0;
			dirToJump.setTo( act.dirX, act.dirY );
			dirToJump.normalize( 1.0 );
		}
		
		public function teardown():void
		{
			//act.invincibilityDown();
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
		}
		
		private var lastPos:Point;
		public function tick( deltaTime:Number ):void
		{
			lastPos = act.pos;
			act.move( dirToJump.x * SPEED_UPS * data.GRID_UNIT_SIZE * deltaTime,
			          dirToJump.y * SPEED_UPS * data.GRID_UNIT_SIZE * deltaTime );
					  
			if ( data.playerOnDeck != null )
			{
				data.playerOnDeck.setPos( act.x, act.y );	
			}
			
			distanceMovedSquared += Maths.distanceSq( lastPos, act.pos );
			
			if ( distanceMovedSquared >= distanceSquared )
			{
				dispatchEvent(new StateChangeEvent("cooldown", StateChangeEvent.STATE_CHANGE));
			}
		}
		
		public override function toString():String
		{
			return "JumpState:{ " + Serializer.seralizeFormatted(this, "distance") + "}";
		}
	}

}