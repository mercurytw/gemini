package org.xangarath.gemini.performs 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.quadtree.QuadTreeNode;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.interfaces.IActionState;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.weapons.WeaponData;
	
	/**
	 * Character has a sheild making them invulnerable for a while. Attack then deals splash damage equal to a proportion 
	 * of damage taken
	 * @author Jeffrey Cochran
	 */
	public class ShieldBombState extends EventDispatcher implements ITickable, IState, IWeaponStatAcceptor, IActionState 
	{
		public static const TYPE:String = "shieldbombtype";
		private var actor:Actor;
		private var data:GeminiData;
		private var hitbox:Rectangle;
		private var movementPercentage:Number;
		private var damagePercentage:Number;
		private var timeBeforeExplosion:Number;
		private var timeRemaining:Number;
		private var damageTaken:Number;
		private var _weaponData:WeaponData;
		private var registeredColliders:Array;
		public function ShieldBombState( actor:Actor, explosionRadius:Number, movePercent:Number, damagePercent:Number, sheildTime:Number, wdat:WeaponData, data:GeminiData ) 
		{
			super();
			this.actor = actor;
			this.data = data;
			this._weaponData = wdat;
			this.movementPercentage = movePercent;
			this.damagePercentage = damagePercent;
			this.timeBeforeExplosion = sheildTime;
			registeredColliders = new Array();
			
			// TODO: maybe update to be an actual circle
			hitbox = new Rectangle( 0.0, 0.0, explosionRadius * 2 * data.GRID_UNIT_SIZE, explosionRadius * 2 * data.GRID_UNIT_SIZE );
		}
		
		private function handleDamage( e:Event ):void
		{
			var collision:HealthEvent = e as HealthEvent;
			
			if ( registeredColliders.indexOf( collision.instigator.id ) == -1 )
			{
				registeredColliders.push( collision.instigator.id );
				damageTaken += ( e as HealthEvent ).amount;
			}
		}
		
		private function explode():void
		{
			hitbox.x = actor.x - ( hitbox.width / 2.0 );
			hitbox.y = actor.y - ( hitbox.height / 2.0 );
			
			var colliders:Vector.<QuadTreeNode> = data.sceneMgr.rectSceneQuery( hitbox );
			while ( colliders.length > 0 )
			{
				var o:Object = colliders.pop();
				if (o == null)
				{
					continue;
				}
				
				var tmp:Object = o.owner;
				if ( !(tmp is Actor) )
				{
					continue;
				}
				
				var act:Actor = tmp as Actor;
				if ( act.team != _weaponData.team )
				{
					act.notifyHolder( new HealthEvent( HealthEvent.DAMAGE_TYPE, 
																		 actor,
					( damageTaken * damagePercentage ) + _weaponData.damage + _weaponData.stats.calculateEffectiveAttack( act ), 
												  _weaponData.armorPenetration, 
												         _weaponData.knockback, 
														   _weaponData.stunTime));
				}
			}
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IWeaponStatAcceptor */
		
		public function set weaponData( data:WeaponData ):void
		{
			_weaponData = data;
		}
		
		public function get weaponData():WeaponData
		{
			return _weaponData;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IActionState */
			
		public function getDoesAllowMovement():Number 
		{
			return movementPercentage;
		}
			
		public function getIsBlocking():Boolean 
		{
			return true;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
			(actor as Creature).invincibilityUp();
			timeRemaining = timeBeforeExplosion;
			damageTaken = 0.0;
			actor.addEventListener( HealthEvent.DAMAGE_TYPE, handleDamage );
		}
		
		public function teardown():void 
		{
			actor.removeEventListener( HealthEvent.DAMAGE_TYPE, handleDamage );
			(actor as Creature).invincibilityDown();
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
			while ( registeredColliders.length > 0 )
			{
				registeredColliders.pop();
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( ( timeRemaining -= deltaTime ) <= 0.0 )
			{
				explode();
				this.dispatchEvent( new StateChangeEvent( "cooldown", StateChangeEvent.STATE_CHANGE ) );
			}
		}
		
		public override function toString():String
		{
			return "ShieldBombAttack:{" + "MovementPercent: " + movementPercentage +
			", DamagePercent: " + damagePercentage + "}";
		}
	}

}