package org.xangarath.gemini.performs 
{
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.SceneManager;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.quadtree.QuadTreeNode;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.interfaces.IActionState;
	
	/**
	 * Circular attack state
	 * @author Jeffrey Cochran
	 */
	public class CircleAttackState extends EventDispatcher implements ITickable, IState, IWeaponStatAcceptor, IActionState
	{
		public static const CIRCLE_ATTACK_TYPE:String = "circletype";
		private var actor:Actor;
		private var data:GeminiData;
		private var hitbox:Rectangle;
		private var _weaponData:WeaponData;
		public function CircleAttackState( actor:Actor, weaponData:WeaponData, data:GeminiData ) 
		{
			super();
			this.actor = actor;
			this.data = data;
			this._weaponData = weaponData;
			hitbox = new Rectangle( 0.0, 0.0, 3 * data.GRID_UNIT_SIZE, 3 * data.GRID_UNIT_SIZE );
		}
		
		/* INTERFACE gemini.IActionState */
		
		public function getDoesAllowMovement():Number
		{
			return 0.0;
		}
		
		public function getIsBlocking():Boolean
		{
			return true;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IWeaponStatAcceptor */
		
		public function set weaponData( data:WeaponData ):void
		{
			_weaponData = data;
		}
		
		public function get weaponData():WeaponData
		{
			return _weaponData;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
		}
		
		public function teardown():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			hitbox.x = actor.x - (data.GRID_UNIT_SIZE + data.HALF_GRID_UNIT_SIZE);
			hitbox.y = actor.y - (data.GRID_UNIT_SIZE + data.HALF_GRID_UNIT_SIZE);
			
			var colliders:Vector.<QuadTreeNode> = data.sceneMgr.rectSceneQuery( hitbox );
			while ( colliders.length > 0 )
			{
				var o:Object = colliders.pop();
				if (o == null)
				{
					continue;
				}
				
				var tmp:Object = o.owner;
				if ( !(tmp is Creature) )
				{
					continue;
				}
				
				var act:Actor = tmp as Actor;
				var dmg:Number = (_weaponData.damage == 0) ? 0 : _weaponData.damage + _weaponData.stats.calculateEffectiveAttack( act );
				if (act.team != _weaponData.team)
				{
					act.notifyHolder(new HealthEvent(HealthEvent.DAMAGE_TYPE, actor, dmg, _weaponData.armorPenetration, _weaponData.knockback, _weaponData.stunTime));
				}
			}
			
			this.dispatchEvent(new StateChangeEvent("cooldown", StateChangeEvent.STATE_CHANGE));
		}
		
				
		public override function toString():String
		{
			return "CircleAttack:{" + _weaponData.toString() + "}"; 
		}
	}

}