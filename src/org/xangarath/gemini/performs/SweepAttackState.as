package org.xangarath.gemini.performs 
{
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.SceneManager;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.quadtree.QuadTreeNode;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.interfaces.IActionState;
	
	/**
	 * Sweep attack. Just like the rest
	 * @author Jeffrey Cochran
	 */
	public class SweepAttackState extends EventDispatcher implements ITickable, IState, IWeaponStatAcceptor, IActionState
	{
		public static const SWEEP_ATTACK_TYPE:String = "sweeptype";
		private var actor:Actor;
		private var data:GeminiData;
		private var hitbox1:Rectangle;
		private var hitbox2:Rectangle;
		private var useSecondaryRect:Boolean;
		private var _weaponData:WeaponData;
		public function SweepAttackState( actor:Actor, weaponData:WeaponData, data:GeminiData ) 
		{
			super();
			this.actor = actor;
			this.data = data;
			this._weaponData = weaponData;
			hitbox1 = new Rectangle( 0.0, 0.0, data.GRID_UNIT_SIZE, data.GRID_UNIT_SIZE );
			hitbox2 = new Rectangle( 0.0, 0.0, data.GRID_UNIT_SIZE, data.GRID_UNIT_SIZE );
			useSecondaryRect = false;
		}
		
		private function sign(a:Number):int
		{
			if (a >= 0.0)
			{
				return 1;
			}
			else
			return -1;
		}
		
		private function calcDamageRects():void
		{
			if ((actor.dirX == 0) || (actor.dirY == 0))
			{
				useSecondaryRect = false;
				hitbox1.x = actor.x - (data.GRID_UNIT_SIZE + data.HALF_GRID_UNIT_SIZE);
				hitbox1.y = actor.y - (data.GRID_UNIT_SIZE + data.HALF_GRID_UNIT_SIZE);
				hitbox1.width = (actor.dirX == 0.0) ? data.GRID_UNIT_SIZE * 3 : data.GRID_UNIT_SIZE * 2;
				hitbox1.height = (actor.dirX == 0.0) ? data.GRID_UNIT_SIZE * 2 : data.GRID_UNIT_SIZE * 3;
				
				if (actor.dirX > 0.0)
				{
					hitbox1.x += data.GRID_UNIT_SIZE * 2;
				}
				else if (actor.dirY > 0.0)
				{
					hitbox1.y += data.GRID_UNIT_SIZE;
				}
			}
			else
			{
				useSecondaryRect = true;
				hitbox1.x = actor.x - data.HALF_GRID_UNIT_SIZE;
				hitbox1.y = actor.y - (data.HALF_GRID_UNIT_SIZE + data.GRID_UNIT_SIZE);
				hitbox1.width = data.GRID_UNIT_SIZE;
				hitbox1.height = data.GRID_UNIT_SIZE * 3;
				hitbox1.x += sign(actor.dirX) * data.GRID_UNIT_SIZE;
					
				hitbox2.x = actor.x - data.GRID_UNIT_SIZE;
				hitbox2.y = actor.y - data.HALF_GRID_UNIT_SIZE;
				hitbox2.width = data.GRID_UNIT_SIZE * 2;
				hitbox2.height = data.GRID_UNIT_SIZE;
				hitbox2.x -= sign(actor.dirX) * data.HALF_GRID_UNIT_SIZE;
				hitbox2.y += sign(actor.dirY) * data.GRID_UNIT_SIZE
			}
		}
		
		/* INTERFACE gemini.IActionState */
		
		public function getDoesAllowMovement():Number
		{
			return 0.0;
		}
		
		public function getIsBlocking():Boolean
		{
			return true;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IWeaponStatAcceptor */
		
		public function set weaponData( data:WeaponData ):void
		{
			_weaponData = data;
		}
		
		public function get weaponData():WeaponData
		{
			return _weaponData;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
		}
		
		public function teardown():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			calcDamageRects();
			
			var colliders:Vector.<QuadTreeNode> = data.sceneMgr.rectSceneQuery(hitbox1);
			
			if(useSecondaryRect)
			{
				var arr2:Vector.<QuadTreeNode> = data.sceneMgr.rectSceneQuery(hitbox2);
				for(var i:int = 0; i < arr2.length; i++)
				{
					if(colliders.indexOf(arr2[i]) == -1)
					{
						colliders.push(arr2[i]);
					}
				}
			}
			
			while ( colliders.length > 0 )
			{
				var o:Object = colliders.pop();
				if (o == null)
				{
					continue;
				}
				
				var tmp:Object = o.owner;
				if ( !(tmp is Actor) )
				{
					continue;
				}
				
				var act:Actor = tmp as Actor;
				var dmg:Number = (_weaponData.damage == 0) ? 0 : _weaponData.damage + _weaponData.stats.calculateEffectiveAttack( act );
				if (act.team != _weaponData.team)
				{
					act.notifyHolder(new HealthEvent(HealthEvent.DAMAGE_TYPE, actor, dmg, _weaponData.armorPenetration, _weaponData.knockback, _weaponData.stunTime));
				}
			}
			
			this.dispatchEvent(new StateChangeEvent("cooldown", StateChangeEvent.STATE_CHANGE));
		}
		
				
		public override function toString():String
		{
			return "SweepAttack:{" + _weaponData.toString() + "}"; 
		}
		
	}

}