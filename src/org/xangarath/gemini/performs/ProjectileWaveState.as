package org.xangarath.gemini.performs 
{
	import flash.display.Bitmap;
	import flash.events.EventDispatcher;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IPoolable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.pool.Pool;
	import org.xangarath.gemini.actors.AutonomousProjectile;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.actors.FancyAutonomousProjectile;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.gemini.interfaces.IActionState;
	
	/**
	 * Represents a projectile-firing state
	 * @author Jeffrey Cochran
	 */
	public class ProjectileWaveState extends EventDispatcher implements IState, ITickable, IWeaponStatAcceptor, IActionState, IDestructable
	{
		private static const TAG:String = "ProjectileWaveState";
		public static const PROJECTILE_WAVE_TYPE:String = "projectiletype";
		private var actor:Actor;
		private var _weaponData:WeaponData;
		private var data:GeminiData;
		private var bulletPool:Pool;
		private var penetrate:Boolean;
		private var speed:Number;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function ProjectileWaveState( act:Actor, penetrate:Boolean, speed:Number, data:GeminiData, weaponData:WeaponData, team:int, bitmap:Bitmap = null, proj:Actor = null ) 
		{
			super();
			this._weaponData = weaponData;
			this.actor = act;
			this.data = data;
			this.penetrate = penetrate;
			this.speed = speed;
			var bmp:Bitmap = ( bitmap == null ) ? data.resMgr.getBitmap("pellet") : bitmap;
			var archetype:IPoolable;
			if ( proj == null )
			{
				archetype = new AutonomousProjectile( data, penetrate, speed, weaponData, false, data.sceneMgr.gameLayer, bmp, team );
			}
			else
			{
				archetype = proj as IPoolable;
			}
			( archetype as Actor ).visible = false;
			bulletPool = new Pool( archetype, 10, destroyCback );
		}
		
		/* INTERFACE gemini.IActionState */
		
		public function getDoesAllowMovement():Number
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return 0.0;
		}
		
		public function getIsBlocking():Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return true;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IWeaponStatAcceptor */
		
		public function get weaponData():WeaponData 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return _weaponData;
		}
		
		public function set weaponData(value:WeaponData):void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			_weaponData = value;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( isSetup )
			{
				return;
			}
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
			var proj:Actor = bulletPool.getObject() as Actor;
			proj.setPos( actor.x, actor.y );
			if ( proj is AutonomousProjectile )
			{
				( proj as AutonomousProjectile ).direction[0] = actor.dirX;
				( proj as AutonomousProjectile ).direction[1] = actor.dirY;
			}
			else
			{
				( proj as FancyAutonomousProjectile ).direction.x = actor.dirX;
				( proj as FancyAutonomousProjectile ).direction.y = actor.dirY;
			}
			isSetup = true;
			this.dispatchEvent(new StateChangeEvent("cooldown", StateChangeEvent.STATE_CHANGE));
		}
		
		public function teardown():void 
		{
			if ( !isSetup )
			{
				return;
			}
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
			isSetup = false;
		}
		
		private function destroyCback( o:Object ):void
		{
			var act:Actor = ErrorUtil.dbgSafeCast( TAG, o, Actor );
			act.despawn( data );
			act = null;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			_weaponData.destroy();
			_weaponData = null;
			
			actor = null;
			bulletPool.destroy();
			bulletPool = null;
			
			data = null;
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			
		}
		
		public override function toString():String
		{
			if ( !isConstructed )
			{
				return "Destroyed";
			}
			return "ProjectileWaveState:{ \"penetrate\":" + penetrate + ",\"speed\":" + speed + ", " + _weaponData.toString() + "}"; 
		}
	}

}