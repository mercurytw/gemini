package org.xangarath.gemini.performs 
{
	import flash.display.Bitmap;
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.quadtree.QuadTreeNode;
	import org.xangarath.gemini.events.ActionEvent;
	
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.gemini.interfaces.IActionState;
	
	/**
	 * A stab-type attack that attacks the enemy with two stunning damage attacks, then one knockback attack
	 * @author Jeffrey Cochran
	 */
	public class OneTwoThreeState extends EventDispatcher implements IState, ITickable, IWeaponStatAcceptor, IActionState 
	{
		public static const TYPE:String = "123type";
		private var actor:Actor;
		private var data:GeminiData;
		private var hitbox:Rectangle;
		private var _weaponData:WeaponData;
		private var stage:uint;
		public function OneTwoThreeState( actor:Actor, weaponData:WeaponData, data:GeminiData ) 
		{
			super();
			this.actor = actor;
			this.data = data;
			_weaponData = weaponData;
			hitbox = new Rectangle( 0.0, 0.0, data.GRID_UNIT_SIZE, data.GRID_UNIT_SIZE );
		}
		
		/* INTERFACE gemini.IActionState */
		
		public function getDoesAllowMovement():Number
		{
			return 0.0;
		}
		
		public function getIsBlocking():Boolean
		{
			return true;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IWeaponStatAcceptor */
		
		public function get weaponData():WeaponData 
		{
			return _weaponData;
		}
		
		public function set weaponData( value:WeaponData ):void 
		{
			_weaponData = value;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
			hitbox.x = actor.x - data.HALF_GRID_UNIT_SIZE;
			hitbox.y = actor.y - data.HALF_GRID_UNIT_SIZE;
			hitbox.x += actor.dirX * data.GRID_UNIT_SIZE;
			hitbox.y += actor.dirY * data.GRID_UNIT_SIZE;
			
			stage = 0;
			waitTime = 0.0;
		}
		
		public function teardown():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
		}
		
		private function attack():void
		{
			var colliders:Vector.<QuadTreeNode> = data.sceneMgr.rectSceneQuery( hitbox );
			while ( colliders.length > 0 )
			{
				var tmp:Object =  colliders.pop().owner;
				if ( !(tmp is Actor) )
				{
					continue;
				}
				
				var act:Actor = tmp as Actor;
				var dmg:Number = (_weaponData.damage == 0) ? 0 : _weaponData.damage + _weaponData.stats.calculateEffectiveAttack( act );
				if (act.team != weaponData.team)
				{
					act.notifyHolder( new HealthEvent( HealthEvent.DAMAGE_TYPE, actor, dmg, _weaponData.armorPenetration, 0.0, _weaponData.stunTime ) );
				}
			}
		}
		
		private function knock():void
		{
			var colliders:Vector.<QuadTreeNode> = data.sceneMgr.rectSceneQuery( hitbox );
			while ( colliders.length > 0 )
			{
				var o:Object = colliders.pop();
				if (o == null)
				{
					continue;
				}
				
				var tmp:Object = o.owner;
				if ( !(tmp is Actor) )
				{
					continue;
				}
				
				var act:Actor = tmp as Actor;
				if (act.team != weaponData.team)
				{
					act.notifyHolder( new HealthEvent( HealthEvent.DAMAGE_TYPE, actor, 0.0, _weaponData.armorPenetration, _weaponData.knockback, 0.0 ) );
				}
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		private var waitTime:Number = 0.0;
		public function tick( deltaTime:Number ):void 
		{
			if ( ( waitTime -= deltaTime ) > 0.0 )
			{
				if ( ( waitTime <= ( _weaponData.stunTime / 2.0 ) ) )
				{
					_weaponData.bmp.visible = false;
				}
				return;
			}
			if ( stage < 2 )
			{
				_weaponData.bmp.visible = true;
				attack();
				waitTime = _weaponData.stunTime - 0.5;
				stage++;
			}
			else
			{
				_weaponData.bmp.visible = true;
				knock();
				dispatchEvent( new StateChangeEvent( "cooldown", StateChangeEvent.STATE_CHANGE ) );
			}
		}
		
		public override function toString():String
		{
			return "123Attack:{" + _weaponData.toString() + "}"; 
		}
	}

}