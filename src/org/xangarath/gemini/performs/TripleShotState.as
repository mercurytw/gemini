package org.xangarath.gemini.performs 
{
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IPoolable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.pool.Pool;
	import org.xangarath.gemini.actors.AutonomousProjectile;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.actors.FancyAutonomousProjectile;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.gemini.interfaces.IActionState;
	
	/**
	 * ...
	 * @author Jeffrey Cochran
	 */
	public class TripleShotState extends EventDispatcher implements IState, ITickable, IWeaponStatAcceptor, IActionState
	{
		public static const TRIPLE_SHOT_TYPE:String = "tripletype";
		private static const DEG_TO_RAD:Number = Math.PI / 180.0;
		private var actor:Actor;
		private var _weaponData:WeaponData;
		private var data:GeminiData;
		private var bulletPool:Pool;
		private var penetrate:Boolean;
		private var speed:Number;
		public function TripleShotState( act:Actor, data:GeminiData, speed:Number, weaponData:WeaponData, team:int, penetrate:Boolean, proj:Actor = null ) 
		{
			super();
			this._weaponData = weaponData;
			this.actor = act;
			this.data = data;
			this.penetrate = penetrate;
			this.speed = speed;
			var archetype:IPoolable;
			if ( proj == null )
			{
				archetype = new AutonomousProjectile( data, penetrate, speed, weaponData, false, data.sceneMgr.gameLayer, data.resMgr.getBitmap("pellet"), team );
			}
			else
			{
				archetype = proj as IPoolable;
			}
			( archetype as Actor ).visible = false;
			bulletPool = new Pool( archetype, 10 );
		}
		
		/* INTERFACE gemini.IActionState */
		
		public function getDoesAllowMovement():Number
		{
			return 0.0;
		}
		
		public function getIsBlocking():Boolean
		{
			return true;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IWeaponStatAcceptor */
		
		public function get weaponData():WeaponData 
		{
			return _weaponData;
		}
		
		public function set weaponData(value:WeaponData):void 
		{
			_weaponData = value;
		}
		
		private function rotvec( x:Number, y:Number, theta:Number, result:Point ):void
		{
			theta = theta * DEG_TO_RAD;
			
			var cs:Number = Math.cos(theta);
			var sn:Number = Math.sin(theta);
			
			result.x = x * cs - y * sn;
			result.y = x * sn + y * cs;
			
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		private var pt:Point = new Point();
		public function setup():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
			var proj:Actor = bulletPool.getObject() as Actor;
			proj.setPos( actor.x, actor.y );
			if ( proj is AutonomousProjectile )
			{
				( proj as AutonomousProjectile ).direction[0] = actor.dirX;
				( proj as AutonomousProjectile ).direction[1] = actor.dirY;
			}
			else
			{
				( proj as FancyAutonomousProjectile ).direction.x = actor.dirX;
				( proj as FancyAutonomousProjectile ).direction.y = actor.dirY;
			}
			
			proj = bulletPool.getObject() as Actor;
			proj.setPos( actor.x, actor.y );
			rotvec( actor.dirX, actor.dirY, 25.0, pt );
			if ( proj is AutonomousProjectile )
			{
				( proj as AutonomousProjectile ).direction[0] = pt.x;
				( proj as AutonomousProjectile ).direction[1] = pt.y;
			}
			else
			{
				( proj as FancyAutonomousProjectile ).direction.x = pt.x;
				( proj as FancyAutonomousProjectile ).direction.y = pt.y;
			}
			
			
			proj = bulletPool.getObject() as Actor;
			proj.setPos( actor.x, actor.y );
			rotvec( actor.dirX, actor.dirY, -25.0, pt );
			if ( proj is AutonomousProjectile )
			{
				( proj as AutonomousProjectile ).direction[0] = pt.x;
				( proj as AutonomousProjectile ).direction[1] = pt.y;
			}
			else
			{
				( proj as FancyAutonomousProjectile ).direction.x = pt.x;
				( proj as FancyAutonomousProjectile ).direction.y = pt.y;
			}
			
			this.dispatchEvent(new StateChangeEvent("cooldown", StateChangeEvent.STATE_CHANGE));
		}
		
		public function teardown():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			
		}
		
		public override function toString():String
		{
			return "TripleShotState:{ \"penetrate\":" + penetrate + ",\"speed\":" + speed + ", " + _weaponData.toString() + "}"; 
		}
	}

}