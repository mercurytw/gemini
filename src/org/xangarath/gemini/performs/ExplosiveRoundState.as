package org.xangarath.gemini.performs 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.StatLogger;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.action.Action;
	import org.xangarath.gemini.ActionFactory;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.interfaces.IActionState;
	import org.xangarath.gemini.stats.StatPage;
	
	/**
	 * Replaces connor's basic attacks
	 * @author Jeffrey Cochran
	 */
	public class ExplosiveRoundState extends EventDispatcher implements ITickable, IState, IActionState 
	{
		private static const TAG:String = "ExplosiveRoundState";
		public static const TYPE:String = "explosiveroundtype";
		private var actor:Creature;
		private var actionBackups:Vector.<Action>;
		private var fact:ActionFactory;
		private var perfHappened:Boolean;
		private static const COOLOFF_TIME:Number = 0.75;
		private var cooloffTimeRemaining:Number;
		private var stats:StatPage;
		public function ExplosiveRoundState( actor:Actor, fact:ActionFactory, jsobj:Object, stats:StatPage ) 
		{
			super();
			this.actor = actor as Creature;
			actionBackups = new Vector.<Action>();
			this.fact = fact;
			this.stats = stats;
			loadPerformsFromJSON( jsobj );
		}
		
		public function loadPerformsFromJSON( jsobj:Object ):void
		{
			var actions:Object = Argonaut.getAndCheck( TAG, jsobj, "actions" );
			
			for ( var i:int = 0; actions[ i ] != null; i++ )
			{
				if ( i > 2 )
				{
					trace( TAG + ": Specified too many explosive round actions" );
					return;
				}
				var a:Action = fact.makeActionFromJson( actions[ i ], actor, Actor.PLAYER_TEAM, this.stats );
				actionBackups.push( a );
				a.addActionListener( ActionEvent.ACTION_COMPLETE, performHandler );
			}
		}
		
		private function performHandler( e:Event ):void
		{
			perfHappened = true;
			cooloffTimeRemaining = COOLOFF_TIME;
			swapActions();
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IActionState */
		
		public function getDoesAllowMovement():Number 
		{
			return 1.0;
		}
		
		public function getIsBlocking():Boolean 
		{
			return perfHappened;
		}
		
		private function swapActions():void
		{
			for ( var i:int = 0; i < actionBackups.length; i++ )
			{
				if ( ( i + 1 ) > actor.actions.length )
				{
					break;
				}
				
				var bk:Action = actor.actions[ i ];
				
				if ( bk != null ) 
				{
					actor.detatchAction( i );
					actor.attachAction( actionBackups[ i ], i );
					actionBackups.splice( i, 1, bk);
				}
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			perfHappened = false;
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
			
			swapActions();
			StatLogger.getSingleton().connorUlting = true;
		}
		
		public function teardown():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
			perfHappened = false;
			StatLogger.getSingleton().connorUlting = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			for ( var i:int = 0; i < actionBackups.length; i++ )
			{
				if ( actionBackups[ i ] != null )
				{
					actionBackups[ i ].tick( deltaTime );
				}
			}
			
			if ( perfHappened )
			{
				if ( ( cooloffTimeRemaining -= deltaTime ) > 0.0 )
				{
					dispatchEvent( new StateChangeEvent( "cooldown", StateChangeEvent.STATE_CHANGE ) );
				}
			}
		}
		
	}

}