package org.xangarath.gemini.performs 
{
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.SceneManager;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.events.StateChangeEvent;
	import org.xangarath.engine.quadtree.QuadTreeNode;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.gemini.actors.Creature;
	import org.xangarath.gemini.events.ActionEvent;
	import org.xangarath.gemini.GeminiData;
	import org.xangarath.gemini.events.HealthEvent;
	import org.xangarath.gemini.weapons.WeaponData;
	import org.xangarath.gemini.interfaces.IWeaponStatAcceptor;
	import org.xangarath.gemini.interfaces.IActionState;
	
	/**
	 * Action representing a stab attack, such as from annie's sword
	 * @author Jeffrey Cochran
	 */
	public class StabAttackState extends EventDispatcher implements IState, ITickable, IWeaponStatAcceptor, IActionState, IDestructable
	{
		private static const TAG:String = "StabAttackState";
		public static const STAB_ATTACK_TYPE:String = "stabtype";
		private var actor:Actor;
		private var data:GeminiData;
		private var hitbox:Rectangle;
		private var _weaponData:WeaponData;
		private var isSetup:Boolean = false;
		private var isConstructed:Boolean = true;
		public function StabAttackState( actor:Actor, weaponData:WeaponData, data:GeminiData ) 
		{
			super();
			this.actor = actor;
			this.data = data;
			_weaponData = weaponData;
			hitbox = new Rectangle( 0.0, 0.0, data.GRID_UNIT_SIZE, data.GRID_UNIT_SIZE );
		}
		
		/* INTERFACE gemini.IActionState */
		
		public function getDoesAllowMovement():Number
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return 0.0;
		}
		
		public function getIsBlocking():Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return true;
		}
		
		/* INTERFACE org.xangarath.gemini.interfaces.IWeaponStatAcceptor */
		
		public function set weaponData( data:WeaponData ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			_weaponData = data;
		}
		
		public function get weaponData():WeaponData
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return _weaponData;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IState */
		
		public function setup():void 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_BEGIN ) );
			isSetup = true;
		}
		
		public function teardown():void 
		{
			dispatchEvent( new ActionEvent( ActionEvent.ACTION_COMPLETE ) );
			isSetup = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSetup )
			{
				teardown();
			}
			
			
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed )
			{
				return;
			}
			
			hitbox.x = actor.x - data.HALF_GRID_UNIT_SIZE;
			hitbox.y = actor.y - data.HALF_GRID_UNIT_SIZE;
			hitbox.x += actor.dirX * data.GRID_UNIT_SIZE;
			hitbox.y += actor.dirY * data.GRID_UNIT_SIZE;
			
			var colliders:Vector.<QuadTreeNode> = data.sceneMgr.rectSceneQuery( hitbox );
			while ( colliders.length > 0 )
			{
				var o:Object = colliders.pop();
				if (o == null)
				{
					continue;
				}
				
				var collider:Object = o.owner;
				if ( !(collider is Creature ) )
					continue;
				var act:Actor = collider as Actor;
				var dmg:Number = (_weaponData.damage == 0) ? 0 : _weaponData.damage + _weaponData.stats.calculateEffectiveAttack( act );
				if (act == null)
				{
					continue;
				}
				if (act.team != weaponData.team)
				{
					act.notifyHolder(new HealthEvent(HealthEvent.DAMAGE_TYPE, actor, dmg, _weaponData.armorPenetration, _weaponData.knockback, _weaponData.stunTime));
				}
			}
			
			this.dispatchEvent(new StateChangeEvent("cooldown", StateChangeEvent.STATE_CHANGE));
		}
		
		public override function toString():String
		{
			return "StabAttack:{" + _weaponData.toString() + "}"; 
		}
	}
}