package org.xangarath.engine.script 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.filesystem.*;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import luaAlchemy.lua_wrapper;
	import org.xangarath.engine.dbg.ErrorUtil;
	
	import org.xangarath.engine.ResourceManager;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.GameObjectRegistry;
	import org.xangarath.engine.util.CRC32;
	import luaAlchemy.LuaAlchemy; 
	/**
	 * A manager for lua scripting. Is a singleton
	 * @author Jeff Cochran
	 */
	public class ScriptManager
	{	
		private static const TAG:String = "ScriptManager";
		public static const VFS_SCRIPT_ROOT:String = "script://";
		public static var SCRIPT_PATH:String = "data/script/";
		public static var prependPath:Boolean = true;
		private static const RESERVED_FOLDER:String = "__reserved/";
		private static const GLUE_FILE:String = "ScriptManager.lua";
		private static const ENVIRONMENT_NAME:String = "XANGARATH_ENVIRONMENT";
		private static const INVOKER_NAME:String = "INVOKER";
		
		[Embed(source="../../../../../assets/lua/ScriptManager.lua", mimeType="application/octet-stream")]
		private var luaScript:Class;
		
		private var environment:Object;
		private var lua:LuaAlchemy;
		private var resMgr:ResourceManager;
		private var reloadableScriptMap:Dictionary;
		public function ScriptManager( res:ResourceManager ) 
		{
			lua = new LuaAlchemy( VFS_SCRIPT_ROOT, false );
			lua.init();
			resMgr = res;
			constructEnvironment();
			reloadableScriptMap = new Dictionary();
			lua.setGlobal(ENVIRONMENT_NAME, environment);
			lua.supplyFile(VFS_SCRIPT_ROOT + RESERVED_FOLDER + GLUE_FILE, new luaScript() as ByteArray);
			this.doFile(VFS_SCRIPT_ROOT + RESERVED_FOLDER + GLUE_FILE);
			
			mountScriptDirectory();
		}
		
		public function addToEnvironment( key:String, value:* ):void
		{
			environment[key] = value;
		}
		
		public function addLuaGlobal( key:String, value:* ):void
		{
			lua.setGlobalLuaValue(key, value);
		}
		
		private function constructEnvironment():void
		{
			environment = new Object();
			environment.logger = DebugLog.getSingleton();
			environment.registry = GameObjectRegistry.getSingleton();
			environment.registerEventListener = _hook;
			environment.unregisterEventListener = _unhook;
			environment.band = band;
			environment.bor = bor;
			environment.event = null;
			environment.dynFnMaker = createDynamicFunction;
		}
		
		private function luaEventBus(e:Event):void
		{
			environment.event = e;
			this.doString('return luaEventBus("' + e.type + '",' + e.target.id + ')', e.target);
			environment.event = null;
		}
		
		private function getInvoker():*
		{
			var a:Array = lua.doString('return INVOKER');
			if (a[0] == false)
			{
				return null;
			}
			return a[1];
		}
		
		private function createDynamicFunction( dynamicFunctionId:Number ):Function
		{
			return function( ...args ) : * {
				//var invokerOld:* = getInvoker();
				//lua.setGlobal(INVOKER_NAME, invoker);
				var retArr:Array = lua.callGlobal("dynamicFunctionRegistryInvoke", dynamicFunctionId, args );
				if (retArr[0] == false)
				{
					DebugLog.getSingleton().e(TAG, "Failed to invoke dynamic as3 function: " + retArr[1]);
					//lua.setGlobal(INVOKER_NAME, invokerOld);
					return null;
				}
				//lua.setGlobal(INVOKER_NAME, invokerOld);
				return retArr[1];
			};
		}
		
		private function _hook( id:uint, event:String ):Boolean
		{
			var obj:IEventDispatcher = (GameObjectRegistry.getSingleton().getObject( id ) as IEventDispatcher);
			if (obj == null)
			{
				DebugLog.getSingleton().e(TAG, " Failed to locate game object!");
				return false;
			}
			obj.addEventListener(event, luaEventBus);
			return true;
		}
		
		private function _unhook( id:uint, event:String ):Boolean
		{
			var obj:IEventDispatcher = (GameObjectRegistry.getSingleton().getObject( id ) as IEventDispatcher);
			if (obj == null)
			{
				DebugLog.getSingleton().e(TAG, " Failed to locate game object!");
				return false;
			}
			obj.removeEventListener(event, luaEventBus);
			return true;
		}
		
		private function mountScriptDirectoryHelper( directory:File, root:File ):void
		{
			var files:Array = directory.getDirectoryListing();
			for each (var entry:File in files)
			{
				if (entry.isDirectory)
				{
					mountScriptDirectoryHelper( entry, root );
				}
				else
				{
					mountFile( root.getRelativePath(entry) );
				}
			}
		}
		
		private function mountScriptDirectory():void
		{
			var rootDir:File = File.applicationDirectory.resolvePath(SCRIPT_PATH);
			if ( ( rootDir == null ) || ( !rootDir.isDirectory ) )
			{
				DebugLog.getSingleton().e( TAG, "unable to find root script directory!");
				throw new Error( TAG + "unable to find root script directory!");
			}
			
			mountScriptDirectoryHelper( rootDir, rootDir );
		}
		
		public function doString( str:String, invoker:* = null ):Array
		{
			var invokerOld:* = getInvoker();
			lua.setGlobal(INVOKER_NAME, invoker);
			var a:Array = lua.doString(str);
			lua.setGlobal(INVOKER_NAME, invokerOld);
			if ((a[0] as Boolean) != true)
			{
				DebugLog.getSingleton().e(TAG, "Lua Error: " + (a[1] as String));
				return a;
			}
			return a;
		}
		
		public function doReloadableFile( location:String, invoker:* = null):Array
		{
			var hash:uint = CRC32.getSingleton().Hash(location);
			location = location.substring( VFS_SCRIPT_ROOT.length );
			var script:String = ResourceManager.loadFileAsString( SCRIPT_PATH + location, prependPath );
			var ret:Array = doString( script, invoker );
			if (ret[0] == false)
			{
				return ret;
			}
			
			var scriptEntry:Object = new Object();
			scriptEntry.invoker = invoker;
			scriptEntry.cleanFunc = ret[1] as Function;
			ErrorUtil.dbgAssert( TAG, reloadableScriptMap[hash] == null, "HASH COLLISION!" );
			if (reloadableScriptMap[hash] != null)
			{
				return null;
			}
			reloadableScriptMap[hash] = scriptEntry;
			return ret;
		}
		
		public function redoReloadableFile( location:String ):Array
		{
			var basePath:String = location.substring( VFS_SCRIPT_ROOT.length );
			var invokerOld:* = getInvoker();
			var hash:uint = CRC32.getSingleton().Hash(location);
			if (reloadableScriptMap[hash] == null)
			{
				DebugLog.getSingleton().e(TAG, "Requested redo of unregistered script");
				return null;
			}
			var obj:Object = reloadableScriptMap[hash];
			lua.setGlobal(INVOKER_NAME, obj.invoker);
			(obj.cleanFunc as Function).call();
			reloadableScriptMap[hash] = null;
			lua.setGlobal(INVOKER_NAME, invokerOld);
			return doReloadableFile( VFS_SCRIPT_ROOT + basePath, obj.invoker );
		}
		
		public function undoReloadableFile( location:String ):void
		{
			var basePath:String = location.substring( VFS_SCRIPT_ROOT.length );
			var invokerOld:* = getInvoker();
			var hash:uint = CRC32.getSingleton().Hash(location);
			ErrorUtil.dbgAssert(TAG, reloadableScriptMap[hash] != null, "Requested redo of unregistered script");
			if (reloadableScriptMap[hash] == null)
			{
				return;
			}
			var obj:Object = reloadableScriptMap[hash];
			lua.setGlobal(INVOKER_NAME, obj.invoker);
			(obj.cleanFunc as Function).call();
			reloadableScriptMap[hash] = null;
			lua.setGlobal(INVOKER_NAME, invokerOld);
		}
		
		public function doFile( location:String, invoker:* = null ):Array
		{
			var invokerOld:* = getInvoker();
			lua.setGlobal(INVOKER_NAME, invoker);
			var a:Array = lua.doFile(location);
			lua.setGlobal(INVOKER_NAME, invokerOld);
			if ((a[0] as Boolean) != true)
			{
				DebugLog.getSingleton().e(TAG, "Lua Error: " + (a[1] as String));
				return a;
			}
			return a;
		}
		
		public function mountFile( location:String ):String
		{
			var bar:ByteArray = ResourceManager.loadFileAsByteArray(SCRIPT_PATH + location, prependPath);
			lua.supplyFile(VFS_SCRIPT_ROOT + location, bar);
			bar = null;
			return VFS_SCRIPT_ROOT + location;
		}
		
		private function band( a:uint, b:uint ):uint
		{
			return a & b;
		}
		
		private function bor( a:uint, b:uint ):uint
		{
			return a | b;
		}
	}

}