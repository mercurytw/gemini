package org.xangarath.engine.util 
{
	import org.xangarath.engine.interfaces.IDestructable;
	/**
	 * C++ style (but untyped) pair
	 * @author ...
	 */
	public class Pair implements IDestructable
	{
		public var first:Object;
		public var second:Object;
		private var shouldDestroyContents:Boolean;
		private var isConstructed:Boolean = true;
		/**
		 * @param	f  First object
		 * @param	s  Second object
		 * @param	destroyContents Set to "true" if this instance should destroy it's objects when it is destroyed
		 */
		public function Pair( f:Object, s:Object, destroyContents:Boolean = false ) 
		{
			first = f;
			second = s;
			
			shouldDestroyContents = destroyContents;
		}
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if (shouldDestroyContents)
			{
				if ( first is IDestructable )
				{
					(first as IDestructable).destroy();
				}
				
				if ( second is IDestructable )
				{
					(second as IDestructable).destroy();
				}
			}
			
			first = null;
			second = null;
			isConstructed = false;
		}
		
	}

}