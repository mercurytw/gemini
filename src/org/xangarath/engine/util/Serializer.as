package org.xangarath.engine.util 
{
	/**
	 * A class that contains the seralize function
	 * @author Jeffrey Cochran
	 */
	public class Serializer 
	{
		
		public function Serializer() 
		{
			
		}
		
		public static function seralizeFormatted( o:Object, ...args):String
		{
			var s:String = "";
			for ( var i:int = 0; i < args.length; i++ )
			{
				if ( i != 0 )
				{
					s += ", ";
				}
				
				s += (args[i] as String);
				
				if ( (args[i] as String) in o )
				{
					s += ':' + o[args[i] as String];
				}
			}
			return s;
		}
	}

}