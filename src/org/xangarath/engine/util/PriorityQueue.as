package org.xangarath.engine.util 
{
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IComparator;
	import org.xangarath.engine.interfaces.IDestructable;
	/**
	 * Ye olde priority queue. Uses binary search for insertion, and an Array as the underlying container
	 * @author Jeff Cochran
	 */
	public class PriorityQueue implements IDestructable
	{
		private static const TAG:String = "PriorityQueue";
		private var comp:IComparator;
		public var arr:Array;
		private var cachedObject:Object = null;
		private var cachedIdx:int = -1;
		private var isConstructed:Boolean = true;
		public function PriorityQueue( comparator:IComparator ) 
		{
			comp = comparator;
			arr = new Array();
		}
		
		public function clear():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			arr.splice( 0, arr.length );
			cachedIdx = -1;
			cachedObject = null;
		}
		
		public function peekHead():Object
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if (length == 0)
				return null;
				
			return arr[0];
		}
		
		public function get length():uint
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return arr.length;
		}
		
		private function deleteThisDbgPrinter( l:int, r:int, target:int, value:int ):void
		{
			trace( "Target difference: " + value + ", target: " + target + ", l: " + l + ", r" + r + "\n" );
		}
		
		/**------------------------
		 * ---------------------------------------BEGIN INSERT
		 *-------------------------*/ 
		
		private function binaryInsert( obj:Object ):int
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var l:int = 0
			var r:int = arr.length - 1;
			var target:int = Math.ceil(r / 2);
			var tmp:int;
			
			if ( !( obj is comp.getComparableClass() ) )
				return -1;
				
			if ( arr.length == 0 )
			{
				arr.push( obj );
				return 0;
			}
			else if ( arr.length == 1 )
			{
				tmp = comp.compare( arr[l], obj );
				tmp = (tmp < 0) ? l + 1 : l;
				arr.splice( tmp, 0, obj );
				return tmp;
			}
			
			if ( comp.compare(arr[r], obj) <= 0 )
			{
				arr.push( obj );
				return arr.length;
			}
			else if ( comp.compare(arr[l], obj) >= 0 )
			{
				arr.splice( l, 0, obj );
				return 0;
			}
			
			for ( ;; )
			{
				
				tmp = comp.compare( arr[target], obj );
				if ( tmp == 0 )
				{
					//trace( "Found index: " + target );
					arr.splice( target, 0, obj );
					return target;
				}
				else if ( ( r - l ) == 1 )
				{
					target = r;
					//trace( "Found index: " + target );
					arr.splice( target, 0, obj );
					return target;
				}
				else if ( tmp < 0 )
				{
					l = target;
					target += Math.ceil(( r - target ) / 2);
					//deleteThisDbgPrinter( l, r, target, tmp ); 
				}
				else
				{
					r = target;
					target = Math.ceil((target - l) / 2) + l;
					//deleteThisDbgPrinter( l, r, target, tmp ); 
				}
			}
			
			// control never reaches this...
			return -1;
		}
		
		/**------------------------
		 * --------------------------------------- END INSERT
		 *-------------------------*/ 
		
		private function binarySearch( obj:Object, slideLeft:Boolean ):int
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var l:int = 0
			var r:int = arr.length - 1;
			var target:int = Math.ceil(r / 2);
			var tmp:int;
			
			if ( !( obj is comp.getComparableClass() ) )
				return -1;
				
			if ( arr.length == 0 )
			{
				return -1;
			}
			else if ( arr.length == 1 )
			{
				tmp = comp.compare( arr[l], obj );
				return (tmp == 0) ? 0 : -1;
			}
			
			if ( comp.compare( arr[l], obj ) == 0 )
			{
				return l;
			}
			else if ( comp.compare( arr[r], obj ) == 0 )
			{
				// no need to slide left, since we just checked l above
				return r;
			}
			
			for ( ;; )
			{
				
				tmp = comp.compare( arr[target], obj );
				if ( tmp == 0 )
				{
					if (slideLeft)
					{
						// I want the leftmost valid index
						for ( tmp = target - 1; ( tmp >= 0 ) && (comp.compare( arr[tmp], obj ) == 0); tmp-- )
						{
							target = tmp;
						}
					}
					//trace( "Found index: " + target );
					return target;
				}
				else if ( ( r - l ) == 1 )
				{
					//trace( "Not found" );
					return -1;
				}
				else if ( tmp < 0 )
				{
					l = target;
					target += Math.ceil(( r - target ) / 2);
					//deleteThisDbgPrinter( l, r, target, tmp ); 
				}
				else
				{
					r = target;
					target = Math.ceil((target - l) / 2) + l;
					//deleteThisDbgPrinter( l, r, target, tmp ); 
				}
			}
			
			// control never reaches this...
			return -1;
		}
		
		 /**------------------------
		 * --------------------------------------- END SEARCH
		 *-------------------------*/ 
		
		public function enqueue( obj:Object ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( !( obj is comp.getComparableClass() ) )
				return;
				
			if ( obj == null )
				return;
				
			binaryInsert( obj );
			cachedIdx = -1;
			cachedObject = null;
		}
		
		public function dequeue():Object
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( arr.length == 0 )
				return null;
			
			cachedIdx = -1;
			cachedObject = null;
			return arr.splice( 0, 1 )[0];
		}
		
		public function containsValue( obj:Object ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( !( obj is comp.getComparableClass() ) )
				return false;
				
			var idx:int = binarySearch( obj, false );
			if ( idx == -1 )
			{
				//trace("not found" );
				return false;
			}
			else 
			{
				//trace(" found index " + idx );
				cachedIdx = idx;
				cachedObject = obj;
				return true;
			}
		}
		
		public function containsInstance( obj:Object ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var idx:int;
			
			if ( !( obj is comp.getComparableClass() ) )
				return false;
				
			idx = binarySearch( obj, true );
			if ( idx == -1 )
			{
				//trace("not found" );
				return false;
			}
				
			while ( (idx < arr.length) && ( comp.compare( arr[ idx ], obj ) == 0 ) )
			{
				if ( arr[ idx ] == obj )
				{
					//trace( "found at index " + idx );
					cachedIdx = idx;
				    cachedObject = obj;
					return true;
				}
				idx++;
			}
			//trace( "not found" );
			return false;
		}
		
		public function removeInstance( obj:Object ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var idx:int;
			
			if ( !( obj is comp.getComparableClass() ) )
				return false;
			
			if ( ( cachedObject != null ) && ( cachedObject == obj ) )
			{
				arr.splice( cachedIdx, 1 );
				cachedIdx = -1;
				cachedObject = null;
				return true;
			}
			
			idx = binarySearch( obj, true );
			if ( idx == -1 )
				return false;
			
			while ( (idx < arr.length) && ( comp.compare( arr[ idx ], obj ) == 0 ) )
			{
				if ( arr[ idx ] == obj )
				{
					cachedIdx = -1;
				    cachedObject = null;
					arr.splice( idx, 1 );
					return true;
				}
				idx++;
			}
			return false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			arr.length = 0;
			arr = null;
			
			cachedObject = null;
			comp = null;
			
			isConstructed = false;
		}
		
		public function toString():String
		{
			var str:String = "pq:{ ";
			
			if ( !isConstructed )
			{
				return "Destroyed";
			}
			
			for (var i:int = 0; i < arr.length; i++ )
			{
				if ( i != 0 )
				{
					str += ", ";
				}
				str += "" + arr[i];
			}
			
			str += " }";
			return str;
		}
	}

}