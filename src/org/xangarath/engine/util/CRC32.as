package org.xangarath.engine.util
{
	import flash.utils.ByteArray;
	
	public class CRC32
	{
		//private var crcTable:Vector.<uint> = new Vector.<uint>(256, true);
		private var crcTable:Vector.<uint> = new <uint>[
			0x0000000,0x6233697,0x5C45641,0x3E760D6,
			0x20A97ED,0x429A17A,0x7CEC1AC,0x1EDF73B,
			0x4152FDA,0x236194D,0x1D1799B,0x7F24F0C,
			0x61FB837,0x03C8EA0,0x03DBEE76,0x5F8D8E1,
			0x1A864DB,0x78B524C,0x46C329A,0x24F040D,
			0x3A2F336,0x581C5A1,0x666A577,0x4593E0,
			0x5BD4B01,0x39E7D96,0x0791D40,0x65A2BD7,
			0x7B7DCEC,0x194EA7B,0x2738AAD,0x450BC3A,
			0x350C9B6,0x573FF21,0x6949FF7,0xB7A960,
			0x15A5E5B,0x77968CC,0x49E081A,0x2BD3E8D,
			0x745E66C,0x166D0FB,0x281B02D,0x4A286BA,
			0x54F7181,0x36C4716,0x08B27C0,0x6A81157,
			0x2F8AD6D,0x4DB9BFA,0x73CFB2C,0x11FCDBB,
			0x0F23A80,0x6D10C17,0x5366CC1,0x3155A56,
			0x6ED82B7,0xCEB420,0x329D4F6,0x50AE261,
			0x4E7155A,0x2C423CD,0x123431B,0x700758C,
			0x6A1936C,0x82A5FB,0x365C52D,0x546F3BA,
			0x4AB0481,0x2883216,0x16F52C0,0x74C6457,
			0x2B4BCB6,0x4978A21,0x770EAF7,0x153DC60,
			0xBE2B5B,0x69D1DCC,0x57A7D1A,0x3594B8D,
			0x709F7B7,0x12AC120,0x2CDA1F6,0x4EE9761,
			0x503605A,0x32056CD,0xC7361B,0x6E4008C,
			0x31CD86D,0x53FEEFA,0x6D88E2C,0xFBB8BB,
			0x1164F80,0x7357917,0x4D219C1,0x2F12F56,
			0x5F15ADA,0x3D26C4D,0x350C9B,0x6163A0C,
			0x7FBCD37,0x1D8FBA0,0x23F9B76,0x41CADE1,
			0x1E47500,0x7C74397,0x4202341,0x20315D6,
			0x3EEE2ED,0x5CDD47A,0x62AB4AC,0x9823B,
			0x4593E01,0x27A0896,0x19D6840,0x7BE5ED7,
			0x653A9EC,0x709F7B,0x397FFAD,0x5B4C93A,
			0x4C11DB,0x66F274C,0x588479A,0x3AB710D,
			0x2468636,0x465B0A1,0x782D077,0x1A1E6E0,
			0x4C11DB7,0x2E22B20,0x1054BF6,0x7267D61,
			0x6CB8A5A,0xE8BCCD,0x30FDC1B,0x52CEA8C,
			0xD4326D,0x6F704FA,0x510642C,0x33352BB,
			0x2DEA580,0x4FD9317,0x71AF3C1,0x139C556,
			0x569796C,0x34A4FFB,0xAD2F2D,0x68E19BA,
			0x763EE81,0x140D816,0x2A7B8C0,0x4848E57,
			0x17C56B6,0x75F6021,0x4B800F7,0x29B3660,
			0x376C15B,0x555F7CC,0x6B2971A,0x91A18D,
			0x791D401,0x1B2E296,0x2558240,0x476B4D7,
			0x59B43EC,0x3B8757B,0x5F15AD,0x67C233A,
			0x384FBDB,0x5A7CD4C,0x640AD9A,0x639B0D,
			0x18E6C36,0x7AD5AA1,0x44A3A77,0x2690CE0,
			0x639B0DA,0x1A864D,0x3FDE69B,0x5DED00C,
			0x4332737,0x21011A0,0x1F77176,0x7D447E1,
			0x22C9F00,0x40FA997,0x7E8C941,0x1CBFFD6,
			0x2608ED,0x6053E7A,0x5E25EAC,0x3C1683B,
			0x2608EDB,0x443B84C,0x7A4D89A,0x187EE0D,
			0x6A1936,0x6492FA1,0x5AE4F77,0x38D79E0,
			0x675A101,0x569796,0x3B1F740,0x592C1D7,
			0x47F36EC,0x25C007B,0x1BB60AD,0x798563A,
			0x3C8EA00,0x5EBDC97,0x60CBC41,0x2F8AD6,
			0x1C27DED,0x7E14B7A,0x4062BAC,0x2251D3B,
			0x7DDC5DA,0x1FEF34D,0x219939B,0x43AA50C,
			0x5D75237,0x3F464A0,0x130476,0x63032E1,
			0x130476D,0x71371FA,0x4F4112C,0x2D727BB,
			0x33AD080,0x519E617,0x6FE86C1,0xDDB056,
			0x52568B7,0x3065E20,0xE13EF6,0x6C20861,
			0x72FFF5A,0x10CC9CD,0x2EBA91B,0x4C89F8C,
			0x9823B6,0x6BB1521,0x55C75F7,0x37F4360,
			0x292B45B,0x4B182CC,0x756E21A,0x175D48D,
			0x48D0C6C,0x2AE3AFB,0x1495A2D,0x76A6CBA,
			0x6879B81,0xA4AD16,0x343CDC0,0x560FB57
			];

		
		public function CRC32()
		{
			//makeCrcTable();
		}
		
		
		private function makeCrcTable():void
		{
			for (var n:int = 0; n < 256; n++) 
			{
				var c:uint = n;
				for (var k:int = 8; --k >= 0; ) 
				{
					if ((c & 1) != 0)
					{	
						c = 0x4C11DB7 ^ (c >>> 1);
					}
					else c = c >>> 1;
				}
				crcTable[n] = c;
			}
			
			var table:String = "private var barr:Array = [";
			for (var i:int = 0; i < 256; i += 4)
			{
				table += "\n";
				table += "0x" + crcTable[i + 0].toString(16) + ",";
				table += "0x" + crcTable[i + 1].toString(16) + ",";
				table += "0x" + crcTable[i + 2].toString(16) + ",";
				table += "0x" + crcTable[i + 3].toString(16) + ",";
			}
			table = table.substr( 0, table.length - 1 );
			table += "\n];";
			trace( table );
        }
		
		
		private static var singleton:CRC32 = null;
		public static function getSingleton():CRC32
		{
			if (singleton == null)
			{
				singleton = new CRC32();
			}
			return singleton;
		}

		public function Hash( val:String ):uint
		{
			var crc:uint = ~0x0;
			var data:ByteArray = new ByteArray();
			data.writeUTFBytes(val);
			data.position = 0;
			while ( data.position < data.length )
			{
				crc = crcTable[(crc ^ data.readUnsignedByte()) & 0xff] ^ (crc >>> 8);
			}
			data.clear();
			data = null;
			return crc ^ ~0x0;
		}
	}
}