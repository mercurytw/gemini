package org.xangarath.engine 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display3D.textures.RectangleTexture;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.xangarath.engine.anim.AnimatedSprite;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.quadtree.QuadTreeNode;
	
	/**
	 * Actor clas which represents a "physical" object in the game world. Standard actor-controller model
	 * @author Jeff Cochran
	 */
	public class Actor extends Sprite implements IDestructable
	{
		private static const TAG:String = "Actor";
		private static const RAD_TO_DEG:Number = 180.0 / Math.PI;
		public static const NO_TEAM:uint = 0;
		public static const PLAYER_TEAM:uint = 1;
		public static const ENEMY_TEAM:uint = 2;
		public static const SPAWN_EVENT:String = "spawn";
		public static const DESPAWN_EVENT:String = "despawn";
		
		public var drawable:DisplayObject;
		private var _id:uint;
		public var team:uint;
		public var _dirFacing:Vector.<Number> = new Vector.<Number>(2);
		public var attachments:Sprite = new Sprite();
		public var overlays:Sprite = new Sprite();
		public var rotateTogether:Boolean;
		public var facingOffset:Number;
		public var isSolid:Boolean;
		public var ignoreEvents:Boolean = false; // should we ignore events that happen to us?
		public var badWallMask:uint;
		public var node:QuadTreeNode;
		private var bounds:Rectangle = new Rectangle();
		protected var isSpawned:Boolean = false;
		protected var isConstructed:Boolean = true;
		public function Actor( isSolid:Boolean, drawable:DisplayObject, team:uint, xPosition:Number = 0.0, yPosition:Number = 0.0, rotateTogether:Boolean = false ) 
		{
			super();
			this._id = GameObjectRegistry.getSingleton().register( this );
			this.isSolid = isSolid;
			this.drawable = drawable;
			this.addChild(drawable);
			this.drawable.x = - drawable.width / 2.0;
			this.drawable.y = - drawable.height / 2.0;
			this.x = xPosition;
			this.y = yPosition;
			this.rotateTogether = rotateTogether;
			this.team = team;
			_dirFacing[0] = 0.0;
			_dirFacing[1] = 1.0;
			super.addChild(attachments);
			addChild( overlays );
			overlays.graphics.beginFill(0x0, 0.0);
			overlays.graphics.drawRect( 0., 0., drawable.width + 25., drawable.height + 25. );
			overlays.graphics.endFill();
			overlays.x = -((25. / 2.) + (drawable.width / 2.0));
			overlays.y = -((25. / 2.) + (drawable.height / 2.0));
			facingOffset = 90.0;
			
			this.name = "Actor#" + id;
			
			node = new QuadTreeNode( this, getBoundingVolume );
		}
		
		public function get animSprite():AnimatedSprite
		{
			if ( drawable is AnimatedSprite )
			{
				return drawable as AnimatedSprite;
			}
			return null;
		}
	
		public function spawn( data:SharedData ):void
		{
			if ( !isSpawned )
			{
				data.sceneMgr.gameLayer.addChild( this );
				data.sceneMgr.tree.insert( node );
				node.move();
				isSpawned = true;
				dispatchEvent( new Event( SPAWN_EVENT ) );
			}
		}
		
		public function spawnInTheSameRoomAs( other:Actor, data:SharedData, minDistance:Number = 2.0 ):Boolean
		{
			if (isSpawned)
				return true;
			
			var searchArea:Rectangle = data.playArea.clone();
			searchArea.width = searchArea.width - (2 * data.GRID_UNIT_SIZE);
			searchArea.height = searchArea.height - (2 * data.GRID_UNIT_SIZE);
			searchArea.x = other.x - (searchArea.width / 2.0 );
			searchArea.y = other.y - (searchArea.height / 2.0 );
			var pt:Point = data.terrainMgr.getTextureIndexForWorldPoint( new Point( searchArea.x, searchArea.y ) );
			searchArea.setTo( pt.x * data.GRID_UNIT_SIZE, 
							  pt.y * data.GRID_UNIT_SIZE, 
							  Math.round(searchArea.width / data.GRID_UNIT_SIZE) * data.GRID_UNIT_SIZE, 
							  Math.round(searchArea.height / data.GRID_UNIT_SIZE) * data.GRID_UNIT_SIZE );
			
			var searchSpace:Array = new Array();
			
			var wInGridUnits:int = searchArea.width / (data.HALF_GRID_UNIT_SIZE / 2);
			var hInGridUnits:int = searchArea.height / (data.HALF_GRID_UNIT_SIZE / 2);
			for ( var i:int = 0; i < wInGridUnits; i++ )
			{
				for ( var j:int = 0; j < hInGridUnits; j++ )
				{
					searchSpace.push( new Point( i, j ) );
				}
			}
			
			this.spawn( data );
			
			var test:Rectangle = this.getBoundingVolume().clone();
			test.setTo( other.getBoundingVolume().x, other.getBoundingVolume().y, test.width, test.height );
			
			while ( searchSpace.length > 0 )
			{
				pt = searchSpace.splice( [ Maths.randInt( 0, searchSpace.length - 1 ) ], 1 )[0] as Point;
				this.setPos( searchArea.x + ( pt.x * (data.HALF_GRID_UNIT_SIZE / 2) ), 
							 searchArea.y + ( pt.y * (data.HALF_GRID_UNIT_SIZE / 2) ));
				
				if ( data.sceneMgr.isEmptySpace( this, data.terrainMgr ) && 
					 (( data.terrainMgr.getTerrainForAABBMove( this.getBoundingVolume(), test ) & this.badWallMask ) == 0x0) &&
					 Maths.distance( this.pos, other.pos ) >= (minDistance * data.GRID_UNIT_SIZE) )
				{
					return true;
				}
			}
			
			DebugLog.getSingleton().w( TAG, "Unable to find appropriate spawn-in position for " + this.name + ". Not Spawning" );
			this.despawn( data );
			return false;
		}
		
		public function despawn( data:SharedData ):void
		{
			if ( isSpawned )
			{
				data.sceneMgr.gameLayer.removeChild( this );
				var idx:int = data.tickList.indexOf( this );
				if ( idx != -1 )
				{
					data.tickList.splice( idx, 1 );
				}
				data.sceneMgr.tree.remove( node );
				isSpawned = false;
				dispatchEvent( new Event( DESPAWN_EVENT ) );
			}
		}
		
		public function get id():uint
		{
			return _id;
		}
		
		public function setName( s:String ):void
		{
			this.name = s;
		}
		
		public function get boundingVolume():Rectangle
		{
			bounds.copyFrom( drawable.getRect( this ) );
			bounds.offset( x, y );
			return bounds;
		}
		
		public function getBoundingVolume():Rectangle
		{
			return boundingVolume;
		}
		
		private var dirBkp:Point = new Point();
		public function setDrawable( disp:DisplayObject ):void
		{
			this.removeChild( drawable );
			drawable = disp;
			this.addChild( drawable );
			drawable.x = - drawable.width / 2.0;
			drawable.y = - drawable.height / 2.0;
			
			
			// reset the quad tree node
			dirBkp.setTo( dirX, dirY );
			setPos( this.x + 0.1, this.y + 0.1 );
			setPos( this.x - 0.1, this.y - 0.1 );
			setDirection( dirBkp.x, dirBkp.y );
		}
		
		public function setPos( x:Number, y:Number ):void
		{
			var dx:Number = x - this.x;
			var dy:Number = y - this.y;
			if ( ( dx != 0.0 ) || ( dy != 0.0 ) )
			{
				setDirection( dx, dy );
			}
			this.x = x;
			this.y = y;
			
			if ( node.isInTree )
			{
				node.move();
			}
		}
		
		public function set dirX( x:Number ):void
		{
			setDirection(x, _dirFacing[1]);
		}
		
		public function get dirX():Number
		{
			return _dirFacing[0];
		}
		
		public function set dirY( x:Number ):void
		{
			setDirection(_dirFacing[0], x);
		}
		
		public function setDirection( x:Number, y:Number ):void
		{
			_dirFacing[0] = x;
			_dirFacing[1] = y;
			var mag:Number = Math.sqrt((_dirFacing[0] * _dirFacing[0]) + (_dirFacing[1] * _dirFacing[1]));
			_dirFacing[0] /= mag;
			_dirFacing[1] /= mag;
			var rot:Number = (Math.atan2(_dirFacing[1], _dirFacing[0]) * RAD_TO_DEG);
			attachments.rotation = rot  + facingOffset;
			this.rotation = rot + facingOffset;
		}
		
		public function get dirY():Number
		{
			return _dirFacing[1];
		}
		
		public function set pos( a:Point ):void
		{
			setPos( a.x, a.y );
		}
		
		/**
		 * Is this the top lefthand corer or the center?
		 */
		public function get pos():Point
		{
			return new Point(this.x, this.y);
		}
		
		public function rotate( n:Number ):void
		{
			this.attachments.rotation += n;
		}
		
		public override function set rotation( n:Number ):void
		{
			if ( rotateTogether )
			{
				super.rotation = n;
			
			} else
			{
				this.attachments.rotation = n;
			}
		}
		
		public override function get rotation():Number
		{
			if ( rotateTogether )
			{
				return super.rotation;
			}
			return this.attachments.rotation;
		}
		
		public function attachBitmap( child:DisplayObject ):void
		{
			this.attachments.addChild(child);
		}
		
		public function detatchBitmap( child:DisplayObject ):void
		{
			this.attachments.removeChild( child );
		}
		
		public function move( x:Number, y:Number ):void
		{
			setPos( this.x + x, this.y + y );
		}
		
		public function notifyHolder(e:Event):void
		{
			this.dispatchEvent(e);
		}
		
		public function face( other:Actor ):void
		{
			this.setDirection( other.x - this.x, other.y - this.y );
		}
		
		public function destroy():void
		{
			var i:int;
			
			CONFIG::debug
			{
				if ( isSpawned )
				{
					DebugLog.getSingleton().w( TAG, "Destroying a spawned actor" );
				}
			}
			
			// Unreference everything. Is this the right choice?
			if ( !isConstructed )
			{
				DebugLog.getSingleton().w( TAG, "Double destroy" );
				return;
			}
			
			var spr:AnimatedSprite = animSprite;
			if ( animSprite != null )
			{
				spr.destroy();
			}
			
			removeChildren();
			
			// hmm will this work?
			if ( parent != null )
			{
				parent.removeChild( this );
			}
			
			// the drawable needs to be destroyed.
			drawable = null;
			
			_dirFacing = null;
			
			attachments.removeChildren();
			attachments = null;
			
			overlays.removeChildren();
			overlays = null;
			node.destroy();
			node = null;
			bounds = null;
			
			dirBkp = null;
			
			GameObjectRegistry.getSingleton().unregister( this._id );
			
			isConstructed = false;
		}
		
		public override function toString():String
		{
			var str:String = this.name + "\n";
			str += "   Position: { " + this.x + ", " + this.y + "}\n";
			return( str );
		}
		
	}

}