package org.xangarath.engine.events 
{
	import flash.events.Event;
	import org.xangarath.engine.Actor;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	
	/**
	 * Denotes a collision between actors
	 * @author Jeffrey Cochran
	 */
	public class CollisionEvent extends Event implements IDestructable 
	{
		public static const COLLISION:String = "collision";
		public var instigator:Actor;
		public function CollisionEvent( instigator:Actor, type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			this.instigator = instigator;
		} 
		
		public override function clone():Event 
		{ 
			ErrorUtil.dbgAssert( "CollisionEvent", false, "don't do this please" );
			return new CollisionEvent( instigator, type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("CollisionEvent", "instigator", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
		public function destroy():void
		{
			instigator = null;
		}
		
	}
	
}