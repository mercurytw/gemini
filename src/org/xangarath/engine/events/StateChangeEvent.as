package org.xangarath.engine.events 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class StateChangeEvent extends Event 
	{
		public static const STATE_CHANGE:String = "StateChangeEvent";
		public var newState:String;
		public function StateChangeEvent(newState:String, type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			this.newState = newState;
		} 
		
		public override function clone():Event 
		{ 
			return new StateChangeEvent(newState, type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("StateChangeEvent", "newState", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}