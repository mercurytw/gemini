package org.xangarath.engine 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.filesystem.*;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import org.xangarath.engine.dbg.DebugLog;
	
	/**
	 * Embeds used assets and provides them as their appropriate classes.
	 * @author Jeff Cochran
	 */
	public class ResourceManager 
	{
		private static const TAG:String = "ResourceManager";
		
		[Embed(source = "../../../../assets/Metrophobic.ttf", fontName = "Metrophobic", mimeType = "application/x-font", 
		fontWeight = "normal", unicodeRange = "U+0020-U+002F,U+0030-U+0039,U+003A-U+0040,U+0041-U+005A,U+005B-U+0060,U+0061-U+007A,U+007B-U+007E", 
		advancedAntiAliasing="true", embedAsCFF="false")]
		public static const METROPHOBIC_FONT:String;

		public static const METROPHOBIC:String = "Metrophobic";
		
		[Embed(source = "../../../../assets/Gemini.ttf", fontName = "Gemini", mimeType = "application/x-font", 
		fontWeight = "normal", unicodeRange = "U+0020-U+002F,U+0030-U+0039,U+003A-U+0040,U+0041-U+005A,U+005B-U+0060,U+0061-U+007A,U+007B-U+007E", 
		advancedAntiAliasing="true", embedAsCFF="false")]
		public static const GEMINI_FONT:String;

		public static const GEMINI:String = "Gemini";

		private var mBitmaps:Dictionary; /** String to Class map */
		public function ResourceManager() 
		{
			mBitmaps = new Dictionary();
			
		}
		
		public function addBitmap( name:String, bmp:Bitmap ):void
		{
			if ( mBitmaps[name] )
			{
				trace(name + " already exists in the dictionary");
				return;
			}
			mBitmaps[name] = bmp;
		}
		
		public function hasBitmap( name:String ):Boolean
		{
			return mBitmaps[ name ] != null;
		}
		
		public function getBitmap( name:String, clone:Boolean = true ):Bitmap
		{
			if ( mBitmaps[name] ) // if it's in the dictionary
			{
				if ( clone )
				{
					return new Bitmap(((mBitmaps[name]) as Bitmap).bitmapData.clone());
				}
				else
				{
					return mBitmaps[name] as Bitmap;
				}
			}
			DebugLog.getSingleton().e( TAG, "Requested unknown bitmap \"" + name + "\"" );
			return null;
		}
		
		public function addBitmapsFromSpritesheet( sheet:Bitmap, json:String ):void
		{
			var dict:Object = JSON.parse( json );
			
			var frames:Object = Argonaut.getAndCheck( TAG, dict, "frames" );
			
			var imgName:String;
			var source:Rectangle = new Rectangle();
			var newSize:Point = new Point();
			var bmpData:BitmapData;
			var tmpObject:Object;
			for ( var i:int = 0; frames[ i ] != null; i++ )
			{
				imgName = Argonaut.getAndCheck( TAG, frames[ i ], "filename" );
				tmpObject = Argonaut.getAndCheck( TAG, frames[ i ], "frame" );
				source.x = Argonaut.getAndCheckN( TAG, tmpObject, "x" );
				source.y = Argonaut.getAndCheckN( TAG, tmpObject, "y" );
				source.width = Argonaut.getAndCheckN( TAG, tmpObject, "w" );
				source.height = Argonaut.getAndCheckN( TAG, tmpObject, "h" );
				
				tmpObject = Argonaut.getAndCheck( TAG, frames[ i ], "sourceSize" );
				newSize.x = Argonaut.getAndCheck( TAG, tmpObject, "w" );
				newSize.y = Argonaut.getAndCheck( TAG, tmpObject, "h" );
				
				bmpData = new BitmapData( newSize.x, newSize.y );
				
				bmpData.copyPixels( sheet.bitmapData, source, new Point( (newSize.x - source.width) / 2., (newSize.y - source.height) / 2. ) );
				addBitmap( imgName, new Bitmap( bmpData ) );
			}
		}
		
		public static function loadFileAsString( path:String, prependPath:Boolean = true ):String
		{
			var file:File = new File(( prependPath ) ? File.applicationDirectory.nativePath + File.separator + path : path);
			if (!file.exists)
			{
				DebugLog.getSingleton().e( TAG, "unable to resolve file: " + path );
				throw new Error( TAG + "unable to resolve file: " + path );
			}
			var fs:FileStream = new FileStream();
			fs.open( file, FileMode.READ );
			var ret:String;
			ret = fs.readUTFBytes(fs.bytesAvailable);
			fs.close();
			return ret;
		}
		
		public static function loadFileAsByteArray( path:String, prependPath:Boolean = true ):ByteArray
		{
			var file:File = new File(( prependPath ) ? File.applicationDirectory.nativePath + File.separator + path : path);
			if (!file.exists)
			{
				DebugLog.getSingleton().e( TAG, "unable to resolve file: " + path );
				throw new Error( TAG + "unable to resolve file: " + path );
			}
			var fs:FileStream = new FileStream();
			fs.open( file, FileMode.READ );
			var ret:ByteArray = new ByteArray();
			fs.readBytes( ret );
			fs.close();
			return ret;
		}
	}
}