package org.xangarath.engine 
{
	import flash.geom.Point;
	import org.xangarath.engine.dbg.DebugLog;
	/**
	 * Does error checked JSON access
	 * @author Jeffrey Cochran
	 */
	public class Argonaut 
	{
		public function Argonaut() 
		{
			
		}
		
		/**
		 * Get a JSON property, checking for its existence.
		 * @param	tag A log tag to print in the event of an error
		 * @param	jsobj The dynamic JSON object to retrieve the property from
		 * @param	property The key of the property to retrieve
		 * @return The requested property, if available
		 * @throws An Error if the requested property did not exist
		 * @see Argonaut#getAndCheckN and Argonaut#getAndCheckI for retrieval and typechecking of
		 * integer and numeric properties
		 */
		public static function getAndCheck( tag:String, jsobj:Object, property:String ):*
		{
			if ( jsobj[ property ] == null )
			{
				DebugLog.getSingleton().e( tag, " invalid json, couldn't find " + property );
				throw new Error( tag + " invalid json, couldn't find " + property );
			}
			return jsobj[ property ];
		}
		
		/**
		 * Get a JSON property as a Number, checking for its existence and proper parsing
		 * @param	tag A log tag to print in the event of an error
		 * @param	jsobj The dynamic JSON object to retrieve the numeric property from
		 * @param	property The key of the numeric property to retrieve
		 * @return The requested property, if available
		 * @throws An Error if the requested property did not exist or was parsable to a Number
		 * @see Argonaut#getAndCheckI for retrieval and typechecking of
		 * integer properties
		 */
		public static function getAndCheckN( tag:String, jsobj:Object, property:String ):Number
		{
			var res:Number;
			if ( jsobj[ property ] == null )
			{
				DebugLog.getSingleton().e( tag, " invalid json, couldn't find " + property );
				throw new Error( tag + " invalid json, couldn't find " + property );
			}
			res = parseFloat( jsobj[ property ] );
			
			if ( isNaN( res ) )
			{
				DebugLog.getSingleton().e( tag, " invalid json, property " + property + " is not a number " );
				throw new Error( tag + " invalid json, property " + property + " is not a number " );
			}
			return res;
		}
		
		/**
		 * Get a JSON property as a numeric percentage, checking for its existence and proper parsing
		 * @param	tag A log tag to print in the event of an error
		 * @param	jsobj The dynamic JSON object to retrieve the numeric property from
		 * @param	property The key of the numeric property to retrieve
		 * @return The requested property, if available
		 * @throws An Error if the requested property did not exist or was parsable to a numeric percentage
		 * @see Argonaut#getAndCheckI for retrieval and typechecking of
		 * integer properties
		 */
		public static function getAndCheckPercent( tag:String, jsobj:Object, property:String ):Number
		{
			var res:Number;
			if ( jsobj[ property ] == null )
			{
				DebugLog.getSingleton().e( tag, " invalid json, couldn't find " + property );
				throw new Error( tag + " invalid json, couldn't find " + property );
			}
			res = parseFloat( jsobj[ property ] );
			
			if ( isNaN( res ) )
			{
				DebugLog.getSingleton().e( tag, " invalid json, property " + property + " is not a number " );
				throw new Error( tag + " invalid json, property " + property + " is not a number " );
			}
			
			if ( ( res < 0.0 ) || ( res > 1.0 ) )
			{
				DebugLog.getSingleton().e( tag, " invalid json, property " + property + " is not in the range 0-1 " );
				throw new Error( tag + " invalid json, property " + property + " is not in the range 0-1 " );
			}
			return res;
		}
		
		/**
		 * Get a JSON property as an integer, checking for its existence and proper parsing
		 * @param	tag A log tag to print in the event of an error
		 * @param	jsobj The dynamic JSON object to retrieve the numeric property from
		 * @param	property The key of the integer property to retrieve
		 * @return The requested property, if available
		 * @throws An Error if the requested property did not exist or was parsable to a Number
		 * @see Argonaut#getAndCheckN for retrieval and typechecking of
		 * Number properties
		 */
		public static function getAndCheckI( tag:String, jsobj:Object, property:String ):int
		{
			var res:int;
			if ( jsobj[ property ] == null )
			{
				DebugLog.getSingleton().e( tag, " invalid json, couldn't find " + property );
				throw new Error( tag + " invalid json, couldn't find " + property );
			}
			res = parseInt( jsobj[ property ] );
			
			if ( isNaN( res ) )
			{
				DebugLog.getSingleton().e( tag, " invalid json, property " + property + " is not an int " );
				throw new Error( tag + " invalid json, property " + property + " is not an int " );
			}
			return res;
		}
		
		
		/**
		 * Get a JSON property as an unsigned integer, checking for its existence and proper parsing. Removes negatives if present
		 * @param	tag A log tag to print in the event of an error
		 * @param	jsobj The dynamic JSON object to retrieve the numeric property from
		 * @param	property The key of the unsigned integer property to retrieve
		 * @return The requested property, if available
		 * @throws An Error if the requested property did not exist or was parsable to a Number
		 * @see Argonaut#getAndCheckN for retrieval and typechecking of
		 * Number properties
		 */
		public static function getAndCheckUI( tag:String, jsobj:Object, property:String ):uint
		{
			var tmp:int;
			var res:uint;
			if ( jsobj[ property ] == null )
			{
				DebugLog.getSingleton().e( tag, " invalid json, couldn't find " + property );
				throw new Error( tag + " invalid json, couldn't find " + property );
			}
			tmp = parseInt( jsobj[ property ] );
			
			if ( isNaN( tmp ) )
			{
				DebugLog.getSingleton().e( tag, " invalid json, property " + property + " is not an int " );
				throw new Error( tag + " invalid json, property " + property + " is not an int " );
			}
			
			if ( tmp < 0 )
			{
				DebugLog.getSingleton().w( tag, " property \"" + property + "\" was found to be negative. Dropping sign..." );
				res = ( ( tmp * -1. ) as uint );
			}
			else
			{
				res = tmp as uint;
			}
			
			return res;
		}
		
		/**
		 * Get a JSON property as a boolean, checking for its existence and proper parsing
		 * @param	tag A log tag to print in the event of an error
		 * @param	jsobj The dynamic JSON object to retrieve the property from
		 * @param	property The key of theproperty to retrieve
		 * @return The requested property, if available
		 * @throws An Error if the requested property did not exist or was parsable to a Boolean
		 */
		public static function getAndCheckB( tag:String, jsobj:Object, property:String ):Boolean
		{
			var res:Boolean = false;
			var str:String = getAndCheck( tag, jsobj, property );
			str = str.toLowerCase();
			
			if ( str == "true" )
			{
				res = true;
			} else if ( str == "false" )
			{
				res = false;
			} else 
			{
				DebugLog.getSingleton().e( tag, " invalid json, property " + property + " was not parseable to a boolean value " );
				throw new Error( tag + " invalid json, property " + property + " was not parseable to a boolean value " );
			}
			
			return res;
		}
		
		/**
		 * Get a JSON property as a Hex-encoded color, checking for its existence and proper parsing
		 * @param	tag A log tag to print in the event of an error
		 * @param	jsobj The dynamic JSON object to retrieve the property from
		 * @param	property The key of the color property to retrieve
		 * @return The requested property, if available
		 * @throws An Error if the requested property did not exist or was parsable to a hex-encoded color
		 */
		public static function getAndCheckColorHex( tag:String, jsobj:Object, property:String ):uint
		{
			var res:uint;
			var str:String = getAndCheck( tag, jsobj, property );
			
			str = str.replace( "#", "0x" );
			if ( str.indexOf( "0x" ) == -1 )
			{
				str = "0x" + str;
			}
			
			res = parseInt( str );
			
			if ( isNaN( res ) )
			{
				DebugLog.getSingleton().e( tag, " invalid json, property " + property + " is not a number " );
				throw new Error( tag + " invalid json, property " + property + " is not a number " );
			}
			
			return res;
		}
		
		/**
		 * Get a JSON property as a point, checking for its existence and proper parsing
		 * @param	tag A log tag to print in the event of an error
		 * @param	jsobj The dynamic JSON object to retrieve the property from
		 * @param	property The key of the property to retrieve
		 * @return The requested property, if available
		 * @throws An Error if the requested property did not exist or was parsable
		 */
		public static function getAndCheckPoint( tag:String, jsobj:Object, property:String ):Point
		{
			var res:Point = new Point();
			
			var prop:Object = getAndCheck( tag, jsobj, property );
			if ( ( prop[ 0 ] == null ) || ( prop[ 1 ] == null ) )
			{
				DebugLog.getSingleton().e( tag, "invalid json, property " + property + " does not contain two subelements" );
				throw new Error( tag +  " invalid json, property " + property + " does not contain two subelements" );
			}
			res.x = parseFloat( prop[ 0 ] );
			res.y = parseFloat( prop[ 1 ] );
			
			if ( isNaN( res.x ) || isNaN( res.y ) )
			{
				DebugLog.getSingleton().e( tag, "invalid json, property " + property + " does not contain numeric subelements!" );
				throw new Error ( tag + " invalid json, property " + property + " does not contain numeric subelements!" );
			}
			return res;
		}
	}

}