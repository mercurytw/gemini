package org.xangarath.engine.graphics 
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.Point;
	/**
	 * Collection of convenience draw functions
	 * @author Jeff Cochran
	 */
	public class RenderHelper 
	{
		private static var _singleton:RenderHelper = null;
		public function RenderHelper()
		{
		}
		
		public static function getSingleton():RenderHelper
		{
			if (_singleton == null)
			{
				_singleton = new RenderHelper();
			}
			return _singleton;
		}
		
		public function DrawLines( target:Sprite, color:uint, weight:Number, points:Array ):void
		{
			var pt:Point;
			target.graphics.lineStyle( weight, color );
			
			for ( var i:int = 0; i < points.length; i++ )
			{
				pt = points[i] as Point;
				if (i != 0)
				{
					target.graphics.lineTo( pt.x, pt.y );
				}
				else
				{
					target.graphics.moveTo( pt.x, pt.y );
				}
			}
		}
	}

}