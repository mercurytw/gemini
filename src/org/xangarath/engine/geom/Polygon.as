package org.xangarath.engine.geom 
{
	import flash.geom.Point;
	import org.xangarath.engine.dbg.ErrorUtil;
	/**
	 * Polygon.
	 * @author Jeff Cochran
	 */
	public class Polygon 
	{
		private static const TAG:String = "Polygon";
		private var _pts:Vector.<Point>;
		private var _length:uint;
		public function Polygon( reserve:uint = 0 ) 
		{
			_pts = new Vector.<Point>( reserve );
			for ( var i:int = 0; i < reserve; i++ )
			{
				_pts[ i ] = new Point();
			}
			_length = 0;
		}
		
		public function get length():uint
		{
			return _length;
		}
		
		public function set length( l:uint ):void
		{
			if ( l < _length )
			{
				_length = l;
				return;
			}
		
			var oldLen:int = _pts.length;
			var i:int;
			_pts.length = l;
			
			for ( i = oldLen; i < l; i++ )
			{
				_pts[ i ] = new Point();
			}
			_length = l;
		}
		
		public function getPointAt( index:uint ):Point
		{
			ErrorUtil.dbgAssert( TAG, index < _length, "requested point out of bounds" );
			
			if ( index >= _length )
			{
				return null;
			}
			
			return _pts[ index ];
		}
		
		public function push(... args):uint
		{
			var valid:Boolean = true;
			for each ( var o:Object in args )
			{
				ErrorUtil.dbgAssertNonNull( TAG, o );
				ErrorUtil.dbgAssert( TAG, (o is Point) || (o is Number), "passed in an object that was not a point!" );
				
				if ( o is Number )
				{
					if ( valid )
					{
						if ( _length == _pts.length )
						{
							_pts.length++;
							_pts[ _length ] = new Point();
						}
						_pts[ _length ].x = o as Number;
					}
					else
					{
						_pts[ _length ].y = o as Number;
						_length++;
					}
					
					valid = !valid;
				}
				else
				{
					ErrorUtil.dbgAssert( TAG, valid, "arg list contained unpaired numbers" );
					if ( _length < _pts.length )
					{
						_pts[ _length ].copyFrom( o as Point );
					}
					else
					{
						_pts.push( ( o as Point ).clone() );
					}
					_length++;
				}
			}
			ErrorUtil.dbgAssert( TAG, valid, "arg list contained unpaired numbers" );
			return _length;
		}
		
		public function cleanup():void
		{
			_pts.length = _length;
		}
	}

}