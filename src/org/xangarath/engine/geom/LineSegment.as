package org.xangarath.engine.geom 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class LineSegment 
	{
		public var a:Point;
		public var b:Point;
		public function LineSegment( ptA:Point = null, ptB:Point = null ) 
		{
			if ( ptA == null )
			{
				ptA = new Point();
			}
			
			if ( ptB == null )
			{
				ptB = new Point();
			}
			
			a = ptA.clone();
			b = ptB.clone();
		}
		
		public function equals( other:LineSegment ):Boolean
		{
			return ( ( this.a.equals(other.a) ) && ( this.b.equals(other.b) ) || ( this.a.equals(other.b) ) && ( this.b.equals(other.a) ) );
		}
	}

}