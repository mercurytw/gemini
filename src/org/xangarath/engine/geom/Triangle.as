package org.xangarath.engine.geom 
{
	import flash.geom.Point;
	import org.xangarath.engine.math.Maths;
	
	/**
	 * Represents a simple triangle right now. Will be extended only as needed
	 * @author Jeffrey Cochran
	 */
	public class Triangle 
	{
		public var A:Point;
		public var B:Point;
		public var C:Point;
		public function Triangle( ptA:Point = null, ptB:Point = null, ptC:Point = null ) 
		{
			if ( ptA == null )
			{
				A = new Point();
			} else
			{
				A = ptA;
			}
			
			if ( ptB == null )
			{
				B = new Point();
			} else
			{
				B = ptB;
			}
			
			if ( ptC == null )
			{
				C = new Point();
			} else
			{
				C = ptB;
			}
		}
		
		public function copyFrom( other:Triangle ):void
		{
			A.copyFrom( other.A );
			B.copyFrom( other.B );
			C.copyFrom( other.C );
		}
		
		public function containsPoint( P:Point ):Boolean
		{
			if ( Maths.ccw( A, B, C ) && Maths.ccw( A, B, P ) && Maths.ccw( B, C, P ) && Maths.ccw( C, A, P ) )
			{
				return true;
			}
			else if ( !Maths.ccw( A, B, C ) && !Maths.ccw( A, B, P ) && !Maths.ccw( B, C, P ) && !Maths.ccw( C, A, P ) )
			{
				return true;
			}
			
			return false;
		}
	}

}