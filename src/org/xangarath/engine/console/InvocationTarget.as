package org.xangarath.engine.console 
{
	/**
	 * Wraps name and callbacks for a class that can be invoked from the debug console.
	 * @author Jeffrey Cochran
	 */
	public class InvocationTarget 
	{
		public var invocationName:String;
		private var invocationCallback:Function;
		public var description:String;
		public function InvocationTarget(name:String, invocation:Function, description:String) 
		{
			invocationName = name;
			invocationCallback = invocation;
			this.description = description;
		}
		
		public function invoke( a:Array ):String
		{
			return invocationCallback.call(null, a);
		}
	}

}