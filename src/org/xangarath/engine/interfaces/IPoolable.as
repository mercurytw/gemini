package org.xangarath.engine.interfaces 
{
	
	/**
	 * Things that can be in a pool
	 * @author Jeffrey Cochran
	 */
	public interface IPoolable 
	{
		function clone():IPoolable;
		function isActive():Boolean;
		function activate():void;
		function deactivate():void;
	}
	
}