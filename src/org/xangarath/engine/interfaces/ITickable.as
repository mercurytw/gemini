package org.xangarath.engine.interfaces 
{
	
	/**
	 * Takes time in seconds as a parameter
	 * @author Jeff Cochran
	 */
	public interface ITickable 
	{
		function tick( deltaTime:Number ):void;
	}
	
}