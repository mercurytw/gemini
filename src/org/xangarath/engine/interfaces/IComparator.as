package org.xangarath.engine.interfaces 
{
	/**
	 * A comparator is able to compare two entities
	 * @author Jeff Cochran
	 */
	public interface IComparator 
	{
		function getComparableClass():Class;
		/**
		 * Compares this instance to other instance
		 * @param	other The instance to compare to 
		 * @return Returns a negative number if this is less than other, a positive number if this is more than other, and zero if the two have the same value
		 */
		function compare( lhs:Object, rhs:Object ):int;
	}
	
}