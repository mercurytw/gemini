package org.xangarath.engine.interfaces 
{
	
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public interface ITerrainInterpreter 
	{	
		function getTerrainType( terrain:String ):uint;
	}
	
}