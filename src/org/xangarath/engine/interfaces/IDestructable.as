package org.xangarath.engine.interfaces 
{
	
	/**
	 * Destructable as in contstructor/destructor not as in "can explode"
	 * @author Jeff Cochran
	 */
	public interface IDestructable 
	{
		/**
		 * Tears down this object for garbage collection (maybe)
		 */
		function destroy():void;
	}
	
}