package org.xangarath.engine 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITickable;
	import org.xangarath.engine.quadtree.QuadTreeNode;
	import org.xangarath.engine.script.ScriptManager;
	import org.xangarath.engine.tiled.TiledObject;
	/**
	 * An invisible rectangle in the game world which can be subclassed or scripted to 
	 * do useful work
	 * @author Jeff Cochran
	 */
	public class Hotspot extends EventDispatcher implements ITickable, IDestructable
	{
		private static const TAG:String = "Hotspot";
		private static var dbgDrawHotspots:Boolean = false;
		private var _id:uint;
		public var isSpawned:Boolean = false;
		protected var collisionVolume:Rectangle;
		protected var script:String;
		protected var data:SharedData;
		public var node:QuadTreeNode;
		public var name:String;
		protected var contents:Array;
		public var scriptOnActorEnter:Function = null;
		public var scriptOnActorExit:Function = null;
		public var scriptOnTick:Function = null;
		protected var isConstructed:Boolean = true;
		
		public function Hotspot( data:SharedData, cv:Rectangle, script:String = null ) 
		{
			super();
			this._id = GameObjectRegistry.getSingleton().register( this );
			this.collisionVolume = new Rectangle(cv.x, cv.y, cv.width, cv.height);
			this.data = data;
			this.contents = new Array();
			
			name = "Hotspot#" + _id;
			
			node = new QuadTreeNode( this, getBoundingVolume );
			
			applyScript(script);
		}
		
		public static function getHotspotFromTiled( to:TiledObject,  data:SharedData, scriptPath:String = "" ):Hotspot
		{
			var ret:Hotspot;
			var cv:Rectangle = new Rectangle( to.x, to.y, to.width, to.height );
			var script:String = to.properties[ "script" ];
			if ( script != null )
			{
				script = scriptPath + script;
			}
			
			ret = new Hotspot(data, cv, script);
			if (to.name != "")
			{
				ret.name = to.name;
			}
			return ret;
		}
		
		public function spawn():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			data.sceneMgr.insertHotspot( this );
			data.tickList.push( this );
			node.move();
			isSpawned = true;
		}
		
		public function despawn():void
		{
			if ( isSpawned )
			{
				ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
				data.sceneMgr.removeHotspot( this );
				var idx:int = data.tickList.indexOf( this );
				if ( idx != -1 )
				{
					data.tickList.splice( idx, 1 );
				}
				// scene node removal handled by scene manager
				isSpawned = false;
			}
		}
		
		protected function onActorEntry( a:Actor ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if (scriptOnActorEnter != null)
			{
				scriptOnActorEnter.apply( this, [ a ] );
			}
		}
		
		protected function onActorExit( a:Actor ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if (scriptOnActorExit != null)
			{
				scriptOnActorExit.apply( this, [ a ] );
			}
		}
		
		public function receiveEvent( e:Event ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if (isSpawned)
			{
				if (e.type == CollisionEvent.COLLISION)
				{
					var ce:CollisionEvent = e as CollisionEvent;
					
					if (contents.indexOf(ce.instigator) == -1 )
					{
						contents.push(ce.instigator);
						onActorEntry( ce.instigator );
					}
				}
				dispatchEvent(e);
			}
		}
		
		public function get id():uint
		{
			return _id;
		}
		
		public function getBoundingVolume():Rectangle
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return collisionVolume;
		}
		
		public function setPos( x:Number, y:Number ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			collisionVolume.x = x;
			collisionVolume.y = y;
			
			if ( node.isInTree )
			{
				node.move();
			}
		}
		
		public function getPos():Point
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return new Point(collisionVolume.x, collisionVolume.y);
		}
		
		private function draw():void
		{
			// TODO: Move me to the cheat system, and just expose the hotspot's bounding volume
		}
		
		public function applyScript( p:String ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			this.script = p;
			this.scriptOnActorEnter = null;
			this.scriptOnActorExit = null;
			if (script != null)
			{
				data.scriptMgr.doReloadableFile( ScriptManager.VFS_SCRIPT_ROOT + script, this );
			}
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isSpawned )
			{
				despawn();
			}
			
			name = null;
			node.destroy();
			node = null;
			
			scriptOnActorEnter = null;
			scriptOnActorExit = null;
			scriptOnTick = null;
			
			GameObjectRegistry.getSingleton().unregister( this._id );
			
			collisionVolume = null;
			contents.length = 0;
			contents = null;
			
			if ( script != null )
			{
				data.scriptMgr.undoReloadableFile( ScriptManager.VFS_SCRIPT_ROOT + script );
			}
			
			data = null;
			script = null;
			isConstructed = false;
		}
		
		public function tick( deltaTime:Number ):void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if (isSpawned)
			{
				for each ( var a:Actor in contents )
				{
					if (!collisionVolume.intersects( a.getBoundingVolume() ))
					{
						contents.splice( contents.indexOf( a ), 1 );
						onActorExit( a );
					}
				}
				
				if (scriptOnTick != null)
				{
					scriptOnTick.apply( this );
				}
			}
		}
	}

}