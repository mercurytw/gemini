package org.xangarath.engine  
{
	import flash.display.Stage;
	import flash.geom.Rectangle;
	import org.xangarath.engine.anim.AnimationManager;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITerrainInterpreter;
	import org.xangarath.engine.script.ScriptManager;
	import org.xangarath.engine.world.TerrainManager;
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class SharedData 
	{
		public var secondsPerFrame:Number;
		public var tickList:Array;
		public var playArea:Rectangle;
		public var sceneMgr:SceneManager;
		public var resMgr:ResourceManager;
		public var animMgr:AnimationManager;
		public var scriptMgr:ScriptManager;
		public var terrainMgr:TerrainManager;
		public const GRID_UNIT_SIZE:Number = 50;
		public const HALF_GRID_UNIT_SIZE:Number = 25;
		public var gameIsPaused:Boolean = false;
		public var requestExit:Boolean = false;
		public var trashcan:Array = new Array();
		public function SharedData(stage:Stage, terrainInterp:ITerrainInterpreter) 
		{
			this.secondsPerFrame = 1.0 / stage.frameRate;
			this.playArea = new Rectangle(0., 0., stage.stageWidth, stage.stageHeight);
			this.sceneMgr = new SceneManager(stage);
			this.resMgr = new ResourceManager();
			this.animMgr = new AnimationManager( resMgr );
			USER::ENABLE_SCRIPTING
			{
				this.scriptMgr = new ScriptManager( resMgr );
			}
			this.terrainMgr = new TerrainManager( terrainInterp );
			tickList = new Array();	
		}
	}

}