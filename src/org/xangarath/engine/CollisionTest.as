/**
 * Pixel Perfect Collision Detection
 * ---------------------
 * VERSION: 1.0.1
 * DATE: 8/23/2011
 * AS3
 * UPDATES AND DOCUMENTATION AT: http://www.FreeActionScript.com
 *  modified to fit the purposes of the game by jcochran
 **/
package org.xangarath.engine 
{
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.math.Maths;
	
	public class CollisionTest implements IDestructable
	{
		private static const TAG:String = "CollisionTest";
		// vars
		private var _returnValue:Boolean;
		
		private var _onePoint:Point;
		private var _twoPoint:Point;
		
		private var _oneRectangle:Rectangle;
		private var _twoRectangle:Rectangle;
		
		private var _oneClipBmpData:BitmapData;
		private var _twoClipBmpData:BitmapData;
		
		private var _oneOffset:Matrix;
		private var _twoOffset:Matrix;
		
		private var _resultArray:Array;
		
		private var _A:Point;
		private var _B:Point;
		private var _C:Point;
		private var _D:Point;
		
		private var _oneVect:Vector.<Point> = null;
		private var _twoVect:Vector.<Point> = null;
		
		private var _linesFound:Vector.<Boolean> = null;
		
		private var isConstructed:Boolean = true;
		/**
		 * Simple collision test. Use this for objects that are not rotated.
		 * @param	clip1	Takes DisplayObjectContainer as argument. Can be a Sprite, MovieClip, etc.
		 * @param	clip2	Takes DisplayObjectContainer as argument. Can be a Sprite, MovieClip, etc.
		 * @return	Collision True/False
		 */
		public function simple(clip1:DisplayObjectContainer, clip2:DisplayObjectContainer):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			_returnValue = false;
			
			_oneRectangle = clip1.getBounds( clip1.parent );
			_oneClipBmpData = new BitmapData(_oneRectangle.width, _oneRectangle.height, true, 0);
			_oneClipBmpData.draw(clip1);
			
			_twoRectangle = clip2.getBounds( clip2.parent );
			_twoClipBmpData = new BitmapData(_twoRectangle.width, _twoRectangle.height, true, 0);
			_twoClipBmpData.draw(clip2);
			
			_onePoint = new Point(clip1.x, clip1.y)
			_twoPoint =  new Point(clip2.x, clip2.y)
			
			if (_oneClipBmpData.hitTest(_onePoint, 255, _twoClipBmpData, _twoPoint, 255))
			{
				_returnValue = true;
			}
			
			return _returnValue;
		}
		
		/**
		 * Complex collision test. Use this for objects that are rotated, scaled, skewed, etc
		 * @param	clip1	Takes DisplayObjectContainer as argument. Can be a Sprite, MovieClip, etc.
		 * @param	clip2	Takes DisplayObjectContainer as argument. Can be a Sprite, MovieClip, etc.
		 * @return	Collision True/False
		 */
		public function complex(clip1:DisplayObjectContainer, clip2:DisplayObjectContainer):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			_returnValue = false;
			
			_twoRectangle = clip1.getBounds(clip1);
			_oneOffset = clip1.transform.matrix;
			_oneOffset.tx = clip1.x - clip2.x;
			_oneOffset.ty = clip1.y - clip2.y;	
			
			_twoClipBmpData = new BitmapData(_twoRectangle.width, _twoRectangle.height, true, 0);
			_twoClipBmpData.draw(clip1, _oneOffset);		
			
			_oneRectangle = clip2.getBounds(clip2);
			_oneClipBmpData = new BitmapData(_oneRectangle.width, _oneRectangle.height, true, 0);
			
			_twoOffset = clip2.transform.matrix;
			_twoOffset.tx = clip2.x - clip2.x;
			_twoOffset.ty = clip2.y - clip2.y;	
			
			_oneClipBmpData.draw(clip2, _twoOffset);
			
			_onePoint = new Point(_oneRectangle.x, _oneRectangle.y);
			_twoPoint = new Point(_twoRectangle.x, _twoRectangle.y);
			
			if(_oneClipBmpData.hitTest(_onePoint, 255, _twoClipBmpData, _twoPoint, 255))
			{
				_returnValue = true;
			}
			
			_twoClipBmpData.dispose();
			_oneClipBmpData.dispose();
			
			return _returnValue;
		}
		
		/**
		 * Complex collision test. Use this for objects that are rotated, scaled, skewed, etc
		 * @param	clip1	Takes Actor as argument.
		 * @param	clip2	Takes Actor as argument. 
		 * @return	Collision True/False
		 */
		public function complexActor( clip1:Actor, clip2:Actor ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			_returnValue = false;
			
			_twoRectangle = clip1.drawable.getBounds( clip1 );
			_oneOffset = clip1.transform.matrix;
			_oneOffset.tx = clip1.x - clip2.x;
			_oneOffset.ty = clip1.y - clip2.y;	
			
			_twoClipBmpData = new BitmapData(_twoRectangle.width, _twoRectangle.height, true, 0);
			_twoClipBmpData.draw(clip1.drawable, _oneOffset);		
			
			_oneRectangle = clip2.drawable.getBounds( clip2 );
			_oneClipBmpData = new BitmapData(_oneRectangle.width, _oneRectangle.height, true, 0);
			
			_twoOffset = clip2.transform.matrix;
			_twoOffset.tx = clip2.x - clip2.x;
			_twoOffset.ty = clip2.y - clip2.y;	
			
			_oneClipBmpData.draw(clip2.drawable, _twoOffset);
			
			_onePoint = new Point(_oneRectangle.x, _oneRectangle.y);
			_twoPoint = new Point(_twoRectangle.x, _twoRectangle.y);
			
			if(_oneClipBmpData.hitTest(_onePoint, 255, _twoClipBmpData, _twoPoint, 255))
			{
				_returnValue = true;
			}
			
			_twoClipBmpData.dispose();
			_oneClipBmpData.dispose();
			
			return _returnValue;
		}
		
		/**
		 * Returns an array of lines in clip2 that are intersecting clip1. This simple function is 
		 * calculated from the AABB of clip1 and clip2, and only handles intersecting lines. 
		 * @param	clip1  Takes Actor as arguement
		 * @param	clip2  Takes Actor as arguement
		 * @return  an Array of points in right-handed order representing the lines in clip2's aabb that intersect clip1
		 */
		public function getIntersectingLinesSimple( clip1:Actor, clip2:Actor ):Array
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			_resultArray = new Array();
			_oneRectangle = clip1.drawable.getBounds( clip1.parent );
			_twoRectangle = clip2.drawable.getBounds( clip2.parent );
			
			if ( _oneVect == null )
			{
				_oneVect = new Vector.<Point>();
				_oneVect.push( new Point(), new Point(), new Point(), new Point() ); 
				_twoVect = new Vector.<Point>();
				_twoVect.push( new Point(), new Point(), new Point(), new Point() );
				_linesFound = new Vector.<Boolean>();
				_linesFound.push( false, false, false, false );
			}
			
			_linesFound[ 0 ] = false;
			_linesFound[ 1 ] = false;
			_linesFound[ 2 ] = false;
			_linesFound[ 3 ] = false;
			
			// Push the lines in anticlockwise winding order
			_oneVect[ 0 ].copyFrom( _oneRectangle.topLeft );
			_oneVect[ 1 ].setTo( _oneRectangle.left, _oneRectangle.bottom );
			_oneVect[ 2 ].copyFrom( _oneRectangle.bottomRight );
			_oneVect[ 3 ].setTo( _oneRectangle.right, _oneRectangle.top );
			
			_twoVect[ 0 ].copyFrom( _twoRectangle.topLeft );
			_twoVect[ 1 ].setTo( _twoRectangle.left, _twoRectangle.bottom );
			_twoVect[ 2 ].copyFrom( _twoRectangle.bottomRight );
			_twoVect[ 3 ].setTo( _twoRectangle.right, _twoRectangle.top );
			
			for ( var i:int = 0; i < _oneVect.length; i++ )
			{
				_A = _oneVect[ i ];
				_B = _oneVect[ ( i + 1 ) % _oneVect.length ];
				
				for ( var j:int = 0; j < _twoVect.length; j++ )
				{
					if ( _linesFound[ j ] == true ) // early out. Don't push the same line multiple times
					{
						continue;
					}
					_C = _twoVect[ j ];
					_D = _twoVect[ ( j + 1 ) % _twoVect.length ];
					
					if ( Maths.lineLineIntersectCheck( _A, _B, _C, _D ) )
					{
						_resultArray.push( _C.clone() );
						_resultArray.push( _D.clone() );
						_linesFound[ j ] = true;
					}
				}
			}
			
			return _resultArray;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			_A = _B = _C = _D = null;
			
			if ( _linesFound != null )
			{
				_linesFound.length = 0;
				_linesFound = null;
			}
			
			if ( _oneClipBmpData != null )
			{
				_oneClipBmpData.dispose();
				_oneClipBmpData = null;
			}
			
			if ( _oneOffset != null )
			{
				_oneOffset = null;
			}
			
			_onePoint = null;
			_oneRectangle = null;
			
			if ( _oneVect != null )
			{
				_oneVect.length = 0;
				_oneVect = null;
			}
			
			if ( _twoClipBmpData != null )
			{
				_twoClipBmpData.dispose();
				_twoClipBmpData = null;
			}
			
			_twoOffset = null;
			_twoPoint = null;
			
			_twoRectangle = null;
			
			if ( _twoVect != null )
			{
				_twoVect.length = 0;
				_twoVect = null;
			}
			
			isConstructed = false;
		}
		
	}

}