package org.xangarath.engine.world.astar 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.util.PriorityQueue;
	import org.xangarath.engine.world.TerrainManager;
	/**
	 * Performs an A* Search
	 * @author Jeff Cochran
	 */
	public class AStarSearcher implements IDestructable
	{
		private static const TAG:String = "AStarSearcher";
		private var open:PriorityQueue;
		private var closed:Array;
		private static const HEURISTIC_WEIGHT_DEFAULT:Number = 10.0 / 1000.0;
		private static const MAX_SEARCH_DEPTH:int = 2000;
		private var hW:Number = HEURISTIC_WEIGHT_DEFAULT;
		private var comparator:AStarComparator;
		private var createdNodes:Dictionary;
		private var neighbors:Vector.<Point> = new Vector.<Point>(8);
		private var gridUnitSize:Number;
		private var nodesCreated:int;
		private var isConstructed:Boolean = true;
		public function AStarSearcher( unitSize:Number ) 
		{
			comparator = new AStarComparator();
			open = new PriorityQueue( comparator );
			closed = new Array();
			neighbors[0] = new Point( -0.5, -0.5 );
			neighbors[1] = new Point(    0, -0.5 );
			neighbors[2] = new Point(  0.5, -0.5 );
			neighbors[3] = new Point(  0.5, 0    );
			neighbors[4] = new Point(  0.5, 0.5  );
			neighbors[5] = new Point(    0, 0.5  );
			neighbors[6] = new Point( -0.5, 0.5  );
			neighbors[7] = new Point( -0.5, 0    );
			AStarNode.D = 10.0;
			gridUnitSize = unitSize;
		}
		
		private function lookupNode( x:Number, y:Number ):AStarNode
		{
			if ( ( createdNodes[ "" + x + "a" + y  ] != null ) )
				return createdNodes[ "" + x + "a" + y ] as AStarNode;
				
			return null;
		}
		
		private function rememberNode( node:AStarNode ):void
		{
			createdNodes[ "" + node.pt.x + "a" + node.pt.y ] = node;
			nodesCreated++;
		}
		
		private function isValidGeneralizedMove( from:Point, to:Point, halfSize:Point, tm:TerrainManager, badTerrainMask:uint ):Boolean
		{
			var aabb:Rectangle;
			
			if ( !tm.pointInMap( to.x, to.y ) )
				return false;
				
			aabb = new Rectangle( to.x - halfSize.x, to.y - halfSize.y, 2 * halfSize.x, 2 * halfSize.y );
				
			var terr:uint = tm.getTerrainForRectangle( aabb );
			if ( ( terr & badTerrainMask ) != 0 )
				return false;
			
			return true;
		}
		
		private function isValidMove( from:Point, neighborIndex:uint, halfSize:Point, tm:TerrainManager, badTerrainMask:uint ):Boolean
		{
			var toX:Number = from.x + (neighbors[neighborIndex].x * gridUnitSize);
			var toY:Number = from.y + (neighbors[neighborIndex].y * gridUnitSize);
			var toAabb:Rectangle;
			var fromAabb:Rectangle;
			
			if ( !tm.pointInMap( toX, toY ) )
				return false;
				
			fromAabb = new Rectangle( from.x - halfSize.x, from.y - halfSize.y, 2 * halfSize.x, 2 * halfSize.y );
				
			toAabb = new Rectangle( toX - halfSize.x, toY - halfSize.y, 2 * halfSize.x, 2 * halfSize.y );
				
			//var terr:uint = tm.getTerrainForRectangle( toAabb );
			var terr:uint = tm.getTerrainForAABBMove( fromAabb, toAabb );
			if ( ( terr & badTerrainMask ) != 0 )
				return false;
			
			return true;
		}
		
		private function findNearestNodePoint( source:Point, tm:TerrainManager, badTerrainMask:uint ):Point
		{
			var res:Point = new Point();
			res.x = Math.round(source.x / (gridUnitSize * 0.5) ) * (gridUnitSize * 0.5);
			res.y = Math.round(source.y / (gridUnitSize * 0.5) ) * (gridUnitSize * 0.5);
			return res;
		}
		
		public function findPath( from:Point, to:Point, size:Point, tm:TerrainManager, badTerrainMask:uint ):Array
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var i:int;
			var j:int;
			var neighbor:AStarNode;
			var current:AStarNode;
			var tmp:AStarNode;
			var openIdx:int;
			var closedIdx:int;
			var terr:uint;
			var entries:Dictionary = new Dictionary();
			var neighborX:Number;
			var neighborY:Number;
			var cost:Number;
			var currentSearchDepth:int = 0;
			var halfSize:Point = new Point();
			halfSize.x = ((Math.ceil( (size.x / gridUnitSize) ) * gridUnitSize) / 2.0) - 0.5;
			halfSize.y = ((Math.ceil( size.y / gridUnitSize ) * gridUnitSize) / 2.0) - 0.5;
			if ( ( from.x == to.x ) && ( from.y == to.y ) )
				return null;
				
			open.clear();
			closed.splice( 0, closed.length );
			createdNodes = new Dictionary();
			nodesCreated = 0;
			
			var toPrime:Point = findNearestNodePoint( to, tm, badTerrainMask );
			var fromPrime:Point = findNearestNodePoint( from, tm, badTerrainMask );

			var goal:AStarNode = new AStarNode( toPrime, hW );
			var start:AStarNode = new AStarNode( fromPrime, hW );
			
			comparator.goal = goal;
			
			start.g = 0.0;
			open.enqueue(start);
			
			while (open.peekHead() != goal)
			{
				if (open.length == 0)
				{
					DebugLog.getSingleton().w( TAG, "No path exists.");
					return null;
				}
				
				//trace("====== Head is " + (open.peekHead().pt.x / gridUnitSize) + ", " + (open.peekHead().pt.y / gridUnitSize));
				current = open.dequeue() as AStarNode;
				if ( ++currentSearchDepth >= MAX_SEARCH_DEPTH )
				{
					// give up
					DebugLog.getSingleton().w( TAG, "Ran out of search time");
					return null;
				}
				closed.push(current);
				openIdx = closedIdx = -1;
				
				for (i = 0; i < 8; i++)
				{
					neighborX = current.pt.x + (neighbors[i].x * gridUnitSize);
					neighborY = current.pt.y + (neighbors[i].y * gridUnitSize);
					
					if ( !isValidMove( current.pt, i, halfSize, tm, badTerrainMask ) )
						continue;
						
					//trace("testing neighbor " + (neighborX / gridUnitSize) + ", " + (neighborY / gridUnitSize) );
						
					cost = current.g + ( ((neighbors[i].x != 0.0) && (neighbors[i].y != 0.0)) ? 14.0 : 10.0 );
					tmp = lookupNode( neighborX, neighborY );
					if ( tmp != null )
					{
						neighbor = tmp;
						if ( open.containsInstance( neighbor ) && ( cost < neighbor.g ) )
						{
							open.removeInstance( neighbor );
							neighbor.g = cost;
							neighbor.parent = current;
							open.enqueue( neighbor );
						}
						else
						{
							j = closed.indexOf( neighbor );
							if ( ( j >= 0 ) && ( cost < neighbor.g ) )
							{
								neighbor.parent = current;
								neighbor.g = cost;
								closed.splice( j, 1 );
								open.enqueue( neighbor );
							}
						}
					}
					else
					{
						if ( ( neighborX == goal.pt.x ) && ( neighborY == goal.pt.y ) )
							neighbor = goal;
						else
							neighbor = new AStarNode( new Point( neighborX, neighborY ), hW );
						neighbor.g = cost;
						neighbor.parent = current;
						rememberNode( neighbor );
						open.enqueue( neighbor );
					}
				}
			}
			
			tmp = goal;
			var res:Array = new Array();
			while (tmp != null)
			{
				//tm.getPointForTextureIndex( tmp.pt, true );
				res.push( tmp.pt.clone() );
				tmp = tmp.parent;
			}
			res.reverse();
			//trace("Done.\nNodes created: " + nodesCreated);
			//trace("found path: " + res);
			return res;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			closed.length = 0;
			closed = null;
			
			comparator.destroy();
			comparator = null;
			
			createdNodes = null;
			
			neighbors.length = 0;
			neighbors = null;
			
			open.destroy();
			open = null;
			
			isConstructed = false;
		}
	}
}