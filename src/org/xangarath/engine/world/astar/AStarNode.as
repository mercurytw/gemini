package org.xangarath.engine.world.astar 
{
	import flash.geom.Point;
	/**
	 * A node in an A* graph
	 * @author Jeff Cochran
	 */
	public class AStarNode 
	{
		public var pt:Point;
		public var parent:AStarNode = null;
		public var g:Number = -1;
		private var _h:Number = -1;
		private var hW:Number;
		public static var D:Number;
		// terrain
		public function AStarNode( pnt:Point, hWeight:Number ) 
		{
			pt = pnt;
			hW = hWeight
		}
		
		public function f( goal:AStarNode ):Number
		{
			if ( _h == -1 )
				return h( goal ) + g;
			else
				return _h + g;
		}
		
		public function h( goal:AStarNode ):Number
		{
			if ( _h != -1 )
				return _h;
			
			_h = (Math.abs(pt.x - goal.pt.x) + Math.abs(pt.y - goal.pt.y)) * AStarNode.D;
			_h *= 1.0 + hW;
			return _h;
		}
	}

}