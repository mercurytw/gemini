package org.xangarath.engine.world.astar 
{
	import flash.geom.Point;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IComparator;
	import org.xangarath.engine.interfaces.IDestructable;
	
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class AStarComparator implements IComparator, IDestructable 
	{
		private static const TAG:String = "AStarComparator";
		public var goal:AStarNode;
		private var isConstructed:Boolean = true;
		public function AStarComparator() 
		{
			
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IComparator */
		
		public function getComparableClass():Class 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return AStarNode;
		}
		
		public function compare( lhs:Object, rhs:Object ):int 
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return (lhs as AStarNode).f( goal ) - (rhs as AStarNode).f( goal );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}	
			
			goal = null;
			
			isConstructed = false;
		}
		
	}

}