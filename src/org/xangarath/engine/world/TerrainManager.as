package org.xangarath.engine.world 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.geom.Polygon;
	import org.xangarath.engine.interfaces.ITerrainInterpreter;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.util.Pair;
	
	/**
	 * Manages tracking terrain types and applying 
	 * terrain effects to actors
	 * @author Jeffrey Cochran
	 */
	public class TerrainManager
	{
		private static const TAG:String = "TerrainManager";
		
		public static const USER_TERRAIN_SHIFT:uint = 3;
		public static const TERRAIN_TYPE_NONE:uint = (0x1 << 0);
		public static const TERRAIN_TYPE_UNCROSSABLE:uint = (0x1 << 1);
		public static const TERRAIN_TYPE_UNKNOWN:uint = (0x1 << 2);
		
		public static const SEPARATING_AXIS_THEOREM:uint = 0;
		
		private var map:Array;
		private var tileSize:Point;
		private var mapSize:Point;
		private var terrainData:Dictionary;
		private var isInitialized:Boolean = false;
		private var interp:ITerrainInterpreter;
		public function TerrainManager( interpreter:ITerrainInterpreter ) 
		{
			interp = interpreter;
		}
		
		public function init( map:Array, mapSize:Point, tileSize:Point, terrainData:Dictionary ):void
		{
			if ( isInitialized )
			{
				
			}
			
			this.map = map;
			this.tileSize = tileSize;
			this.mapSize = mapSize;
			this.terrainData = terrainData;
		
			// TODO: Convert terrain type strings to uint enumerations for fast lookup
			
			isInitialized = true;
		}
		
		private function getTerrainType( terrain:String ):uint
		{
			switch ( terrain.toLowerCase() )
			{
				case "none":
					return TerrainManager.TERRAIN_TYPE_NONE;
				case "uncrossable":
					return TerrainManager.TERRAIN_TYPE_UNCROSSABLE;
				default:
					return interp.getTerrainType( terrain );
			}
		}

		public function coordInMap( x:int, y:int ):Boolean
		{
			return ( ( 0 <= x ) && (x < mapSize.x) &&
					 ( 0 <= y ) && (y < mapSize.y));
		}
		
		public function pointInMap( x:Number, y:Number ):Boolean
		{
			return coordInMap( x / tileSize.x,  y / tileSize.y );
		}
		
		public function getTextureIndexForWorldPoint( pt:Point ):Point
		{
			return new Point( Math.floor(Maths.clamp(0, mapSize.x - 1, pt.x / tileSize.x)), 
							  Math.floor(Maths.clamp(0, mapSize.y - 1, pt.y / tileSize.y)) );
		}
		
		public function getTextureIndexForWorldCoordinate( x:Number, y:Number , result:Point = null ):Point
		{
			if ( result == null )
			{
				result = new Point();
			}
			result.setTo( Math.floor(Maths.clamp(0, mapSize.x - 1, x / tileSize.x)), 
						  Math.floor(Maths.clamp(0, mapSize.y - 1, y / tileSize.y)) );
			return result;
		}
		
		public function getWorldPointForTextureIndex( pt:Point, inPlace:Boolean ):Point
		{
			if (inPlace)
			{
				pt.x = (pt.x * tileSize.x) + (tileSize.x / 2.);
				pt.y = (pt.y * tileSize.y) + (tileSize.y / 2.);
				return pt;
			}
			return new Point( (pt.x * tileSize.x) + (tileSize.x / 2.), (pt.y * tileSize.y) + (tileSize.y / 2.) );
		}
		
		public function getTerrainForPoint( pt:Point ):uint
		{
			var xIdx:Number;
			var yIdx:Number;
			var result:uint = 0;
			var leftAndRight:Boolean = false;
			if ( !isInitialized )
			{
				return TERRAIN_TYPE_UNKNOWN;
			}
			
			// clamp
			
			xIdx = Maths.clamp(0, mapSize.x - 1, pt.x / tileSize.x);
			yIdx = Maths.clamp(0, mapSize.y - 1, pt.y / tileSize.y);
			
			if ( (xIdx - Math.floor(xIdx)) == 0 )
			{
				leftAndRight = true;
			}
			else
			{
				xIdx = Math.floor( xIdx );
			}
			
			if ( ( yIdx - Math.floor( yIdx ) ) == 0 )
			{
				if ( leftAndRight )
				{
					result |= getTerrainAtTerrainIndex( xIdx - 1.0, yIdx - 1.0 );
					result |= getTerrainAtTerrainIndex( xIdx, yIdx - 1.0 );
					result |= getTerrainAtTerrainIndex( xIdx - 1.0, yIdx );
					result |= getTerrainAtTerrainIndex( xIdx, yIdx );
					return result;
				}
				else
				{
					result |= getTerrainAtTerrainIndex( xIdx, yIdx - 1.0 );
					result |= getTerrainAtTerrainIndex( xIdx, yIdx );
					return result;
				}
			}
			else
			{
				yIdx = Math.floor( yIdx );
				
				if ( leftAndRight )
				{
					result |= getTerrainAtTerrainIndex( xIdx - 1.0, yIdx );
					result |= getTerrainAtTerrainIndex( xIdx, yIdx );
					return result;
				}
				else
				{
					return getTerrainAtTerrainIndex( xIdx, yIdx );
				}
			}
		}
		
		// TODO: Update me to match getTerrainForPoint
		public function getTerrainForCoord( x:int, y:int ):uint
		{
			if ( !isInitialized )
			{
				return TERRAIN_TYPE_UNKNOWN;
			}
			
			// clamp
			
			var xIdx:int;
			var yIdx:int;
			
			xIdx = Maths.clamp(0, mapSize.x - 1, x / tileSize.x);
			yIdx = Maths.clamp(0, mapSize.y - 1, y / tileSize.y);
			
			var dat:String = terrainData[ map[yIdx][xIdx] ];
			
			if (dat == null)
			{
				return TERRAIN_TYPE_NONE;
			}
			
			return getTerrainType(dat);
		}
		
		public function getTerrainAtTerrainIndex( x:int, y:int ):uint
		{
			if ( !isInitialized )
			{
				return TERRAIN_TYPE_UNKNOWN;
			}
			
			x = Maths.clamp( 0, mapSize.x - 1, x );
			y = Maths.clamp( 0, mapSize.y - 1, y );
			
			var dat:String = terrainData[ map[y][x] ];
			
			if (dat == null)
			{
				return TERRAIN_TYPE_NONE;
			}
			
			return getTerrainType(dat);
		}
		
		// TODO: FIX ME! What if the rectangle is big?
		public function getTerrainForRectangle( r:Rectangle ):uint
		{
			return getTerrainForPoint( r.topLeft ) | 
				   getTerrainForCoord( r.right, r.top ) |
				   getTerrainForCoord( r.left, r.bottom ) |
				   getTerrainForPoint( r.bottomRight );
		}
		
		private function stretchPointsHelper( a:Point, b:Point ):void
		{
			if ( a.x < b.x )
			{
				if ( ( a.x - Math.floor( a.x ) ) == 0.0 )
				{
					a.x -= 0.5;
				}
				if (  ( b.x - Math.floor( b.x ) ) == 0.0 )
				{
					b.x += 0.5;
				}
			}
			else
			{
				if ( ( b.x - Math.floor( b.x ) ) == 0.0 )
				{
					b.x -= 0.5;
				}
				if (  ( a.x - Math.floor( a.x ) ) == 0.0 )
				{
					a.x += 0.5;
				}
			}
			
			if ( a.y < b.y )
			{
				if ( ( a.y - Math.floor( a.y ) ) == 0.0 )
				{
					a.y -= 0.5;
				}
				if (  ( b.y - Math.floor( b.y ) ) == 0.0 )
				{
					b.y += 0.5;
				}
			}
			else
			{
				if ( ( b.y - Math.floor( b.y ) ) == 0.0 )
				{
					b.y -= 0.5;
				}
				if (  ( a.y - Math.floor( a.y ) ) == 0.0 )
				{
					a.y += 0.5;
				}
			}
		}
		
		private var tempPointA:Point = new Point();
		private var tempPointB:Point = new Point();
		public function getTerrainForLineSegment( a:Point, b:Point ):uint
		{
			var result:uint = 0;
			
			if ( !isInitialized || !pointInMap( a.x, a.y ) || !pointInMap( b.x, b.y ) )
			{
				return TERRAIN_TYPE_UNKNOWN;
			}
			
			if ( a.x == b.x ) // vertical
			{
				if ( a.y > b.y )
				{
					tempPointA.copyFrom(b);
					tempPointB.copyFrom(a);
				}
				else
				{
					tempPointA.copyFrom(a);
					tempPointB.copyFrom(b);
				}
				
				while ( tempPointA.y < tempPointB.y )
				{
					result |= getTerrainForPoint( tempPointA );
					if ( ( tempPointA.y + tileSize.y ) >= tempPointB.y )
					{
						result |= getTerrainForPoint( tempPointB );
					}
					tempPointA.y += tileSize.y;
				}
				
				return result;
			}
			
			if ( a.y == b.y ) // horizontal
			{
				if ( a.x > b.x )
				{
					tempPointA.copyFrom(b);
					tempPointB.copyFrom(a);
				}
				else
				{
					tempPointA.copyFrom(a);
					tempPointB.copyFrom(b);
				}
				
				while ( tempPointA.x < tempPointB.x )
				{
					result |= getTerrainForPoint( tempPointA );
					if ( ( tempPointA.x + tileSize.x ) >= tempPointB.x )
					{
						result |= getTerrainForPoint( tempPointB );
					}
					tempPointA.x += tileSize.x;
				}
				
				return result;
			}
			
			tempPointA.copyFrom(a);
			tempPointB.copyFrom(b);
			
			tempPointA.x /= tileSize.x;
			tempPointA.y /= tileSize.y;
			tempPointB.x /= tileSize.x;
			tempPointB.y /= tileSize.y;
			
			stretchPointsHelper( tempPointA, tempPointB );
			var xStart:Number;
			var yStart:Number;
			var xStop:Number;
			var yStop:Number;
			var tempRect:Rectangle = new Rectangle();
			
			if ( tempPointA.x > tempPointB.x )
			{
				xStart = tempPointB.x;
				xStop = tempPointA.x;
			}
			else
			{
				xStart = tempPointA.x;
				xStop = tempPointB.x;
			}
			
			if (tempPointA.y > tempPointB.y )
			{
				yStart = tempPointB.y;
				yStop = tempPointA.y;
			}
			else
			{
				yStart = tempPointA.y;
				yStop = tempPointB.y;
			}
			
			for ( var i:Number = xStart; i <= xStop; ((i < xStop) && (i + 1 > xStop)) ? i = xStop : i++ )
			{
				for ( var j:Number = yStart; j <= yStop; ((j < yStop) && (j + 1 > yStop)) ? j = yStop : j++ )
				{
					tempRect.setTo( Math.floor(i), Math.floor(j), 1, 1 );
					if ( Maths.lineRectangleIntersectCheck( tempPointA, tempPointB, tempRect ) )
					{
						result |= getTerrainAtTerrainIndex( Math.floor(i), Math.floor(j) );
					}
				}
			}
			
			return result;
		}
		
		
		private var getTerrAABBMoveResult:Polygon = new Polygon( 6 );
		public function getTerrainForAABBMove( aabbBefore:Rectangle, aabbAfter:Rectangle ):uint
		{
			var result:uint = 0;
			/*
			result |= getTerrainForRectangle( aabbBefore );
			result |= getTerrainForRectangle( aabbAfter );
			result |= getTerrainForLineSegment( aabbBefore.topLeft, aabbAfter.topLeft );
			result |= getTerrainForLineSegment( new Point( aabbBefore.left, aabbBefore.bottom ), new Point( aabbAfter.left, aabbAfter.bottom ) );
			result |= getTerrainForLineSegment( aabbBefore.bottomRight, aabbAfter.bottomRight );
			result |= getTerrainForLineSegment( new Point( aabbBefore.right, aabbBefore.top ), new Point( aabbAfter.right, aabbAfter.top ) );
			*/
			Maths.calculateAABBSweptVolume( aabbBefore, aabbAfter, getTerrAABBMoveResult );
			result = getTerrainForPolygon( getTerrAABBMoveResult, SEPARATING_AXIS_THEOREM );
			return result;
		}
				
		private var polyQueryEnclosingRect:Rectangle = new Rectangle();
		private var polyQueryTestRect:Rectangle = new Rectangle();
		private var polyQueryRectTlc:Point = new Point();
		private var polyQueryRectBrc:Point = new Point();
		/**
		 * Calculates the terrain for the given closed, convex polygon
		 * @param	poly
		 * @return The terrain mask
		 */
		public function getTerrainForPolygon( poly:Polygon, algorithm:uint = SEPARATING_AXIS_THEOREM ):uint
		{
			ErrorUtil.dbgAssertNonNull( TAG, poly );
			ErrorUtil.dbgAssert( TAG, poly.length > 2, "Polygon must be closed" );
			ErrorUtil.dbgAssert( TAG, isInitialized, "Must be initialized" );
			
			if ( !isInitialized )
			{
				return TERRAIN_TYPE_UNKNOWN;
			}
			
			var result:uint = 0;
			
			
			var minX:Number = Infinity;
			var maxX:Number = -Infinity;
			var minY:Number = Infinity;
			var maxY:Number = -Infinity;
			
			var polyLength:int = poly.length;
			var i:int;
			for ( i = 0; i < polyLength; i++ )
			{
				var pt:Point = poly.getPointAt( i );
				
				if ( pt.x < minX )
				{
					minX = pt.x;
				}
				
				if ( pt.x > maxX )
				{
					maxX = pt.x;
				}
				
				if ( pt.y < minY )
				{
					minY = pt.y;
				}
				
				if ( pt.y > maxY )
				{
					maxY = pt.y;
				}
			}
			
			getTextureIndexForWorldCoordinate( minX, minY, polyQueryRectTlc );
			getTextureIndexForWorldCoordinate( maxX, maxY, polyQueryRectBrc );
			
			polyQueryEnclosingRect.setTo( polyQueryRectTlc.x, polyQueryRectTlc.y, polyQueryRectBrc.x - polyQueryRectTlc.x, polyQueryRectBrc.y - polyQueryRectTlc.y );
			
			polyQueryTestRect.width = tileSize.x;
			polyQueryTestRect.height = tileSize.y;
			
			
			if ( algorithm == SEPARATING_AXIS_THEOREM )
			{
				var y:int;
				var x:int;
				var nTested:uint = 0;
				var nCheapHits:uint = 0;
				var nExpensiveHits:uint = 0;
				var top:int = polyQueryEnclosingRect.top;
				var bottom:int = polyQueryEnclosingRect.bottom;
				var left:int = polyQueryEnclosingRect.left;
				var right:int = polyQueryEnclosingRect.right;
				for ( y = top; y <= bottom; y++ )
				{
					for ( x = left; x <= right; x++ )
					{
						polyQueryTestRect.x = x * tileSize.x;
						polyQueryTestRect.y = y * tileSize.y;
						
						if ( Maths.checkPointInPolygon( poly, polyQueryTestRect.left, polyQueryTestRect.top ) ||
							 Maths.checkPointInPolygon( poly, polyQueryTestRect.left, polyQueryTestRect.bottom ) ||
							 Maths.checkPointInPolygon( poly, polyQueryTestRect.right, polyQueryTestRect.bottom ) ||
							 Maths.checkPointInPolygon( poly, polyQueryTestRect.right, polyQueryTestRect.top ) )
						{
							result |= getTerrainAtTerrainIndex( x, y );
							//nCheapHits++;
						}
						else if ( Maths.polygonRectangleIntersectCheck( poly, polyQueryTestRect ) )
						{
							result |= getTerrainAtTerrainIndex( x, y );
							//nExpensiveHits++;
						}
						//nTested++;
					}
				}
				//DebugLog.getSingleton().d( TAG, "Done testing points. Hits: " + (nCheapHits + nExpensiveHits) + ", Cheap: " + nCheapHits + " expensive: " + nExpensiveHits + " Tested: " + nTested );
			}
			else
			{
				ErrorUtil.dbgAssert( TAG, false, "Requested unknown algorithm!" );
				return result;
			}
			
			return result;
		}
	}

}