package org.xangarath.engine.tiled 
{
	import flash.concurrent.Mutex;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.InteractiveObject;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.system.System;
	import flash.utils.Dictionary;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.ResourceManager;
	import org.xangarath.gemini.GeminiData;
	/**
	 * Represents a Tileset object in a Tiled map
	 * @author Jeffrey Cochran
	 */
	public class TiledTileset
	{
		public static const TAG:String = "TiledTileset";
		public var name:String;
		public var image:Bitmap;
		public var tileheight:int;
		public var tilewidth:int;
		public var imagewidth:int;
		public var imageheight:int;
		public var width:int;
		public var height:int;
		public var tiles:Dictionary;
		private var sampler:Rectangle;
		public var imagename:String;
		public var imagefolder:String;
		public var tileproperties:Object;
		public var numberOfTiles:int;
		private var __imageIsLoaded:Boolean = false;
		public function TiledTileset() 
		{
			tiles = new Dictionary();
			sampler = new Rectangle();
		}
		
		public function buildTlut( tlut:Dictionary, firstgid:int ):void
		{
			if (tileproperties == null)
			{
				return;
			}
			
			for ( var i:int = 0; i < numberOfTiles; i++)
			{	
				if ((tileproperties[ "" + i ] != null) && (tileproperties[ "" + i ][ "type" ] != null))
				{
					tlut[i + firstgid] = (tileproperties[ "" + i ][ "type" ] as String);
				}
			}
		}
		
		public function writeTileByGID( tid:int, dest:BitmapData, where:Point ):void
		{
			ErrorUtil.dbgAssert( TAG, image != null, "writeTileByGID called before image was loaded!" );
			var idx:int = tid;
		
			var x:int = ( idx % width ) * tilewidth;
			var y:int = ( Math.floor( idx / width ) as int ) * tileheight;
			sampler.x = x;
			sampler.y = y;
			sampler.width = tilewidth;
			sampler.height = tileheight;
			
			dest.copyPixels( image.bitmapData, sampler, where );
		}
		
		private var zerozero:Point = new Point();
		/*
		public function getTileByGID( gid:int ):BitmapData
		{
			if ( ( gid < firstgid ) || ( gid > lastgid ) )
			{
				trace( TAG + " requested invalid tile " );
				return null;
			}
			
			var idx:int = gid - firstgid;
			
			if ( tiles[ idx ] != null )
			{
				return tiles[ idx ];
			}
		
			var x:int = ( idx % height ) * tilewidth;
			var y:int = ( Math.floor( idx / height ) as int ) * tileheight;
			sampler.x = x;
			sampler.y = y;
			sampler.width = tilewidth;
			sampler.height = tileheight;
			
			var bmpData:BitmapData = new BitmapData( tilewidth, tileheight );
			
			bmpData.copyPixels( image.bitmapData, sampler, zerozero );
			
			if ( tiles[ idx ] == null )
			{
				tiles[ idx ] = bmpData;
			}
			return bmpData;
		}
		*/
		
		public static function getTilesetFromJson( jsobj:Object ):TiledTileset
		{
			if ( jsobj == null )
			{
				throw new SyntaxError( TAG + " invalid json " );
			}
			var ts:TiledTileset = new TiledTileset();
			ts.name = Argonaut.getAndCheck( TAG, jsobj, "name" );
			ts.tileheight = Argonaut.getAndCheckI( TAG, jsobj, "tileheight" );
			ts.tilewidth = Argonaut.getAndCheckI( TAG, jsobj, "tilewidth" );
			ts.imagewidth = Argonaut.getAndCheckI( TAG, jsobj, "imagewidth" );
			ts.imageheight = Argonaut.getAndCheckI( TAG, jsobj, "imageheight" );
			ts.tileproperties = jsobj[ "tileproperties" ];
			ts.width = ts.imagewidth / ts.tilewidth;
			ts.height = ts.imageheight / ts.tileheight;
			var loc:String = Argonaut.getAndCheck( TAG, jsobj, "image" );
			var arr:Array = loc.split( "\/" );
			loc = arr[ arr.length - 1 ] as String;
			ts.imagename = loc;
			loc = arr[ arr.length - 2 ] as String;
			ts.imagefolder = loc;
			ts.numberOfTiles = ( ( ts.imagewidth / ts.tilewidth ) * ( ts.imageheight / ts.tileheight ) );
			return ts;
		}
		
		private static function buildProperties( tsx:XML ):Object
		{
			var props:Dictionary = new Dictionary();
			for each ( var tiledat:XML in tsx.child("tile"))
			{
				var tileprops:Dictionary = new Dictionary();
				for each ( var prop:XML in tiledat.properties.child("property") )
				{
					tileprops[ "" + prop.@name ] = "" + prop.@value; 
				}
				props[ "" + tiledat.@id ] = tileprops;
			}
			return props;
		}
		
		public static function getTilesetFromTSX( tsx:XML, folder:String ):TiledTileset
		{
			if ( tsx == null )
			{
				throw new SyntaxError( TAG + " invalid XML " );
			}
			var ts:TiledTileset = new TiledTileset();
			ts.name = tsx.@name;
			ts.tileheight = tsx.@tileheight;
			ts.tilewidth = tsx.@tilewidth;
			ts.imagewidth = tsx.image.@width;
			ts.imageheight = tsx.image.@height;
			ts.width = ts.imagewidth / ts.tilewidth;
			ts.height = ts.imageheight / ts.tileheight;
			ts.tileproperties = buildProperties( tsx );
			var loc:String = tsx.image.@source;
			var idx:int = loc.lastIndexOf("/");
			if (idx == -1)
			{
				ts.imagename = loc;
				ts.imagefolder = folder;
			}
			else
			{
				var arr:Array = loc.split( "\/" );
				loc = arr[ arr.length - 1 ] as String;
				ts.imagename = loc;
				loc = arr[ arr.length - 2 ] as String;
				ts.imagefolder = loc;
			}
			ts.numberOfTiles = ( ( ts.imagewidth / ts.tilewidth ) * ( ts.imageheight / ts.tileheight ) );
			return ts;
		}
		
		public function loadImage( resMgr:ResourceManager ):void
		{
			image = resMgr.getBitmap( imagename, false );
			ErrorUtil.dbgAssertNonNull( TAG, image );
		}
	}

}