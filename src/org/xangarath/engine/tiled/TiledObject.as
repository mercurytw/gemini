package org.xangarath.engine.tiled 
{
	import org.xangarath.engine.Argonaut;
	/**
	 * Represents a Tiled object description
	 * @author Jeffrey Cochran
	 */
	public dynamic class TiledObject 
	{
		private static const TAG:String = "TiledObject";
		public var name:String;
		public var type:String;
		public var properties:Object;
		public var x:Number;
		public var y:Number;
		public var width:Number;
		public var height:Number;
		public var visible:Boolean;
		public function TiledObject( json:Object ) 
		{
			name = Argonaut.getAndCheck( TAG, json, "name" );
			type = Argonaut.getAndCheck( TAG, json, "type" );
			x = Argonaut.getAndCheckN( TAG, json, "x" );
			y = Argonaut.getAndCheckN( TAG, json, "y" );
			height = Argonaut.getAndCheckN( TAG, json, "height" );
			width = Argonaut.getAndCheckN( TAG, json, "width" );
			visible = Argonaut.getAndCheckB( TAG, json, "visible" );
			properties = Argonaut.getAndCheck( TAG, json, "properties" );
			
			if ( json[ "gid" ] != null )
			{
				this.gid = Argonaut.getAndCheck( TAG, json, "gid" );
			}
		}
		
	}

}