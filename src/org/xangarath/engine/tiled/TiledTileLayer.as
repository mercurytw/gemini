package org.xangarath.engine.tiled 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.util.Pair;
	/**
	 * Represents a regular old tile layer from the Tiled map generator
	 * @author Jeffrey Cochran
	 */
	public class TiledTileLayer 
	{
		public static const TAG:String = "TiledTileset";
		public var name:String;
		public var height:int;
		public var width:int;
		public var data:Object;
		private var bmp:Bitmap = null;
		public function TiledTileLayer() 
		{
			
		}
		
		public function buildGIDMap():Array
		{
			var outer:Array = new Array(height);
			var i:int;
			
			for ( i = 0; i < height; i++ )
			{
				outer[i] = new Array(width);
			}
			
			for ( i = 0; data[ i ] != null; i++ )
			{
				var row:int = i / width;
				outer[row][i - (row * width)] = data[i];
			}
			
			return outer;
		}
		
		public function buildBmp( sets:Dictionary, tilewidth:int, tileheight:int ):Bitmap
		{
			if ( bmp != null )
			{
				return bmp;
			}
			
			var bmpData:BitmapData = new BitmapData( tilewidth * width, tileheight * height, true, 0x00000000 );
			
			var pt:Point = new Point();
			for ( var i:int = 0; data[ i ] != null; i++ )
			{
				if ( data[ i ] == 0 ) 
				{
					continue;
				}
				
				pt.x = (i % width) * tilewidth;
				pt.y = Math.floor( i / width ) * tileheight;

				var ts:TiledTileset = ErrorUtil.dbgSafeCast( TAG, (sets[ data[ i ] ] as Pair).first, TiledTileset );
				var firstgid:int = (sets[ data[ i ] ] as Pair).second as int;
				ts.writeTileByGID( data[ i ] - firstgid, bmpData, pt );
			}
			
			return new Bitmap( bmpData );
		}
		
		public static function getTileLayerFromJson( jsobj:Object ):TiledTileLayer
		{
			if ( jsobj == null )
			{
				throw new SyntaxError( TAG + " invalid json " );
			}
			var tl:TiledTileLayer = new TiledTileLayer();
			tl.name = Argonaut.getAndCheck( TAG, jsobj, "name" );
			tl.height = Argonaut.getAndCheckI( TAG, jsobj, "height" );
			tl.width = Argonaut.getAndCheckI( TAG, jsobj, "width" );
			tl.data = Argonaut.getAndCheck( TAG, jsobj, "data" );
			
			return tl;
		}
	}

}