package org.xangarath.engine.tiled 
{
	import flash.filesystem.File;
	import flash.system.System;
	import flash.utils.Dictionary;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.ResourceManager;
	import org.xangarath.engine.SharedData;
	/**
	 * Singleton 
	 * Keeps track of all the tiled tilesets which have been read so far
	 * @author Jeff Cochran
	 */
	public class TiledTilesetManager 
	{
		private static const TAG:String = "TiledTilesetManager";
		private static var singleton:TiledTilesetManager = null;
		private var tilesets:Dictionary;
		public function TiledTilesetManager() 
		{
			tilesets = new Dictionary();
		}
		
		public static function getSingleton():TiledTilesetManager
		{
			if ( singleton == null )
			{
				singleton = new TiledTilesetManager();
			}
			return singleton;
		}
		
		public function init( path:String, resMgr:ResourceManager ):void
		{
			var rootDir:File = File.applicationDirectory.resolvePath(path);
			ErrorUtil.dbgAssert( TAG, (rootDir != null) && rootDir.isDirectory, "unable to find tileset directory!" );
			
			var files:Array = rootDir.getDirectoryListing();
			for each (var entry:File in files)
			{
				if ( entry.type != ".tsx" )
				{
					continue;
				}
				
				var str:String = ResourceManager.loadFileAsString( path + entry.name, true );
				var tsx:XML = new XML(str);
				
				var tileset:TiledTileset = TiledTileset.getTilesetFromTSX( tsx, entry.parent.name );
				ErrorUtil.dbgAssert( TAG, tilesets[ tileset.name ] == null, "multiply defined tileset!" );
				tilesets[ tileset.name ] = tileset;
				tileset.loadImage( resMgr );
				System.disposeXML( tsx );
			}
		}
		
		public function addTileset( tileset:TiledTileset ):Boolean
		{
			if ( tilesets[ tileset.name ] != null )
			{
				DebugLog.getSingleton().w( TAG, "Tried to re-add tileset " + tileset.name );
				return false;
			}
			tilesets[ tileset.name ] = tileset;
			return true;
		}
		
		public function getTileset( name:String ):TiledTileset
		{
			return tilesets[ name ];
		}
	}

}