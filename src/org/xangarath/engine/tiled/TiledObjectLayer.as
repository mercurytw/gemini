package org.xangarath.engine.tiled 
{
	import org.xangarath.engine.Argonaut;
	/**
	 * Represents a collection of objects as parsed from a Tiled script
	 * @author Jeffrey Cochran
	 */
	public class TiledObjectLayer 
	{
		private static const TAG:String = "TiledObjectLayer";
		public var color:uint;
		public var name:String;
		public var opacity:Number;
		public var visible:Boolean;
		private var _objects:Object;
		private var objectsInflated:Array = null;
		public function TiledObjectLayer( json:Object ) 
		{
			name = Argonaut.getAndCheck( TAG, json, "name" );
			if ( json[ "color" ] != null )
			{
				color = 0xFF000000 + Argonaut.getAndCheckColorHex( TAG, json, "color" );
			}
			opacity = Argonaut.getAndCheckN( TAG, json, "opacity" );
			visible = Argonaut.getAndCheckB( TAG, json, "visible" );
			_objects = Argonaut.getAndCheck( TAG, json, "objects" );
		}
		
		public function get objects():Array
		{
			if ( objectsInflated != null )
			{
				return objectsInflated;
			}
			var objs:Array = new Array();
			
			for ( var i:int = 0; _objects[ i ] != null; i++ )
			{
				objs.push( new TiledObject( _objects[ i ] ) );
			}
			objectsInflated = objs;
			return objs;
		}
		
		public function deflate():void
		{
			objectsInflated = null;
		}
	}

}