package org.xangarath.engine 
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.AntiAliasType;
	import flash.text.TextFormatAlign;
	import flash.text.TextFormat;
	
	/**
	 * Represents an action the player can peform, along with a readiness bar
	 * @author Jeff Cochran
	 */
	public class ActionWidget extends Sprite 
	{
		private static const FULL_WIDTH:Number = 200.0;
		private static const FULL_HEIGHT:Number = 10.0;
		private var titleText:String;
		private var txtField:TextField;
		private var loadyBar:Sprite;
		private var color:uint;
		private var _width:Number = 1.0;
		public function ActionWidget(posX:Number, posY:Number, barColor:uint = 0x0000FF) 
		{
			super();
			x = posX;
			y = posY;
			color = barColor;
		}
		
		public function set title(s:String):void
		{
			titleText = s;
			txtField.text = s;
			txtField.width = txtField.textWidth + 5;
			txtField.height = txtField.textHeight + 5;
			loadyBar.x = txtField.width + 5;
		}
		
		public function get title():String
		{
			return titleText;
		}
		
		public function set barWidth(v:Number):void
		{
			_width = v;
			drawBar(v);
		}
		
		public function get barWidth():Number
		{
			return _width;
		}
		
		public function init():void
		{
			txtField = new TextField();
			txtField.embedFonts = true;
			txtField.antiAliasType = AntiAliasType.ADVANCED;
			txtField.selectable = false;
			txtField.defaultTextFormat = new TextFormat(ResourceManager.GEMINI, 12.0, 0x000000, null, null, null, null, null, TextFormatAlign.LEFT );
			txtField.text = "Test";
			txtField.width = stage.stageWidth;
			txtField.height = txtField.textHeight + 5;
			txtField.x = 0;
			txtField.y = 0;
			addChild(txtField);
			
			loadyBar = new Sprite();
			addChild(loadyBar);
			loadyBar.graphics.lineStyle(0);
			loadyBar.graphics.beginFill(color, 1.0);
			loadyBar.graphics.drawRect(0.0, 0.0, FULL_WIDTH, FULL_HEIGHT);
			loadyBar.graphics.endFill();
			loadyBar.x = txtField.width + 5;
			loadyBar.y = loadyBar.height / 4.0;
		}
		
		public function drawBar(barScale:Number):void
		{
			loadyBar.graphics.clear();
			loadyBar.graphics.lineStyle(0);
			loadyBar.graphics.beginFill(color, 1.0);
			loadyBar.graphics.drawRect(0.0, 0.0, FULL_WIDTH * barScale, FULL_HEIGHT);
			loadyBar.graphics.endFill();
		}
	}

}