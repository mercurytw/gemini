package org.xangarath.engine.math
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.geom.LineSegment;
	import org.xangarath.engine.geom.Polygon;
	import org.xangarath.engine.geom.Triangle;
	/**
	 * ...
	 * @author Jeffrey Cochran
	 */
	public class Maths 
	{
		private static const TAG:String = "Maths";
		private static const RAD_TO_DEG:Number = 180.0 / Math.PI;
		private static const DEG_TO_RAD:Number = Math.PI / 180.0;
		public static const SQRT_TWO:Number = Math.sqrt(2);
		private static var rand:Random = new Random(0);
		
		public function Maths() 
		{
			
		}
		
		public static function seed_random( seed:int ):void
		{
			rand = new Random( seed );
		}
		
		public static function randInt(lowerBound:int = int.MIN_VALUE, upperBound:int = int.MAX_VALUE):int
		{
			return rand.nextMinMax(lowerBound, upperBound);
		}
		
		public static function randNumber():Number
		{
			return rand.nextNumber();
		}
		
		public static function degToRad( deg:Number ):Number
		{
			return deg * DEG_TO_RAD;
		}
		
		public static function radToDeg( rad:Number ):Number
		{
			return rad * RAD_TO_DEG;
		}
		
		/**
		  * Returns the distance squared from point a to point b
		 * @param	a A point in 2D
		 * @param	b A point in 2D
		 * @return The distance squared between the two
		 */
		public static function distanceSqComponent( aX:Number, aY:Number, bX:Number, bY:Number ):Number
		{
			var dx:Number = bX - aX;
			var dy:Number = bY - aY;
			
			return ( dx * dx ) + ( dy * dy );
		}
		
		/**
		  * Returns the distance squared from point a to point b
		 * @param	a A point in 2D
		 * @param	b A point in 2D
		 * @return The distance squared between the two
		 */
		public static function distanceSq( a:Point, b:Point ):Number
		{
			return distanceSqComponent( a.x, a.y, b.x, b.y );
		}
		 
		/**
		 * Returns the distance from point a to point b
		 * @param	a A point in 2D
		 * @param	b A point in 2D
		 * @return The distance between the two
		 */
		public static function distance( a:Point, b:Point ):Number
		{
			return Math.sqrt( distanceSq( a, b ) );
		}
		
		/**
		 * Calculate the dot product of  lhs dot rhs
		 * @param	lhs
		 * @param	rhs
		 * @return The resulting dot product
		 */
		public static function dotProduct( lhs:Point, rhs:Point ):Number
		{
			return ( lhs.x * rhs.x ) + ( lhs.y * rhs.y );
		}
		
		/**
		 * Normalizes a point
		 * @param p The Point to normalize
		 * @return the normalized point
		 */
		public static function normalize( p:Point ):Point
		{
			var mag:Number = Math.sqrt( ( p.x * p.x ) + ( p.y * p.y ) );
			p.x /= mag;
			p.y /= mag;
			return p;
		}
		
		/**
		 * Rotate a vector in place
		 * @param	v The vector to rotate
		 * @param   theta The amount of rotation to apply to the vector in degrees
		 */
		public static function rotateVect( v:Point, theta:Number ):void
		{
			var oldx:Number = v.x;
			v.x = ( v.x * Math.cos( Maths.degToRad( theta ) ) ) - ( v.y * Math.sin( Maths.degToRad( theta ) ) );
			v.y = ( v.y * Math.cos( Maths.degToRad( theta ) ) ) + ( oldx * Math.sin( Maths.degToRad( theta ) ) );
		}
		
		/**
		 * Checks to see if the points A, B, and C are passed in counterclockwise order
		 * @param A The first Point
		 * @param B The second Point
		 * @param C The third point
		 * @return true if the A B and C are counterclockwise
		 */
		public static function ccwComp( Ax:Number, Ay:Number, Bx:Number, By:Number, Cx:Number, Cy:Number ):Boolean
		{
			return ( ( ( Cy - Ay ) * ( Bx - Ax ) ) > ( ( By - Ay ) * ( Cx - Ax ) ) );
		}
		
		/**
		 * Checks to see if the points A, B, and C are passed in counterclockwise order
		 * @param A The first Point
		 * @param B The second Point
		 * @param C The third point
		 * @return true if the A B and C are counterclockwise
		 */
		public static function ccw( A:Point, B:Point, C:Point ):Boolean
        {
			return ccwComp( A.x, A.y, B.x, B.y, C.x, C.y );
		}

		/**
		 * Checks to see if the lines AB and CD are intersecting. 
		 * @param	A The first point in line AB
		 * @param	B The second point in AB
		 * @param	C The first point in CD
		 * @param	D The second point in CD
		 * @return true if AB and CD intersect
		 */
        public static function lineLineIntersectCheck( A:Point, B:Point, C:Point, D:Point ):Boolean
        {
            return( ( ccw( A, C, D ) != ccw( B, C, D ) ) && ( ccw( A, B, C ) != ccw( A, B, D ) ) );
        }
		
		private static var glD:Point = new Point();
		private static var glE:Point = new Point();
		/**
		 * Checks to see if a line intersects with Rectangle rect
		 * @param	A the starting point of the line
		 * @param   B the endpoint of the line
		 * @param	rect The rectangle to check for intersection
		 * @return true if line and rect intersect
		 */
		public static function lineRectangleIntersectCheck( A:Point, B:Point, rect:Rectangle ):Boolean
		{
			glD.copyFrom( rect.topLeft );
			glE.setTo( rect.topLeft.x, rect.topLeft.y + rect.height );
			
			if ( lineLineIntersectCheck( A, B, glD, glE ) ) { return true; }
			
			glD.copyFrom( glE );
			glE.copyFrom( rect.bottomRight );
			
			if ( lineLineIntersectCheck( A, B, glD, glE ) ) { return true; }
			
			glD.copyFrom( glE );
			glE.setTo( rect.bottomRight.x, rect.bottomRight.y - rect.height );
			
			if ( lineLineIntersectCheck( A, B, glD, glE ) ) { return true; }
			
			glD.copyFrom( glE );
			glE.copyFrom( rect.topLeft );
			
			if ( lineLineIntersectCheck( A, B, glD, glE ) ) { return true; }
			
			return false;
		}
		
		/**
		 * Checks to see if Triangle triangle intersects with Rectangle rect
		 * @param	triangle The triangle to check for intersection
		 * @param	rect The rectangle to check for intersection
		 * @return true if triangle and rect intersect
		 */
		public static function triangleRectangleIntersectCheck( triangle:Triangle, rect:Rectangle ):Boolean
		{
			if ( rect.containsPoint( triangle.A ) ) // in case the rect contains the triangle
			{
				return true;
			}
			else if ( triangle.containsPoint( rect.topLeft ) ) // in case the triangle contains the rect
			{
				return true;
			}
			
			return ( lineRectangleIntersectCheck( triangle.A, triangle.B, rect ) || 
					 lineRectangleIntersectCheck( triangle.B, triangle.C, rect ) || 
				     lineRectangleIntersectCheck( triangle.C, triangle.A, rect ) );
		}
		
		/**
		 * Clamps a number to a range
		 * @param lower The lower bound to clamp to
		 * @param upper The upper bound to clamp to
		 * @param num The number to be clamped
		 * @return The clamped value
		 */
		public static function clamp( lower:Number, upper:Number, num:Number ):Number
		{
			var ret:Number = (lower > num) ? lower : num;
			return (ret > upper) ? upper : ret;
		}
		
		/**
		 * Calculate the normal to the line AB
		 * @param	a
		 * @param	b
		 * @return a vector pointing in the direction of the line's normal
		 */
		public static function calculateNormal( a:Point, b:Point, result:Point = null ):Point
		{
			if ( result == null )
			{
				result = new Point();
			}
			
			result.x = -( b.y - a.y );
			result.y =  ( b.x - a.x );
			result.normalize( 1.0 );
			
			return result;
		}
		
		/**
		 * Project vector v onto point "onto"
		 * @param	v   The vector to be projected
		 * @param	onto The vector to be projected onto
		 * @return The new projected vector
		 */
		public static function calculateVectorProjection( v:Point, onto:Point, result:Point = null ):Point
		{
			ErrorUtil.dbgAssertNonNull( TAG, v );
			ErrorUtil.dbgAssertNonNull( TAG, onto );
			
			if ( result == null )
			{
				result = new Point();
			}
			
			var scalar:Number = dotProduct( v, onto ) / dotProduct( onto, onto );
			result.setTo( onto.x * scalar, onto.y * scalar );
			
			return result;
		}
		
		/**
		 * A convenience function to turn a rectangle into a properly-formed polygon
		 * @param	r
		 * @param	poly
		 */
		public static function getRectangleAsPolygon( r:Rectangle, poly:Polygon ):void
		{
			ErrorUtil.dbgAssertNonNull( TAG, r );
			ErrorUtil.dbgAssertNonNull( TAG, poly );
			
			poly.length = 4;
			poly.getPointAt( 0 ).setTo( r.left, r.top );
			poly.getPointAt( 1 ).setTo( r.left, r.bottom );
			poly.getPointAt( 2 ).setTo( r.right, r.bottom ); 
			poly.getPointAt( 3 ).setTo( r.right, r.top );
		}
		
		private static var calcSwtpVolumeTmp:Point;
		private static var calcSweptVolumeTmpRect:Rectangle;
		private static var calcSweptVolmeSeg:LineSegment;
		/**
		 * Calculate swept volume for the displacement of an AABB
		 * @note this function will fail if the size of the AABB changes between start & stop
		 * @param start the starting point for the aabb
		 * @param stop the ending point for the aabb
		 * @return The collection of points representing the swept volume
		 **/
		public static function calculateAABBSweptVolume( start:Rectangle, stop:Rectangle, result:Polygon = null ):Polygon
		{
			var i:int;
			var j:int;
			var k:int;
			var pt:Point;
			
			if ( result == null )
			{
				result = new Polygon( 6 );
			}
			result.length = 0;
			
			ErrorUtil.dbgAssert( "Maths", (start.width == stop.width) && (start.height == stop.height), "AABB must not change in size during motion for a swept volume calculation!" );
			calcSwtpVolumeTmp = start.topLeft.subtract(stop.topLeft);
			if ( ( calcSwtpVolumeTmp.x == 0 ) && ( calcSwtpVolumeTmp.y == 0) )
			{
				result.length = 4;
				result.getPointAt( 0 ).setTo( start.topLeft.x, start.topLeft.y );
				result.getPointAt( 1 ).setTo( start.left, start.bottom );
				result.getPointAt( 2 ).setTo( start.bottomRight.x, start.bottomRight.y );
				result.getPointAt( 3 ).setTo( start.right, start.top );
				return result;
			}
			else if ( calcSwtpVolumeTmp.x == 0 )
			{
				if ( start.topLeft.y > stop.topLeft.y )
				{
					calcSweptVolumeTmpRect = stop;
					stop = start;
					start = calcSweptVolumeTmpRect;
				}
				
				// start must be above stop
				result.length = 4;
				result.getPointAt( 0 ).setTo( start.topLeft.x, start.topLeft.y );
				result.getPointAt( 1 ).setTo( start.left, stop.bottom );
				result.getPointAt( 2 ).setTo( stop.bottomRight.x, stop.bottomRight.y );
				result.getPointAt( 3 ).setTo( stop.right, start.top );
				return result;
			}
			else if ( calcSwtpVolumeTmp.y == 0 )
			{
				if ( start.topLeft.x > stop.topLeft.x )
				{
					calcSweptVolumeTmpRect = stop;
					stop = start;
					start = calcSweptVolumeTmpRect;
				}
				
				// start must be left of stop
				result.length = 4;
				result.getPointAt( 0 ).setTo( start.topLeft.x, start.topLeft.y );
				result.getPointAt( 1 ).setTo( start.left, start.bottom );
				result.getPointAt( 2 ).setTo( stop.bottomRight.x, stop.bottomRight.y );
				result.getPointAt( 3 ).setTo( stop.right, start.top );
				return result;
			}
			
			// We have motion in two axies
			
			if ( start.left > stop.left )
			{
				calcSweptVolumeTmpRect = stop;
				stop = start;
				start = calcSweptVolumeTmpRect;
			}
			
			if ( start.top < stop.top ) // start is top-left of stop
			{
				result.length = 6;
				result.getPointAt( 0 ).setTo( start.topLeft.x, start.topLeft.y );
				result.getPointAt( 1 ).setTo( start.left, start.bottom );
				result.getPointAt( 2 ).setTo( stop.left, stop.bottom );
				result.getPointAt( 3 ).setTo( stop.bottomRight.x, stop.bottomRight.y );
				result.getPointAt( 4 ).setTo( stop.right, stop.top );
				result.getPointAt( 5 ).setTo( start.right, start.top );
			}
			else // start is bottom-left of stop
			{
				result.length = 6;
				result.getPointAt( 0 ).setTo( start.topLeft.x, start.topLeft.y );
				result.getPointAt( 1 ).setTo( start.left, start.bottom );
				result.getPointAt( 2 ).setTo( start.right, start.bottom );
				result.getPointAt( 3 ).setTo( stop.bottomRight.x, stop.bottomRight.y );
				result.getPointAt( 4 ).setTo( stop.right, stop.top );
				result.getPointAt( 5 ).setTo( stop.left, stop.top );
			}
			
			return result;
		}
		
		/**
		 * Returns true if pt is in the given polygon
		 * @param	poly a concave polygon in counterclockwise winding order
		 * @param	pt
		 * @return True if the polygon contains the point
		 */
		public static function checkPointInPolygon( poly:Polygon, ptX:Number, ptY:Number ):Boolean
		{
			ErrorUtil.dbgAssertNonNull( TAG, poly );
			ErrorUtil.dbgAssert( TAG, poly.length > 0, "Polygon contained no points" );
			
			for ( var i:int = 0; i < poly.length; i++ )
			{
				var A:Point = poly.getPointAt( i );
				var B:Point = poly.getPointAt( ( ( i + 1 ) == poly.length ) ? 0 : i + 1 );
				
				// HEY! the reason this line returns false if ccw returns true is because the y-axis in our game is inverted,
				// so clockwiseness can be backwards
				if ( ccwComp( A.x, A.y, B.x, B.y, ptX, ptY ) )
				{
					return false;
				}
			}
			
			return true;
		}
		
		private static var polyRectIsctCheckVect:Polygon = new Polygon( 4 );
		/**
		 * Check to see if a convex polygon intersects with the given rectangle
		 * @param	poly The convex polygon to check for intersection. Must be passed in right-handed winding order
		 * @param	rect The rectangle to check for intersection
		 * @return true if the volumes intersect
		 * @note Results are undefined if the polygon is concave
		 */
		public static function polygonRectangleIntersectCheck( poly:Polygon, rect:Rectangle ):Boolean
		{
			ErrorUtil.dbgAssertNonNull( TAG, poly );
			ErrorUtil.dbgAssertNonNull( TAG, rect );
			ErrorUtil.dbgAssert( TAG, poly.length > 0, "Polygon contained no points" );
			// @TODO: Write a convexity test
			
			if ( polyRectIsctCheckVect.length == 0 )
			{
				polyRectIsctCheckVect.length = 4;
			}
			polyRectIsctCheckVect.getPointAt( 0 ).copyFrom( rect.topLeft );
			polyRectIsctCheckVect.getPointAt( 1 ).setTo( rect.left, rect.bottom );
			polyRectIsctCheckVect.getPointAt( 2 ).copyFrom( rect.bottomRight );
			polyRectIsctCheckVect.getPointAt( 3 ).setTo( rect.right, rect.top );
			
			return polygonPolygonIntersectCheck( poly, polyRectIsctCheckVect );
		}
		
		private static function delThisPtPrint( p:Point ):String
		{
			return "{ " + p.x + ", " + p.y + " }";
		}
		
		private static var ppIsctSignPt:Point = new Point();
		// returns true if pointing in the positive direction, false if negative
		private static function calcSignPPIntersect( ptA:Point, normal:Point, inQuest:Point ):Boolean
		{
			
			ppIsctSignPt.setTo( inQuest.x - ptA.x, inQuest.y - ptA.y );
			
			if ( !( ( ppIsctSignPt.x >= 0 ) && ( normal.x >= 0 ) ) && !( ( ppIsctSignPt.x < 0 ) && ( normal.x < 0 ) ) )
			{
				return false;
			}
			
			if ( !( ( ppIsctSignPt.y >= 0 ) && ( normal.y >= 0 ) ) && !( ( ppIsctSignPt.y < 0 ) && ( normal.y < 0 ) ) )
			{
				return false;
			}
			
			return true;
		}
		
		
		private static var ppIsctPtA:Point = new Point();
		private static var ppIsctPtB:Point = new Point();
		private static var ppIsctNormal:Point = new Point();
		private static var ppIsctTemp:Point = new Point();
		private static var ppIsctPtForCompare:Point = new Point();
		private static function polyPolyIntersectHelper( poly1:Polygon, poly2:Polygon ):Boolean
		{
			var i:int;
			var j:int;
			var min1:Number;
			var max1:Number;
			var min2:Number;
			var max2:Number;
			var dist:Number;
			
			for ( i = 0; i < poly1.length; i++ )
			{	
				min1 = Infinity;
				max1 = -Infinity;
				min2 = Infinity;
				max2 = -Infinity;
				dist = 0;
				
				ppIsctPtA.copyFrom( poly1.getPointAt( i ) );
				ppIsctPtB.copyFrom( poly1.getPointAt( ( i + 1 ) % poly1.length ) );
				calculateNormal( ppIsctPtA, ppIsctPtB, ppIsctNormal );
				
				calculateVectorProjection( ppIsctPtA, ppIsctNormal, ppIsctPtForCompare );
				
				//DebugLog.getSingleton().d( TAG, "> Point a: " + delThisPtPrint(ptA) + ", pt b: " + delThisPtPrint(ptB) + ", normal: " + delThisPtPrint(normal) );
				
				for ( j = 0; j < poly1.length; j++ )
				{
					calculateVectorProjection( poly1.getPointAt( j ), ppIsctNormal, ppIsctTemp );
					
					dist = distanceSq( ppIsctPtForCompare, ppIsctTemp );
					if ( !calcSignPPIntersect( ppIsctPtForCompare, ppIsctNormal, ppIsctTemp ) )
					{
						dist = -dist;
					}
					
					if ( dist < min1 )
					{
						min1 = dist;
						//DebugLog.getSingleton().d( TAG, ">> Found new min from poly1: " + delThisPtPrint(poly1[ j ]) );
					}
					
					if ( dist > max1 )
					{
						max1 = dist;
						//DebugLog.getSingleton().d( TAG, ">> Found new max from poly1: " + delThisPtPrint(poly1[ j ]) );
					}
				}
				
				for ( j = 0; j < poly2.length; j++ )
				{
					calculateVectorProjection( poly2.getPointAt( j ), ppIsctNormal, ppIsctTemp );
					
					dist = distanceSq( ppIsctPtForCompare, ppIsctTemp );
					if ( !calcSignPPIntersect( ppIsctPtForCompare, ppIsctNormal, ppIsctTemp ) )
					{
						dist = -dist;
					}
					
					if ( dist < min2 )
					{
						min2 = dist;
						//DebugLog.getSingleton().d( TAG, ">> Found new min from poly2: " + delThisPtPrint(poly2[ j ]) );
					}
					
					if ( dist > max2 )
					{
						max2 = dist;
						//DebugLog.getSingleton().d( TAG, ">> Found new max from poly2: " + delThisPtPrint(poly2[ j ]) );
					}
				}
				
				//DebugLog.getSingleton().d( TAG, ">> ask: ( " + max1 + " < " + min2 + " ) || ( " + max2 + " < " + min1 + " ) ");
				
				if ( ( max1 < min2 ) || ( max2 < min1 ) )
				{
					//DebugLog.getSingleton().d( TAG, ">> Found a separating axis!" );
					return false;
				}
				/*
				else
				{
					DebugLog.getSingleton().d( TAG, ">> Not a separating axis. Continue..." );
				}
				*/
			}
			
			return true;
		}
		
		/**
		 * Check to see if two convex polygons intersect with each other, using the separating axis theorem
		 * @param	poly1 A convex polygon in CCW winding order
		 * @param	poly2 A convex polygon in CCW winding order
		 * @return
		 */
		public static function polygonPolygonIntersectCheck( poly1:Polygon, poly2:Polygon ):Boolean
		{
			ErrorUtil.dbgAssertNonNull( TAG, poly1 );
			ErrorUtil.dbgAssertNonNull( TAG, poly2 );
			ErrorUtil.dbgAssert( TAG, ( poly1.length > 2 ) && ( poly2.length > 2 ), "Polygon must be closed" );

			// @TODO: Verify clockwiseness of the given polygons
			// Actually, I might not need to have the polygons be CCW??
			
			//DebugLog.getSingleton().d( TAG, "Beginning intersection test with poly1" );
			var res:Boolean = polyPolyIntersectHelper( poly1, poly2 );
			
			if ( !res )
			{
				//DebugLog.getSingleton().d( TAG, "Done. Found a separating axis" );
				return false;
			}
			
			//DebugLog.getSingleton().d( TAG, "Testing poly2" );
			
			res = polyPolyIntersectHelper( poly2, poly1 );
			/*
			if ( res == true )
			{
				DebugLog.getSingleton().d( TAG, "No separting axis" );
			}
			else
			{
				DebugLog.getSingleton().d( TAG, "Found a separating axis!" );
			}
			*/
			
			return res;
			
		}
	}

}