package org.xangarath.engine.anim 
{
	import flash.filesystem.*;
	import flash.utils.Dictionary;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.ResourceManager;
	/**
	 * Handles the creation of animations and sprite sheets
	 * @author Jeffrey Cochran
	 */
	public class AnimationManager 
	{
		private static const TAG:String = "AnimationManager";
		private var sheets:Dictionary;
		private var sequences:Dictionary;
		private var sprites:Dictionary;
		private var resMgr:ResourceManager;
		public function AnimationManager( mgr:ResourceManager ) 
		{
			resMgr = mgr;
			sheets = new Dictionary();
			sequences = new Dictionary();
			sprites = new Dictionary();
		}
		
		public function loadAnimations():void
		{
			var file:File = File.applicationDirectory.resolvePath("data/anim/animations.json");
			if ( !file.exists )
			{
				DebugLog.getSingleton().e( TAG, "unable to find animation JSON!" );
				throw new Error( TAG + "unable to find animation JSON!" );
				return;
			}
			var fs:FileStream = new FileStream();
			fs.open( file, FileMode.READ );
			
			var rawData:String = fs.readUTFBytes( fs.bytesAvailable );
			fs.close();
			
			var script:Object = JSON.parse( rawData );
			Argonaut.getAndCheck( TAG, script, "spritesheets" );
			for ( var i:int = 0; script[ "spritesheets" ][ i ] != null; i++ )
			{
				makeTextureSheet( Argonaut.getAndCheck( TAG, script[ "spritesheets" ][ i ], "name" ),
								  Argonaut.getAndCheck( TAG, script[ "spritesheets" ][ i ], "file" ) );
			}
			
			var seqs:Object = Argonaut.getAndCheck( TAG, script, "sequences" );
			for ( i = 0; seqs[ i ] != null; i++ )
			{
				var sequence:AnimationSequence = new AnimationSequence( seqs[ i ], this.getSheet );
				sequences[ sequence.name ] = sequence;
			}
			
			var spriteJson:Object = Argonaut.getAndCheck( TAG, script, "sprites" );
			for ( i = 0; spriteJson[ i ] != null; i++ )
			{
				var sprite:AnimatedSprite = makeSpriteFromJson( spriteJson[ i ] );
				sprites[ Argonaut.getAndCheck( TAG, spriteJson[ i ], "name" ) ] = sprite;
			}
		}
		
		private function makeSpriteFromJson( json:Object ):AnimatedSprite
		{
			var name:String = Argonaut.getAndCheck( TAG, json, "name" );
			var filename:String = Argonaut.getAndCheck( TAG, json, "file" );
			var script:Object = JSON.parse( ResourceManager.loadFileAsString( "data/anim/" + filename ) );
			
			var sheetJson:Object = Argonaut.getAndCheck( TAG, script, "spritesheet" );
			var sheetName:String = Argonaut.getAndCheck( TAG, sheetJson, "name" );
			var sheetFileName:String = Argonaut.getAndCheck( TAG, sheetJson, "file" );
			var sheet:SpriteSheet = makeTextureSheet( sheetName, sheetFileName );
			
			var seqs:Object = Argonaut.getAndCheck( TAG, script, "sequences" );
			var seqVec:Vector.<AnimationSequence> = new Vector.<AnimationSequence>();
			
			for ( var i:int = 0; seqs[ i ] != null; i++ )
			{
				seqVec.push( new AnimationSequence( seqs[ i ], getSheet ) );
			}
			
			return new AnimatedSprite( seqVec, this );
		}
		
		private function makeTextureSheet( name:String, filename:String ):SpriteSheet
		{
			DebugLog.getSingleton().i( TAG, "Reading sheet " + filename );
			var file:File = File.applicationDirectory.resolvePath( "data/anim/" + filename );
			if ( !file.exists )
			{
				DebugLog.getSingleton().e( TAG, "unable to find JSON " + filename );
				throw new Error( TAG + "unable to find JSON " + filename );
				return null;
			}
			var fs:FileStream = new FileStream();
			fs.open( file, FileMode.READ );
			
			var rawData:String = fs.readUTFBytes( fs.bytesAvailable );
			fs.close();
			
			var script:Object = JSON.parse( rawData );
			var meta:Object = Argonaut.getAndCheck( TAG, script, "meta" );
			var imagename:String = Argonaut.getAndCheck( TAG, meta, "image" );
			var sheet:SpriteSheet = new SpriteSheet( name, script, resMgr.getBitmap( imagename ).bitmapData );
			sheets[ sheet.name ] = sheet;
			return sheet;
		}
		
		public function getSequence( name:String ):AnimationSequence
		{
			if ( sequences[ name ] == null )
			{
				DebugLog.getSingleton().w( TAG, "Sequence \"" + name + "\" not found!" );
			}
			return sequences[ name ];
		}
		
		public function getSheet( name:String ):SpriteSheet
		{
			if ( sheets[ name ] == null )
			{
				DebugLog.getSingleton().w( TAG, "Sprite Sheet \"" + name + "\" not found!" );
			}
			return sheets[ name ];
		}
		
		public function getSprite( name:String ):AnimatedSprite
		{
			if ( sprites[ name ] == null )
			{
				DebugLog.getSingleton().w( TAG, "Animated Sprite \"" + name + "\" not found!" );
			}
			return (sprites[ name ] as AnimatedSprite).clone();
		}
	}

}