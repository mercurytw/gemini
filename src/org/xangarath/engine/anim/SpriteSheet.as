package org.xangarath.engine.anim 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	/**
	 * Represents a texture sheet and everything needed to create it
	 * @author Jeffrey Cochran
	 */
	public class SpriteSheet extends Bitmap 
	{
		private static const TAG:String = "SpriteSheet";
		private var frameArr:Vector.<TexturePackerFrame>;
		private var frameLookup:Dictionary;
		public function SpriteSheet( name:String, json:Object, bdat:BitmapData ) 
		{
			super( bdat );
			DebugLog.getSingleton().i( TAG, "Making sheet " + name );
			this.name = name;
			frameArr = new Vector.<TexturePackerFrame>();
			var frames:Object = Argonaut.getAndCheck( TAG, json, "frames" );
			frameLookup = new Dictionary();
			getFrames( frames );
		}
		
		private function getFrames( frames:Object ):void
		{
			for ( var i:int = 0; frames[ i ] != null; i++ )
			{
				var frame:TexturePackerFrame = new TexturePackerFrame( frames[ i ] );
				frameArr.push( frame );
				frameLookup[ frame.name ] = i;
			}
			return;
		}
		
		public function getFrameIndexByName( name:String ):int
		{
			if ( frameLookup[ name ] == null )
			{
				DebugLog.getSingleton().e( TAG, "Requested frame \"" + name + "\" was not found." );
				return -1;
			}
			return frameLookup[ name ];
		}
		
		private var defaultPt:Point = new Point();
		public function drawFrame( index:int, bDat:BitmapData ):void
		{
			if ( frameArr[ index ] == null )
			{
				DebugLog.getSingleton().e( TAG, "Could not find frame number " + index + " in animation " + name );
				return;
			}
			var frame:TexturePackerFrame = frameArr[ index ];
			
			if ( !frame.trimmed )
			{
				defaultPt.x =  (bDat.width - frame.sourceSize.x) / 2;
				defaultPt.y =  (bDat.height - frame.sourceSize.y) / 2;
				bDat.copyPixels( this.bitmapData, frame.frame, defaultPt );
			}
			else
			{
				DebugLog.getSingleton().e( TAG, "Trimmed sprite sheets are not supported yet!" );
				throw new Error("Trimmed sprite sheets are not supported yet!");
			}
		}
	}

}