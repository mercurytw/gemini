package org.xangarath.engine.anim 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import org.xangarath.engine.Argonaut;
	/**
	 * Represents a frame in a packed texture, from texture packer
	 * @author Jeffrey Cochran
	 */
	public class TexturePackerFrame 
	{
		private static const TAG:String = "TexturePackerFrame";
		public var name:String;
		public var trimmed:Boolean;
		public var frame:Rectangle;
		public var spriteSourceSize:Rectangle;
		public var sourceSize:Point;
		public function TexturePackerFrame( json:Object ) 
		{
			name = Argonaut.getAndCheck( TAG, json, "filename" );
			trimmed = Argonaut.getAndCheckB( TAG, json, "trimmed" );
			var frameObj:Object = Argonaut.getAndCheck( TAG, json, "frame" );
			frame = new Rectangle( Argonaut.getAndCheckN( TAG, frameObj, "x" ),
								   Argonaut.getAndCheckN( TAG, frameObj, "y" ),
								   Argonaut.getAndCheckN( TAG, frameObj, "w" ),
								   Argonaut.getAndCheckN( TAG, frameObj, "h" ) );
			var spriteSourceSizeObj:Object = Argonaut.getAndCheck( TAG, json, "spriteSourceSize" );
			spriteSourceSize = new Rectangle( Argonaut.getAndCheckN( TAG, spriteSourceSizeObj, "x" ),
											  Argonaut.getAndCheckN( TAG, spriteSourceSizeObj, "y" ),
								              Argonaut.getAndCheckN( TAG, spriteSourceSizeObj, "w" ),
								              Argonaut.getAndCheckN( TAG, spriteSourceSizeObj, "h" ) );
											  
			var sourceSizeObj:Object = Argonaut.getAndCheck( TAG, json, "sourceSize" );
			sourceSize = new Point( Argonaut.getAndCheckN( TAG, sourceSizeObj, "w" ), 
									Argonaut.getAndCheckN( TAG, sourceSizeObj, "h" ) );
		}
		
	}

}