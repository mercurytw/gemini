package org.xangarath.engine.anim 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.ITickable;
	
	/**
	 * An animated sprite, based on a bitmap
	 * @author Jeffrey Cochran
	 */
	public class AnimatedSprite extends Bitmap implements ITickable, IDestructable
	{
		private static const TAG:String = "AnimatedSprite";
		private var activeSequence:AnimationSequence = null;
		private var activeSheet:SpriteSheet = null;
		private var manager:AnimationManager;
		private var isPlaying:Boolean = false;
		private var timeSinceLastFlip:Number;
		private var currentAnimIndex:int;
		private var _donePlaying:Boolean = true;
		private var fillingRectangle:Rectangle = new Rectangle( 0, 0 );
		public var animations:Vector.<AnimationSequence>;
		private var fullSize:Point;
		private var isConstructed:Boolean = true;
		public function AnimatedSprite( sequences:Vector.<AnimationSequence>, manager:AnimationManager ) 
		{
			super();
			this.manager = manager;
			fullSize = new Point();
			
			if ( sequences != null )
			{
				animations = sequences.slice();
				
				for each ( var seq:AnimationSequence in animations )
				{
					if ( fullSize.x < seq.size.x )
					{
						fullSize.x = seq.size.x;
					}
					
					if ( fullSize.y < seq.size.y )
					{
						fullSize.y = seq.size.y;
					}
				}
				bitmapData = new BitmapData( fullSize.x, fullSize.y );
				
				activeSequence = animations[ 0 ];
				activeSheet = manager.getSheet( activeSequence.sheet );
			}
			else
			{
				DebugLog.getSingleton().w( TAG, "Created an empty animated sprite!" );
			}
		}
		
		public function get animationName():String
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( activeSequence != null )
			{
				return activeSequence.name;
			}
			return null;
		}
		
		public function clone():AnimatedSprite
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( activeSequence == null )
			{
				return new AnimatedSprite( null, manager );
			}
			return new AnimatedSprite( animations, manager );
		}
		
		public function get isDonePlaying():Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return _donePlaying;
		}
		
		public function play():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( !isPlaying )
			{
				if ( activeSequence == null )
				{
					DebugLog.getSingleton().w( TAG, "Called play on an animation with no active sequence!" );
					return;
				}
				isPlaying = true;
				timeSinceLastFlip = 0.0;
				currentAnimIndex = 0;
				draw();
				_donePlaying = false;
			}
		}
		
		public function hasAnimation( name:String ):Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			for ( var i:int = 0; i < animations.length; i++ )
			{
				if ( animations[ i ].name == name )
				{
					return true;
				}
			}
			
			return false;
		}
		
		public function setAnimation( name:String ):void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			var seq:AnimationSequence = null;
			
			for ( var i:int = 0; i < animations.length; i++ )
			{
				if ( animations[ i ].name == name )
				{
					seq = animations[ i ];
					break;
				}
			}
			
			if ( seq == null )
			{
				DebugLog.getSingleton().e( TAG, "Requested animation sequence \"" + name + "\" was not found" );
				return;
			}
			
			activeSequence = seq;
			var wasPlaying:Boolean = isPlaying;
			stop();
			activeSheet = manager.getSheet( activeSequence.sheet );
			if ( wasPlaying )
			{
				play();
			}
		}
		
		public override function get width():Number
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( activeSequence == null )
			{
				return 0;
			}
			
			return activeSequence.size.x;
		}
		
		public override function get height():Number
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( activeSequence == null )
			{
				return 0;
			}
			
			return activeSequence.size.y;
		}
		
		public function stop():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			isPlaying = false;
			_donePlaying = true;
		}
		
		private function draw():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			fillingRectangle.width = fullSize.x;
			fillingRectangle.height = fullSize.y;
			bitmapData.fillRect( fillingRectangle, 0x00FFFFFFF );
			activeSheet.drawFrame( activeSequence.sequence[ currentAnimIndex ], bitmapData );
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isPlaying )
			{
				stop();
			}
			
			// don't destroy the sequences!
			animations.length = 0;
			animations = null;
			
			// don't destroy the sheet!
			activeSheet = null;
			
			fillingRectangle = null;
			fullSize = null;
			manager = null;
			
			
			isConstructed = false;
		}
		
		/* INTERFACE org.xangarath.engine.interfaces.ITickable */
		
		public function tick( deltaTime:Number ):void 
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( !isPlaying )
			{
				return;
			}
			timeSinceLastFlip += deltaTime;
			
			if ( timeSinceLastFlip >= activeSequence.getFrameDuration( currentAnimIndex ) )
			{
				timeSinceLastFlip = 0.0;
				currentAnimIndex++;
				if ( !activeSequence.loop && ( currentAnimIndex == activeSequence.sequence.length ) )
				{
					dispatchEvent( new Event( Event.COMPLETE ) );
					stop();
					return;
				}
				if ( currentAnimIndex == activeSequence.sequence.length )
				{
					currentAnimIndex = 0;
				}
				draw();
			}
		}
		
	}

}