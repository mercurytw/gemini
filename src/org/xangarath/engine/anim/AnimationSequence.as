package org.xangarath.engine.anim 
{
	import flash.geom.Point;
	import org.xangarath.engine.Argonaut;
	import org.xangarath.engine.dbg.DebugLog;
	/**
	 * Represents the sequence of frames in a sprite sheet that an animation should 
	 * play.
	 * @author Jeffrey Cochran
	 */
	public class AnimationSequence 
	{
		private static const TAG:String = "AnimationSequence";
		public var sequence:Vector.<int>;
		private var frameTimes:Vector.<Number>;
		public var loop:Boolean;
		private var rate:Number;
		public var name:String;
		public var sheet:String;
		public var size:Point;
		public function AnimationSequence( json:Object, sheetLookup:Function ) 
		{
			var i:int;
			sequence = new Vector.<int>();
			frameTimes = null;
			name = Argonaut.getAndCheck( TAG, json, "name" );
			sheet = Argonaut.getAndCheck( TAG, json, "sheet" );
			loop = Argonaut.getAndCheckB( TAG, json, "loop" );
			var isRaw:Boolean = true;
			var s_sheet:SpriteSheet = null;
			
			if ( json[ "rate" ] != null )
			{
				rate = Argonaut.getAndCheckN( TAG, json, "rate" );
			}
			else
			{
				rate = -1;
			}
			
			if ( json[ "type" ] != null )
			{
				isRaw = (Argonaut.getAndCheck( TAG, json, "type" ) as String).toLowerCase() != "string";
			}
			if ( !isRaw )
			{
				s_sheet = sheetLookup( sheet );
				if ( s_sheet == null )
				{
					DebugLog.getSingleton().e( TAG, "Failed to get sprite sheet \"" + sheet + "\"" );
					throw new Error("Failed to get sprite sheet \"" + sheet + "\"");
				}
			}
			
			var seq:Object = Argonaut.getAndCheck( TAG, json, "seq" );
			for ( i = 0; seq[ i ] != null; i++ )
			{
				if ( isRaw )
				{
					sequence.push( seq[ i ] );
				}
				else
				{
					sequence.push( s_sheet.getFrameIndexByName( seq[ i ] ) );
				}
			}
			
			if ( json[ "times" ] != null )
			{
				frameTimes = new Vector.<Number>();
				var times:Object = Argonaut.getAndCheck( TAG, json, "times" );
				for ( i = 0; times[ i ] != null; i++ )
				{
					frameTimes.push( times[ i ] );
				}
				if ( frameTimes.length != sequence.length )
				{
					DebugLog.getSingleton().e( TAG, "Frame Times array did not match length of sequence array" );
					throw new Error( "Frame Times array did not match length of sequence array" );
				}
				rate = -1;
			}
			
			if ( ( rate == -1 ) && ( frameTimes == null ) )
			{
				DebugLog.getSingleton().e( TAG, "Unspecified durations for animation" );
				throw new Error( "Unspecified durations for animation" );
			}
			
			size = Argonaut.getAndCheckPoint( TAG, json, "size" );
		}
		
		
		public function getFrameDuration( index:int ):Number
		{
			CONFIG::debug
			{
				if ( ( index < 0 ) || ( index >= sequence.length ) )
				{
					DebugLog.getSingleton().e( TAG, "Requested frame duration at invalid index" );
					throw new Error( "Requested frame duration at invalid index" );
				}
			}
			
			if ( frameTimes == null )
			{
				return rate;
			}
			return frameTimes[ index ];
		}
	}

}