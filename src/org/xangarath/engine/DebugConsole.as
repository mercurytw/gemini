package org.xangarath.engine
{
	import flash.errors.InvalidSWFError;
	import flash.utils.Dictionary;
	import org.xangarath.engine.console.InvocationTarget;
	import org.xangarath.engine.SharedData;
	import flash.display.Sprite;
	import flash.display3D.textures.Texture;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	/**
	 * Standard game debugging console.
	 * @author Jeff Cochran
	 */
	public class DebugConsole extends Sprite
	{
		private var isActive:Boolean = false; 
		private var windowText:TextField = new TextField();
		private var textFields:Vector.<TextField> = new Vector.<TextField>();
		private var lines:Vector.<String> = new Vector.<String>();
		private var invocationTargets:Dictionary = new Dictionary();
		private var CONSOLE_SIZE:int;
		
		private const FONT_SIZE:int = 14;
		private const TAG:String = "DebugConsole: ";
		
		private var data:SharedData;
		private var lastCommand:String = "";
		public function DebugConsole(data:SharedData) 
		{
			this.visible = false;
			this.data = data;
		}
		
		public function init():void
		{
			this.graphics.beginFill( 0x000000, 0.8 );
			this.graphics.drawRect( 0., 0., stage.stageWidth, stage.stageHeight );
			this.graphics.endFill();
			
			initText();
			stage.addEventListener( KeyboardEvent.KEY_DOWN, registerKeyEvent );
		}
		
		public function registerInvocationTarget( target:InvocationTarget ):void
		{
			var name:String = target.invocationName.toLowerCase();
			if (invocationTargets[name] != null)
			{
				trace(TAG + "Warning: Overwriting invocation target " + name);
			}
			invocationTargets[name] = target;
		}
		
		private function setLines():void
		{
			textFields[CONSOLE_SIZE - 1].text = "> " + lines[CONSOLE_SIZE - 1];
		}
		
		private function pushLine( s:String ):void
		{
			for ( var i:int = 1; i < CONSOLE_SIZE - 1; i++)
			{
				textFields[i - 1].text = textFields[i].text;
			}
			textFields[CONSOLE_SIZE - 2].text = s;
			textFields[CONSOLE_SIZE - 1].text = "> ";
		}
		
		private function initText():void
		{
			var fmt:TextFormat = windowText.getTextFormat();
			fmt.size = FONT_SIZE;
			CONSOLE_SIZE = Math.floor(stage.stageHeight / (FONT_SIZE * 1.0));
			fmt.color = 0xFFFFFF;
			fmt.align = "left";
			
			for ( var i:int = 0; i < CONSOLE_SIZE; i++ )
			{
				var txt:TextField = new TextField();
				txt.wordWrap = false;
				txt.defaultTextFormat = fmt;
				txt.text = "";
				txt.width = stage.stageWidth + 2.;
				txt.y = i * ( fmt.size + fmt.leading );
				this.addChild( txt );
				textFields.push( txt );
				lines[i] = "";
			}
			
			setLines();
		}
		
		private function getListOfInvokeables():String
		{
			var res:String = "Valid invokables:";
			for ( var key:* in invocationTargets )
			{ 
				res += "\n" + (key as String)
			}
			return res;
		}
		
		private function perform( cmd:String ):void
		{
			var res:String = "";
			var re:RegExp =  /\s+/;
			var arr:Array = cmd.split(re);
			
			if ( arr[0] == "" ) { arr.shift(); }
			if ( arr[arr.length - 1] == "" ) { arr.pop(); }
			
			if ( arr[0] == "help" )
			{
				if ( arr.length == 1 )
				{
					res = getListOfInvokeables();
				}
				else if ( arr.length == 2 )
				{
					res = (invocationTargets[arr[1]] as InvocationTarget).description;
				}
			}
			else if ( arr[0] == "resume" )
			{
				toggle();
			}
			else if (invocationTargets[arr[0]] != null)
			{
				var name:String = arr.shift();
				res = (invocationTargets[name] as InvocationTarget).invoke(arr);
			}
			else
			{
				res = "Unknown command: " + cmd;
			}
			
			arr = res.split("\n");
			for ( var i:int = 0; i < arr.length; i++)
			{
				pushLine(arr[i]);
			}
			
			lines[CONSOLE_SIZE - 1] = "";
		}
		
		private function checkValidPrintingChar( code:uint ):Boolean
		{
			if(((code >= Keyboard.A) && (code <= Keyboard.Z)) ||
			   ((code >= Keyboard.NUMBER_0) && (code <= Keyboard.NUMBER_9)))
			{
				return true;
			}
			else if (( code == Keyboard.PERIOD ) || 
					 ( code == Keyboard.COMMA ) || 
					 ( code == Keyboard.SPACE ) || 
					 (code == Keyboard.MINUS) ||
					 (code == Keyboard.SLASH) ||
					 (code == Keyboard.BACKSLASH) ||
					 (code == Keyboard.SEMICOLON))
			{
				return true;
			}
			return false;
		}
		
		public function toggle():void
		{
			isActive = this.visible = data.gameIsPaused = !isActive;
			buffer = "";
		}
		
		private var buffer:String = "";
		public function registerKeyEvent( keyEvent:KeyboardEvent ):void
		{
			if (keyEvent.keyCode == Keyboard.BACKQUOTE)
			{
				toggle();
			}
			if (!isActive)
			{
				return;
			}

			if ( keyEvent.keyCode == Keyboard.BACKSPACE )
			{
				buffer = buffer.substring( 0, buffer.length - 1 );
			}
			else if ( keyEvent.keyCode == Keyboard.ENTER )
			{
				if ( buffer != "resume" )
				{
					lastCommand = buffer;
				}
				
				perform(buffer);
				buffer = "";
			}
			else if ( checkValidPrintingChar(keyEvent.keyCode) )
			{
				buffer += String.fromCharCode(keyEvent.charCode).toLowerCase();
			}
			else if (keyEvent.keyCode == Keyboard.UP)
			{	
				buffer = lastCommand;
			}
			lines[CONSOLE_SIZE - 1] = buffer.toLowerCase();
			setLines();
		}
	}

}