package org.xangarath.engine.quadtree 
{
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.geom.Polygon;
	import org.xangarath.engine.geom.Triangle;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.util.Serializer;
	
	
	/**
	 * A space partitioning system to reduce collsion detection complexity.
	 * Implemented by me
	 * @author Jeffrey Cochran
	 **/
	public class QuadTree extends Rectangle
	{
		private static const TAG:String = "QuadTree";
		public static const DIVISION_THRESH:uint = 4; // how many nodes can we hold before subdividing?
		private var parent:QuadTree = null;
		private var nodes:Vector.<QuadTreeNode>;
		private var subdivisions:Vector.<QuadTree>;
		
		/**
		 * Constructs a new quadtree with the given shape and position
		 * @param	x The x-position of the upper-righthand corner of the tree
		 * @param	y The y-position of the upper-righthand corner of the tree
		 * @param	width The width of the tree
		 * @param	height The height of the tree
		 */
		public function QuadTree( x:Number, y:Number, width:Number, height:Number ) 
		{
			super( x, y, width, height );
			nodes = new Vector.<QuadTreeNode>();
			subdivisions = new Vector.<QuadTree>();
		}
		
		public function get root():QuadTree
		{
			var node:QuadTree = this;
			
			while ( node.parent != null )
			{
				node = node.parent;
			}
			
			return node;
		}
		
		/**
		 * Try to pass the node down to a subdivision. If possible, the node will be added to a subdivision, else
		 * nothing will happen
		 * @param	a The node to try to be pushed down
		 * @return True if the node was pushed down, False if nothing happened
		 */
		private function tryPushDown( a:QuadTreeNode ):Boolean
		{
			if ( subdivisions.length == 0 )
			{
				return false;
			}
			
			for ( var i:int = 0; i < subdivisions.length; i++ )
			{
				if ( subdivisions[i].containsRect( a.getAABB() ) )
				{
					//trace( "Placing actor in node " + Serializer.seralizeFormatted( subdivisions[i], "x", "y", "width", "height" ) );
					subdivisions[i].insert( a );
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * If we reach the division threshhold, we should divide ourselves into quadrants, and pass the children
		 * down appropriately.
		 */
		private function subdivide():void
		{
			var quad:QuadTree;
			
			if ( subdivisions.length > 0 )
			{
				DebugLog.getSingleton().w( TAG, ": subdivide called when already subdivided" );
				return;
			}
			
			// TLC
			subdivisions.push( quad = new QuadTree( x, y, width / 2.0, height / 2.0 ) );
			quad.parent = this;
			
			// BLC
			subdivisions.push( quad = new QuadTree( x, y + height / 2.0, width / 2.0, height / 2.0 ) );
			quad.parent = this;
			
			// BRC
			subdivisions.push( quad = new QuadTree( x + width / 2.0, y + height / 2.0, width / 2.0, height / 2.0 ) );
			quad.parent = this;
			
			// TRC
			subdivisions.push( quad = new QuadTree( x + width / 2.0, y, width / 2.0, height / 2.0 ) );
			quad.parent = this;
			
			for ( var i:int = 0; i < nodes.length; i++ )
			{
				if ( tryPushDown( nodes[ i ] ) )
				{
					nodes.splice( i, 1 );
					i--; // we're shrinking the length of the nodes array, so this value needs to keep up
				}
			}
		}
		
		/**
		 * Collects up nodes recursively, removing them from the tree below
		 * This is a helper function for undivide()
		 **/
		private function collectNodes( v:Vector.<QuadTreeNode> ):void
		{
			if ( subdivisions.length > 0 )
			{
				subdivisions[ 0 ].collectNodes( v );
				subdivisions[ 1 ].collectNodes( v );
				subdivisions[ 2 ].collectNodes( v );
				subdivisions[ 3 ].collectNodes( v );
				
				subdivisions.splice( 0, 4 );
			}
			
			while ( nodes.length > 0 )
			{
				v.push( nodes[ 0 ] );
				nodes[ 0 ]._tree = parent;
				nodes.splice( 0, 1 );
			}
			this.parent = null;
		}
		
		/**
		 * This function unsubdivides the tree
		 */
		private function undivide():void
		{
		//	trace( "calling undivide" );
			if ( subdivisions.length == 0 )
			{
				DebugLog.getSingleton().w( TAG, ": undivide called when there were no subdivisions!" );
				return;
			}
			for ( var i:int = 0; i < subdivisions.length; i++ )
			{
				subdivisions[ i ].collectNodes( nodes );
			}
			subdivisions.splice( 0, 4 );
			
			for ( var j:int = 0; j < nodes.length; j++ )
			{
				nodes[ j ]._tree = this;
			}
		}
		
		/**
		 * Insert a new actor into this quadtree
		 * @param	a The actor to be inserted
		 */
		public function insert( a:QuadTreeNode ):void
		{
			//var actorBeingProcessed:String = a.name;
			//trace( "Got actor " + actorBeingProcessed );
			if ( this.containsRect( a.getAABB() ) )
			{
				//trace( "Actor was contained by rect" );
				if ( !tryPushDown( a ) )
				{
					//trace( "Actor added to this quad " );
					nodes.push( a );
					a._tree = this;
				}
				
				if ( ( subdivisions.length == 0 ) && ( nodes.length == DIVISION_THRESH ) )
				{
					//trace( "subdividing" );
					subdivide();
				}
			} 
			else if ( parent == null )
			{
				//trace( " This node is the root, so it is added to the top level ");
				nodes.push( a );
				a._tree = this;
			}
			else 
			{
				//trace( "This node does not fully contain, passing up" );
				parent.insert( a );
			}
		}
		
		/**
		 * Removes the actor a and returns it
		 * @param	a The actor to remove from the tree
		 * @return the actor to remove, or null if the actor was not in the tree
		 */
		public function remove( a:QuadTreeNode ):QuadTreeNode
		{
			//var actorBeingProcessed:String = a.name;
			//trace( "Remove: Got actor " + actorBeingProcessed );
			//trace( "in node: " + Serializer.seralizeFormatted( this, "x", "y", "width", "height" ) );
			
			if ( this != a._tree )
			{
				return a._tree.remove( a );
			}
			
			if ( nodes.indexOf( a ) != -1 )
			{
				nodes.splice( nodes.indexOf( a ), 1 );
				a._tree = null;
				
				if ( ( ( parent != null ) && ( parent.count() < DIVISION_THRESH ) ) ||
				     ( ( subdivisions.length > 0 ) && ( count() < DIVISION_THRESH ) ) )
				{
					var node:QuadTree = this;
					
					while ( ( node.parent != null ) && ( node.parent.count() < DIVISION_THRESH ) )
					{
						node = node.parent;
					}
					node.undivide();
				}
				
				return a;
			}
			else
			{
				return null;
			}
		}
		
		/**
		 * Notify the Quadtree that actor A has moved
		 * @param	a The actor which has moved
		 */
		public function move( a:QuadTreeNode ):void
		{	
			var rootBkp:QuadTree = root;
			a._tree.remove( a );
			rootBkp.insert( a );
		}
		
		private function rectQueryHelper( rect:Rectangle, v:Vector.<QuadTreeNode> ):void
		{
			if ( !rect.intersects( this ) )
			{
				return;
			}
			
			for ( var i:int = 0; i < nodes.length; i++ )
			{
				if ( rect.intersects( nodes[ i ].getAABB() ) )
				{
					v.push( nodes[ i ] );
				}
			}
			
			if ( subdivisions.length > 0 )
			{
				subdivisions[ 0 ].rectQueryHelper( rect, v );
				subdivisions[ 1 ].rectQueryHelper( rect, v );
				subdivisions[ 2 ].rectQueryHelper( rect, v );
				subdivisions[ 3 ].rectQueryHelper( rect, v );
			}
		}
		
		private function rectQueryHelperArray( rect:Rectangle, v:Array ):void
		{
			if ( !rect.intersects( this ) )
			{
				return;
			}
			
			for ( var i:int = 0; i < nodes.length; i++ )
			{
				if ( rect.intersects( nodes[ i ].getAABB() ) )
				{
					v.push( nodes[ i ] );
				}
			}
			
			if ( subdivisions.length > 0 )
			{
				subdivisions[ 0 ].rectQueryHelperArray( rect, v );
				subdivisions[ 1 ].rectQueryHelperArray( rect, v );
				subdivisions[ 2 ].rectQueryHelperArray( rect, v );
				subdivisions[ 3 ].rectQueryHelperArray( rect, v );
			}
		}
		
		/**
		 * Does a rectangular scene query, adding the found actors to the vector
		 * @param rect The Rectangle representing the query
		 * @param	v  A preallocated vector of QuadTreeNodes to populate
		 */
		public function rectQuery( rect:Rectangle, v:Vector.<QuadTreeNode> ):void
		{
			root.rectQueryHelper( rect, v );
		}
		
		public function rectQueryArray( rect:Rectangle, v:Array ):void
		{
			root.rectQueryHelperArray( rect, v );
		}
		
		
		private function triQueryHelper( tri:Triangle, v:Vector.<QuadTreeNode> ):void
		{
			if ( !Maths.triangleRectangleIntersectCheck( tri, this ) )
			{
				return;
			}
			
			for ( var i:int = 0; i < nodes.length; i++ )
			{
				if ( Maths.triangleRectangleIntersectCheck( tri, nodes[ i ].getAABB() ) )
				{
					if (nodes[i] != null)
					{
						v.push( nodes[ i ] );
					}
				}
			}
			
			if ( subdivisions.length > 0 )
			{
				subdivisions[ 0 ].triQueryHelper( tri, v );
				subdivisions[ 1 ].triQueryHelper( tri, v );
				subdivisions[ 2 ].triQueryHelper( tri, v );
				subdivisions[ 3 ].triQueryHelper( tri, v );
			}
		}
		
		/**
		 * Does a triangular scene query, adding the found actors to the vector
		 * @param tri The Triangle representing the query
		 * @param	v  A preallocated vector of QuadTreeNodes to populate
		 */
		public function triQuery( tri:Triangle, v:Vector.<QuadTreeNode> ):void
		{
			root.triQueryHelper( tri, v );
		}
		
		private function polyQueryHelper( poly:Polygon, v:Array ):void
		{
			if ( !Maths.polygonRectangleIntersectCheck( poly, this ) )
			{
				return;
			}
			
			for ( var i:int = 0; i < nodes.length; i++ )
			{
				if ( Maths.polygonRectangleIntersectCheck( poly, nodes[ i ].getAABB() ) )
				{
					if (nodes[i] != null)
					{
						v.push( nodes[ i ] );
					}
				}
			}
			
			if ( subdivisions.length > 0 )
			{
				subdivisions[ 0 ].polyQueryHelper( poly, v );
				subdivisions[ 1 ].polyQueryHelper( poly, v );
				subdivisions[ 2 ].polyQueryHelper( poly, v );
				subdivisions[ 3 ].polyQueryHelper( poly, v );
			}
		}
		
		/**
		 * Does a polygonal scene query, addng the found actors to the vector.
		 * @param	poly The closed polygon, with points passed in CCW winding order
		 * @param	v An array of QuadTreeNodes to populate
		 */
		public function polygonalQuery( poly:Polygon, v:Array ):void
		{
			root.polyQueryHelper( poly, v );
		}
		
		/**
		 * Counts the number of nodes in this tree
		 * @return The number of nodes in the tree
		 */
		public function count():int
		{
			if ( subdivisions.length != 0 )
			{
				return subdivisions[ 0 ].count() + 
					   subdivisions[ 1 ].count() + 
					   subdivisions[ 2 ].count() + 
					   subdivisions[ 3 ].count() + 
					   nodes.length;
			}
			return nodes.length;
		}
		
		/**
		 * Draws the quadtree onto the given Graphics instance
		 * @param	s The Graphics to draw onto
		 */
		public function draw( g:Graphics ):void
		{
			// This is an if-check rather than a for loop, because doing so minimizes stack frame size, and
			// therefore allows for deeper recursion. remember, if we did an if-loop, we'd need to allocate 
			// the loop variable. Note, however, that this function has no local vars
			if ( subdivisions.length > 0 )
			{
				subdivisions[0].draw( g );
				subdivisions[1].draw( g );
				subdivisions[2].draw( g );
				subdivisions[3].draw( g );
			}
			g.lineStyle( 2, 0x00FF00 );
			g.drawRect( this.x, this.y, this.width, this.height );
		}
		
		public override function toString():String
		{
			var i:int;
			var res:String = "QuadTree { " + Serializer.seralizeFormatted( this, "x", "y", "width", "height" );
			res += ", actors [ ";
			for ( i = 0; i < nodes.length; i++ )
			{
				if ( i != 0 )
				{
					res += ", ";
				}
				//res += actors[ i ].name;
			}
			res += " ], subdivisions [ ";
			for ( i = 0; i < subdivisions.length; i++ )
			{
				if ( i != 0 )
				{
					res += ", ";
				}
				res += subdivisions[ i ].toString();
			}
			res += "] }";
			return res;
		}
		
	}

}