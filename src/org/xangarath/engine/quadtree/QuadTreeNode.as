package org.xangarath.engine.quadtree 
{
	import flash.geom.Rectangle;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	/**
	 * Represents a quadtree node
	 * @author Jeffrey Cochran
	 */
	public class QuadTreeNode implements IDestructable
	{
		private static const TAG:String = "QuadTreeNode";
		public var owner:Object;
		public var _tree:QuadTree;
		private var _aabb:Function;
		private var isConstructed:Boolean = true;
		public function QuadTreeNode( own:Object, aabbFunc:Function ) 
		{
			owner = own;
			_aabb = aabbFunc;
			_tree = null;
		}
		
		/**
		 * Is this node in the tree?
		 * @return
		 */
		public function get isInTree():Boolean
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return _tree != null;
		}
		
		/**
		 * Updates the quadtree node in the tree
		 */
		public function move():void
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			if ( isInTree )
			{
				_tree.move( this );
			}
		}
		
		public function getAABB():Rectangle
		{
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
			return _aabb.call(); // this was such a fucking good idea I love you past Jeff
		}
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( isInTree )
			{
				_tree.remove( this );
			}
			_tree = null;
			
			owner = null;
			_aabb = null;
			
			isConstructed = false;
		}
	}

}