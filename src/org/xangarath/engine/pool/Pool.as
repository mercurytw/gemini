package org.xangarath.engine.pool 
{
	import org.xangarath.engine.dbg.DebugLog;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IPoolable;
	/**
	 * A pool of IPoolables
	 * @author Jeffrey Cochran
	 */
	public class Pool implements IDestructable
	{
		private static const TAG:String = "Pool";
		private var objects:Vector.<IPoolable>;
		private var isConstructed:Boolean = true;
		private var destroyCallback:Function = null;
        public function Pool( archetype:IPoolable, numToAlloc:Number, destroyCback:Function = null ) 
		{	
            objects = new Vector.<IPoolable>();
			objects.push(archetype);
			numToAlloc--;
			destroyCallback = destroyCback;
            for ( var i:int = 0; i < numToAlloc; i++ )
            {
                var blah:IPoolable = archetype.clone();
				objects.push(blah);
            }
        }
        
        public function getObject():IPoolable
        {
			ErrorUtil.dbgAssert( TAG, isConstructed, "tried to operate on destroyed object" );
            var obj:IPoolable = null;
            for ( var i:int = 0; i < objects.length; i++ )
            {
                if ( !(objects[i].isActive()) )
                {
                    objects[i].activate();
                    return objects[i];
                }
            }
			DebugLog.getSingleton().e( TAG, "requested object when none were available!" );
			DebugLog.getSingleton().i( TAG, "length of array is " + objects.length );
            return null;
        }
		
		/* INTERFACE org.xangarath.engine.interfaces.IDestructable */
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			for each ( var obj:Object in objects )
			{
				destroyCallback( obj );
				
				if ( obj is IDestructable )
				{
					(obj as IDestructable).destroy();
				}
			}
			destroyCallback = null;
			
			objects.length = 0;
			objects = null;
			
			isConstructed = false;
		}
	}

}