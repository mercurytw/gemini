package org.xangarath.engine.dbg 
{
	import flash.utils.Dictionary;
	/**
	 * This class is used to log various stats for printing when the program exits
	 * @author Jeffrey Cochran
	 */
	public class StatLogger 
	{
		private static const TAG:String = "StatLogger";
		
		// TODO: Move all this silliness out and use the new CRC32
		public static const ANNIE_ATTACKS:String = "aa";
		public static const CONNOR_ATTACKS:String = "csef";
		public static const ANNIE_Q:String = "egewge";
		public static const ANNIE_W:String = "ewgwewe";
		public static const ANNIE_E:String = "egweeewg";
		public static const ANNIE_R:String = "2t2g3";
		
		public static const CONNOR_Q:String = "wgqewg";
		public static const CONNOR_W:String = "wgegeewe";
		public static const CONNOR_E:String = "wgwgweegw";
		public static const CONNOR_R:String = "2ggwewweg";
		
		public static const CONNOR_RQ:String = "2g232ewg";
		public static const CONNOR_RW:String = "32r2t323";
		public static const CONNOR_RE:String = "wgwegww";
		
		public static const DAMAGE_TAKEN:String = "wgwgewewgwe";
		public static const DAMAGE_SPIDER:String = "wwbwgebew";
		public static const DAMAGE_WIZARD:String = "gwwegbwee";
		public static const DAMAGE_GHOST:String = "gwegwbwe";
		public static const DAMAGE_TENTACLE:String = "wbgegwe";
		public static const DAMAGE_ROCKET:String = "wgewbbeegv";
		public static const DAMAGE_DOG:String = "111dde67bd02e575ee7cbfe106bd2720";
		
		public static const JUMPS:String = "ewbwebwb";
		
		private static var singleton:StatLogger = null;
		private var stats:Dictionary;
		public var connorUlting:Boolean = false;
		private var keys:Array;
		public static function getSingleton():StatLogger
		{
			if ( singleton == null )
			{
				singleton = new StatLogger;
			}
			return singleton;
		}
		
		public function StatLogger() 
		{
			stats = new Dictionary();
			keys = new Array();
			initDictionary();	
		}
		
		public function addStat( key:String, description:String ):void
		{
			if ( stats[ key ] == null )
			{
				var entry:StatLoggerEntry = new StatLoggerEntry();
				entry.description = description;
				stats[ key ] = entry;
				keys.push( key );
			}
		}
		
		public function setValue( key:String, value:Number ):void
		{
			if ( stats[ key ] != null )
			{
				( stats[ key ] as StatLoggerEntry ).value = value;
			}
			else
			{
				DebugLog.getSingleton().w( TAG, "Requested stat \"" + key + "\" not found" );
			}
		}
		
		public function addValue( key:String, value:Number ):void
		{
			if ( stats[ key ] != null )
			{
				( stats[ key ] as StatLoggerEntry ).value += value;
			}
			else
			{
				DebugLog.getSingleton().w( TAG, "Requested stat \"" + key + "\" not found" );
			}
		}
		
		public function subtractValue( key:String, value:Number ):void
		{
			addValue( key, -value );
		}
		
		public function getValue( key:String, value:Number ):Number
		{
			if ( stats[ key ] != null )
			{
				return ( stats[ key ] as StatLoggerEntry ).value;
			}
			DebugLog.getSingleton().w( TAG, "Requested stat \"" + key + "\" not found" );
			return NaN;
		}
		
		public function printStats():void
		{
			for ( var i:uint = 0; i < keys.length; i++ )
			{
				var stat:StatLoggerEntry = stats[ keys[ i ] ] as StatLoggerEntry;
				DebugLog.getSingleton().i( TAG, stat.description + ": " + stat.value );
			}
		}
		
		private function initDictionary():void
		{
			addStat( ANNIE_ATTACKS, "Number of attacks by annie" );
			addStat( CONNOR_ATTACKS, "Number of attacks by connor" );
			addStat( ANNIE_Q, "Number of stabs performed" );
			addStat( ANNIE_W, "Number of slashes performed" );
			addStat( ANNIE_E, "Number of 123's performed" );
			addStat( ANNIE_R, "Number of sheild bombs performed" );
			
			addStat( CONNOR_Q, "Number of projectiles performed" );
			addStat( CONNOR_W, "Number of triple shots performed" );
			addStat( CONNOR_E, "Number of pen. shots performed" );
			addStat( CONNOR_R, "Number of Ex. Rounds activated" );
			
			addStat( CONNOR_RQ, "Number of Ex. shots performed" );
			addStat( CONNOR_RW, "Number of triple ex. shots shots performed" );
			addStat( CONNOR_RE, "Number of pen. ex. shots performed" );
			
			addStat( JUMPS, "Number of jumps used" );
			
			addStat( DAMAGE_TAKEN, "Total damage taken" );
			addStat( DAMAGE_SPIDER, "Damage taken from spiders" );
			addStat( DAMAGE_WIZARD, "Damage taken from wizards" );
			addStat( DAMAGE_GHOST, "Damage taken from haunters" );
			addStat( DAMAGE_TENTACLE, "Damage taken from tentacle monsters" );
			addStat( DAMAGE_ROCKET, "Damage taken from rocketloo" );
			addStat( DAMAGE_DOG, "Damage taken from attack dog" );
		}
	}
}