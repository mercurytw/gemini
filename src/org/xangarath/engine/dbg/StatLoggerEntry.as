package org.xangarath.engine.dbg 
{
	/**
	 * An entry in a stat logger
	 * @author Jeffrey Cochran
	 */
	public class StatLoggerEntry
	{	
		public var description:String = "";
		public var value:Number = 0.0;
		public function StatLoggerEntry()
		{
			
		}
	}
}