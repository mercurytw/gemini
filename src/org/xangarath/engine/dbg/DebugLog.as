package org.xangarath.engine.dbg 
{
	import flash.events.EventDispatcher;
	import flash.filesystem.*;
	import flash.system.System;
	import flash.utils.getTimer;
	/**
	 * A simple debug logger
	 * @author Jeffrey Cochran
	 */
	public class DebugLog extends EventDispatcher
	{
		private static const TAG:String = "DebugLog";
		private static var singleton:DebugLog = null;
		public static const ERROR:int = 0;
		public static const WARN:int = 1;
		public static const DEBUG:int = 2;
		public static const INFO:int = 3;
		public static const VERBOSE:int = 4;
		private var isInited:Boolean = false;
		public var logname:String;
		private var fs:FileStream;
		private var level:int = 0;
		private var lastTime:int = -1;
		private var zeroTime:int = -1;
		
		public static function getSingleton():DebugLog
		{
			if ( singleton == null )
			{
				singleton = new DebugLog;
			}
			return singleton;
		}
		
		public function DebugLog() 
		{
			
		}
		
		public function setLoglevel( level:int ):void
		{
			this.level = Math.max( Math.min( level, VERBOSE ), 0 );
		}
		
		public function setLogfile( filename:String ):void
		{
			logname = filename;
			trace( TAG + ": " + File.applicationStorageDirectory.nativePath );
		}
		
		public function init():void
		{
			var file:File = File.applicationStorageDirectory.resolvePath( logname );
			if ( file == null )
			{
				throw new Error( TAG + "unable to find logfile!" );
				return;
			}
			fs = new FileStream();
			fs.open( file, FileMode.WRITE );
			isInited = true;
			
			var d:Date = new Date();
			trace( "Program Started: " + d.toString() );
			fs.writeUTFBytes( "Program Started: " + d.toString() + File.lineEnding );
		}
		
		public function close():void
		{
			if ( isInited )
			{
				isInited = false;
				trace( "Program Closing" );
				fs.writeUTFBytes( "Program closing" + File.lineEnding );
				fs.close();
			}
		}
		
		public function d( tag:String, message:String ):void
		{
			trace( "DEBUG: " + tag + ": " + message );
			if ( isInited && ( level >= DEBUG ) )
			{
				fs.writeUTFBytes( "DEBUG: " + tag + ": " + message + File.lineEnding );
			}
		}
		
		public function w( tag:String, message:String ):void
		{
			trace( "WARN: " + tag + ": " + message );
			if ( isInited && ( level >= WARN ) )
			{
				fs.writeUTFBytes( "WARN: " + tag + ": " + message + File.lineEnding );
			}
		}
		
		public function i( tag:String, message:String ):void
		{
			trace( "INFO: " + tag + ": " + message );
			if ( isInited && ( level >= INFO ) )
			{
				fs.writeUTFBytes( "INFO: " + tag + ": " + message + File.lineEnding );
			}
		}
		
		public function e( tag:String, message:String ):void
		{
			trace( "ERROR: " + tag + ": " + message );
			if ( isInited && ( level >= ERROR ) )
			{
				fs.writeUTFBytes( "ERROR: " + tag + ": " + message + File.lineEnding );
			}
		}
		
		public function v( tag:String, message:String ):void
		{
			trace( "VERBOSE: " + tag + ": " + message );
			if ( isInited && ( level >= VERBOSE ) )
			{
				fs.writeUTFBytes( "VERBOSE: " + tag + ": " + message + File.lineEnding );
			}
		}
		
		public function setFlatProfileZero( tag:String ):void
		{
			var timeNow:int = getTimer();
			if ( zeroTime != -1 )
			{
				d( tag, "total time: " + ( timeNow - zeroTime ) + "ms " );
			}
			else
			{
				d( tag, "set zero time" );
			}
			zeroTime = lastTime = timeNow;
		}
		
		public function markFlatProfile( tag:String, message:String ):void
		{
			if ( zeroTime == -1 )
			{
				setFlatProfileZero( tag );
				return;
			}
			
			var timeNow:int = getTimer();
			d( tag, message + ": " + ( timeNow - lastTime ) + "ms" );
			lastTime = timeNow;
		}
	}

}