package org.xangarath.engine.dbg 
{
	/**
	 * Error utility functions
	 * @author Jeff Cochran
	 */
	public class ErrorUtil
	{
		public static function assertNonNull( tag:String, o:Object ):void
		{
			if ( o == null )
			{
				DebugLog.getSingleton().e( tag, "encountered null value" );
				throw new Error( tag + ": encountered null value" );
			}
		}
		
		public static function dbgAssert( tag:String, expression:Boolean, message:String ):void
		{
			CONFIG::debug
			{
				if ( !expression )
				{
					DebugLog.getSingleton().e( tag, message );
					throw new Error( tag + ": " + message );
				}
			}
		}
		
		public static function dbgAssertNonNull( tag:String, o:Object ):void
		{
			CONFIG::debug
			{
				assertNonNull( tag, o );
			}
		}
		
		// should I use * instead?
		public static function dbgSafeCast( tag:String, o:Object, to:Class ):*
		{
			CONFIG::debug
			{
				// wait do I want to assert on null or just return null? hmm....
				dbgAssertNonNull( tag, o );
				dbgAssert( tag, o is to, "Attempted to cast object to unrelated type " + to );
			}
			
			return o as to;
		}
	}

}