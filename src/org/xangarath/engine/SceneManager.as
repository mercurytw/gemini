package org.xangarath.engine 
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.events.CollisionEvent;
	import org.xangarath.engine.geom.Polygon;
	import org.xangarath.engine.geom.Triangle;
	import org.xangarath.engine.math.Maths;
	import org.xangarath.engine.quadtree.QuadTree;
	import org.xangarath.engine.quadtree.QuadTreeNode;
	import org.xangarath.engine.world.TerrainManager;
	/**
	 * Ye olde scene manager class
	 * @author Jeff Cochran
	 */
	public class SceneManager 
	{
		private static const TAG:String = "SceneManager";
		public var backgroundLayer:Sprite;
		public var gameLayer:Sprite;
		public var fxLayer:Sprite;
		public var debugLayer:Sprite;
		public var root:Sprite;
		private var stage:Stage;
		private var hotspots:Array;
		public var tree:QuadTree;
		public function SceneManager( stage:Stage ) 
		{
			this.stage = stage;
			root = new Sprite();
			stage.addChild( root );
			
			backgroundLayer = new Sprite();
			backgroundLayer.name = "backgroundLayer";
			root.addChild(backgroundLayer);
			
			gameLayer = new Sprite();
			gameLayer.name = "gameLayer";
			root.addChild(gameLayer);
			
			fxLayer = new Sprite();
			fxLayer.name = "fxLayer";
			root.addChild(fxLayer);
			
			debugLayer = new Sprite();
			debugLayer.name = "debugLayer";
			root.addChild(debugLayer);
			
			hotspots = new Array();
		}
		
		public function insertHotspot( h:Hotspot ):void
		{
			if ( hotspots.indexOf( h ) == -1 )
			{
				tree.insert( h.node );
				hotspots.push( h );
			}
		}
		
		public function removeHotspot( h:Hotspot ):void
		{
			var idx:int;
			if ( ( idx = hotspots.indexOf( h ) ) != -1 )
			{
				tree.remove( h.node );
				hotspots.splice( idx, 1 );
			}
		}
		
		public function setQuadtreeSize( width:Number, height:Number ):void
		{
			if ( tree != null )
			{
				trace( TAG + "IMPLEMENT TEARDOWN" );
			}
			tree = new QuadTree( 0, 0, width, height );
		}
		
		public function rectSceneQuery( rect:Rectangle, queryVect:Vector.<QuadTreeNode> = null ):Vector.<QuadTreeNode>
		{
			if ( queryVect == null )
			{
				queryVect = new Vector.<QuadTreeNode>();
			}
			
			tree.rectQuery( rect, queryVect );
			
			return queryVect;
		}
		
		public function rectSceneQueryArray( rect:Rectangle, queryVect:Array = null ):Array
		{
			if ( queryVect == null )
			{
				queryVect = new Array();
			}
			
			tree.rectQueryArray( rect, queryVect );
			
			return queryVect;
		}
		
		public function triSceneQuery( tri:Triangle, queryVect:Vector.<QuadTreeNode> = null ):Vector.<QuadTreeNode>
		{
			if ( queryVect == null )
			{
				queryVect = new Vector.<QuadTreeNode>();
			}
			
			tree.triQuery( tri, queryVect );
			
			return queryVect;
		}
		
		private var polygonalSceneQueryTempRect:Rectangle = new Rectangle();
		public function polygonalSceneQuery( poly:Polygon, result:Array = null ):Array
		{
			ErrorUtil.dbgAssertNonNull( TAG, poly );
			
			if ( result == null )
			{
				result = new Array();
			}
			
			var minX:Number = Infinity;
			var maxX:Number = -Infinity;
			var minY:Number = Infinity;
			var maxY:Number = -Infinity;
			
			for each ( var pt:Point in poly )
			{
				if ( pt.x < minX )
				{
					minX = pt.x;
				}
				
				if ( pt.x > maxX )
				{
					maxX = pt.x;
				}
				
				if ( pt.y < minY )
				{
					minY = pt.y;
				}
				
				if ( pt.y > maxY )
				{
					maxY = pt.y;
				}
			}
			
			polygonalSceneQueryTempRect.setTo( minX, minY, maxX - minX, maxY - minY );
			
			tree.rectQueryArray( polygonalSceneQueryTempRect, result );
			
			var i:int = 0;
			while ( i < result.length )
			{
				var collider:QuadTreeNode = result[ i ] as QuadTreeNode;
				polygonalSceneQueryTempRect.copyFrom( collider.getAABB() );
				
				if ( !Maths.checkPointInPolygon( poly, polygonalSceneQueryTempRect.left, polygonalSceneQueryTempRect.top ) &&
					 !Maths.checkPointInPolygon( poly, polygonalSceneQueryTempRect.left, polygonalSceneQueryTempRect.bottom ) &&
					 !Maths.checkPointInPolygon( poly, polygonalSceneQueryTempRect.right, polygonalSceneQueryTempRect.bottom ) &&
					 !Maths.checkPointInPolygon( poly, polygonalSceneQueryTempRect.right, polygonalSceneQueryTempRect.top ) &&
					 !Maths.polygonRectangleIntersectCheck( poly, polygonalSceneQueryTempRect ) )
				{
					result.splice( i, 1 );
				}
				else
				{
					i++;
				}
			}
			
			return result;
		}
		
		// TODO: Implement me!
		public function raySceneQuery( start:Point, direction:Point ):Array
		{
			direction = Maths.normalize( direction );
			
			return null;
		}
		
		public function applyCollisions( data:SharedData ):void
		{
			var evt:CollisionEvent;
			for ( var i:int = 0; i < gameLayer.numChildren; i++ )
			{	
				var instigator:Actor = gameLayer.getChildAt( i ) as Actor;
				
				if ( !instigator.isSolid ) // we only care about receiving collisions
				{
					continue;
				}
					
				var query:Vector.<QuadTreeNode> = rectSceneQuery( instigator.getBoundingVolume() );
				
				for ( var j:int = 0; j < query.length; j++ )
				{
					if (query[ j ].owner is Actor)
					{
						var recipient:Actor = query[ j ].owner as Actor;
						
						if ( !recipient.ignoreEvents && ( recipient != instigator ) ) // do we care about events?
						{
							evt = new CollisionEvent( instigator, CollisionEvent.COLLISION );
							recipient.notifyHolder( evt );
							data.trashcan.push( evt );
						}
					}
					else if (query[ j ].owner is Hotspot)
					{
						evt = new CollisionEvent( instigator, CollisionEvent.COLLISION );
						(query[ j ].owner as Hotspot).receiveEvent( evt );
						data.trashcan.push( evt );
					}
				}
			}
		}
		
		private var validMoveQueryArray:Array = new Array();
		private var validMoveRectBefore:Rectangle = new Rectangle();
		private var validMoveRectAfter:Rectangle = new Rectangle();
		private var validMovePolygon:Polygon = new Polygon( 6 );
		/**
		 * Returns true if the given move is valid
		 * Being over a hotspot does not invalidate a position
		 * @param	a The actor to test
		 * @param  destX The destination of the actor in the X
		 * @param  destY The destination of the actor in the Y
		 * @param  tm The TerrainManager
		 * @param colliders An optional preallocated array that will contain the objects that would be collided with if the move occured.
		 * @return True if the space is valid and empty
		 */
		public function isValidMove( a:Actor, destX:Number, destY:Number, tm:TerrainManager, colliders:Array = null ):Boolean
		{
			validMoveRectBefore.copyFrom( a.getBoundingVolume() );
			validMoveRectAfter.copyFrom( validMoveRectBefore );
			validMoveRectAfter.offset( destX - a.x, destY - a.y );
			
			//DebugLog.getSingleton().markFlatProfile( TAG, "Misc" );
			
			validMovePolygon.length = 0;
			Maths.calculateAABBSweptVolume( validMoveRectBefore, validMoveRectAfter, validMovePolygon );
			//DebugLog.getSingleton().markFlatProfile( TAG, "Calcluating swept volume" );
			
			validMoveQueryArray.splice( 0, validMoveQueryArray.length );
			polygonalSceneQuery( validMovePolygon, validMoveQueryArray );
			//DebugLog.getSingleton().markFlatProfile( TAG, "Polygonal Scene Query" );
			var hadColliders:Boolean = false;
			
			for each ( var node:QuadTreeNode in validMoveQueryArray )
			{
				var o:Object = node.owner;
				
				if ( ( o == null ) || ( o == a ) || ( o is Hotspot ) )
				{
					continue;
				}
				
				if ( o is Actor )
				{
					if ( !(o as Actor).isSolid )
					{
						continue;
					}
				}
				
				hadColliders = true;
				if ( colliders != null )
				{
					colliders.push( o );
				}
			}
			//DebugLog.getSingleton().markFlatProfile( TAG, "Evaluating colliders" );
			
			var hadBadTerrain:Boolean = false;
			var terrain:uint = tm.getTerrainForPolygon( validMovePolygon );
			//DebugLog.getSingleton().markFlatProfile( TAG, "Polygonal Terrain Query" );
			if ( ( terrain & a.badWallMask ) != 0 )
			{
				hadBadTerrain = true;
			}
			return !hadColliders && !hadBadTerrain;
		}
		
		/**
		 * Returns true if the actor is in a valid and empty location.
		 * Being over a hotspot does not invalidate a position
		 * @param	a The actor to test
		 * @return True if the space is valid and empty
		 */
		public function isEmptySpace( a:Actor, tm:TerrainManager ):Boolean
		{	
			if ( !tm.pointInMap( a.pos.x, a.pos.y ) )
				return false;
				
			var bounds:Rectangle = a.getBoundingVolume();
			var ret:Vector.<QuadTreeNode> = rectSceneQuery( bounds );
			if ( ret != null )
			{
				for each ( var node:QuadTreeNode in ret )
				{
					if ( node.owner is Hotspot )
					{
						continue;
					}
					else if ( node.owner == a )
					{
						continue;
					}
					ret = null;
					return false;
				}
			}
			
			if ( ( tm.getTerrainForRectangle( bounds ) & a.badWallMask ) != 0x0 )
				return false;
				
			return true;
		}
		
		/**
		 * Keeps Actor a in the center of the screen
		 * @param	a  The Actor to follow
		 */
		public function trackActor( a:Actor ):void
		{
			root.x = (stage.stageWidth / 2.0) - a.x;
			root.y = (stage.stageHeight / 2.0) - a.y;
		}
		
	}

}