package org.xangarath.engine 
{
	import flash.utils.Dictionary;
	/**
	 * Game object registry. Singleton
	 * @author Jeff Cochran
	 */
	public class GameObjectRegistry 
	{
		private static var singleton:GameObjectRegistry = null;
		
		private var nextId:uint = 0;
		private var registry:Dictionary;
		public static function getSingleton():GameObjectRegistry
		{
			if ( singleton == null )
			{
				singleton = new GameObjectRegistry();
			}
			return singleton;
		}
		
		public function register( o:Object ):uint
		{
			var id:uint = nextId++;
			registry[id] = o;
			return id;
		}
		
		public function unregister( id:uint ):void
		{
			registry[id] = null;
		}
		
		public function getObjectByName( name:String ):Object
		{
			for each ( var entry:Object in registry )
			{
				if ("name" in entry)
				{
					if (entry.name == name)
					{
						return entry;
					}
				}
			}
			return null;
		}
		
		public function getObject( id:uint ):Object
		{
			return registry[id];
		}
		
		public function GameObjectRegistry() 
		{
			registry = new Dictionary();
		}
		
	}

}