package org.xangarath.engine.statemachine 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;
	import org.xangarath.engine.dbg.ErrorUtil;
	import org.xangarath.engine.interfaces.IDestructable;
	import org.xangarath.engine.interfaces.IState;
	import org.xangarath.engine.events.StateChangeEvent;
	/**
	 * ...
	 * @author Jeff Cochran
	 */
	public class StateMachine extends Object implements IDestructable
	{
		private static const TAG:String = "StateMachine";
		public var mActiveState:IState;
		public var mStates:Dictionary;
		public var mNames:Vector.<String>;
		protected var isConstructed:Boolean = true;
		public function StateMachine() 
		{
			super();
			mActiveState = null;
			mStates = new Dictionary();
			mStates.toJSON = function (s:String):* {
				var contents:Object = {}; 
				for (var a:Object in mStates) 
				{ 
					var k:String = String(a);
					contents[k] = mStates[k]; 
				}
				return s + contents;
			};
			mNames = new Vector.<String>();
		}
		
		public static function newStateMachineFromObj(obj:Object, ressurect:Function):StateMachine
		{
			var dict:Object = obj["mStates"];
			var sm:StateMachine = new StateMachine();
			
			
			for ( var key:String in dict )
			{
				var s:IState = ressurect( dict[key] );
				
				sm.addState( key, s );
			}
			return( sm );
		}
		
		public function enterState( name:String ):Boolean
		{
			if ( !isConstructed )
			{
				return false;
			}
			
			if (mStates[name] == null)
			{
				trace("StateMachine.enterState: Requested nonexistant state: " + name );
				return false;
			}
			if ( mActiveState != null )
			{
				mActiveState.teardown();
			}
			mActiveState = mStates[name];
			mActiveState.setup();
			return true;
		}
		
		public function addState( name:String, state:IState, isEntryState:Boolean = false ):void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			mStates[name] = state;
			mNames.push( name );
			if (isEntryState)
			{
				enterState(name);
			}
			if (state is IEventDispatcher)
			{
				(state as IEventDispatcher).addEventListener(StateChangeEvent.STATE_CHANGE, stateChangeHandler);
			}
		}
		
		public function stateChangeHandler( e:Event ):void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if (e.type == StateChangeEvent.STATE_CHANGE)
			{
				enterState((e as StateChangeEvent).newState);
				(e as StateChangeEvent).newState = null;
			}
		}
		
		public function destroy():void
		{
			if ( !isConstructed )
			{
				return;
			}
			
			if ( mActiveState != null )
			{
				mActiveState.teardown();
			}
			mActiveState = null;
			
			
			
			for each ( var name:String in mNames )
			{
				var state:IState = ErrorUtil.dbgSafeCast( TAG, mStates[ name ], IState );
				
				if (state is IEventDispatcher)
				{
					(state as IEventDispatcher).removeEventListener(StateChangeEvent.STATE_CHANGE, stateChangeHandler);
				}
				
				// This might be superfluous
				if ( state is IDestructable )
				{
					(state as IDestructable).destroy();
				}
				
				mStates[ name ] = null;
			}
			
			mNames.splice( 0, mNames.length );
			
			mStates = null;
			mNames = null;
			
			isConstructed = false;
		}
		
		public function toJSON(s:String):*
		{
			if ( !isConstructed )
			{
				return "Destroyed";
			}
			
			return s + { "mStates":mStates };
		}
	}

}