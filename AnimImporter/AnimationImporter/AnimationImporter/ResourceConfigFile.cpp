#include "ResourceConfigFile.h"

#define EXPRESS

ResourceConfigFile::ResourceConfigFile( void ) :
	isFileLoaded( false )
{
}


ResourceConfigFile::~ResourceConfigFile(void)
{
	closeFile();
	std::map<std::string, sequence*>::iterator itr = sequences.begin();

	for( ; itr != sequences.end(); itr++ )
	{
		while( itr->second->frames->size() != 0 ) 
		{ 
			itr->second->frames->pop(); 
		}
		delete itr->second->frames;
		delete itr->second;
	}
	sequences.clear();

	std::vector<sheet*>::iterator it2 = sheets.begin();

	for( ; it2 != sheets.end(); it2++ )
	{
		delete (*it2);
	}
	sheets.clear();
}

bool ResourceConfigFile::loadFile( _TCHAR* filepath )
{
	if ( isFileLoaded )
	{
		return false;
	}

	cfgFileHandle = CreateFile( filepath,               // name of the file
								GENERIC_READ,           // open for writing
								0,                      // do not share
								NULL,                   // default security
								OPEN_EXISTING,          // open the file only if it exists
								FILE_ATTRIBUTE_NORMAL,  // normal file
								NULL);                  // no attr. template

	if ( cfgFileHandle == INVALID_HANDLE_VALUE )
	{
		std::cerr << "parseArgs: Failed to open config file!" << std::endl;
		return false;
	}

	isFileLoaded = true;
	return true;
}

bool ResourceConfigFile::isNumber( const char c )
{
	return ( ( 48 <= c ) && ( c <= 57 ) ) ? true : false;
}

std::string ResourceConfigFile::inferSequenceName( std::string fileName )
{
	int index = -1;
	std::string retval = "";
	
	for ( unsigned int i = 0; i < fileName.length(); i++ )
	{
		if ( isNumber( fileName.at( i ) ) )
		{
			index = i;
			break;
		}
	}
	
	if ( index != -1 )
	{
		retval = fileName.substr( 0, index );
	}
	else
	{
		retval = fileName;
	}

	return retval;
}

bool ResourceConfigFile::inferLooping( std::string fileName )
{
	std::size_t found;
	char ans;
	found = fileName.find( "Walk" );
	if ( found != std::string::npos )
	{
		return true;
	}
	
	found = fileName.find( "Idle" );
	if ( found != std::string::npos )
	{
		return true;
	}

	found = fileName.find( "Attack" );
	if ( found != std::string::npos )
	{
		return false;
	}

	std::cout << "Could not infer looping for file \"" << fileName.c_str() << "\"\n Does it loop? (y\\n) ";
	std::cin.get( ans );
	std::cout << std::endl << std::endl;

	if ( ans == 'y' )
	{
		return true;
	}

	return false;
}

bool ResourceConfigFile::parseFrame( std::string sheetName, const Value& frame, int frameNo )
{
	const char* subimageName;
	sequence* seq;
	std::string seqName;
	std::map<std::string, sequence*>::iterator itr;

	if ( !frame[ "filename" ].IsString() )
	{
		std::cerr << "Malformed JSON: filename is not a string!" << std::endl;
		return false;
	}

	subimageName = frame[ "filename" ].GetString();
	seqName = inferSequenceName( std::string( subimageName ) );
	
	if ( ( itr = sequences.find( seqName ) ) == sequences.end() )
	{
		seq = new sequence;
		seq->seqName = seqName;
		seq->sheetName = sheetName;
		seq->frames = new std::queue<int>();
		seq->isLooping = inferLooping( subimageName );
		if ( !frame.HasMember( "sourceSize" ) || !frame[ "sourceSize" ].HasMember( "w" ) || !frame[ "sourceSize" ].HasMember( "h" ) )
		{
			std::cerr << "Malformed JSON: no sourceSize member" << std::endl;
			return false;
		}
		seq->size.first = ( float ) frame[ "sourceSize" ][ "w" ].GetDouble();
		seq->size.second = ( float ) frame[ "sourceSize" ][ "h" ].GetDouble();
		sequences.insert( std::pair<std::string, sequence*>( seqName, seq ) );
		itr = sequences.find( seqName );
	}

	itr->second->frames->push( frameNo );

	std::cout << "Got frame: " << subimageName << std::endl;
	std::cout << "Inferred sequence name: " << seqName << std::endl << std::endl;
	return true;
}

bool ResourceConfigFile::parseSpriteJson( const char* filename )
{
#if !defined(EXPRESS)
	USES_CONVERSION;
#endif
	Document document;
	LPVOID buffer;
	DWORD fileSize;
	DWORD bytesRead;
	HANDLE fileHandle = INVALID_HANDLE_VALUE;
	std::string trueFileName;
	std::string simpleFileName;

#if !defined(EXPRESS) && defined(UNICODE)
	LPCWSTR filePath = A2W( filename );
#else
	const char* filePath = filename;
#endif
	
	std::cout << "Got file: " << filename << std::endl;


	fileHandle = CreateFile( filePath,				 // name of the file
							 GENERIC_READ,           // open for writing
							 0,                      // do not share
							 NULL,                   // default security
							 OPEN_EXISTING,          // open the file only if it exists
							 FILE_ATTRIBUTE_NORMAL,  // normal file
							 NULL);                  // no attr. template

	if ( fileHandle == INVALID_HANDLE_VALUE )
	{
		std::cerr << "parseSpriteJson: Failed to open file " << filePath << std::endl;
		return false;
	}

	fileSize = GetFileSize( fileHandle, NULL );

	buffer = ( LPVOID ) alloca( ( fileSize + 1 ) * sizeof( char ) );
	memset( buffer, '\0', fileSize + 1 );

	if ( !ReadFile( fileHandle, buffer, fileSize, &bytesRead, NULL ) )
	{
		std::cerr << "Failed to read file " << filePath << std::endl;
		return false;
	}

	CloseHandle( fileHandle );

	if ( document.Parse<0>( ( char * ) buffer ).HasParseError() )
	{
		std::cerr << "Failed to parse JSON in" << filePath << std::endl;
		return false;
	}

	if ( !document.IsObject() )
	{
		goto fileFail;
	}

	if ( !document.HasMember("frames") || !document["frames"].IsArray() )
	{
		goto fileFail;
	}

	trueFileName = std::string( filename );
	if ( trueFileName.find( '\\' ) != -1 )
	{
		trueFileName = trueFileName.substr( trueFileName.rfind( '\\' ) + 1 );
	}
	simpleFileName = trueFileName;
	trueFileName = trueFileName.substr( 0, trueFileName.rfind( '.' ) );

	sheet* newSheet = new sheet;
	newSheet->fileName = simpleFileName;
	newSheet->sheetName = trueFileName;
	sheets.push_back( newSheet );
	
	const Value& a = document["frames"];
	for ( SizeType i = 0; i < a.Size(); i++ )
	{
		if ( !a[ i ].IsObject() )
		{
			std::cerr << "Frame entry is not object!" << std::endl;
			goto fileFail;
		}
		if ( !a[ i ].HasMember("filename") )
		{
			std::cerr << "Filename member not found" << std::endl;
			goto fileFail;
		}
		const Value& blah = a[ i ];
		if ( !parseFrame( trueFileName, blah, i ) )
		{
			goto fileFail;
		}
	}

	return true;

fileFail:
	std::cerr << "Malformed JSON " << filePath << std::endl;
	return false;
}

bool ResourceConfigFile::readConfig()
{
	Document document;
	LPVOID buffer;
	DWORD fileSize;
	DWORD bytesRead;

	if ( !isFileLoaded )
	{
		return false;
	}

	fileSize = GetFileSize( cfgFileHandle, NULL );

	buffer = ( LPVOID ) malloc( ( fileSize + 1 ) * sizeof( char ) );
	memset( buffer, '\0', fileSize + 1 );

	if ( !ReadFile( cfgFileHandle, buffer, fileSize, &bytesRead, NULL ) )
	{
		std::cerr << "Failed to read file" << std::endl;
		free( buffer );
		return false;
	}

	if ( document.Parse<0>( ( char * ) buffer ).HasParseError() )
	{
		std::cerr << "Failed to parse JSON" << std::endl;
		free( buffer );
		return false;
	}

	std::cout << "Parsing document succeeded" << std::endl;

	if ( !document.IsObject() )
	{
		std::cerr << "Invalid JSON" << std::endl;
		free( buffer );
		return false;
	}

	if ( !document.HasMember("animations") || !document["animations"].IsArray() )
	{
		goto fail;
	}

	const Value& a = document["animations"];
	for ( SizeType i = 0; i < a.Size(); i++ )
	{
		if ( !a[ i ].IsString() )
		{
			goto fail;
		}
		const char* str = a[ i ].GetString();
		if ( !parseSpriteJson( str ) )
		{
			goto fail;
		}
	}

	free( buffer );
	return true;

fail:
	std::cerr << "Invalid JSON" << std::endl;
	free( buffer );
	return false;
}

bool ResourceConfigFile::writeOutfile( _TCHAR* filename )
{
	//USES_CONVERSION;
	HANDLE fileHandle = INVALID_HANDLE_VALUE;
	std::stringstream sstrm;

	fileHandle = CreateFile( filename,				 // name of the file
							 GENERIC_READ | GENERIC_WRITE,       // open for writing
							 0,                      // do not share
							 NULL,                   // default security
							 CREATE_ALWAYS,          // open/create the outfile
							 FILE_ATTRIBUTE_NORMAL,  // normal file
							 NULL);                  // no attr. template

	if ( fileHandle == INVALID_HANDLE_VALUE )
	{
		std::cerr << "parseSpriteJson: Failed to create/open file " << filename << std::endl;
		return false;
	}

	sstrm << "{\r\x0a \t\"spritesheets\":[\r\x0a";

	for ( int i = 0; i < static_cast<int>(sheets.size()); i++ )
	{
		if ( i != 0 )
		{
			sstrm << ",\r\x0a";
		}
		sstrm << "\t\t{\"name\":\"" << sheets[ i ]->sheetName << "\", ";
		sstrm << "\"file\":\"" << sheets[ i ]->fileName << "\"}";
	}
	sstrm << "\r\x0a";
	sstrm << "\t],\r\x0a";
	sstrm << "\t\"sequences\":[\r\x0a";

	std::map<std::string, sequence*>::iterator itr = sequences.begin();
	bool first = true;
	bool firstFrame;
	for ( ; itr != sequences.end(); itr++ )
	{
		if ( !first )
		{
			sstrm << ",\r\x0a";
		}
		sstrm << "\t\t{\"name\":\"" << itr->second->seqName << "\", ";
		sstrm << "\"sheet\":\"" << itr->second->sheetName << "\", ";
		sstrm << "\"rate\":0.1, ";
		sstrm << "\"loop\":"<< (( itr->second->isLooping ) ? "true" : "false" ) << ", ";
		sstrm << "\"size\":[ " << itr->second->size.first << ", " << itr->second->size.second << " ], ";
		sstrm << "\"seq\":[ ";

		firstFrame = true;
		while( itr->second->frames->size() > 0 )
		{
			if ( !firstFrame )
			{
				sstrm << ", ";
			}
			int frmno = itr->second->frames->front();
			sstrm << frmno;
			itr->second->frames->pop();
			firstFrame = false;
		}
		sstrm << " ]}";

		first = false;
	}
	sstrm << "\r\x0a\t]\r\x0a";

	sstrm << "}";
	
	DWORD at;
	if ( !WriteFile( fileHandle, sstrm.str().c_str(), sstrm.str().length() * sizeof( char ), &at, NULL ) )
	{
		std::cerr << "OH SHIerror: failed to write outfile: " << iGetLastErrorText( GetLastError() ) << std::endl;
		CloseHandle( fileHandle );
		return false;
	}

	CloseHandle( fileHandle );
	return true;
}
