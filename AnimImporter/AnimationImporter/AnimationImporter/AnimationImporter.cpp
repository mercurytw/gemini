// AnimationImporter.cpp : Defines the entry point for the console application.
//
#include <windows.h>
#include <tchar.h>
#include <iostream>
#include "stdafx.h"

#include "ResourceConfigFile.h"

ResourceConfigFile cfgFile;

//! Command line "AnImp path/to/resources.cfg path/to/outfile.json"
int _tmain( int argc, _TCHAR* argv[] )
{
	if ( argc != 3 )
	{
		std::cout << "Usage: " << argv[ 0 ] << " path/to/resources.cfg path/to/outfile.json" << std::endl;
		exit( EXIT_SUCCESS );
	}

	if ( !cfgFile.loadFile( argv[ 1 ] ) )
	{
		std::cerr << "Failed to load cfg file." << std::endl;
		exit( EXIT_FAILURE );
	}

	if ( !cfgFile.readConfig() )
	{
		std::cerr << "Failed to parse cfg file." << std::endl;
		exit( EXIT_FAILURE );
	}

	if ( !cfgFile.writeOutfile( argv[ 2 ] ) )
	{
		std::cerr << "Failed to write outfile." << std::endl;
		exit( EXIT_FAILURE );
	}

	exit( EXIT_SUCCESS );
}

