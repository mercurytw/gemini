#pragma once

#include <stdlib.h>
#include <queue>
#include <map>
#include <string>
#include <windows.h>
#include <tchar.h>
#include <iostream>
#include <sstream>

#define EXPRESS

#if !defined(EXPRESS)
	#include <atlbase.h>
	#include <AtlConv.h>
#endif

#include "rapidjson.h"
#include "document.h"

using namespace rapidjson;

typedef struct st_sheet
{
	std::string sheetName;
	std::string fileName;
} sheet;

typedef struct st_sequence
{
	std::string seqName;
	std::string sheetName;
	bool isLooping;
	std::queue<int>* frames;
	std::pair< float, float > size;
} sequence;

class ResourceConfigFile
{
public:
	ResourceConfigFile(void);
	~ResourceConfigFile(void);

	bool loadFile( _TCHAR* filepath );
	bool readConfig();
	bool writeOutfile( _TCHAR* filename );
private:
	HANDLE cfgFileHandle;
	bool isFileLoaded;
	std::vector<sheet*> sheets;
	std::map<std::string, sequence*> sequences;

	bool parseSpriteJson( const char* filename );

	inline bool isNumber( const char c );
	std::string inferSequenceName( std::string fileName );
	inline bool inferLooping( std::string fileName );
	bool parseFrame( std::string sheetName, const Value& frame, int frameNo );
	
	char* iGetLastErrorText(DWORD nErrorCode)
	{
		char* msg;
		// Ask Windows to prepare a standard message for a GetLastError() code:
		FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, nErrorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&msg, 0, NULL);
		// Return the message
		if (!msg)
			return("Unknown error");
		else
			return(msg);
	}

	void closeFile()
	{
		if ( isFileLoaded )
		{
			isFileLoaded = false;
			CloseHandle( cfgFileHandle );
		}
	}
};

