<?xml version="1.0" encoding="UTF-8"?>
<tileset name="myKit" tilewidth="50" tileheight="50">
 <image source="postmodern.png" width="200" height="100"/>
 <tile id="3">
  <properties>
   <property name="type" value="wall"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="type" value="water"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="type" value="acid"/>
  </properties>
 </tile>
</tileset>
