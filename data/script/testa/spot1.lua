local TAG = "spot1.lua"

assert(INVOKER ~= nil)

local dave = nil
local daveCtlr = nil
local playerHasEntered = false
local bossIsDead = false
local bossID = nil
local hotspot = INVOKER

local sillypantsDynEntry = nil
local function sillypants( args )
	local data = getData()
	local collider = args[0]
	local playerAtBat = data.party.atBat
	if (as3.tolua(collider.id) == as3.tolua(playerAtBat.id)) then
		local testa = createActor( "scootaspider", "dave" )
		local pt = hotspot.getPos()
		logDebug( TAG, as3.tolua(pt.x) )
		testa.setPos(as3.tolua(pt.x) + 10, as3.tolua(pt.y) + 20)
		testa.spawn(data)
		dave = testa
		local ctlr = createController( "scootaspider", dave )
		data.tickList.push( ctlr )
		daveCtlr = ctlr
		data.activeLevel.registerDynamicDestructable( ctlr )
		data.activeLevel.registerDynamicDestructable( testa )
	end
end
sillypantsDynEntry = createDynamicFunction(sillypants)
INVOKER.scriptOnActorEnter = sillypantsDynEntry.fn

local cleanupEntry = nil
local function cleanup( args )
	destroyDynamicFunction( sillypantsDynEntry.id )
	destroyDynamicFunction( cleanupEntry.id )
	if (dave ~= nil) then
		dave.despawn(getData())
		dave = nil
	end
	
	if (daveCtlr ~= nil) then
		getData().tickList.splice(getData().tickList.indexOf(daveCtlr), 1)
		daveCtlr = nil
	end
	hotspot.scriptOnActorEnter = nil
end
cleanupEntry = createDynamicFunction(cleanup)

logDebug(TAG, "Script loaded. Invoker was "..as3.tolua(INVOKER.name))

return cleanupEntry.fn