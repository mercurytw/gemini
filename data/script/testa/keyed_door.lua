local TAG = "KeyedDoor.lua"

assert(INVOKER ~= nil)

local keyName = "Key"
local door = INVOKER

local collisionDynEntry = nil
local function onCollision( args )
	local collider = args[0].instigator
	if ((as3.tolua(collider.id) == as3.tolua(conner().id)) or (as3.tolua(collider.id) == as3.tolua(annie().id))) then
		logDebug( TAG, "It was connor or annie" )
		local con = conner();
		if (as3.tolua(con.inventory.remove(keyName)) == true) then
			door.despawn( getData() )
		end
	end
end
collisionDynEntry = createDynamicFunction(onCollision)
INVOKER.scriptOnCollision = collisionDynEntry.fn

local cleanupEntry = nil
local function cleanup( args )
	destroyDynamicFunction( collisionDynEntry.id )
	destroyDynamicFunction( cleanupEntry.id )
	
	door.scriptOnCollision = nil
end
cleanupEntry = createDynamicFunction(cleanup)

logDebug(TAG, "Script loaded. Invoker was "..as3.tolua(INVOKER.name))

return cleanupEntry.fn