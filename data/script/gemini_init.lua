local TAG = "gemini_init.lua"

assert(GEMINI_ENVIRONMENT ~= nil)

function annie()
	return registryLookup(as3.tolua(__ANNIE))
end

function conner()
	return registryLookup(as3.tolua(__CONNER))
end

function getData()
	return GEMINI_ENVIRONMENT.data
end

function createActor( aType, name )
	local ret = GEMINI_ENVIRONMENT.actFact.getGenericActor( aType, name )
	return ret
end

function createController( ctlrType, actor )
	local ret = GEMINI_ENVIRONMENT.ctlrFact.getController( actor, ctlrType )
	return ret
end

logDebug( TAG, "loaded." )
