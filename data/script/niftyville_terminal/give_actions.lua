local TAG = "spot1.lua"

assert(INVOKER ~= nil)

local playerHasEntered = false

local onActorEnterDynEntry = nil
local function onActorEnter( args )
	if (playerHasEntered == false) then
		local data = getData()
		local collider = args[0]
		local playerAtBat = data.playerAtBat
		if (as3.tolua(collider.id) == as3.tolua(playerAtBat.id)) then
			logDebug(TAG, "Hello")
			as3.class.org.xangarath.gemini.events.GameMilestoneBus.getSingleton().trigger(as3.class.org.xangarath.gemini.events.GameMilestoneBus.CONNER_GET_Q)
			data.conner.actions[0].enabled = true
			playerHasEntered = true;
		end
	end
end
onActorEnterDynEntry = createDynamicFunction(onActorEnter)
INVOKER.scriptOnActorEnter = onActorEnterDynEntry.fn

local cleanupEntry = nil
local function cleanup( args )
	destroyDynamicFunction( onActorEnterDynEntry.id )
	
	INVOKER.scriptOnActorEnter = nil
	INVOKER.scriptOnTick = nil
end
cleanupEntry = createDynamicFunction(cleanup)

logDebug(TAG, "Script loaded. Invoker was "..as3.tolua(INVOKER.name))

return cleanupEntry.fn