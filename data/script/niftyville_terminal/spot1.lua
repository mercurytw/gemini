local TAG = "spot1.lua"

assert(INVOKER ~= nil)

local dave = nil
local daveCtlr = nil
local door = nil
local playerHasEntered = false
local bossIsDead = false
local boss = nil
local spider1 = nil
local spider2 = nil
local bossIsSpawned = false

door = registryLookupByName( "BossDoor" )
logDebug(TAG, "Got door: "..as3.tolua(door.id))

local sillypantsDynEntry = nil
local function sillypants( args )
	if (playerHasEntered == false) then
		local data = getData()
		local collider = args[0]
		local playerAtBat = data.playerAtBat
		if (as3.tolua(collider.id) == as3.tolua(playerAtBat.id)) then
			door.spawn( data )
			spider1 = {}
			spider1.actor = createActor( "scootaspider", "" )
			spider2 = {}
			spider2.actor = createActor( "scootaspider", "" )
			spider1.ctlr = createController( "scootaspider", spider1.actor )
			spider1.actor.spawnInTheSameRoomAs( playerAtBat, getData(), 2.0 )
			spider2.ctlr = createController( "scootaspider", spider2.actor )
			spider2.actor.spawnInTheSameRoomAs( playerAtBat, getData(), 2.0 )
			getData().tickList.push( spider1.ctlr, spider2.ctlr )
			playerHasEntered = true;
		end
	end
end
sillypantsDynEntry = createDynamicFunction(sillypants)
INVOKER.scriptOnActorEnter = sillypantsDynEntry.fn


local function spawnBoss()
	boss = {}
	boss.actor = createActor( "bull", "" )
	boss.ctlr = createController( "bull", boss.actor )
	boss.actor.spawnInTheSameRoomAs( getData().playerAtBat, getData(), 2.0 )
	getData().tickList.push( boss.ctlr )
	bossIsSpawned = true
end

local tickDynEntry = nil
local function tick( args )
	if (spider1 ~= nil) then
		if ( as3.tolua(getData().sceneMgr.gameLayer.contains(spider1.actor)) == false ) then
			spider1 = nil
			if (spider2 == nil) then
				spawnBoss()
			end
		end
	end
	
	if (spider2 ~= nil) then
		if ( as3.tolua(getData().sceneMgr.gameLayer.contains(spider2.actor)) == false ) then
			spider2 = nil
			if (spider1 == nil) then
				spawnBoss()
			end
		end
	end
	
	if (bossIsSpawned == true) then
		if ( bossIsDead == false ) then
			if ( as3.tolua(getData().sceneMgr.gameLayer.contains(boss.actor)) == false ) then
				bossIsDead = true
				boss = nil
				door.despawn( getData() )
				logDebug( TAG, "Boss died" )
			end
		end
	end
end
tickDynEntry = createDynamicFunction(tick)
INVOKER.scriptOnTick = tickDynEntry.fn

local function cleanActorCtlr( both )
	if ( both ~= nil ) then
		both.actor.despawn( getData() )
		local idx = getData().tickList.indexOf(both.ctlr)
		if ( as3.tolua(idx) ~= -1 ) then
			getData().tickList.splice(idx, 1 )
		end
	end
end

local cleanupEntry = nil
local function cleanup( args )
	destroyDynamicFunction( sillypantsDynEntry.id )
	destroyDynamicFunction( cleanupEntry.id )
	destroyDynamicFunction( tickDynEntry.id )
	
	if (door ~= nil) then
		door.despawn( getData() )
	end
	
	cleanActorCtlr( spider1 )
	cleanActorCtlr( spider2 )
	cleanActorCtlr( boss )
	
	INVOKER.scriptOnActorEnter = nil
	INVOKER.scriptOnTick = nil
end
cleanupEntry = createDynamicFunction(cleanup)

logDebug(TAG, "Script loaded. Invoker was "..as3.tolua(INVOKER.name))

return cleanupEntry.fn
