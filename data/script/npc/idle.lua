local TAG = "idle.lua"

assert(INVOKER ~= nil)

local creature = INVOKER.userData
assert(creature ~= nil)

local function trySetAnimation( name )
	if (as3.tolua(creature.animSprite.hasAnimation( name )) == true ) then
		creature.animSprite.setAnimation( name )
	else
		logWarn( TAG, "Unable to find animation \""..name.."\"")
	end
end

local setupDynEntry = nil
local function setup( args )
	if (creature.animSprite ~= nil) then
		local facing = as3.tolua(creature.getCardinalDirection());
		if     facing == as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_RIGHT) then trySetAnimation("RightIdle")
		elseif facing == as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_LEFT)  then trySetAnimation("LeftIdle")
		elseif facing == as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_UP)    then trySetAnimation("UpIdle")
		elseif facing == as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_DOWN)  then trySetAnimation("DownIdle")
		else   logError( TAG, "Couldn't find cardinal direction!" )
		end
	else
		logDebug( TAG, "animation was null??" )
	end
end
setupDynEntry = createDynamicFunction(setup)
INVOKER.setupCback = setupDynEntry.fn

local cleanupEntry = nil
local function cleanup( args )
	INVOKER.scriptRebindCallback()
	INVOKER.setupCback = nil
end
cleanupEntry = createDynamicFunction(cleanup)

logInfo(TAG, "Script loaded. Creature was "..as3.tolua(creature.name))
return cleanupEntry.fn
