-- Make my cleanup more robust

local TAG = "wander.lua"

assert(INVOKER ~= nil)

local minWaitTimeSeconds = 2
local maxWaitTimeSeconds = 5
local maxWander = 2
local gridUnitSize = nil

local UP = as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_UP)
local DOWN = as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_DOWN)
local LEFT = as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_LEFT)
local RIGHT = as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_RIGHT)

local direction = nil
local creature = INVOKER.userData
assert(creature ~= nil)
local startingPoint = nil
local wanderPos = {}
wanderPos.x = 0
wanderPos.y = 0
local destination = {}
destination.x = 0
destination.y = 0
local wanderGrid = nil
local wanderGridSize = 0
local needsDirection = true
local waitingTime = 0
local isMoving = false
local lastPosX = 0
local lastPosY = 0

-- Build the wander grid
local function buildWanderGrid()
	wanderGrid = {}
	local x = 0
	local y = 0
	local width = ( maxWander * 2 ) 
	for i = 0, width do
		wanderGrid[ i ] = {}
		x = startingPoint.x + ( gridUnitSize * ( i - maxWander ) )
		for j = 0, width do
			local pos = {}
			y = startingPoint.y + ( gridUnitSize * ( j - maxWander ) )
			pos.x = x
			pos.y = y
			wanderGrid[ i ][ j ] = pos
			--logDebug( TAG, "Got position { "..pos.x..", "..pos.y.." } at [ "..i..", "..j.." ] " )
		end
	end
	wanderGridSize = ( maxWander * 2 ) + 1
	wanderPos.x = maxWander
	wanderPos.y = maxWander
end

local function trySetAnimation( name )
	if (as3.tolua(creature.animSprite.hasAnimation( name )) == true ) then
		creature.animSprite.setAnimation( name )
	else
		logWarn( TAG, "Unable to find animation \""..name.."\"")
	end
end

-- Setup
local setupDynEntry = nil
local function setup( args )
	gridUnitSize = as3.tolua( getData().GRID_UNIT_SIZE )
	startingPoint = {}
	startingPoint.x = as3.tolua(creature.pos.x)
	startingPoint.y = as3.tolua(creature.pos.y)
	lastPosX = startingPoint.x
	lastPosY = startingPoint.y
	buildWanderGrid()
	
	if (creature.animSprite ~= nil) then
		local facing = as3.tolua(creature.getCardinalDirection());
		
		if     facing == as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_RIGHT) then trySetAnimation("RightIdle")
		elseif facing == as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_LEFT)  then trySetAnimation("LeftIdle")
		elseif facing == as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_UP)    then trySetAnimation("UpIdle")
		elseif facing == as3.tolua(as3.class.org.xangarath.gemini.actors.Creature.ANIM_DOWN)  then trySetAnimation("DownIdle")
		else   logError( TAG, "Couldn't find cardinal direction!" )
		end
		
		direction = facing
	else
		logDebug( TAG, "animation was null??" )
	end
end
setupDynEntry = createDynamicFunction(setup)
INVOKER.setupCback = setupDynEntry.fn

-- Set up new directional movement
local function chooseDirection()
	local dir = as3.tolua( as3.class.org.xangarath.engine.math.Maths.randInt( 0, 4 ) )
	if      ( dir == 0 ) then dir = UP
	elseif  ( dir == 1 ) then dir = LEFT 
	elseif  ( dir == 2 ) then dir = RIGHT
	else    dir = DOWN
	end
	
	local creaturePosX = as3.tolua(creature.x)
	local creaturePosY = as3.tolua(creature.y)
	
	if     ( ( dir == UP   ) and ( creaturePosY <= wanderGrid[0][0].y ) )   			     then dir = DOWN
	elseif ( ( dir == DOWN ) and ( creaturePosY >= wanderGrid[0][ wanderGridSize - 1 ].y ) ) then dir = UP end
	
	if 	   ( ( dir == LEFT ) and ( creaturePosX <= wanderGrid[0][0].x ) )  	                 then dir = RIGHT
	elseif ( ( dir == RIGHT) and ( creaturePosX >= wanderGrid[ wanderGridSize - 1 ][0].x ) ) then dir = LEFT end
	
	direction = dir
	
	if      ( dir == UP ) 	then creature.setDirection(  0,-1 )
	elseif  ( dir == DOWN ) then creature.setDirection(  0, 1 )
	elseif  ( dir == LEFT ) then creature.setDirection( -1, 0 )
	else    					 creature.setDirection(  1, 0 )
	end
end

-- Get the closest grid index
local function getClosestIdx()
	local closest = {}
	closest.x = 0
	closest.y = 0
	local xDist = 9999999
	local yDist = 9999999
	local xTemp = 0
	local yTemp = 0
	
	local creatureX = as3.tolua( creature.x )
	local creatureY = as3.tolua( creature.y )
	
	for i = 0, wanderGridSize - 1 do 
		xTemp = math.abs( creatureX - wanderGrid[ i ][ 0 ].x )
		yTemp = math.abs( creatureY - wanderGrid[ 0 ][ i ].y )
		
		if ( xTemp < xDist ) then
			xDist = xTemp
			closest.x = i
		end
		
		if ( yTemp < yDist ) then
			yDist = yTemp
			closest.y = i
		end
	end
	
	return closest
end

-- Try to get a destination and walk
local function tryGetDestination()
	
	local xIdx = 0
	local yIdx = 0
	local closest = getClosestIdx()
	
	if  ( direction == UP )   
	then 
		xIdx = closest.x
		yIdx = as3.tolua( as3.class.org.xangarath.engine.math.Maths.randInt( 0, closest.y ) )
		--logDebug( TAG, "Going up Found closest [ "..closest.x..", "..closest.y.." ] trying point [ "..xIdx..", "..yIdx.." ]" )
		if ( as3.tolua( getData().sceneMgr.isValidMove( creature, wanderGrid[ xIdx ][ yIdx ].x, wanderGrid[ xIdx ][ yIdx ].y, getData().terrainMgr, null ) ) == true ) then
			trySetAnimation("UpWalk")
			isMoving = true
		else
			trySetAnimation("UpIdle")
			isMoving = false
		end
	elseif  ( direction == DOWN ) 
	then 
		xIdx = closest.x
		yIdx = as3.tolua( as3.class.org.xangarath.engine.math.Maths.randInt( closest.y, wanderGridSize - 1 ) )
		--logDebug( TAG, "Going Down Found closest [ "..closest.x..", "..closest.y.." ] trying point [ "..xIdx..", "..yIdx.." ]" )
		if ( as3.tolua( getData().sceneMgr.isValidMove( creature, wanderGrid[ xIdx ][ yIdx ].x, wanderGrid[ xIdx ][ yIdx ].y, getData().terrainMgr, null ) ) == true ) then
			trySetAnimation("DownWalk")
			isMoving = true
		else
			trySetAnimation("DownIdle")
			isMoving = false
		end
	elseif  ( direction == LEFT ) 
	then 
		yIdx = closest.y
		xIdx = as3.tolua( as3.class.org.xangarath.engine.math.Maths.randInt( 0, closest.x ) )
		--logDebug( TAG, "Going Down Found closest [ "..closest.x..", "..closest.y.." ] trying point [ "..xIdx..", "..yIdx.." ]" )
		if ( as3.tolua( getData().sceneMgr.isValidMove( creature, wanderGrid[ xIdx ][ yIdx ].x, wanderGrid[ xIdx ][ yIdx ].y, getData().terrainMgr, null ) ) == true ) then
			trySetAnimation("LeftWalk")
			isMoving = true
		else
			trySetAnimation("LeftIdle")
			isMoving = false
		end
	else    					  	   
		yIdx = closest.y
		xIdx = as3.tolua( as3.class.org.xangarath.engine.math.Maths.randInt( closest.x, wanderGridSize - 1 ) )
		--logDebug( TAG, "Going Down Found closest [ "..closest.x..", "..closest.y.." ] trying point [ "..xIdx..", "..yIdx.." ]" )
		if ( as3.tolua( getData().sceneMgr.isValidMove( creature, wanderGrid[ xIdx ][ yIdx ].x, wanderGrid[ xIdx ][ yIdx ].y, getData().terrainMgr, null ) ) == true ) then
			trySetAnimation("RightWalk")
			isMoving = true
		else
			trySetAnimation("RightIdle")
			isMoving = false
		end
	end
	
	destination.x = wanderGrid[ xIdx ][ yIdx ].x
	destination.y = wanderGrid[ xIdx ][ yIdx ].y
end

-- Event handler
local eventDynEntry = nil
local function eventHandler( args )
	local event = args[ 0 ]
	if ( as3.tolua( event.type ) == as3.tolua( as3.class.org.xangarath.engine.events.CollisionEvent.COLLISION ) ) then	
		if ( isMoving ) then
			creature.setPos( lastPosX, lastPosY )
			isMoving = false
			
			if      ( direction == UP )   then trySetAnimation("UpIdle")
			elseif  ( direction == DOWN ) then trySetAnimation("DownIdle")
			elseif  ( direction == LEFT ) then trySetAnimation("LeftIdle")
			else        				       trySetAnimation("RightIdle")
			end
		end
		
		if ( as3.tolua( getData().party.atBat.id ) == as3.tolua( event.instigator ) ) then
			if ( waitingTime < 5 ) then
				waitingTime = 5
			end
		end
	end
end
eventDynEntry = createDynamicFunction(eventHandler)
INVOKER.eventCback = eventDynEntry.fn

-- Tick
local tickDynEntry = nil
local function tick( args )
	local deltaTime = as3.tolua( args[ 0 ] )
	
	if ( needDirection == true ) then
		chooseDirection()
		tryGetDestination()
		waitingTime = as3.tolua( as3.class.org.xangarath.engine.math.Maths.randInt( minWaitTimeSeconds, maxWaitTimeSeconds ) )
		needDirection = false
	end
	
	if ( isMoving == true ) then
		if ( as3.tolua( as3.class.org.xangarath.engine.math.Maths.distanceSqComponent( creature.x, creature.y, destination.x, destination.y ) ) <= 10 )
		then
			isMoving = false
			if      ( direction == UP )   then trySetAnimation("UpIdle")
			elseif  ( direction == DOWN ) then trySetAnimation("DownIdle")
			elseif  ( direction == LEFT ) then trySetAnimation("LeftIdle")
			else        				       trySetAnimation("RightIdle")
			end
		else
			lastPosX = as3.tolua( creature.x )
			lastPosY = as3.tolua( creature.y )
			local dirX = as3.tolua( creature.dirX )
			local dirY = as3.tolua( creature.dirY )
			creature.move( dirX * as3.tolua( creature.stats.moveSpeed ) * gridUnitSize * deltaTime, 
				           dirY * as3.tolua( creature.stats.moveSpeed ) * gridUnitSize * deltaTime );
			
			local terr = as3.tolua( getData().terrainMgr.getTerrainForRectangle(creature.getBoundingVolume()) )
			
			if ( band( terr, creature.badWallMask ) ~= 0 ) then
				creature.setPos( lastPosX, lastPosY )
				isMoving = false
				if      ( direction == UP )   then trySetAnimation("UpIdle")
				elseif  ( direction == DOWN ) then trySetAnimation("DownIdle")
				elseif  ( direction == LEFT ) then trySetAnimation("LeftIdle")
				else        				       trySetAnimation("RightIdle")
				end
			end
		end
	else
		waitingTime = waitingTime - deltaTime
		if ( waitingTime <= 0 ) then
			needDirection = true
		end
	end
end
tickDynEntry = createDynamicFunction(tick)
INVOKER.tickCback = tickDynEntry.fn

-- Cleanup
local cleanupEntry = nil
local function cleanup( args )
	logDebug( TAG, "rebinding..." )
	destroyDynamicFunction( setupDynEntry .id )
	destroyDynamicFunction( tickDynEntry.id )
	destroyDynamicFunction( cleanupEntry.id )
	destroyDynamicFunction( eventDynEntry.id )
	
	
	INVOKER.scriptRebindCallback()
	INVOKER.setupCback = nil
	INVOKER.tickCback = nil
	INVOKER.eventCback = nil
	
	if ( startingPoint ~= nil ) then
		creature.setPos( startingPoint.x, startingPoint.y )
	end
	
end
cleanupEntry = createDynamicFunction(cleanup)

logInfo(TAG, "Script loaded. Creature was "..as3.tolua(creature.name))
return cleanupEntry.fn
