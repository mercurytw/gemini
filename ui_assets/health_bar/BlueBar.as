﻿package  
{
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.events.Event;
	
	
	public class BlueBar extends MovieClip 
	{
		private var _readiness:Number = 1.0;
		public function BlueBar() 
		{
			this.addEventListener(Event.ADDED_TO_STAGE, function () { stop(); });
		}
		
		public function set readiness(x:Number):void
		{
			if (x < 0.0)
			{
				x = 0.0;
			}
			else if (x > 1.0)
			{
				x = 1.0;
			}
			
			_readiness = x;
			
			//trace("readiness is " + (_readiness * 100) );
			
			gotoAndStop(int(Math.round(_readiness * 100)));
		}
		
		public function get readiness():Number
		{
			return _readiness;
		}
	}
	
}
