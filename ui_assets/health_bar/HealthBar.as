﻿package  
{
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.events.Event;
	
	
	public class HealthBar extends MovieClip 
	{
		private var _currHealth:Number = 100;
		private var _maxHealth:Number = 100;
		private var field:TextField;
		public function HealthBar() 
		{
			field = this.displayText as TextField;
			this.addEventListener(Event.ADDED_TO_STAGE, function () { stop(); });
		}
		
		public function set currHealth(x:Number):void
		{
			_currHealth = x;
			if (_maxHealth < _currHealth)
			{
				_maxHealth = _currHealth;
			}
			
			gotoAndStop(int(Math.round((_currHealth/_maxHealth) * 100)) + 1);
			
			field.text = "" + _currHealth + "/" + _maxHealth;
		}
		
		public function set maxHealth(x:Number):void
		{
			_maxHealth = x;
			
			if (_maxHealth < _currHealth)
			{
				_currHealth = _maxHealth;
			}
			
			gotoAndStop(int(Math.round((_currHealth/_maxHealth) * 100)) + 1);
			
			field.text = "" + _currHealth + "/" + _maxHealth;
		}
	}
	
}
