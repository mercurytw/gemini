﻿package  
{
	
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	public class ReadinessWidget extends MovieClip 
	{
		private static const readinessCircleDiameter:Number = 50.0;
		private var portraitAnnie:Bitmap;
		private var portraitConner:Bitmap;
		public var circleSetConner:Vector.<ReadinessCircle>;
		public var circleSetAnnie:Vector.<ReadinessCircle>;
		private var _isShowingAnnie:Boolean = true;
		public function ReadinessWidget() 
		{
			// constructor code
			stop();
			portraitAnnie = new Bitmap(new TopLeftPortraitAnnie() as BitmapData);
			portraitConner = new Bitmap(new TopLeftPortraitConner() as BitmapData);
			addChild(portraitAnnie);
			addChild(portraitConner);
			portraitConner.visible = false;
			
			circleSetConner = new Vector.<ReadinessCircle>();
			circleSetAnnie = new Vector.<ReadinessCircle>();
			
			var tmpCircle1:ReadinessCircle = new ReadinessCircle(new Bitmap( new AnnieAttackOneBitmapData() as BitmapData), readinessCircleDiameter);
			var tmpCircle2:ReadinessCircle = new ReadinessCircle(new Bitmap( new ConnerAttackOneBitmapData() as BitmapData), readinessCircleDiameter);
			this.addChild(tmpCircle1);
			this.addChild(tmpCircle2);
			tmpCircle1.x = tmpCircle2.x = 132;
			tmpCircle1.y = tmpCircle2.y = 8;
			tmpCircle1.setReadiness(75.0);
			tmpCircle2.setReadiness(75.0);
			tmpCircle2.visible = false;
			circleSetConner.push(tmpCircle2);
			circleSetAnnie.push(tmpCircle1);
			
			tmpCircle1 = new ReadinessCircle(new Bitmap( new AnnieAttackTwoBitmapData() as BitmapData), readinessCircleDiameter);
			tmpCircle2 = new ReadinessCircle(new Bitmap( new ConnerAttackTwoBitmapData() as BitmapData), readinessCircleDiameter);
			this.addChild(tmpCircle1);
			this.addChild(tmpCircle2);
			tmpCircle1.x = tmpCircle2.x = 122.50;
			tmpCircle1.y = tmpCircle2.y = 69;
			tmpCircle1.setReadiness(75.0);
			tmpCircle2.setReadiness(75.0);
			tmpCircle2.visible = false;
			circleSetConner.push(tmpCircle2);
			circleSetAnnie.push(tmpCircle1);
			
			tmpCircle1 = new ReadinessCircle(new Bitmap( new AnnieAttackThreeBitmapData() as BitmapData), readinessCircleDiameter);
			tmpCircle2 = new ReadinessCircle(new Bitmap( new ConnerAttackThreeBitmapData() as BitmapData), readinessCircleDiameter);
			this.addChild(tmpCircle1);
			this.addChild(tmpCircle2);
			tmpCircle1.x = tmpCircle2.x = 76;
			tmpCircle1.y = tmpCircle2.y = 113;
			tmpCircle1.setReadiness(75.0);
			tmpCircle2.setReadiness(75.0);
			tmpCircle2.visible = false;
			circleSetConner.push(tmpCircle2);
			circleSetAnnie.push(tmpCircle1);
			
			tmpCircle1 = new ReadinessCircle(new Bitmap( new AnnieAttackFourBitmapData() as BitmapData), readinessCircleDiameter);
			tmpCircle2 = new ReadinessCircle(new Bitmap( new ConnerAttackFourBitmapData() as BitmapData), readinessCircleDiameter);
			this.addChild(tmpCircle1);
			this.addChild(tmpCircle2);
			tmpCircle1.x = tmpCircle2.x = 11.25;
			tmpCircle1.y = tmpCircle2.y = 125.25;
			tmpCircle1.setReadiness(75.0);
			tmpCircle2.setReadiness(75.0);
			tmpCircle2.visible = false;
			circleSetConner.push(tmpCircle2);
			circleSetAnnie.push(tmpCircle1);
		}
		
		public function get isShowingAnnie():Boolean
		{
			return _isShowingAnnie;
		}
		
		public function showConner():void
		{
			portraitAnnie.visible = false;
			circleSetAnnie[0].visible = circleSetAnnie[1].visible = circleSetAnnie[2].visible = circleSetAnnie[3].visible = false;
			
			portraitConner.visible = true;
			circleSetConner[0].visible = circleSetConner[1].visible = circleSetConner[2].visible = circleSetConner[3].visible = true;
			
			_isShowingAnnie = false;
		}
		
		public function showAnnie():void
		{
			portraitConner.visible = false;
			circleSetConner[0].visible = circleSetConner[1].visible = circleSetConner[2].visible = circleSetConner[3].visible = false;
			
			portraitAnnie.visible = true;
			circleSetAnnie[0].visible = circleSetAnnie[1].visible = circleSetAnnie[2].visible = circleSetAnnie[3].visible = true;
			
			_isShowingAnnie = true;
		}
	}
	
}
