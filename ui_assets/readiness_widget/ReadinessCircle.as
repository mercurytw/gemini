﻿package  
{
	
	import flash.display.MovieClip;
	import flash.geom.Point;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	
	public class ReadinessCircle extends MovieClip 
	{
		private static const color:uint = 0x0033FF;
		private var icon:Bitmap = null;
		private var unavailBitmap:Bitmap;
		private var radius:Number = 0.0;
		private var _readiness:Number;
		public function ReadinessCircle(icon:Bitmap, diameter:Number) 
		{
			this.icon = icon;
			radius = diameter / 2.0;
			
			this.addChild(icon);
			icon.x += (diameter - icon.width) / 2;
			icon.y += (diameter - icon.height) / 2;
			_readiness = -1.0;
			unavailBitmap = new Bitmap(new UnavailBitmapData() as BitmapData);
			this.addChild(unavailBitmap);
			unavailBitmap.x += (diameter - unavailBitmap.width) / 2;
			unavailBitmap.y += (diameter - unavailBitmap.height) / 2;
			unavailBitmap.visible = false;
		}
		
		private static function rotateVect( v:Point, theta:Number ):void
		{
			var oldx:Number = v.x;
			v.x = ( v.x * Math.cos( theta ) ) - ( v.y * Math.sin( theta ) );
			v.y = ( v.y * Math.cos( theta ) ) + ( oldx * Math.sin( theta ) );
		}
		
		public function get readiness():Number
		{
			return _readiness;
		}
		
		public function setReadiness(readinessPercent:Number):void
		{
			if (readinessPercent < 0)
				readinessPercent = 0;
			
			if (readinessPercent > 100)
				readinessPercent = 100;
			
			var theta:Number = (readinessPercent * 360.0) / 100.0;
			var oneDeg:Number = Math.PI / 180.0;
			theta *= oneDeg;
			var agg:Number = 0;
			
			
			graphics.clear();
			
			var ptCenter:Point = new Point();
			var ptOne:Point = new Point();
			ptOne.x = 1.0;
			ptOne.y = 0.0;
			var ptTwo:Point = new Point();
			ptTwo.x = radius;
			ptTwo.y = 0.0;
			
			var vIndicies:Vector.<int> = new Vector.<int>();
			var vPoints:Vector.<Number> = new Vector.<Number>();
			
			
			var indicies:int = 0;
			while ((agg += oneDeg) <= theta)
			{
				rotateVect( ptOne, oneDeg );
				ptOne.normalize(1.0);
				ptOne.x *= radius;
				ptOne.y *= radius;
				vPoints.push( radius, radius, ptOne.x + radius, ptOne.y + radius, ptTwo.x + radius, ptTwo.y + radius);
				vIndicies.push( indicies, indicies + 1, indicies + 2, indicies + 3, indicies + 4, indicies + 5 );
				indicies += 6;
				
				ptTwo.x = ptOne.x;
				ptTwo.y = ptOne.y;
			}
			
			graphics.beginFill( color );
			graphics.drawTriangles( vPoints, vIndicies );
			graphics.endFill();
			
			_readiness = readinessPercent;
			unavailBitmap.visible = readinessPercent != 100.0;
		}
	}
}
