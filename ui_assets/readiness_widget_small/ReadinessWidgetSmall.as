﻿package  
{
	
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	
	public class ReadinessWidgetSmall extends MovieClip 
	{
		private var portraitAnnie:Bitmap;
		private var portraitConner:Bitmap;
		//private var portraitFrame:Bitmap;
		public var circleSetConner:Vector.<ReadinessCircleSmall>;
		public var circleSetAnnie:Vector.<ReadinessCircleSmall>;
		private var _isShowingAnnie:Boolean = true;
		public function ReadinessWidgetSmall() 
		{
			// constructor code
			stop();
			portraitAnnie = new Bitmap(new TopRightPortraitAnnie() as BitmapData);
			portraitConner = new Bitmap(new TopRightPortraitConner() as BitmapData);
			//portraitFrame = new Bitmap(new TopRightPortraitFrame() as BitmapData);
			//addChild(portraitFrame);
			//portraitFrame.x -= portraitFrame.width;
			addChild(portraitAnnie);
			portraitAnnie.x = -portraitAnnie.width;
			addChild(portraitConner);
			portraitConner.x = -portraitConner.width;
			portraitConner.visible = false;
			
			circleSetConner = new Vector.<ReadinessCircleSmall>();
			circleSetAnnie = new Vector.<ReadinessCircleSmall>();
			
			var tmpCircle1:ReadinessCircleSmall = new ReadinessCircleSmall( new Bitmap( new AnnieAttackOneBitmapDataSmall() as BitmapData));
			var tmpCircle2:ReadinessCircleSmall = new ReadinessCircleSmall( new Bitmap( new ConnerAttackOneBitmapDataSmall() as BitmapData));
			this.addChild(tmpCircle1);
			this.addChild(tmpCircle2);
			tmpCircle1.x = tmpCircle2.x = -81.25;
			tmpCircle1.y = tmpCircle2.y = 1.50;
			tmpCircle2.visible = false;
			circleSetAnnie.push(tmpCircle1);
			circleSetConner.push(tmpCircle2);
			
			tmpCircle1 = new ReadinessCircleSmall(new Bitmap( new AnnieAttackTwoBitmapDataSmall() as BitmapData));
			tmpCircle2 = new ReadinessCircleSmall(new Bitmap( new ConnerAttackTwoBitmapDataSmall() as BitmapData));
			this.addChild(tmpCircle1);
			this.addChild(tmpCircle2);
			tmpCircle1.x = tmpCircle2.x = -76.25;
			tmpCircle1.y = tmpCircle2.y = 27.75;
			tmpCircle2.visible = false;
			circleSetAnnie.push(tmpCircle1);
			circleSetConner.push(tmpCircle2);
			
			tmpCircle1 = new ReadinessCircleSmall(new Bitmap( new AnnieAttackThreeBitmapDataSmall() as BitmapData));
			tmpCircle2 = new ReadinessCircleSmall(new Bitmap( new ConnerAttackThreeBitmapDataSmall() as BitmapData));
			this.addChild(tmpCircle1);
			this.addChild(tmpCircle2);
			tmpCircle1.x = tmpCircle2.x = -56.25;
			tmpCircle1.y = tmpCircle2.y = 46;
			tmpCircle2.visible = false;
			circleSetAnnie.push(tmpCircle1);
			circleSetConner.push(tmpCircle2);
			
			tmpCircle1 = new ReadinessCircleSmall(new Bitmap( new AnnieAttackFourBitmapDataSmall() as BitmapData));
			tmpCircle2 = new ReadinessCircleSmall(new Bitmap( new ConnerAttackFourBitmapDataSmall() as BitmapData));
			this.addChild(tmpCircle1);
			this.addChild(tmpCircle2);
			tmpCircle1.x = tmpCircle2.x = -28.50;
			tmpCircle1.y = tmpCircle2.y = 52.75;
			tmpCircle2.visible = false;
			circleSetAnnie.push(tmpCircle1);
			circleSetConner.push(tmpCircle2);
			
			
		}
		
		public function get isShowingAnnie():Boolean
		{
			return _isShowingAnnie;
		}
		
		public function showConner():void
		{
			portraitAnnie.visible = false;
			circleSetAnnie[0].visible = circleSetAnnie[1].visible = circleSetAnnie[2].visible = circleSetAnnie[3].visible = false;
			
			portraitConner.visible = true;
			circleSetConner[0].visible = circleSetConner[1].visible = circleSetConner[2].visible = circleSetConner[3].visible = true;
			
			_isShowingAnnie = false;
		}
		
		public function showAnnie():void
		{
			portraitConner.visible = false;
			circleSetConner[0].visible = circleSetConner[1].visible = circleSetConner[2].visible = circleSetConner[3].visible = false;
			
			portraitAnnie.visible = true;
			circleSetAnnie[0].visible = circleSetAnnie[1].visible = circleSetAnnie[2].visible = circleSetAnnie[3].visible = true;
			
			_isShowingAnnie = true;
		}
	}
	
}
