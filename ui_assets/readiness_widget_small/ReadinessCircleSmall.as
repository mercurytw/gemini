﻿package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	
	public class ReadinessCircleSmall extends Sprite
	{
		private var unavailBitmap:Bitmap;
		private var icon:Bitmap;
		private var _readiness:Number;
		public function ReadinessCircleSmall(image:Bitmap) 
		{
			super();
			icon = image;
			addChild(icon);
			//icon.x = icon.width / 2;
			//icon.y = icon.height / 2;
			unavailBitmap = new Bitmap(new UnavailBitmapDataSmall() as BitmapData);
			addChild(unavailBitmap);
			//unavailBitmap.x = unavailBitmap.width / 2;
			//unavailBitmap.y = unavailBitmap.height / 2;
			unavailBitmap.visible = false;
			_readiness = 100.0;
		}
		
		public function get readiness():Number
		{
			return _readiness;
		}
		
		public function setReadiness(readinessPercent:Number):void
		{
			if (readinessPercent < 0)
			{
				readinessPercent = 0.0;
			}
			else if (readinessPercent > 100)
			{
				readinessPercent = 100.0;
			}
			
			_readiness = readinessPercent;
			
			readinessPercent /= 100;
			
			icon.alpha = 1.0 - ((1.0 - readinessPercent) / 2.0);
			
			unavailBitmap.visible = icon.alpha != 1.0;
		}

	}
	
}
